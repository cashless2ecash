// This file is part of taler-cashless2ecash.
// Copyright (C) 2024 Joel Häberli
//
// taler-cashless2ecash is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as published
// by the Free Software Foundation, either version 3 of the License,
// or (at your option) any later version.
//
// taler-cashless2ecash is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// SPDX-License-Identifier: AGPL3.0-or-later

package internal_utils

import (
	"errors"
	"fmt"
	"strings"
)

const PAYTO_PARTS_SEPARATOR = "/"

const PAYTO_SCHEME_PREFIX = "payto://"
const PAYTO_TAGRET_TYPE_IBAN = "iban"
const PAYTO_TARGET_TYPE_WALLEE_TRANSACTION = "wallee-transaction"

var REGISTERED_TARGET_TYPES = []string{
	"ach",
	"bic",
	"iban",
	"upi",
	"bitcoin",
	"ilp",
	"void",
	"ldap",
	"eth",
	"interac-etransfer",
	"wallee-transaction",
}

// This function parses a payto-uri (RFC 8905: https://www.rfc-editor.org/rfc/rfc8905.html)
// The function only parses the target type "wallee-transaction" as specified
// in the payto GANA registry (https://gana.gnunet.org/payto-payment-target-types/payto_payment_target_types.html)
func ParsePaytoWalleeTransaction(uri string) (string, string, error) {

	if t, i, err := ParsePaytoUri(uri); err != nil {

		if t != "wallee-transaction" {
			return "", "", errors.New("expected payto target type 'wallee-transaction'")
		}

		return t, i, nil
	} else {
		return t, "", err
	}
}

// returns the Payto Target Type and Target Identifier as string
// if the uri is malformed, an error is returned (target type and
// identifier will be empty strings).
func ParsePaytoUri(uri string) (string, string, error) {

	if raw, found := strings.CutPrefix(uri, PAYTO_SCHEME_PREFIX); found {

		parts := strings.Split(raw, PAYTO_PARTS_SEPARATOR)
		if len(parts) < 2 {
			return "", "", errors.New("invalid payto-uri")
		}

		return parts[0], parts[1], nil
	}
	return "", "", errors.New("invalid payto-uri")
}

func FormatPaytoWalleeTransaction(tid int) string {
	return fmt.Sprintf("%s%s/%d",
		PAYTO_SCHEME_PREFIX,
		PAYTO_TARGET_TYPE_WALLEE_TRANSACTION,
		tid,
	)
}

func ParsePaytoTargetType(uri string) (string, error) {

	if raw, found := strings.CutPrefix(uri, PAYTO_SCHEME_PREFIX); found {

		parts := strings.Split(raw, PAYTO_PARTS_SEPARATOR)
		if len(parts) < 2 {
			return "", errors.New("invalid wallee-transaction payto-uri")
		}

		for _, target := range REGISTERED_TARGET_TYPES {
			if strings.EqualFold(target, parts[0]) {
				return parts[0], nil
			}
		}
		return "", errors.New("target type '" + parts[0] + "' is not registered")
	}
	return "", errors.New("invalid payto-uri")
}
