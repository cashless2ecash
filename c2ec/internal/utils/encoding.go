// This file is part of taler-cashless2ecash.
// Copyright (C) 2024 Joel Häberli
//
// taler-cashless2ecash is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as published
// by the Free Software Foundation, either version 3 of the License,
// or (at your option) any later version.
//
// taler-cashless2ecash is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// SPDX-License-Identifier: AGPL3.0-or-later

package internal_utils

import (
	"errors"
	"math"
	"strings"
)

func TalerBinaryEncode(byts []byte) string {

	return encodeCrock(byts)
}

func TalerBinaryDecode(str string) ([]byte, error) {

	return decodeCrock(str)
}

func ParseWopid(wopid string) ([]byte, error) {

	wopidBytes, err := TalerBinaryDecode(wopid)
	if err != nil {
		return nil, err
	}

	if len(wopidBytes) != 32 {
		err = errors.New("invalid wopid")
		LogError("encoding", err)
		return nil, err
	}

	return wopidBytes, nil
}

func FormatWopid(wopid []byte) string {

	return TalerBinaryEncode(wopid)
}

func ParseEddsaPubKey(key EddsaPublicKey) ([]byte, error) {

	return TalerBinaryDecode(string(key))
}

func FormatEddsaPubKey(key []byte) string {

	return TalerBinaryEncode(key)
}

func decodeCrock(e string) ([]byte, error) {
	size := len(e)
	bitpos := 0
	bitbuf := 0
	readPosition := 0
	outLen := int(math.Floor((float64(size) * 5.0) / 8.0))
	out := make([]byte, outLen)
	outPos := 0

	getValue := func(c byte) (int, error) {
		alphabet := "0123456789ABCDEFGHJKMNPQRSTVWXYZ"
		switch c {
		case 'o', 'O':
			return 0, nil
		case 'i', 'I', 'l', 'L':
			return 1, nil
		case 'u', 'U':
			return 27, nil
		}

		i := strings.IndexRune(alphabet, rune(c))
		if i > -1 && i < 32 {
			return i, nil
		}

		return -1, errors.New("crockford decoding error")
	}

	for readPosition < size || bitpos > 0 {
		if readPosition < size {
			v, err := getValue(e[readPosition])
			if err != nil {
				return nil, err
			}
			readPosition++
			bitbuf = bitbuf<<5 | v
			bitpos += 5
		}
		for bitpos >= 8 {
			d := byte(bitbuf >> (bitpos - 8) & 0xff)
			out[outPos] = d
			outPos++
			bitpos -= 8
		}
		if readPosition == size && bitpos > 0 {
			bitbuf = bitbuf << (8 - bitpos) & 0xff
			if bitbuf == 0 {
				bitpos = 0
			} else {
				bitpos = 8
			}
		}
	}
	return out, nil
}

func encodeCrock(data []byte) string {
	out := ""
	bitbuf := 0
	bitpos := 0

	encodeValue := func(value int) byte {
		alphabet := "ABCDEFGHJKMNPQRSTVWXYZ"
		switch {
		case value >= 0 && value <= 9:
			return byte('0' + value)
		case value >= 10 && value <= 31:
			return alphabet[value-10]
		default:
			panic("Invalid value for encoding")
		}
	}

	for _, b := range data {
		bitbuf = bitbuf<<8 | int(b&0xff)
		bitpos += 8
		for bitpos >= 5 {
			value := bitbuf >> (bitpos - 5) & 0x1f
			out += string(encodeValue(value))
			bitpos -= 5
		}
	}
	if bitpos > 0 {
		bitbuf = bitbuf << (5 - bitpos)
		value := bitbuf & 0x1f
		out += string(encodeValue(value))
	}
	return out
}
