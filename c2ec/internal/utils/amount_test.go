// This file is part of taler-go, the Taler Go implementation.
// Copyright (C) 2022 Martin Schanzenbach
// Copyright (C) 2024 Joel Häberli
//
// Taler Go is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as published
// by the Free Software Foundation, either version 3 of the License,
// or (at your option) any later version.
//
// Taler Go is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// SPDX-License-Identifier: AGPL3.0-or-later

package internal_utils_test

import (
	internal_utils "c2ec/internal/utils"
	"fmt"
	"strconv"
	"testing"
)

var a = internal_utils.Amount{
	Currency: "EUR",
	Value:    1,
	Fraction: 50000000,
}
var b = internal_utils.Amount{
	Currency: "EUR",
	Value:    23,
	Fraction: 70007000,
}
var c = internal_utils.Amount{
	Currency: "EUR",
	Value:    25,
	Fraction: 20007000,
}

func TestAmountAdd(t *testing.T) {
	d, err := a.Add(b, 8)
	if err != nil {
		t.Errorf("Failed adding amount")
	}
	if c.String(8) != d.String(8) {
		t.Errorf("Failed to add to correct amount")
	}
}

func TestAmountSub(t *testing.T) {
	d, err := c.Sub(b, 8)
	if err != nil {
		t.Errorf("Failed substracting amount")
	}
	if a.String(8) != d.String(8) {
		t.Errorf("Failed to substract to correct amount")
	}
}

func TestAmountLarge(t *testing.T) {
	x, err := internal_utils.ParseAmount("EUR:50", 2)
	if err != nil {
		fmt.Println(err)
		t.Errorf("Failed")
	}
	_, err = x.Add(a, 2)
	if err != nil {
		fmt.Println(err)
		t.Errorf("Failed")
	}
}

func TestAmountSub2(t *testing.T) {

	amnts := []string{
		"CHF:30",
		"EUR:20.34",
		"CHF:23.99",
		"CHF:50.35",
		"USD:109992332",
		"CHF:0.0",
		"EUR:00.0",
		"USD:0.00",
		"CHF:00.00",
	}

	for _, a := range amnts {
		am, err := internal_utils.ParseAmount(a, 2)
		if err != nil {
			fmt.Println("parsing failed!", a, err)
			t.FailNow()
		}
		fmt.Println("subtracting", am.String(2))
		a2, err := am.Sub(*am, 2)
		if err != nil {
			fmt.Println("subtracting failed!", a, err)
			t.FailNow()
		}
		fmt.Println("subtraction result", a2.String(2))
		if !a2.IsZero() {
			fmt.Println("subtracting failure... expected zero amount but was", a2.String(2))
		}
	}
}

func TestAmountSub3(t *testing.T) {

	amnts := []string{
		"CHF:30.0004",
		"CHF:30.004",
		"CHF:30.04",
		"CHF:30.4",
		"CHF:30",
	}

	for _, a := range amnts {
		am, err := internal_utils.ParseAmount(a, 4)
		if err != nil {
			fmt.Println("parsing failed!", a, err)
			t.FailNow()
		}
		fmt.Println("subtracting", am.String(4))
		a2, err := am.Sub(*am, 4)
		if err != nil {
			fmt.Println("subtracting failed!", a, err)
			t.FailNow()
		}
		fmt.Println("subtraction result", a2.String(4))
		if !a2.IsZero() && a2.String(4) != am.Currency+":0" {
			fmt.Println("subtracting failure... expected zero amount but was", a2.String(4))
		}
	}
}

func TestParseValid(t *testing.T) {

	amnts := []string{
		"CHF:30",
		"EUR:20.34",
		"CHF:23.99",
		"CHF:50.35",
		"USD:109992332",
		"CHF:0.0",
		"EUR:00.0",
		"USD:0.00",
		"CHF:00.00",
	}

	for _, a := range amnts {
		_, err := internal_utils.ParseAmount(a, 2)
		if err != nil {
			fmt.Println("failed!", a)
			t.FailNow()
		}
	}
}

func TestParseInvalid(t *testing.T) {

	amnts := []string{
		"CHF",
		"EUR:.34",
		"CHF:23.",
		"EUR:452:001",
		"USD:1099928583593859583332",
		"CHF:4564:005",
		"CHF:.40",
	}

	for _, a := range amnts {
		_, err := internal_utils.ParseAmount(a, 2)
		if err == nil {
			fmt.Println("failed! (expected error)", a)
			t.FailNow()
		}
	}
}

func TestFormatAmountValid(t *testing.T) {

	amnts := []string{
		"CHF:30",
		"EUR:20.34",
		"CHF:23.99",
		"USD:109992332",
		"CHF:20.05",
		"USD:109992332.01",
		"CHF:10.00",
		"",
	}
	amntsParsed := make([]internal_utils.Amount, 0)
	for _, a := range amnts {
		a, err := internal_utils.ParseAmount(a, 2)
		if err != nil {
			fmt.Println("failed!", err)
			t.FailNow()
		}
		amntsParsed = append(amntsParsed, *a)
	}

	amntsFormatted := make([]string, 0)
	for _, a := range amntsParsed {
		amntsFormatted = append(amntsFormatted, internal_utils.FormatAmount(&a, 2))
	}

	for i, frmtd := range amntsFormatted {
		fmt.Println(frmtd)
		expectation, err1 := internal_utils.ParseAmount(amnts[i], 2)
		reality, err2 := internal_utils.ParseAmount(frmtd, 2)
		if err1 != nil || err2 != nil {
			fmt.Println("failed!", err1, err2)
			t.FailNow()
		}

		if expectation.Currency != reality.Currency ||
			expectation.Value != reality.Value ||
			expectation.Fraction != reality.Fraction {

			fmt.Println("failed!", amnts[i], frmtd)
			t.FailNow()
		}

		fmt.Println("success!", amnts[i], frmtd)
	}
}

func TestFormatAmountInvalid(t *testing.T) {

	amnts := []string{
		"CHF:30",
		"EUR:20.34",
		"CHF:23.99",
		"USD:109992332",
		"USD:30.30",
	}
	amntsParsed := make([]internal_utils.Amount, 0)
	for _, a := range amnts {
		a, err := internal_utils.ParseAmount(a, 2)
		if err != nil {
			fmt.Println("failed!", err)
			t.FailNow()
		}
		amntsParsed = append(amntsParsed, *a)
	}

	amntsFormatted := make([]string, 0)
	for _, a := range amntsParsed {
		amntsFormatted = append(amntsFormatted, internal_utils.FormatAmount(&a, 2))
	}

	for i, frmtd := range amntsFormatted {
		fmt.Println(frmtd)
		expectation, err1 := internal_utils.ParseAmount(amnts[i], 2)
		reality, err2 := internal_utils.ParseAmount(frmtd, 2)
		if err1 != nil || err2 != nil {
			fmt.Println("failed!", err1, err2)
			t.FailNow()
		}

		if expectation.Currency != reality.Currency ||
			expectation.Value != reality.Value ||
			expectation.Fraction != reality.Fraction {

			fmt.Println("failed!", amnts[i], frmtd)
			t.FailNow()
		}
	}
}

func TestFeesSub(t *testing.T) {

	amountWithFeesStr := fmt.Sprintf("%s:%s", "CHF", "5.00")
	amountWithFees, err := internal_utils.ParseAmount(amountWithFeesStr, 3)
	if err != nil {
		fmt.Println("failed!", err)
		t.FailNow()
	}

	fees, err := internal_utils.ParseAmount("CHF:0.005", 3)
	if err != nil {
		fmt.Println("failed!", err)
		t.FailNow()
	}

	refundAmount, err := amountWithFees.Sub(*fees, 3)
	if err != nil {
		fmt.Println("failed!", err)
		t.FailNow()
	}

	if amnt := refundAmount.String(3); amnt != "CHF:4.995" {
		fmt.Println("expected the refund amount to be CHF:4.995, but it was", amnt)
	} else {
		fmt.Println("refundable amount:", amnt)
	}
}

func TestFloat64ToAmount(t *testing.T) {

	type tuple struct {
		a string
		b int
	}

	floats := map[float64]tuple{
		2.345:    {"2.345", 3},
		4.204:    {"4.204", 3},
		1293.2:   {"1293.2", 1},
		1294.2:   {"1294.20", 2},
		1295.02:  {"1295.02", 2},
		2424.003: {"2424.003", 3},
	}

	for k, v := range floats {

		str := strconv.FormatFloat(k, 'f', v.b, 64)
		if str != v.a {
			fmt.Println("failed! expected", v.a, "got", str)
			t.FailNow()
		}
	}
}

func TestIsSmallerThan(t *testing.T) {
	amnts := []string{
		"CHF:0",
		"CHF:0.01",
		"CHF:0.1",
		"CHF:10",
		"CHF:20",
		"CHF:20.01",
		"CHF:20.02",
		"CHF:20.023",
	}
	amntsParsed := make([]internal_utils.Amount, 0)
	for _, a := range amnts {
		a, err := internal_utils.ParseAmount(a, 3)
		if err != nil {
			fmt.Println("failed!", err)
			t.FailNow()
		}
		amntsParsed = append(amntsParsed, *a)
	}

	for i, current := range amntsParsed {
		if i == 0 {
			continue
		}

		last := amntsParsed[i-1]
		fmt.Printf("checking: %s < %s\n", last.String(3), current.String(3))
		if smaller, err := last.IsSmallerThan(current); !smaller || err != nil {
			fmt.Println("failed!", err)
			t.FailNow()
		}
	}
}

func TestIsSmallerThanNegative(t *testing.T) {
	amnts := []string{
		"EUR:20.05",
		"EUR:0.05",
		"EUR:0.05",
	}
	amntsParsed := make([]internal_utils.Amount, 0)
	for _, a := range amnts {
		a, err := internal_utils.ParseAmount(a, 2)
		if err != nil {
			fmt.Println("failed!", err)
			t.FailNow()
		}
		amntsParsed = append(amntsParsed, *a)
	}

	for i, current := range amntsParsed {
		if i == 0 {
			continue
		}

		last := amntsParsed[i-1]
		fmt.Printf("checking (negative): %s < %s\n", last.String(2), current.String(2))
		if smaller, err := last.IsSmallerThan(current); smaller || err != nil {
			fmt.Println("failed!", err)
			t.FailNow()
		}
	}
}

func TestIsEqualTo(t *testing.T) {
	amnts := []string{
		"CHF:10",
		"CHF:10.00",
		"CHF:10.1",
		"CHF:10.10",
		"CHF:10.01",
		"CHF:10.01",
	}
	amntsParsed := make([]internal_utils.Amount, 0)
	for _, a := range amnts {
		a, err := internal_utils.ParseAmount(a, 2)
		if err != nil {
			fmt.Println("failed!", err)
			t.FailNow()
		}
		amntsParsed = append(amntsParsed, *a)
	}

	doubleJump := 1
	for doubleJump <= len(amntsParsed) {
		current := amntsParsed[doubleJump]
		last := amntsParsed[doubleJump-1]
		fmt.Printf("checking: %s = %s\n", last.String(2), current.String(2))
		if equal, err := last.IsEqualTo(current); !equal || err != nil {
			fmt.Println("failed!", err)
			t.FailNow()
		}
		doubleJump += 2
	}
}
