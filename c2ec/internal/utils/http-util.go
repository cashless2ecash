// This file is part of taler-cashless2ecash.
// Copyright (C) 2024 Joel Häberli
//
// taler-cashless2ecash is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as published
// by the Free Software Foundation, either version 3 of the License,
// or (at your option) any later version.
//
// taler-cashless2ecash is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// SPDX-License-Identifier: AGPL3.0-or-later

package internal_utils

import (
	"bytes"
	"errors"
	"fmt"
	"io"
	"net/http"
	"strings"
)

const HTTP_GET = "GET"
const HTTP_POST = "POST"

const HTTP_OK = 200
const HTTP_NO_CONTENT = 204
const HTTP_BAD_REQUEST = 400
const HTTP_UNAUTHORIZED = 401
const HTTP_NOT_FOUND = 404
const HTTP_METHOD_NOT_ALLOWED = 405
const HTTP_CONFLICT = 409
const HTTP_INTERNAL_SERVER_ERROR = 500
const HTTP_NOT_IMPLEMENTED = 501

const CONTENT_TYPE_HEADER = "Content-Type"

// Function reads and validates a param of a request in the
// correct format according to the transform function supplied.
// When the transform fails, it returns false as second return
// value. This indicates the caller, that the request shall not
// be further processed and the handle must be returned by the
// caller. Since the parameter is optional, it can be null, even
// if the boolean return value is set to true.
func AcceptOptionalParamOrWriteResponse[T any](
	name string,
	transform func(s string) (T, error),
	req *http.Request,
	res http.ResponseWriter,
) (*T, bool) {

	ptr, err := OptionalQueryParamOrError(name, transform, req)
	if err != nil {
		SetLastResponseCodeForLogger(HTTP_BAD_REQUEST)
		res.WriteHeader(HTTP_BAD_REQUEST)
		return nil, false
	}

	if ptr == nil {
		LogInfo("http", "optional parameter "+name+" was not set")
		return nil, true
	}

	obj := *ptr
	assertedObj, ok := any(obj).(T)
	if !ok {
		// this should generally not happen (due to the implementation)
		SetLastResponseCodeForLogger(HTTP_INTERNAL_SERVER_ERROR)
		res.WriteHeader(HTTP_INTERNAL_SERVER_ERROR)
		return nil, false
	}
	return &assertedObj, true
}

// The function parses a parameter of the query
// of the request. If the parameter is not present
// (empty string) it will not create an error and
// just return nil.
func OptionalQueryParamOrError[T any](
	name string,
	transform func(s string) (T, error),
	req *http.Request,
) (*T, error) {

	paramStr := req.URL.Query().Get(name)
	if paramStr != "" {

		if t, err := transform(paramStr); err != nil {
			return nil, err
		} else {
			return &t, nil
		}
	}
	return nil, nil
}

// Reads a generic argument struct from the requests
// body. It takes the codec as argument which is used to
// decode the struct from the request. If an error occurs
// nil and the error are returned.
func ReadStructFromBody[T any](req *http.Request, codec Codec[T]) (*T, error) {

	bodyBytes, err := ReadBody(req)
	if err != nil {
		return nil, err
	}

	return codec.Decode(bytes.NewReader(bodyBytes))
}

// Reads the body of a request into a byte array.
// If the body is empty, an empty array is returned.
// If an error occurs while reading the body, nil and
// the respective error is returned.
func ReadBody(req *http.Request) ([]byte, error) {

	if req.ContentLength < 0 {
		return nil, errors.New("malformed body")
	}

	body, err := io.ReadAll(req.Body)
	if err != nil {
		LogError("http-util", err)
		return nil, err
	}
	LogInfo("http-util", "read body from request. body="+string(body))
	return body, nil
}

// Executes a GET request at the given url.
// Use FormatUrl for to build the url.
// Headers can be defined using the headers map.
func HttpGet[T any](
	url string,
	headers map[string]string,
	codec Codec[T],
) (*T, int, error) {

	req, err := http.NewRequest(HTTP_GET, url, bytes.NewBufferString(""))
	if err != nil {
		return nil, -1, err
	}

	for k, v := range headers {
		req.Header.Add(k, v)
	}
	req.Header.Add("Accept", codec.HttpApplicationContentHeader())

	res, err := http.DefaultClient.Do(req)
	if err != nil {
		return nil, -1, err
	}

	if codec == nil {
		return nil, res.StatusCode, err
	} else {
		b, err := io.ReadAll(res.Body)
		if err != nil {
			LogError("http-util", err)
			if res.StatusCode > 299 {
				return nil, res.StatusCode, nil
			}
			return nil, -1, err
		}
		if res.StatusCode > 299 {
			LogInfo("http-util", fmt.Sprintf("response: %s", string(b)))
			return nil, res.StatusCode, nil
		}
		resBody, err := codec.Decode(bytes.NewReader(b))
		return resBody, res.StatusCode, err
	}
}

// execute a POST request and parse response or retrieve error
// path- and query-parameters can be set to add query and path parameters
func HttpPost[T any, R any](
	url string,
	headers map[string]string,
	body *T,
	reqCodec Codec[T],
	resCodec Codec[R],
) (*R, int, error) {

	bodyEncoded, err := reqCodec.EncodeToBytes(body)
	if err != nil {
		return nil, -1, err
	}
	LogInfo("http-util", string(bodyEncoded))

	req, err := http.NewRequest(HTTP_POST, url, bytes.NewBuffer(bodyEncoded))
	if err != nil {
		return nil, -1, err
	}

	for k, v := range headers {
		req.Header.Add(k, v)
	}
	if resCodec != nil {
		req.Header.Add("Accept", resCodec.HttpApplicationContentHeader())
	}
	req.Header.Add("Content-Type", reqCodec.HttpApplicationContentHeader())

	res, err := http.DefaultClient.Do(req)
	if err != nil {
		return nil, -1, err
	}

	if resCodec == nil {
		return nil, res.StatusCode, err
	} else {
		b, err := io.ReadAll(res.Body)
		if err != nil {
			LogError("http-util", err)
			if res.StatusCode > 299 {
				return nil, res.StatusCode, nil
			}
			return nil, -1, err
		}
		if res.StatusCode > 299 {
			LogInfo("http-util", fmt.Sprintf("response: %s", string(b)))
			return nil, res.StatusCode, nil
		}
		resBody, err := resCodec.Decode(bytes.NewReader(b))
		return resBody, res.StatusCode, err
	}
}

// builds request URL containing the path and query
// parameters of the respective parameter map.
func FormatUrl(
	req string,
	pathParams map[string]string,
	queryParams map[string]string,
) string {

	return setUrlQuery(setUrlPath(req, pathParams), queryParams)
}

// Sets the parameters which are part of the url.
// The function expects each parameter in the path to be prefixed
// using a ':'. The function handles url as follows:
//
//	/some/:param/tobereplaced -> ':param' will be replaced with value.
//
// For replacements, the pathParams map must be supplied. The map contains
// the name of the parameter with the value mapped to it.
// The names MUST not contain the prefix ':'!
func setUrlPath(
	req string,
	pathParams map[string]string,
) string {

	if pathParams == nil || len(pathParams) < 1 {
		return req
	}

	var url = req
	for k, v := range pathParams {

		if !strings.HasPrefix(k, "/") {
			// prevent scheme postfix replacements
			url = strings.Replace(url, ":"+k, v, 1)
		}
	}
	return url
}

func setUrlQuery(
	req string,
	queryParams map[string]string,
) string {

	if queryParams == nil || len(queryParams) < 1 {
		return req
	}

	var url = req + "?"
	for k, v := range queryParams {

		url = strings.Join([]string{url, k, "=", v, "&"}, "")
	}

	url, _ = strings.CutSuffix(url, "&")
	return url
}
