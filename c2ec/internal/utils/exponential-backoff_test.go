// This file is part of taler-cashless2ecash.
// Copyright (C) 2024 Joel Häberli
//
// taler-cashless2ecash is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as published
// by the Free Software Foundation, either version 3 of the License,
// or (at your option) any later version.
//
// taler-cashless2ecash is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// SPDX-License-Identifier: AGPL3.0-or-later

package internal_utils

import (
	"fmt"
	"testing"
	"time"
)

func TestShouldRetryYes(t *testing.T) {

	lastExecution := time.Now().Add(-(time.Duration(10 * time.Second)))
	retries := 4
	limitMs := 1000

	retry := ShouldStartRetry(lastExecution, retries, limitMs)
	if !retry {
		fmt.Println("expected retry = true but was false")
		t.FailNow()
	}
}

func TestShouldRetryNo(t *testing.T) {

	lastExecution := time.Now().Add(-(time.Duration(10 * time.Second)))
	retries := 1
	limitMs := 1000

	retry := ShouldStartRetry(lastExecution, retries, limitMs)
	if retry {
		fmt.Println("expected retry = false but was true")
		t.FailNow()
	}
}

func TestBackoff(t *testing.T) {

	expectations := []int{1, 2, 4, 8, 16, 32, 64, 128, 256}
	for i := range []int{0, 1, 2, 3, 4, 5, 6, 7, 8} {
		backoff := exponentialBackoffMs(i)
		if backoff != int64(expectations[i]) {
			fmt.Printf("expected %d, but got %d", expectations[i], backoff)
			t.FailNow()
		}
	}
}

func TestRandomization(t *testing.T) {

	input := 100
	lowerBoundary := 80  // -20%
	upperBoundary := 120 // +20%
	rounds := 1000
	currentRound := 0
	for currentRound < rounds {
		randomized := randomizeBackoff(int64(input))
		if randomized < int64(lowerBoundary) || randomized > int64(upperBoundary) {
			fmt.Printf("round %d failed. Expected value between %d and %d but got %d", currentRound, lowerBoundary, upperBoundary, randomized)
			t.FailNow()
		}
		currentRound++
	}
}
