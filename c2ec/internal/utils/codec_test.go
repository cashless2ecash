// This file is part of taler-cashless2ecash.
// Copyright (C) 2024 Joel Häberli
//
// taler-cashless2ecash is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as published
// by the Free Software Foundation, either version 3 of the License,
// or (at your option) any later version.
//
// taler-cashless2ecash is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// SPDX-License-Identifier: AGPL3.0-or-later

package internal_utils_test

import (
	"bytes"
	internal_api "c2ec/internal/api"
	internal_utils "c2ec/internal/utils"
	"fmt"
	"testing"

	"gotest.tools/v3/assert"
)

func TestJsonCodecRoundTrip(t *testing.T) {

	type TestStruct struct {
		A string
		B int
		C []string
		D byte
		E []byte
		F *TestStruct
	}

	testObj := TestStruct{
		"TestA",
		1,
		[]string{"first", "second"},
		'A',
		[]byte{0xdf, 0x01, 0x34},
		&TestStruct{
			"TestAA",
			2,
			[]string{"third", "fourth", "fifth"},
			'B',
			[]byte{0xdf, 0x01, 0x34},
			nil,
		},
	}

	jsonCodec := new(internal_utils.JsonCodec[TestStruct])

	encodedTestObj, err := jsonCodec.Encode(&testObj)
	if err != nil {
		fmt.Println("error happened while encoding test obj", err.Error())
		t.FailNow()
	}

	encodedTestObjBytes := make([]byte, 200)
	_, err = encodedTestObj.Read(encodedTestObjBytes)
	if err != nil {
		fmt.Println("error happened while encoding test obj to byte array", err.Error())
		t.FailNow()
	}

	encodedTestObjReader := bytes.NewReader(encodedTestObjBytes)
	decodedTestObj, err := jsonCodec.Decode(encodedTestObjReader)
	if err != nil {
		fmt.Println("error happened while encoding test obj to byte array", err.Error())
		t.FailNow()
	}

	assert.DeepEqual(t, &testObj, decodedTestObj)
}

func TestTransferRequest(t *testing.T) {

	reqStr := "{\"request_uid\":\"test-1\",\"amount\":\"CHF:4.95\",\"exchange_base_url\":\"https://exchange.chf.taler.net\",\"wtid\":\"\",\"credit_account\":\"payto://wallee-transaction/R361ZT45TZ026EQ0S909C88F0E2YJY11HXV0VQTCHKR2VHA7DQCG\"}"

	fmt.Println("request string:", reqStr)

	codec := new(internal_utils.JsonCodec[internal_api.TransferRequest])

	rdr := bytes.NewReader([]byte(reqStr))

	req, err := codec.Decode(rdr)
	if err != nil {
		fmt.Println("error:", err)
		t.FailNow()
	}

	fmt.Println(req)
}
