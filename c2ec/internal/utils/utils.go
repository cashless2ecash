// This file is part of taler-cashless2ecash.
// Copyright (C) 2024 Joel Häberli
//
// taler-cashless2ecash is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as published
// by the Free Software Foundation, either version 3 of the License,
// or (at your option) any later version.
//
// taler-cashless2ecash is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// SPDX-License-Identifier: AGPL3.0-or-later

package internal_utils

import (
	"encoding/base64"
	"errors"
	"strings"

	"golang.org/x/crypto/argon2"
)

// https://docs.taler.net/core/api-common.html#hash-codes
type WithdrawalIdentifier string

// https://docs.taler.net/core/api-common.html#cryptographic-primitives
type EddsaPublicKey string

// https://docs.taler.net/core/api-common.html#hash-codes
type HashCode string

// https://docs.taler.net/core/api-common.html#hash-codes
type ShortHashCode string

// https://docs.taler.net/core/api-common.html#timestamps
type Timestamp struct {
	Ts int `json:"t_s"`
}

// https://docs.taler.net/core/api-common.html#wadid
type WadId [6]uint32

// according to https://docs.taler.net/core/api-bank-integration.html#tsref-type-BankWithdrawalOperationStatus
type WithdrawalOperationStatus string

const (
	PENDING   WithdrawalOperationStatus = "pending"
	SELECTED  WithdrawalOperationStatus = "selected"
	ABORTED   WithdrawalOperationStatus = "aborted"
	CONFIRMED WithdrawalOperationStatus = "confirmed"
)

func ToWithdrawalOperationStatus(s string) (WithdrawalOperationStatus, error) {
	switch strings.ToLower(s) {
	case string(PENDING):
		return PENDING, nil
	case string(SELECTED):
		return SELECTED, nil
	case string(ABORTED):
		return ABORTED, nil
	case string(CONFIRMED):
		return CONFIRMED, nil
	default:
		return "", errors.New("invalid withdrawal operation status '" + s + "'")
	}
}

func RemoveNulls[T any](l []*T) []*T {

	withoutNulls := make([]*T, 0)
	for _, e := range l {
		if e != nil {
			withoutNulls = append(withoutNulls, e)
		}
	}
	return withoutNulls
}

// takes a password and a base64 encoded password hash, including salt and checks
// the password supplied against it.
// the format of the password hash is expected to be the following:
//
//	[32 BYTES HASH][16 BYTES SALT] = Bytes array with length of 48 bytes.
//
// returns true if password matches the password hash. Otherwise false.
func ValidPassword(pw string, base64EncodedHashAndSalt string) bool {

	hashedBytes := make([]byte, 48)
	decodedLen, err := base64.StdEncoding.Decode(hashedBytes, []byte(base64EncodedHashAndSalt))
	if err != nil {
		return false
	}

	if decodedLen != 48 {
		// malformed credentials
		return false
	}

	salt := hashedBytes[32:48]
	rfcTime := 3
	rfcMemory := 32 * 1024
	key := argon2.Key([]byte(pw), salt, uint32(rfcTime), uint32(rfcMemory), 4, 32)

	if len(key) != 32 {
		// length mismatch
		return false
	}

	for i := range key {
		if key[i] != hashedBytes[i] {
			// wrong password (application user key)
			return false
		}
	}

	// password correct.
	return true
}
