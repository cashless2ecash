// This file is part of taler-cashless2ecash.
// Copyright (C) 2024 Joel Häberli
//
// taler-cashless2ecash is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as published
// by the Free Software Foundation, either version 3 of the License,
// or (at your option) any later version.
//
// taler-cashless2ecash is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// SPDX-License-Identifier: AGPL3.0-or-later

package internal_utils

import (
	"bytes"
	"encoding/json"
	"io"
)

type Codec[T any] interface {
	HttpApplicationContentHeader() string
	Encode(*T) (io.Reader, error)
	EncodeToBytes(body *T) ([]byte, error)
	Decode(io.Reader) (*T, error)
}

type JsonCodec[T any] struct {
	Codec[T]
}

func NewJsonCodec[T any]() Codec[T] {

	return new(JsonCodec[T])
}

func (*JsonCodec[T]) HttpApplicationContentHeader() string {
	return "application/json"
}

func (*JsonCodec[T]) Encode(body *T) (io.Reader, error) {

	encodedBytes, err := json.Marshal(body)
	if err != nil {
		return nil, err
	}

	return bytes.NewReader(encodedBytes), err
}

func (c *JsonCodec[T]) EncodeToBytes(body *T) ([]byte, error) {

	reader, err := c.Encode(body)
	if err != nil {
		return make([]byte, 0), err
	}
	buf, err := io.ReadAll(reader)
	if err != nil {
		return make([]byte, 0), err
	}
	return buf, nil
}

func (*JsonCodec[T]) Decode(reader io.Reader) (*T, error) {

	body := new(T)
	err := json.NewDecoder(reader).Decode(body)
	return body, err
}
