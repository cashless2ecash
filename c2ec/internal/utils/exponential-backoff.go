// This file is part of taler-cashless2ecash.
// Copyright (C) 2024 Joel Häberli
//
// taler-cashless2ecash is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as published
// by the Free Software Foundation, either version 3 of the License,
// or (at your option) any later version.
//
// taler-cashless2ecash is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// SPDX-License-Identifier: AGPL3.0-or-later

package internal_utils

import (
	"crypto/rand"
	"fmt"
	"math"
	"math/big"
	"time"
)

const EXPONENTIAL_BACKOFF_BASE = 2

const RANDOMIZATION_THRESHOLD_FACTOR = 0.2 // +/- 20%

/*
Generic implementation of a limited exponential backoff
algorithm. It includes a randomization to prevent
self-synchronization issues.

Parameters:

  - lastExecution: time of the last execution
  - retryCount   : number of the retries
  - limitMs 	 : field shall be the maximal milliseconds to backoff before retry happens
*/
func ShouldStartRetry(
	lastExecution time.Time,
	retryCount int,
	limitMs int,
) bool {

	backoffMs := exponentialBackoffMs(retryCount)
	randomizedBackoffSeconds := int64(limitMs) / 1000
	if backoffMs < int64(limitMs) {
		randomizedBackoffSeconds = randomizeBackoff(backoffMs)
	} else {
		LogInfo("exponential-backoff", fmt.Sprintf("backoff limit exceeded. setting manual limit: %d", limitMs))
	}

	now := time.Now().Unix()
	backoffTime := lastExecution.Unix() + randomizedBackoffSeconds
	// LogInfo("exponential-backoff", fmt.Sprintf("lastExec=%d, now=%d, backoffTime=%d, shouldStartRetry=%s", lastExecution.Unix(), now, backoffTime, strconv.FormatBool(now >= backoffTime)))
	return now >= backoffTime
}

func exponentialBackoffMs(retries int) int64 {

	return int64(math.Pow(EXPONENTIAL_BACKOFF_BASE, float64(retries)))
}

func randomizeBackoff(backoff int64) int64 {

	// it's about randomizing on millisecond base... we mustn't care about rounding
	threshold := int64(math.Floor(float64(backoff)*RANDOMIZATION_THRESHOLD_FACTOR)) + 1 // +1 to guarantee positive threshold
	randomizedThreshold, err := rand.Int(rand.Reader, big.NewInt(backoff+threshold))
	if err != nil {
		LogError("exponential-backoff", err)
	}
	subtract, err := rand.Int(rand.Reader, big.NewInt(100)) // upper boundary is exclusive (value is between 0 and 99)
	if err != nil {
		LogError("exponential-backoff", err)
	}

	if !randomizedThreshold.IsInt64() {
		LogWarn("exponential-backoff", "the threshold is not int64")
		return backoff
	}

	if subtract.Int64() < 50 {
		subtracted := backoff - randomizedThreshold.Int64()
		if subtracted < 0 {
			return 0
		}
		return subtracted
	}
	return backoff + randomizedThreshold.Int64()
}
