// This file is part of taler-cashless2ecash.
// Copyright (C) 2024 Joel Häberli
//
// taler-cashless2ecash is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as published
// by the Free Software Foundation, either version 3 of the License,
// or (at your option) any later version.
//
// taler-cashless2ecash is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// SPDX-License-Identifier: AGPL3.0-or-later

package internal_utils

import (
	"fmt"
	"net/http"
	"os"
	"time"
)

const LOG_PATH = "c2ec-log.txt"

// LEVEL | TIME | SRC | MESSAGE
const LOG_PATTERN = "level=%d | time=%s | src=%s | %s"
const TIME_FORMAT = "yyyy-MM-dd hh:mm:ss"

type LogLevel int

const (
	INFO LogLevel = iota
	WARN
	ERROR
)

var lastResponseStatus = 0

func LoggingHandler(next http.Handler) http.Handler {

	return http.HandlerFunc(
		func(w http.ResponseWriter, r *http.Request) {

			LogRequest("http-logger", r)

			next.ServeHTTP(w, r)

			LogResponse("http-logger", r)
		},
	)
}

func SetLastResponseCodeForLogger(s int) {
	lastResponseStatus = s
}

func LogRequest(src string, req *http.Request) {

	LogInfo(src, fmt.Sprintf("%s - %s (origin=%s)", req.Method, req.URL, req.RemoteAddr))
}

func LogResponse(src string, req *http.Request) {

	LogInfo(src, fmt.Sprintf("code=%d: responding to %s (destination=%s)", lastResponseStatus, req.RequestURI, req.RemoteAddr))
}

func LogError(src string, err error) {

	go logAppendError(src, ERROR, err)
}

func LogWarn(src string, msg string) {

	go logAppend(src, WARN, msg)
}

func LogInfo(src string, msg string) {

	go logAppend(src, INFO, msg)
}

func logAppendError(src string, level LogLevel, err error) {

	if err == nil {
		fmt.Println("wanted to log from " + src + " but err was nil")
		return
	}
	logAppend(src, level, err.Error())
}

func logAppend(src string, level LogLevel, msg string) {

	openAppendClose(fmt.Sprintf(LOG_PATTERN, level, time.Now().Format(time.UnixDate), src, msg))
}

func openAppendClose(s string) {

	// first try opening only append
	f, err := os.OpenFile(LOG_PATH, os.O_APPEND|os.O_WRONLY, 0600)
	if err != nil || f == nil {
		// if file does not yet exist, open with create flag.
		f, err = os.OpenFile(LOG_PATH, os.O_CREATE|os.O_APPEND|os.O_WRONLY, 0600)
		if err != nil || f == nil {
			fmt.Println("error: ", err.Error())
			panic("failed opening or creating log file")
		}
	}
	f.WriteString(s + "\n")
	f.Close()
}
