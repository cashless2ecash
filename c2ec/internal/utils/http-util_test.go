// This file is part of taler-cashless2ecash.
// Copyright (C) 2024 Joel Häberli
//
// taler-cashless2ecash is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as published
// by the Free Software Foundation, either version 3 of the License,
// or (at your option) any later version.
//
// taler-cashless2ecash is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// SPDX-License-Identifier: AGPL3.0-or-later

package internal_utils

import (
	"fmt"
	"testing"
)

const URL_GET = "https://jsonplaceholder.typicode.com/todos/:id"
const URL_POST = "https://jsonplaceholder.typicode.com/posts"

type TestStruct struct {
	UserId    int    `json:"userId"`
	Id        int    `json:"id"`
	Title     string `json:"title"`
	Completed bool   `json:"completed"`
}

func TestGET(t *testing.T) {

	url := FormatUrl(
		URL_GET,
		map[string]string{
			"id": "1",
		},
		map[string]string{},
	)

	codec := NewJsonCodec[TestStruct]()
	res, status, err := HttpGet(
		url,
		map[string]string{},
		codec,
	)

	if err != nil {
		t.Errorf("%s", err.Error())
		t.FailNow()
	}

	fmt.Println("res:", res, ", status:", status)
}

func TestPOST(t *testing.T) {

	url := FormatUrl(
		URL_POST,
		map[string]string{
			"id": "1",
		},
		map[string]string{},
	)

	res, status, err := HttpPost(
		url,
		map[string]string{},
		&TestStruct{
			UserId:    1,
			Id:        1,
			Title:     "TEST",
			Completed: false,
		},
		NewJsonCodec[TestStruct](),
		NewJsonCodec[TestStruct](),
	)

	if err != nil {
		t.Errorf("%s", err.Error())
		t.FailNow()
	}

	fmt.Println("res:", res, ", status:", status)
}
