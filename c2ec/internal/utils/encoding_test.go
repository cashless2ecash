// This file is part of taler-cashless2ecash.
// Copyright (C) 2024 Joel Häberli
//
// taler-cashless2ecash is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as published
// by the Free Software Foundation, either version 3 of the License,
// or (at your option) any later version.
//
// taler-cashless2ecash is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// SPDX-License-Identifier: AGPL3.0-or-later

package internal_utils

import (
	"crypto/rand"
	"testing"
)

func TestWopidEncodeDecode(t *testing.T) {

	wopid := make([]byte, 32)
	n, err := rand.Read(wopid)
	if err != nil || n != 32 {
		t.Log("failed because retrieving random 32 bytes failed")
		t.FailNow()
	}

	encodedWopid := FormatWopid(wopid)
	t.Log("encoded wopid:", encodedWopid)
	decodedWopid, err := ParseWopid(encodedWopid)
	if err != nil {
		t.Error(err)
		t.FailNow()
	}

	if len(decodedWopid) != len(wopid) {
		t.Log("uneven length.", len(decodedWopid), "!=", len(wopid))
		t.FailNow()
	}

	for i, b := range wopid {

		if b != decodedWopid[i] {
			t.Log("unequal at position", i)
			t.FailNow()
		}
	}
}

func TestTalerBase32(t *testing.T) {

	input := []byte("This is some text")
	t.Log("in:", string(input))
	t.Log("in:", input)
	encoded := TalerBinaryEncode(input)
	t.Log("encoded:", encoded)
	out, err := TalerBinaryDecode(encoded)
	if err != nil {
		t.Error(err)
		t.FailNow()
	}
	t.Log("decoded:", out)
	t.Log("decoded:", string(out))

	if len(out) != len(input) {
		t.Log("uneven length.", len(out), "!=", len(input))
		t.FailNow()
	}

	for i, b := range input {

		if b != out[i] {
			t.Log("unequal at position", i)
			t.FailNow()
		}
	}
}

func TestTalerBase32Rand32(t *testing.T) {

	input := make([]byte, 32)
	n, err := rand.Read(input)
	if err != nil || n != 32 {
		t.Log("failed because retrieving random 32 bytes failed")
		t.FailNow()
	}

	t.Log("in:", input)
	encoded := TalerBinaryEncode(input)
	t.Log("encoded:", encoded)
	out, err := TalerBinaryDecode(encoded)
	if err != nil {
		t.Error(err)
		t.FailNow()
	}
	t.Log("decoded:", out)
	t.Log("decoded:", string(out))

	if len(out) != len(input) {
		t.Log("uneven length.", len(out), "!=", len(input))
		t.FailNow()
	}

	for i, b := range input {

		if b != out[i] {
			t.Log("unequal at position", i)
			t.FailNow()
		}
	}
}

func TestTalerBase32Rand64(t *testing.T) {

	input := make([]byte, 64)
	n, err := rand.Read(input)
	if err != nil || n != 64 {
		t.Log("failed because retrieving random 64 bytes failed")
		t.FailNow()
	}

	t.Log("in:", input)
	encoded := TalerBinaryEncode(input)
	t.Log("encoded:", encoded)
	out, err := TalerBinaryDecode(encoded)
	if err != nil {
		t.Error(err)
		t.FailNow()
	}
	t.Log("decoded:", out)
	t.Log("decoded:", string(out))

	if len(out) != len(input) {
		t.Log("uneven length.", len(out), "!=", len(input))
		t.FailNow()
	}

	for i, b := range input {

		if b != out[i] {
			t.Log("unequal at position", i)
			t.FailNow()
		}
	}
}
