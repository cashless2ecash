// This file is part of taler-cashless2ecash.
// Copyright (C) 2024 Joel Häberli
//
// taler-cashless2ecash is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as published
// by the Free Software Foundation, either version 3 of the License,
// or (at your option) any later version.
//
// taler-cashless2ecash is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// SPDX-License-Identifier: AGPL3.0-or-later

package internal_proc

import (
	internal_utils "c2ec/internal/utils"
	"c2ec/pkg/config"
	"c2ec/pkg/db"
	"c2ec/pkg/provider"
	"context"
	"errors"
	"fmt"
	"time"
)

const RETRY_CHANNEL_BUFFER_SIZE = 10
const PS_RETRY_CHANNEL = "retry"

func RunRetrier(ctx context.Context, errs chan error) {

	// go RunListener(
	// 	ctx,
	// 	PS_RETRY_CHANNEL,
	// 	retryCallback,
	// 	make(chan *Notification, RETRY_CHANNEL_BUFFER_SIZE),
	// 	errs,
	// )

	go func() {
		lastlog := time.Now().Add(time.Minute * -3)
		for {
			withdrawals, err := db.DB.GetWithdrawalsForConfirmation()
			time.Sleep(time.Duration(1000 * time.Millisecond))
			if err != nil {
				internal_utils.LogError("proc-retrier", err)
				errs <- err
				continue
			}
			if lastlog.Before(time.Now().Add(time.Second * -30)) {
				internal_utils.LogInfo("proc-retrier", fmt.Sprintf("retrier confirming 'selected' withdrawals. found %d ready for confirmation", len(withdrawals)))
				lastlog = time.Now()
			}
			for _, w := range withdrawals {
				retryOrSkip(w, errs)
			}
		}
	}()
}

// func retryCallback(n *Notification, errs chan error) {

// 	withdrawalId, err := strconv.Atoi(n.Payload)
// 	if err != nil {
// 		internal_utils.LogError("proc-retrier", err)
// 		errs <- err
// 		return
// 	}

// 	w, err := DB.GetWithdrawalById(withdrawalId)
// 	if err != nil {
// 		internal_utils.LogError("proc-retrier", err)
// 		errs <- err
// 		return
// 	}

// 	retryOrSkip(w, errs)
// }

func retryOrSkip(w *db.Withdrawal, errs chan error) {
	var lastRetryTs int64 = 0
	if w.LastRetryTs != nil {
		lastRetryTs = *w.LastRetryTs
		if internal_utils.ShouldStartRetry(time.Unix(lastRetryTs, 0), int(w.RetryCounter), config.CONFIG.Server.RetryDelayMs) {
			internal_utils.LogInfo("proc-retrier", "retrying for wopid="+internal_utils.TalerBinaryEncode(w.Wopid))
			confirmRetryOrAbort(w, errs)
		}
	} else {
		internal_utils.LogInfo("proc-retrier", "first retry confirming wopid="+internal_utils.TalerBinaryEncode(w.Wopid))
		confirmRetryOrAbort(w, errs)
	}
}

func confirmRetryOrAbort(withdrawal *db.Withdrawal, errs chan error) {

	if withdrawal == nil {
		err := errors.New("withdrawal was null")
		internal_utils.LogError("proc-retrier", err)
		errs <- err
		return
	}

	prvdr, err := db.DB.GetProviderByTerminal(withdrawal.TerminalId)
	if err != nil {
		internal_utils.LogError("proc-retrier", err)
		errs <- err
		return
	}

	client := provider.PROVIDER_CLIENTS[prvdr.Name]
	if client == nil {
		err := fmt.Errorf("the provider client for provider with name=%s is not configured", prvdr.Name)
		internal_utils.LogError("proc-retrier", err)
		errs <- err
		return
	}
	transaction, err := client.GetTransaction(*withdrawal.ProviderTransactionId)
	if err != nil {
		internal_utils.LogError("proc-retrier", err)
		errs <- err
		return
	}

	finaliseOrSetRetry(transaction, int(withdrawal.WithdrawalRowId), errs)
}
