// This file is part of taler-cashless2ecash.
// Copyright (C) 2024 Joel Häberli
//
// taler-cashless2ecash is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as published
// by the Free Software Foundation, either version 3 of the License,
// or (at your option) any later version.
//
// taler-cashless2ecash is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// SPDX-License-Identifier: AGPL3.0-or-later

package internal_proc

import (
	internal_utils "c2ec/internal/utils"
	"c2ec/pkg/db"
	"context"
	"errors"
)

func RunListener(
	ctx context.Context,
	channel string,
	callback func(*db.Notification, chan error),
	notifications chan *db.Notification,
	errs chan error,
) {

	listenFunc, err := db.DB.NewListener(channel, notifications)
	if err != nil {
		internal_utils.LogError("listener", err)
		errs <- errors.New("failed setting up listener")
		return
	}

	go func() {
		internal_utils.LogInfo("listener", "listener starts listening for notifications at the db for channel="+channel)
		err := listenFunc(ctx)
		if err != nil {
			internal_utils.LogError("listener", err)
			errs <- err
		}
		close(notifications)
		close(errs)
	}()

	// Listen is started async. We can therefore block here and must
	// not run the retrieval logic in own goroutine
	for {
		select {
		case notification := <-notifications:
			// the dispatching and further processing can be done asynchronously
			// thus not blocking further incoming notifications.
			go callback(notification, errs)
		case <-ctx.Done():
			errs <- ctx.Err()
			return
		}
	}
}
