package internal

import (
	internal_api "c2ec/internal/api"
	internal_postgres "c2ec/internal/db/postgres"
	internal_proc "c2ec/internal/proc"
	internal_provider_simulation "c2ec/internal/provider/simulation"
	internal_provider_wallee "c2ec/internal/provider/wallee"
	internal_utils "c2ec/internal/utils"
	"c2ec/pkg/config"
	"c2ec/pkg/db"
	"c2ec/pkg/provider"
	"context"
	"errors"
	"fmt"
	"net"
	"net/http"
	"os"
	"os/signal"
	"syscall"
)

const GET = "GET "
const POST = "POST "
const DELETE = "DELETE "

// https://docs.taler.net/core/api-terminal.html#endpoints-for-integrated-sub-apis
const BANK_INTEGRATION_API = "/taler-integration"
const WIRE_GATEWAY_API = "/taler-wire-gateway"

const WIRE_GATEWAY_CONFIG_ENDPOINT = "/config"
const WIRE_GATEWAY_HISTORY_ENDPOINT = "/history"

const WIRE_GATEWAY_CONFIG_PATTERN = WIRE_GATEWAY_CONFIG_ENDPOINT
const WIRE_TRANSFER_PATTERN = "/transfer"
const WIRE_HISTORY_INCOMING_PATTERN = WIRE_GATEWAY_HISTORY_ENDPOINT + "/incoming"
const WIRE_HISTORY_OUTGOING_PATTERN = WIRE_GATEWAY_HISTORY_ENDPOINT + "/outgoing"
const WIRE_ADMIN_ADD_INCOMING_PATTERN = "/admin/add-incoming"

const WITHDRAWAL_OPERATION = "/withdrawal-operation"

const WOPID_PARAMETER = "wopid"
const BANK_INTEGRATION_CONFIG_PATTERN = "/config"
const WITHDRAWAL_OPERATION_PATTERN = WITHDRAWAL_OPERATION
const WITHDRAWAL_OPERATION_BY_WOPID_PATTERN = WITHDRAWAL_OPERATION + "/{" + WOPID_PARAMETER + "}"
const WITHDRAWAL_OPERATION_ABORTION_PATTERN = WITHDRAWAL_OPERATION_BY_WOPID_PATTERN + "/abort"

const TERMINAL_API_CONFIG = "/config"
const TERMINAL_API_REGISTER_WITHDRAWAL = "/withdrawals"
const TERMINAL_API_WITHDRAWAL_STATUS = "/withdrawals/{wopid}"
const TERMINAL_API_CHECK_WITHDRAWAL = "/withdrawals/{wopid}/check"
const TERMINAL_API_ABORT_WITHDRAWAL = "/withdrawals/{wopid}/abort"

func C2EC() {

	d, err := setupDatabase(&config.CONFIG.Database)
	if err != nil {
		panic("unable to connect to datatbase: " + err.Error())
	}
	db.DB = d

	err = setupProviderClients(&config.CONFIG)
	if err != nil {
		panic("unable initialize provider clients: " + err.Error())
	}
	internal_utils.LogInfo("c2ec", "provider clients are setup")

	retryCtx, retryCancel := context.WithCancel(context.Background())
	defer retryCancel()
	retryErrs := make(chan error)
	internal_proc.RunRetrier(retryCtx, retryErrs)
	internal_utils.LogInfo("c2ec", "retrier is running")

	attestorCtx, attestorCancel := context.WithCancel(context.Background())
	defer attestorCancel()
	attestorErrs := make(chan error)
	internal_proc.RunAttestor(attestorCtx, attestorErrs)
	internal_utils.LogInfo("c2ec", "attestor is running")

	transferCtx, transferCancel := context.WithCancel(context.Background())
	defer transferCancel()
	transferErrs := make(chan error)
	internal_proc.RunTransferrer(transferCtx, transferErrs)
	internal_utils.LogInfo("c2ec", "refunder is running")

	router := http.NewServeMux()
	routerErrs := make(chan error)

	setupBankIntegrationRoutes(router)
	setupWireGatewayRoutes(router)
	setupTerminalRoutes(router)
	setupAgplRoutes(router)

	startListening(router, routerErrs)

	// since listening for incoming request, attesting payments and
	// retrying payments are separated processes who can fail
	// we must take care of this here. The main process is used to
	// dispatch incoming http request and parent of the confirmation
	// and retry processes. If the main process fails somehow, also
	// confirmation and retries will end. But if somehow the confirmation
	// or retry process fail, they will be restarted and the error is
	// written to the log. If some setup tasks are failing, the program
	// panics.
	for {
		select {
		case routerError := <-routerErrs:
			internal_utils.LogError("c2ec", routerError)
			attestorCancel()
			retryCancel()
			transferCancel()
			panic(routerError)
		case <-attestorCtx.Done():
			attestorCancel() // first run old cancellation function
			attestorCtx, attestorCancel = context.WithCancel(context.Background())
			internal_proc.RunAttestor(attestorCtx, attestorErrs)
		case <-retryCtx.Done():
			retryCancel() // first run old cancellation function
			retryCtx, retryCancel = context.WithCancel(context.Background())
			internal_proc.RunRetrier(retryCtx, retryErrs)
		case <-transferCtx.Done():
			transferCancel() // first run old cancellation function
			transferCtx, transferCancel = context.WithCancel(context.Background())
			internal_proc.RunTransferrer(transferCtx, transferErrs)
		case confirmationError := <-attestorErrs:
			internal_utils.LogError("c2ec-from-proc-attestor", confirmationError)
		case retryError := <-retryErrs:
			internal_utils.LogError("c2ec-from-proc-retrier", retryError)
		case transferError := <-transferErrs:
			internal_utils.LogError("c2ec-from-proc-transfer", transferError)
		}
	}
}

func setupDatabase(cfg *config.C2ECDatabseConfig) (db.C2ECDatabase, error) {

	return internal_postgres.NewC2ECPostgres(cfg)
}

func setupProviderClients(cfg *config.C2ECConfig) error {

	if db.DB == nil {
		return errors.New("setup database first")
	}

	for _, provider := range cfg.Providers {

		p, err := db.DB.GetTerminalProviderByName(provider.Name)
		if err != nil {
			return err
		}

		if p == nil {
			if cfg.Server.IsProd || cfg.Server.StrictAttestors {
				panic("no provider entry for " + provider.Name)
			} else {
				internal_utils.LogWarn("non-strict attestor initialization. skipping", provider.Name)
				continue
			}
		}

		if !cfg.Server.IsProd {
			// Prevent simulation client to be loaded in productive environments.
			if p.Name == "Simulation" {

				simulationClient := new(internal_provider_simulation.SimulationClient)
				err := simulationClient.SetupClient(p)
				if err != nil {
					return err
				}
				internal_utils.LogInfo("c2ec", "setup the Simulation provider")
			}
		}

		if p.Name == "Wallee" {

			walleeClient := new(internal_provider_wallee.WalleeClient)
			err := walleeClient.SetupClient(p)
			if err != nil {
				return err
			}
			internal_utils.LogInfo("c2ec", "setup the Wallee provider")
		}

		// For new added provider, add the respective if-clause
	}

	for _, p := range config.CONFIG.Providers {
		if provider.PROVIDER_CLIENTS[p.Name] == nil {
			err := errors.New("no provider client initialized for provider " + p.Name)
			internal_utils.LogError("retrier", err)
			return err
		}
	}

	return nil
}

func setupBankIntegrationRoutes(router *http.ServeMux) {

	router.HandleFunc(
		GET+BANK_INTEGRATION_API+BANK_INTEGRATION_CONFIG_PATTERN,
		internal_api.BankIntegrationConfigApi,
	)
	internal_utils.LogInfo("c2ec", "setup "+GET+BANK_INTEGRATION_API+BANK_INTEGRATION_CONFIG_PATTERN)

	router.HandleFunc(
		GET+BANK_INTEGRATION_API+WITHDRAWAL_OPERATION_BY_WOPID_PATTERN,
		internal_api.HandleWithdrawalStatus,
	)
	internal_utils.LogInfo("c2ec", "setup "+GET+BANK_INTEGRATION_API+WITHDRAWAL_OPERATION_BY_WOPID_PATTERN)

	router.HandleFunc(
		POST+BANK_INTEGRATION_API+WITHDRAWAL_OPERATION_BY_WOPID_PATTERN,
		internal_api.HandleParameterRegistration,
	)
	internal_utils.LogInfo("c2ec", "setup "+POST+BANK_INTEGRATION_API+WITHDRAWAL_OPERATION_BY_WOPID_PATTERN)

	router.HandleFunc(
		POST+BANK_INTEGRATION_API+WITHDRAWAL_OPERATION_ABORTION_PATTERN,
		internal_api.HandleWithdrawalAbort,
	)
	internal_utils.LogInfo("c2ec", "setup "+POST+BANK_INTEGRATION_API+WITHDRAWAL_OPERATION_ABORTION_PATTERN)
}

func setupWireGatewayRoutes(router *http.ServeMux) {

	router.HandleFunc(
		GET+WIRE_GATEWAY_API+WIRE_GATEWAY_CONFIG_PATTERN,
		internal_api.WireGatewayConfig,
	)
	internal_utils.LogInfo("c2ec", "setup "+GET+WIRE_GATEWAY_API+WIRE_GATEWAY_CONFIG_PATTERN)

	router.HandleFunc(
		POST+WIRE_GATEWAY_API+WIRE_TRANSFER_PATTERN,
		internal_api.Transfer,
	)
	internal_utils.LogInfo("c2ec", "setup "+POST+WIRE_GATEWAY_API+WIRE_TRANSFER_PATTERN)

	router.HandleFunc(
		GET+WIRE_GATEWAY_API+WIRE_HISTORY_INCOMING_PATTERN,
		internal_api.HistoryIncoming,
	)
	internal_utils.LogInfo("c2ec", "setup "+GET+WIRE_GATEWAY_API+WIRE_HISTORY_INCOMING_PATTERN)

	router.HandleFunc(
		GET+WIRE_GATEWAY_API+WIRE_HISTORY_OUTGOING_PATTERN,
		internal_api.HistoryOutgoing,
	)
	internal_utils.LogInfo("c2ec", "setup "+GET+WIRE_GATEWAY_API+WIRE_HISTORY_OUTGOING_PATTERN)

	router.HandleFunc(
		POST+WIRE_GATEWAY_API+WIRE_ADMIN_ADD_INCOMING_PATTERN,
		internal_api.AdminAddIncoming,
	)
	internal_utils.LogInfo("c2ec", "setup "+POST+WIRE_GATEWAY_API+WIRE_ADMIN_ADD_INCOMING_PATTERN)
}

func setupTerminalRoutes(router *http.ServeMux) {

	router.HandleFunc(
		GET+TERMINAL_API_CONFIG,
		internal_api.HandleTerminalConfig,
	)
	internal_utils.LogInfo("c2ec", "setup "+GET+TERMINAL_API_CONFIG)

	router.HandleFunc(
		POST+TERMINAL_API_REGISTER_WITHDRAWAL,
		internal_api.HandleWithdrawalSetup,
	)
	internal_utils.LogInfo("c2ec", "setup "+POST+TERMINAL_API_REGISTER_WITHDRAWAL)

	router.HandleFunc(
		POST+TERMINAL_API_CHECK_WITHDRAWAL,
		internal_api.HandleWithdrawalCheck,
	)
	internal_utils.LogInfo("c2ec", "setup "+POST+TERMINAL_API_CHECK_WITHDRAWAL)

	router.HandleFunc(
		GET+TERMINAL_API_WITHDRAWAL_STATUS,
		internal_api.HandleWithdrawalStatusTerminal,
	)
	internal_utils.LogInfo("c2ec", "setup "+GET+TERMINAL_API_WITHDRAWAL_STATUS)

	router.HandleFunc(
		DELETE+TERMINAL_API_ABORT_WITHDRAWAL,
		internal_api.HandleWithdrawalAbortTerminal,
	)
	internal_utils.LogInfo("c2ec", "setup "+DELETE+TERMINAL_API_ABORT_WITHDRAWAL)
}

func setupAgplRoutes(router *http.ServeMux) {

	router.HandleFunc(
		GET+"/agpl",
		internal_api.Agpl,
	)
	internal_utils.LogInfo("c2ec", "setup "+GET+"/agpl")
}

func startListening(router *http.ServeMux, errs chan error) {

	server := http.Server{
		Handler: internal_utils.LoggingHandler(router),
	}

	if config.CONFIG.Server.UseUnixDomainSocket {

		internal_utils.LogInfo("c2ec", "using domain sockets")
		socket, err := net.Listen("unix", config.CONFIG.Server.UnixSocketPath)
		if err != nil {
			panic("failed listening on socket: " + err.Error())
		}

		// cleans up socket when process fails and is shutdown.
		c := make(chan os.Signal, 1)
		signal.Notify(c, os.Interrupt, syscall.SIGTERM)
		go func() {
			<-c
			os.Remove(config.CONFIG.Server.UnixSocketPath)
			os.Exit(1)
		}()

		go func() {
			internal_utils.LogInfo("c2ec", "serving at unix-domain-socket "+server.Addr)
			if err = server.Serve(socket); err != nil {
				errs <- err
			}
		}()
	} else {

		internal_utils.LogInfo("c2ec", "using tcp")
		go func() {
			server.Addr = fmt.Sprintf("%s:%d", config.CONFIG.Server.Host, config.CONFIG.Server.Port)
			internal_utils.LogInfo("c2ec", "serving at "+server.Addr)
			if err := server.ListenAndServe(); err != nil {
				internal_utils.LogError("c2ec", err)
				errs <- err
			}
		}()
	}
}
