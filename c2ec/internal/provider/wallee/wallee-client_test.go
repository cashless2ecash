// This file is part of taler-cashless2ecash.
// Copyright (C) 2024 Joel Häberli
//
// taler-cashless2ecash is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as published
// by the Free Software Foundation, either version 3 of the License,
// or (at your option) any later version.
//
// taler-cashless2ecash is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// SPDX-License-Identifier: AGPL3.0-or-later

package internal_provider_wallee

import (
	internal_utils "c2ec/internal/utils"
	"errors"
	"fmt"
	"strconv"
	"strings"
	"testing"
)

// integration tests shall be executed manually
// because of needed stuff credentials.
const ENABLE_WALLEE_INTEGRATION_TEST = false

// configure the INT_* constants to to run integration tests
// be aware that this can possibly trigger and tamper real
// transactions (in case of the refund).
const INT_TEST_SPACE_ID = 0
const INT_TEST_USER_ID = 0
const INT_TEST_ACCESS_TOKEN = ""

const INT_TEST_REFUND_AMOUNT = "0"
const INT_TEST_REFUND_EXT_ID = "" // can be anything -> idempotency
const INT_TEST_REFUND_MERCHANT_REFERENCE = ""
const INT_TEST_REFUND_TRANSACTION_ID = 0

func TestCutSchemeAndHost(t *testing.T) {

	urls := []string{
		"https://app-wallee.com/api/transaction/search?spaceId=54275",
		"https://app-wallee.com/api/transaction/search?spaceId=54275?spaceId=54275&id=212156032",
		"/api/transaction/search?spaceId=54275?spaceId=54275&id=212156032",
		"http://test.com.ag.ch.de-en/api/transaction/search?spaceId=54275?spaceId=54275&id=212156032",
	}

	for _, url := range urls {
		cutted := cutSchemeAndHost(url)
		fmt.Println(cutted)
		if !strings.HasPrefix(cutted, "/api") {
			t.FailNow()
		}
	}
}

func TestWalleeMac(t *testing.T) {

	// https://app-wallee.com/en-us/doc/api/web-service#_java
	// assuming the java example on the website of wallee is correct
	// the following parameters should result to the given expected
	// result using my Golang implementation.

	// authStr := "1|100000|1715454671|GET|/api/transaction/read?spaceId=10000&id=200000000"
	secret := "OWOMg2gnaSx1nukAM6SN2vxedfY1yLPONvcTKbhDv7I="
	expected := "PNqpGIkv+4jVcdIYqp5Pp2tKGWSjO1bNdEAIPgllWb7A6BDRvQQ/I2fnZF20roAIJrP22pe1LvHH8lWpIzJbWg=="

	calculated, err := calculateWalleeAuthToken(100000, int64(1715454671), "GET", "https://some.domain/api/transaction/read?spaceId=10000&id=200000000", secret)
	if err != nil {
		t.Error(err)
		t.FailNow()
	}

	fmt.Println("expected:", expected)
	fmt.Println("calcultd:", calculated)

	if expected != calculated {
		t.Error(errors.New("calculated auth token not equal to expected token"))
		t.FailNow()
	}
}

func TestTransactionSearchIntegration(t *testing.T) {

	if !ENABLE_WALLEE_INTEGRATION_TEST {
		fmt.Println("info: integration test disabled")
		return
	}

	filter := WalleeSearchFilter{
		FieldName: "merchantReference",
		Operator:  EQUALS,
		Type:      LEAF,
		Value:     "TTZQFA2QQ14AARC82F7Z2Q9JCH40ZHXCE3BMXJV1FG87BP2GA3P0",
	}

	req := WalleeTransactionSearchRequest{
		Filter:           filter,
		Language:         "en",
		NumberOfEntities: 1,
		StartingEntity:   0,
	}

	api := "https://app-wallee.com/api/transaction/search"
	api = internal_utils.FormatUrl(api, map[string]string{}, map[string]string{"spaceId": strconv.Itoa(INT_TEST_SPACE_ID)})

	hdrs, err := prepareWalleeHeaders(api, "POST", INT_TEST_USER_ID, INT_TEST_ACCESS_TOKEN)
	if err != nil {
		fmt.Println("Error preparing headers (req1): ", err.Error())
		t.FailNow()
	}

	for k, v := range hdrs {
		fmt.Println("req1", k, v)
	}

	p, s, err := internal_utils.HttpPost(
		api,
		hdrs,
		&req,
		internal_utils.NewJsonCodec[WalleeTransactionSearchRequest](),
		internal_utils.NewJsonCodec[[]*WalleeTransaction](),
	)
	if err != nil {
		fmt.Println("Error executing request: ", err.Error())
		fmt.Println("Status: ", s)
	} else {
		fmt.Println("wallee response status: ", s)
		fmt.Println("wallee response: ", p)
	}
}

func TestRefundIntegration(t *testing.T) {

	if !ENABLE_WALLEE_INTEGRATION_TEST {
		fmt.Println("info: integration test disabled")
		return
	}

	url := "https://app-wallee.com/api/refund/refund"
	url = internal_utils.FormatUrl(url, map[string]string{}, map[string]string{"spaceId": strconv.Itoa(INT_TEST_SPACE_ID)})

	hdrs, err := prepareWalleeHeaders(url, "POST", INT_TEST_USER_ID, INT_TEST_ACCESS_TOKEN)
	if err != nil {
		fmt.Println("Error preparing headers: ", err.Error())
		t.FailNow()
	}

	for k, v := range hdrs {
		fmt.Println("req", k, v)
	}

	refund := &WalleeRefund{
		Amount:            INT_TEST_REFUND_AMOUNT,
		ExternalID:        INT_TEST_REFUND_EXT_ID,
		MerchantReference: INT_TEST_REFUND_MERCHANT_REFERENCE,
		Transaction: WalleeRefundTransaction{
			Id: INT_TEST_REFUND_TRANSACTION_ID,
		},
		Type: "MERCHANT_INITIATED_ONLINE", // this type will refund the transaction using the responsible processor (e.g. VISA, MasterCard, TWINT, etc.)
	}

	_, status, err := internal_utils.HttpPost[WalleeRefund, any](url, hdrs, refund, internal_utils.NewJsonCodec[WalleeRefund](), nil)
	if err != nil {
		fmt.Println("Error sending refund request:", err)
		t.FailNow()
	}
	if status != internal_utils.HTTP_OK {
		fmt.Println("Received unsuccessful status code:", status)
		t.FailNow()
	}
}
