// This file is part of taler-cashless2ecash.
// Copyright (C) 2024 Joel Häberli
//
// taler-cashless2ecash is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as published
// by the Free Software Foundation, either version 3 of the License,
// or (at your option) any later version.
//
// taler-cashless2ecash is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// SPDX-License-Identifier: AGPL3.0-or-later

package internal_provider_wallee

import (
	"c2ec/pkg/provider"
	"time"
)

type WalleeSearchOperator string

type WalleeSearchType string

const (
	LEAF WalleeSearchType = "LEAF"
)

const (
	EQUALS WalleeSearchOperator = "EQUALS"
)

type WalleeSearchFilter struct {
	FieldName string               `json:"fieldName"`
	Operator  WalleeSearchOperator `json:"operator"`
	Type      WalleeSearchType     `json:"type"`
	Value     string               `json:"value"`
}

type WalleeTransactionSearchRequest struct {
	Filter           WalleeSearchFilter `json:"filter"`
	Language         string             `json:"language"`
	NumberOfEntities int                `json:"numberOfEntities"`
	StartingEntity   int                `json:"startingEntity"`
}

type WalleeTransactionCompletion struct {
	Amount              float64          `json:"amount"`
	BaseLineItems       []WalleeLineItem `json:"baseLineItems"`
	CreatedBy           int64            `json:"createdBy"`
	CreatedOn           time.Time        `json:"createdOn"`
	ExternalID          string           `json:"externalId"`
	FailedOn            time.Time        `json:"failedOn"`
	FailureReason       string           `json:"failureReason"`
	ID                  int64            `json:"id"`
	InvoiceMerchantRef  string           `json:"invoiceMerchantReference"`
	Labels              []WalleeLabel    `json:"labels"`
	Language            string           `json:"language"`
	LastCompletion      bool             `json:"lastCompletion"`
	LineItemVersion     string           `json:"lineItemVersion"`
	LineItems           []WalleeLineItem `json:"lineItems"`
	LinkedSpaceID       int64            `json:"linkedSpaceId"`
	LinkedTransaction   int64            `json:"linkedTransaction"`
	Mode                string           `json:"mode"`
	NextUpdateOn        time.Time        `json:"nextUpdateOn"`
	PaymentInformation  string           `json:"paymentInformation"`
	PlannedPurgeDate    time.Time        `json:"plannedPurgeDate"`
	ProcessingOn        time.Time        `json:"processingOn"`
	ProcessorReference  string           `json:"processorReference"`
	RemainingLineItems  []WalleeLineItem `json:"remainingLineItems"`
	SpaceViewID         int64            `json:"spaceViewId"`
	State               string           `json:"state"`
	StatementDescriptor string           `json:"statementDescriptor"`
	SucceededOn         time.Time        `json:"succeededOn"`
	TaxAmount           float64          `json:"taxAmount"`
	TimeZone            string           `json:"timeZone"`
	TimeoutOn           time.Time        `json:"timeoutOn"`
	Version             int              `json:"version"`
}

/*
	{
	    "amount": "14.00",
	    "externalId": "1",
	    "merchantReference": "1BQMAGTYTQVM0B1EM40PDS4H4REVMNCEN9867SJQ26Q43C38RDDG",
	    "transaction": {
	        "id": 213103343
	    },
	    "type": "MERCHANT_INITIATED_ONLINE"
	}
*/
type WalleeRefund struct {
	Amount            string                  `json:"amount"`
	ExternalID        string                  `json:"externalId"` // idempotence support
	MerchantReference string                  `json:"merchantReference"`
	Transaction       WalleeRefundTransaction `json:"transaction"`
	/*
		Refund Type (for testing (not triggered at processor): MERCHANT_INITIATED_OFFLINE
		For real world (triggering at the processor): MERCHANT_INITIATED_ONLINE
	*/
	Type string `json:"type"`
}

type WalleeRefundTransaction struct {
	Id int64 `json:"id"`
}

// type WalleeRefund struct {
// 	Amount            float64                   `json:"amount"`
// 	Completion        int64                     `json:"completion"` // ID of WalleeTransactionCompletion
// 	ExternalID        string                    `json:"externalId"` // Unique per transaction
// 	MerchantReference string                    `json:"merchantReference"`
// 	Reductions        []WalleeLineItemReduction `json:"reductions"`
// 	Transaction       int64                     `json:"transaction"` // ID of WalleeTransaction
// 	Type              string                    `json:"type"`        // Refund Type
// }

type WalleeLabel struct {
	Content         []byte                `json:"content"`
	ContentAsString string                `json:"contentAsString"`
	Descriptor      WalleeLabelDescriptor `json:"descriptor"`
	ID              int64                 `json:"id"`
	Version         int                   `json:"version"`
}

type WalleeLabelDescriptor struct {
	Category    string            `json:"category"`
	Description map[string]string `json:"description"`
	Features    []int64           `json:"features"`
	Group       int64             `json:"group"`
	ID          int64             `json:"id"`
	Name        map[string]string `json:"name"`
	Type        int64             `json:"type"`
	Weight      int               `json:"weight"`
}

type WalleeLineItemReduction struct {
	LineItemUniqueId   string
	QuantityReduction  float64
	UnitPriceReduction float64
}

type WalleeLineItemAttribute struct {
	Label string
	Value string
}

type WalleeTax struct {
	Rate  float64
	Title string
}

type WalleeLineItem struct {
	AggregatedTaxRate              float64                            `json:"aggregatedTaxRate"`
	AmountExcludingTax             float64                            `json:"amountExcludingTax"`
	AmountIncludingTax             float64                            `json:"amountIncludingTax"`
	Attributes                     map[string]WalleeLineItemAttribute `json:"attributes"`
	DiscountExcludingTax           float64                            `json:"discountExcludingTax"`
	DiscountIncludingTax           float64                            `json:"discountIncludingTax"`
	Name                           string                             `json:"name"`
	Quantity                       float64                            `json:"quantity"`
	ShippingRequired               bool                               `json:"shippingRequired"`
	SKU                            string                             `json:"sku"`
	TaxAmount                      float64                            `json:"taxAmount"`
	TaxAmountPerUnit               float64                            `json:"taxAmountPerUnit"`
	Taxes                          []WalleeTax                        `json:"taxes"`
	Type                           string                             `json:"type"`
	UndiscountedAmountExcludingTax float64                            `json:"undiscountedAmountExcludingTax"`
	UndiscountedAmountIncludingTax float64                            `json:"undiscountedAmountIncludingTax"`
	UndiscountedUnitPriceExclTax   float64                            `json:"undiscountedUnitPriceExcludingTax"`
	UndiscountedUnitPriceInclTax   float64                            `json:"undiscountedUnitPriceIncludingTax"`
	UniqueID                       string                             `json:"uniqueId"`
	UnitPriceExcludingTax          float64                            `json:"unitPriceExcludingTax"`
	UnitPriceIncludingTax          float64                            `json:"unitPriceIncludingTax"`
}

type WalleeTransactionState string

const (
	StateCreate     WalleeTransactionState = "CREATE"
	StatePending    WalleeTransactionState = "PENDING"
	StateConfirmed  WalleeTransactionState = "CONFIRMED"
	StateProcessing WalleeTransactionState = "PROCESSING"
	StateFailed     WalleeTransactionState = "FAILED"
	StateAuthorized WalleeTransactionState = "AUTHORIZED"
	StateCompleted  WalleeTransactionState = "COMPLETED"
	StateFulfill    WalleeTransactionState = "FULFILL"
	StateDecline    WalleeTransactionState = "DECLINE"
	StateVoided     WalleeTransactionState = "VOIDED"
)

type WalleeTransaction struct {
	provider.ProviderTransaction
	AcceptHeader                       interface{}                         `json:"acceptHeader"`
	AcceptLanguageHeader               interface{}                         `json:"acceptLanguageHeader"`
	AllowedPaymentMethodBrands         []interface{}                       `json:"allowedPaymentMethodBrands"`
	AllowedPaymentMethodConfigurations []interface{}                       `json:"allowedPaymentMethodConfigurations"`
	AuthorizationAmount                float64                             `json:"authorizationAmount"`
	AuthorizationEnvironment           string                              `json:"authorizationEnvironment"`
	AuthorizationSalesChannel          int64                               `json:"authorizationSalesChannel"`
	AuthorizationTimeoutOn             time.Time                           `json:"authorizationTimeoutOn"`
	AuthorizedOn                       time.Time                           `json:"authorizedOn"`
	AutoConfirmationEnabled            bool                                `json:"autoConfirmationEnabled"`
	BillingAddress                     interface{}                         `json:"billingAddress"`
	ChargeRetryEnabled                 bool                                `json:"chargeRetryEnabled"`
	CompletedAmount                    float64                             `json:"completedAmount"`
	CompletedOn                        interface{}                         `json:"completedOn"`
	CompletionBehavior                 string                              `json:"completionBehavior"`
	CompletionTimeoutOn                interface{}                         `json:"completionTimeoutOn"`
	ConfirmedBy                        int                                 `json:"confirmedBy"`
	ConfirmedOn                        time.Time                           `json:"confirmedOn"`
	CreatedBy                          int                                 `json:"createdBy"`
	CreatedOn                          time.Time                           `json:"createdOn"`
	Currency                           string                              `json:"currency"`
	CustomerEmailAddress               interface{}                         `json:"customerEmailAddress"`
	CustomerId                         interface{}                         `json:"customerId"`
	CustomersPresence                  string                              `json:"customersPresence"`
	DeliveryDecisionMadeOn             interface{}                         `json:"deliveryDecisionMadeOn"`
	DeviceSessionIdentifier            interface{}                         `json:"deviceSessionIdentifier"`
	EmailsDisabled                     bool                                `json:"emailsDisabled"`
	EndOfLife                          time.Time                           `json:"endOfLife"`
	Environment                        string                              `json:"environment"`
	EnvironmentSelectionStrategy       string                              `json:"environmentSelectionStrategy"`
	FailedOn                           interface{}                         `json:"failedOn"`
	FailedUrl                          interface{}                         `json:"failedUrl"`
	FailureReason                      interface{}                         `json:"failureReason"`
	Group                              WalleeGroup                         `json:"group"`
	Id                                 int64                               `json:"id"`
	InternetProtocolAddress            interface{}                         `json:"internetProtocolAddress"`
	InternetProtocolAddressCountry     interface{}                         `json:"internetProtocolAddressCountry"`
	InvoiceMerchantReference           string                              `json:"invoiceMerchantReference"`
	JavaEnabled                        interface{}                         `json:"javaEnabled"`
	Language                           string                              `json:"language"`
	LineItems                          []WalleeLineItem                    `json:"lineItems"`
	LinkedSpaceId                      int                                 `json:"linkedSpaceId"`
	MerchantReference                  string                              `json:"merchantReference"`
	MetaData                           struct{}                            `json:"metaData"`
	Parent                             interface{}                         `json:"parent"`
	PaymentConnectorConfiguration      WalleePaymentConnectorConfiguration `json:"paymentConnectorConfiguration"`
	PlannedPurgeDate                   time.Time                           `json:"plannedPurgeDate"`
	ProcessingOn                       time.Time                           `json:"processingOn"`
	RefundedAmount                     float64                             `json:"refundedAmount"`
	ScreenColorDepth                   interface{}                         `json:"screenColorDepth"`
	ScreenHeight                       interface{}                         `json:"screenHeight"`
	ScreenWidth                        interface{}                         `json:"screenWidth"`
	ShippingAddress                    interface{}                         `json:"shippingAddress"`
	ShippingMethod                     interface{}                         `json:"shippingMethod"`
	SpaceViewId                        interface{}                         `json:"spaceViewId"`
	State                              string                              `json:"state"`
	SuccessUrl                         interface{}                         `json:"successUrl"`
	Terminal                           WalleeTerminal                      `json:"terminal"`
	TimeZone                           interface{}                         `json:"timeZone"`
	Token                              interface{}                         `json:"token"`
	TokenizationMode                   interface{}                         `json:"tokenizationMode"`
	TotalAppliedFees                   float64                             `json:"totalAppliedFees"`
	TotalSettledAmount                 float64                             `json:"totalSettledAmount"`
	UserAgentHeader                    interface{}                         `json:"userAgentHeader"`
	UserFailureMessage                 interface{}                         `json:"userFailureMessage"`
	UserInterfaceType                  string                              `json:"userInterfaceType"`
	Version                            int                                 `json:"version"`
	WindowHeight                       interface{}                         `json:"windowHeight"`
	WindowWidth                        interface{}                         `json:"windowWidth"`
	YearsToKeep                        int                                 `json:"yearsToKeep"`
}

type WalleeGroup struct {
	BeginDate        time.Time   `json:"beginDate"`
	CustomerId       interface{} `json:"customerId"`
	EndDate          time.Time   `json:"endDate"`
	Id               int         `json:"id"`
	LinkedSpaceId    int         `json:"linkedSpaceId"`
	PlannedPurgeDate time.Time   `json:"plannedPurgeDate"`
	State            string      `json:"state"`
	Version          int         `json:"version"`
}

type WalleePaymentConnectorConfiguration struct {
	ApplicableForTransactionProcessing bool                             `json:"applicableForTransactionProcessing"`
	Conditions                         []interface{}                    `json:"conditions"`
	Connector                          int64                            `json:"connector"`
	EnabledSalesChannels               []WalleeEnabledSalesChannels     `json:"enabledSalesChannels"`
	EnabledSpaceViews                  []interface{}                    `json:"enabledSpaceViews"`
	Id                                 int                              `json:"id"`
	ImagePath                          string                           `json:"imagePath"`
	LinkedSpaceId                      int                              `json:"linkedSpaceId"`
	Name                               string                           `json:"name"`
	PaymentMethodConfiguration         WalleePaymentMethodConfiguration `json:"paymentMethodConfiguration"`
	PlannedPurgeDate                   interface{}                      `json:"plannedPurgeDate"`
	Priority                           int                              `json:"priority"`
	ProcessorConfiguration             WalleeProcessorConfiguration     `json:"processorConfiguration"`
	State                              string                           `json:"state"`
	Version                            int                              `json:"version"`
}

type WalleeEnabledSalesChannels struct {
	Description WalleeMultilangProperty `json:"description"`
	Icon        string                  `json:"icon"`
	Id          int64                   `json:"id"`
	Name        WalleeMultilangProperty `json:"name"`
	SortOrder   int                     `json:"sortOrder"`
}

type WalleeMultilangProperty struct {
	DeDE string `json:"de-DE"`
	EnUS string `json:"en-US"`
	FrFR string `json:"fr-FR"`
	ItIT string `json:"it-IT"`
}

type WalleePaymentMethodConfiguration struct {
	DataCollectionType  string                  `json:"dataCollectionType"`
	Description         struct{}                `json:"description"`
	Id                  int                     `json:"id"`
	ImageResourcePath   interface{}             `json:"imageResourcePath"`
	LinkedSpaceId       int                     `json:"linkedSpaceId"`
	Name                string                  `json:"name"`
	OneClickPaymentMode string                  `json:"oneClickPaymentMode"`
	PaymentMethod       int64                   `json:"paymentMethod"`
	PlannedPurgeDate    interface{}             `json:"plannedPurgeDate"`
	ResolvedDescription WalleeMultilangProperty `json:"resolvedDescription"`
	ResolvedImageUrl    string                  `json:"resolvedImageUrl"`
	ResolvedTitle       WalleeMultilangProperty `json:"resolvedTitle"`
	SortOrder           int                     `json:"sortOrder"`
	SpaceId             int                     `json:"spaceId"`
	State               string                  `json:"state"`
	Title               struct{}                `json:"title"`
	Version             int                     `json:"version"`
}

type WalleeProcessorConfiguration struct {
	ApplicationManaged bool        `json:"applicationManaged"`
	ContractId         interface{} `json:"contractId"`
	Id                 int         `json:"id"`
	LinkedSpaceId      int         `json:"linkedSpaceId"`
	Name               string      `json:"name"`
	PlannedPurgeDate   interface{} `json:"plannedPurgeDate"`
	Processor          int64       `json:"processor"`
	State              string      `json:"state"`
	Version            int         `json:"version"`
}

type WalleeTerminal struct {
	ConfigurationVersion WalleeConfigurationVersion `json:"configurationVersion"`
	DefaultCurrency      string                     `json:"defaultCurrency"`
	DeviceName           interface{}                `json:"deviceName"`
	DeviceSerialNumber   string                     `json:"deviceSerialNumber"`
	ExternalId           string                     `json:"externalId"`
	Id                   int                        `json:"id"`
	Identifier           string                     `json:"identifier"`
	LinkedSpaceId        int                        `json:"linkedSpaceId"`
	LocationVersion      WalleeLocationVersion      `json:"locationVersion"`
	Name                 string                     `json:"name"`
	PlannedPurgeDate     interface{}                `json:"plannedPurgeDate"`
	State                string                     `json:"state"`
	Type                 WalleeType                 `json:"type"`
	Version              int                        `json:"version"`
}

type WalleeConfigurationVersion struct {
	Configuration             WalleeConfiguration `json:"configuration"`
	ConnectorConfigurations   []int               `json:"connectorConfigurations"`
	CreatedBy                 int                 `json:"createdBy"`
	CreatedOn                 time.Time           `json:"createdOn"`
	DefaultCurrency           interface{}         `json:"defaultCurrency"`
	Id                        int                 `json:"id"`
	LinkedSpaceId             int                 `json:"linkedSpaceId"`
	MaintenanceWindowDuration string              `json:"maintenanceWindowDuration"`
	MaintenanceWindowStart    string              `json:"maintenanceWindowStart"`
	PlannedPurgeDate          interface{}         `json:"plannedPurgeDate"`
	State                     string              `json:"state"`
	TimeZone                  string              `json:"timeZone"`
	Version                   int                 `json:"version"`
	VersionAppliedImmediately bool                `json:"versionAppliedImmediately"`
}

type WalleeConfiguration struct {
	Id               int         `json:"id"`
	LinkedSpaceId    int         `json:"linkedSpaceId"`
	Name             string      `json:"name"`
	PlannedPurgeDate interface{} `json:"plannedPurgeDate"`
	State            string      `json:"state"`
	Type             WalleeType  `json:"type"`
	Version          int         `json:"version"`
}

type WalleeType struct {
	Description WalleeMultilangProperty `json:"description"`
	Id          int64                   `json:"id"`
	Name        WalleeMultilangProperty `json:"name"`
}

type WalleeLocationVersion struct {
	Address                   WalleeAddress  `json:"address"`
	ContactAddress            interface{}    `json:"contactAddress"`
	CreatedBy                 int            `json:"createdBy"`
	CreatedOn                 time.Time      `json:"createdOn"`
	Id                        int            `json:"id"`
	LinkedSpaceId             int            `json:"linkedSpaceId"`
	Location                  WalleeLocation `json:"location"`
	PlannedPurgeDate          interface{}    `json:"plannedPurgeDate"`
	State                     string         `json:"state"`
	Version                   int            `json:"version"`
	VersionAppliedImmediately bool           `json:"versionAppliedImmediately"`
}

type WalleeAddress struct {
	City              string      `json:"city"`
	Country           string      `json:"country"`
	DependentLocality string      `json:"dependentLocality"`
	EmailAddress      string      `json:"emailAddress"`
	FamilyName        string      `json:"familyName"`
	GivenName         string      `json:"givenName"`
	MobilePhoneNumber string      `json:"mobilePhoneNumber"`
	OrganizationName  string      `json:"organizationName"`
	PhoneNumber       string      `json:"phoneNumber"`
	PostalState       interface{} `json:"postalState"`
	Postcode          string      `json:"postcode"`
	PostCode          string      `json:"postCode"`
	Salutation        string      `json:"salutation"`
	SortingCode       string      `json:"sortingCode"`
	Street            string      `json:"street"`
}

type WalleeLocation struct {
	ExternalId       string      `json:"externalId"`
	Id               int         `json:"id"`
	LinkedSpaceId    int         `json:"linkedSpaceId"`
	Name             string      `json:"name"`
	PlannedPurgeDate interface{} `json:"plannedPurgeDate"`
	State            string      `json:"state"`
	Version          int         `json:"version"`
}
