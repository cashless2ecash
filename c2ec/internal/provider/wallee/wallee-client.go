// This file is part of taler-cashless2ecash.
// Copyright (C) 2024 Joel Häberli
//
// taler-cashless2ecash is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as published
// by the Free Software Foundation, either version 3 of the License,
// or (at your option) any later version.
//
// taler-cashless2ecash is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// SPDX-License-Identifier: AGPL3.0-or-later

package internal_provider_wallee

import (
	"bytes"
	internal_utils "c2ec/internal/utils"
	"c2ec/pkg/config"
	"c2ec/pkg/db"
	"c2ec/pkg/provider"
	"crypto/hmac"
	"crypto/sha512"
	"encoding/base64"
	"errors"
	"fmt"
	"io"
	"regexp"
	"strconv"
	"strings"
	"time"
	"unicode/utf8"
)

const WALLEE_AUTH_HEADER_VERSION = "x-mac-version"
const WALLEE_AUTH_HEADER_USERID = "x-mac-userid"
const WALLEE_AUTH_HEADER_TIMESTAMP = "x-mac-timestamp"
const WALLEE_AUTH_HEADER_MAC = "x-mac-value"

const WALLEE_READ_TRANSACTION_API = "/api/transaction/read"
const WALLEE_SEARCH_TRANSACTION_API = "/api/transaction/search"
const WALLEE_CREATE_REFUND_API = "/api/refund/refund"

const WALLEE_API_SPACEID_PARAM_NAME = "spaceId"

type WalleeCredentials struct {
	SpaceId            int    `json:"spaceId"`
	UserId             int    `json:"userId"`
	ApplicationUserKey string `json:"application-user-key"`
}

type WalleeClient struct {
	provider.ProviderClient

	name        string
	baseUrl     string
	credentials *WalleeCredentials
}

func (wt *WalleeTransaction) AllowWithdrawal() bool {

	return strings.EqualFold(string(wt.State), string(StateFulfill))
}

func (wt *WalleeTransaction) AbortWithdrawal() bool {
	// guaranteed abortion is given when the state of
	// the transaction is a final state but not the
	// success case (which is FULFILL)
	return strings.EqualFold(string(wt.State), string(StateFailed)) ||
		strings.EqualFold(string(wt.State), string(StateVoided)) ||
		strings.EqualFold(string(wt.State), string(StateDecline))
}

func (wt *WalleeTransaction) Confirm(w *db.Withdrawal) error {

	if wt.MerchantReference != *w.ProviderTransactionId {

		return errors.New("the merchant reference does not match the withdrawal")
	}

	amountFloatFrmt := strconv.FormatFloat(wt.CompletedAmount, 'f', config.CONFIG.Server.CurrencyFractionDigits, 64)
	internal_utils.LogInfo("wallee-client", fmt.Sprintf("converted %f (float) to %s (string)", wt.CompletedAmount, amountFloatFrmt))
	completedAmountStr := fmt.Sprintf("%s:%s", config.CONFIG.Server.Currency, amountFloatFrmt)
	completedAmount, err := internal_utils.ParseAmount(completedAmountStr, config.CONFIG.Server.CurrencyFractionDigits)
	if err != nil {
		internal_utils.LogError("wallee-client", err)
		return err
	}

	withdrawAmount, err := internal_utils.ToAmount(w.Amount)
	if err != nil {
		return err
	}
	withdrawFees, err := internal_utils.ToAmount(w.TerminalFees)
	if err != nil {
		return err
	}
	if completedAmountMinusFees, err := completedAmount.Sub(*withdrawFees, config.CONFIG.Server.CurrencyFractionDigits); err == nil {
		if smaller, err := completedAmountMinusFees.IsSmallerThan(*withdrawAmount); smaller || err != nil {

			if err != nil {
				return err
			}

			return fmt.Errorf("the confirmed amount (%s) minus the fees (%s) was smaller than the withdraw amount (%s)",
				completedAmountStr,
				withdrawFees.String(config.CONFIG.Server.CurrencyFractionDigits),
				withdrawAmount.String(config.CONFIG.Server.CurrencyFractionDigits),
			)
		}
	}

	return nil
}

func (wt *WalleeTransaction) Bytes() []byte {

	reader, err := internal_utils.NewJsonCodec[WalleeTransaction]().Encode(wt)
	if err != nil {
		internal_utils.LogError("wallee-client", err)
		return make([]byte, 0)
	}
	bytes, err := io.ReadAll(reader)
	if err != nil {
		internal_utils.LogError("wallee-client", err)
		return make([]byte, 0)
	}
	return bytes
}

func (w *WalleeClient) SetupClient(p *db.Provider) error {

	cfg, err := config.ConfigForProvider(p.Name)
	if err != nil {
		return err
	}

	creds, err := parseCredentials(p.BackendCredentials, cfg)
	if err != nil {
		return err
	}

	w.name = p.Name
	w.baseUrl = p.BackendBaseURL
	w.credentials = creds

	provider.PROVIDER_CLIENTS[w.name] = w

	internal_utils.LogInfo("wallee-client", fmt.Sprintf("Wallee client is setup (user=%d, spaceId=%d, backend=%s)", w.credentials.UserId, w.credentials.SpaceId, w.baseUrl))

	return nil
}

func (w *WalleeClient) GetTransaction(transactionId string) (provider.ProviderTransaction, error) {

	if transactionId == "" {
		return nil, errors.New("transaction id must be specified but was blank")
	}

	call := fmt.Sprintf("%s%s", w.baseUrl, WALLEE_SEARCH_TRANSACTION_API)
	queryParams := map[string]string{
		WALLEE_API_SPACEID_PARAM_NAME: strconv.Itoa(w.credentials.SpaceId),
	}
	url := internal_utils.FormatUrl(call, map[string]string{}, queryParams)

	hdrs, err := prepareWalleeHeaders(url, internal_utils.HTTP_POST, w.credentials.UserId, w.credentials.ApplicationUserKey)
	if err != nil {
		return nil, err
	}

	filter := WalleeSearchFilter{
		FieldName: "merchantReference",
		Operator:  EQUALS,
		Type:      LEAF,
		Value:     transactionId,
	}

	req := WalleeTransactionSearchRequest{
		Filter:           filter,
		Language:         "en",
		NumberOfEntities: 1,
		StartingEntity:   0,
	}

	t, status, err := internal_utils.HttpPost(
		url,
		hdrs,
		&req,
		internal_utils.NewJsonCodec[WalleeTransactionSearchRequest](),
		internal_utils.NewJsonCodec[[]*WalleeTransaction](),
	)
	if err != nil {
		return nil, err
	}
	if status != internal_utils.HTTP_OK {
		return nil, errors.New("no result")
	}
	if t == nil {
		return nil, errors.New("no such transaction for merchantReference=" + transactionId)
	}
	derefRes := *t
	if len(derefRes) < 1 {
		return nil, errors.New("no such transaction for merchantReference=" + transactionId)
	}
	return derefRes[0], nil
}

func (sc *WalleeClient) FormatPayto(w *db.Withdrawal) string {

	if w == nil || w.ProviderTransactionId == nil {
		internal_utils.LogError("wallee-client", errors.New("withdrawal or provider transaction identifier was nil"))
		return ""
	}
	return fmt.Sprintf("payto://wallee-transaction/%s", *w.ProviderTransactionId)
}

func (w *WalleeClient) Refund(transactionId string) error {

	internal_utils.LogInfo("wallee-client", "trying to refund provider transaction "+transactionId)
	call := fmt.Sprintf("%s%s", w.baseUrl, WALLEE_CREATE_REFUND_API)
	queryParams := map[string]string{
		WALLEE_API_SPACEID_PARAM_NAME: strconv.Itoa(w.credentials.SpaceId),
	}
	url := internal_utils.FormatUrl(call, map[string]string{}, queryParams)
	internal_utils.LogInfo("wallee-client", "refund url "+url)

	hdrs, err := prepareWalleeHeaders(url, internal_utils.HTTP_POST, w.credentials.UserId, w.credentials.ApplicationUserKey)
	if err != nil {
		internal_utils.LogError("wallee-client", err)
		return err
	}

	withdrawal, err := db.DB.GetWithdrawalByProviderTransactionId(transactionId)
	if err != nil {
		err = errors.New("error unable to find withdrawal belonging to transactionId=" + transactionId)
		internal_utils.LogError("wallee-client", err)
		return err
	}
	if withdrawal == nil {
		err = errors.New("withdrawal is nil unable to find withdrawal belonging to transactionId=" + transactionId)
		internal_utils.LogError("wallee-client", err)
		return err
	}

	decodedWalleeTransaction, err := internal_utils.NewJsonCodec[WalleeTransaction]().Decode(bytes.NewBuffer(withdrawal.CompletionProof))
	if err != nil {
		internal_utils.LogError("wallee-client", err)
		return err
	}

	refundAmount, err := internal_utils.ToAmount(withdrawal.Amount)
	if err != nil {
		internal_utils.LogError("wallee-client", err)
		return err
	}

	refundableAmount := refundAmount.String(config.CONFIG.Server.CurrencyFractionDigits)
	refundableAmount, _ = strings.CutPrefix(refundableAmount, config.CONFIG.Server.Currency+":")
	internal_utils.LogInfo("wallee-client", fmt.Sprintf("stripped currency from amount %s", refundableAmount))
	refund := &WalleeRefund{
		Amount:            refundableAmount,
		ExternalID:        internal_utils.TalerBinaryEncode(withdrawal.Wopid),
		MerchantReference: decodedWalleeTransaction.MerchantReference,
		Transaction: WalleeRefundTransaction{
			Id: int64(decodedWalleeTransaction.Id),
		},
		Type: "MERCHANT_INITIATED_ONLINE", // this type will refund the transaction using the responsible processor (e.g. VISA, MasterCard, TWINT, etc.)
	}

	_, status, err := internal_utils.HttpPost[WalleeRefund, any](
		url,
		hdrs,
		refund,
		internal_utils.NewJsonCodec[WalleeRefund](),
		nil,
	)
	if err != nil {
		internal_utils.LogError("wallee-client", err)
		return err
	}
	if status != internal_utils.HTTP_OK {
		return errors.New("failed refunding the transaction at the wallee-backend. statuscode=" + strconv.Itoa(status))
	}

	return nil
}

func prepareWalleeHeaders(
	url string,
	method string,
	userId int,
	applicationUserKey string,
) (map[string]string, error) {

	timestamp := time.Time.Unix(time.Now())

	base64Mac, err := calculateWalleeAuthToken(
		userId,
		timestamp,
		method,
		url,
		applicationUserKey,
	)
	if err != nil {
		return nil, err
	}

	headers := map[string]string{
		WALLEE_AUTH_HEADER_VERSION:   "1",
		WALLEE_AUTH_HEADER_USERID:    strconv.Itoa(userId),
		WALLEE_AUTH_HEADER_TIMESTAMP: strconv.Itoa(int(timestamp)),
		WALLEE_AUTH_HEADER_MAC:       base64Mac,
	}

	return headers, nil
}

func parseCredentials(raw string, cfg *config.C2ECProviderConfig) (*WalleeCredentials, error) {

	credsJson := make([]byte, len(raw))
	_, err := base64.StdEncoding.Decode(credsJson, []byte(raw))
	if err != nil {
		return nil, err
	}

	creds, err := internal_utils.NewJsonCodec[WalleeCredentials]().Decode(bytes.NewBuffer(credsJson))
	if err != nil {
		return nil, err
	}

	if !internal_utils.ValidPassword(cfg.Key, creds.ApplicationUserKey) {
		return nil, errors.New("invalid application user key in wallee client configuration")
	}

	// correct application user key.
	creds.ApplicationUserKey = cfg.Key
	return creds, nil
}

// This function calculates the authentication token according
// to the documentation of wallee:
// https://app-wallee.com/en-us/doc/api/web-service#_authentication
// the function returns the token in Base64 format.
func calculateWalleeAuthToken(
	userId int,
	unixTimestamp int64,
	httpMethod string,
	pathWithParams string,
	userKeyBase64 string,
) (string, error) {

	// Put together the correct formatted string
	// Version | UserId | Timestamp | Method | Path
	authMsgStr := fmt.Sprintf("%d|%d|%d|%s|%s",
		1, // version is static
		userId,
		unixTimestamp,
		httpMethod,
		cutSchemeAndHost(pathWithParams),
	)

	authMsg := make([]byte, 0)
	if valid := utf8.ValidString(authMsgStr); !valid {

		// encode the string using utf8
		for _, r := range authMsgStr {
			rbytes := make([]byte, 4)
			utf8.EncodeRune(rbytes, r)
			authMsg = append(authMsg, rbytes...)
		}
	} else {
		authMsg = bytes.NewBufferString(authMsgStr).Bytes()
	}

	internal_utils.LogInfo("wallee-client", fmt.Sprintf("authMsg (utf-8 encoded): %s", string(authMsg)))

	key := make([]byte, 32)
	_, err := base64.StdEncoding.Decode(key, []byte(userKeyBase64))
	if err != nil {
		internal_utils.LogError("wallee-client", err)
		return "", err
	}

	if len(key) != 32 {
		return "", errors.New("malformed secret")
	}

	macer := hmac.New(sha512.New, key)
	_, err = macer.Write(authMsg)
	if err != nil {
		internal_utils.LogError("wallee-client", err)
		return "", err
	}
	mac := macer.Sum(make([]byte, 0))

	return base64.StdEncoding.EncodeToString(mac), nil
}

func cutSchemeAndHost(url string) string {

	reg := regexp.MustCompile(`https?:\/\/[\w-\.]{1,}`)
	return reg.ReplaceAllString(url, "")
}
