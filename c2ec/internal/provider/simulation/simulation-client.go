// This file is part of taler-cashless2ecash.
// Copyright (C) 2024 Joel Häberli
//
// taler-cashless2ecash is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as published
// by the Free Software Foundation, either version 3 of the License,
// or (at your option) any later version.
//
// taler-cashless2ecash is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// SPDX-License-Identifier: AGPL3.0-or-later

package internal_provider_simulation

import (
	"bytes"
	internal_utils "c2ec/internal/utils"
	"c2ec/pkg/db"
	"c2ec/pkg/provider"
	"fmt"
	"time"
)

type SimulationTransaction struct {
	provider.ProviderTransaction

	allow bool
}

type SimulationClient struct {
	provider.ProviderClient

	// toggle this to simulate failed transactions.
	AllowNextWithdrawal bool

	// simulates the provider client fetching confirmation at the providers backend.
	providerBackendConfirmationDelayMs int
}

func (st *SimulationTransaction) AllowWithdrawal() bool {

	return st.allow
}

func (st *SimulationTransaction) AbortWithdrawal() bool {

	return false
}

func (st *SimulationTransaction) Confirm(w *db.Withdrawal) error {

	return nil
}

func (st *SimulationTransaction) Bytes() []byte {

	return bytes.NewBufferString("this is a simulated transaction and therefore has no content.").Bytes()
}

func (sc *SimulationClient) FormatPayto(w *db.Withdrawal) string {

	return fmt.Sprintf("payto://void/%s", *w.ProviderTransactionId)
}

func (sc *SimulationClient) SetupClient(p *db.Provider) error {

	internal_utils.LogInfo("simulation-client", "setting up simulation client. probably not what you want in production")
	fmt.Println("setting up simulation client. probably not what you want in production")

	sc.AllowNextWithdrawal = true
	sc.providerBackendConfirmationDelayMs = 1000 // one second, might be a lot but for testing this is fine.
	provider.PROVIDER_CLIENTS["Simulation"] = sc
	return nil
}

func (sc *SimulationClient) GetTransaction(transactionId string) (provider.ProviderTransaction, error) {

	internal_utils.LogInfo("simulation-client", "getting transaction from simulation provider")
	time.Sleep(time.Duration(sc.providerBackendConfirmationDelayMs) * time.Millisecond)
	st := new(SimulationTransaction)
	st.allow = sc.AllowNextWithdrawal
	return st, nil
}

func (*SimulationClient) Refund(transactionId string) error {

	internal_utils.LogInfo("simulation-client", "refund triggered for simulation provider with transaction id: "+transactionId)
	return nil
}
