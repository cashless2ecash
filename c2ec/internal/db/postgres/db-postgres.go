// This file is part of taler-cashless2ecash.
// Copyright (C) 2024 Joel Häberli
//
// taler-cashless2ecash is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as published
// by the Free Software Foundation, either version 3 of the License,
// or (at your option) any later version.
//
// taler-cashless2ecash is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// SPDX-License-Identifier: AGPL3.0-or-later

package internal_postgres

import (
	internal_db "c2ec/internal/db"
	internal_utils "c2ec/internal/utils"
	"c2ec/pkg/config"
	"c2ec/pkg/db"
	public_db "c2ec/pkg/db"
	"context"
	"errors"
	"fmt"
	"math"
	"os"
	"strconv"
	"strings"
	"time"

	"github.com/jackc/pgx/v5"
	"github.com/jackc/pgx/v5/pgconn"
	"github.com/jackc/pgx/v5/pgxpool"
	"github.com/jackc/pgxlisten"
)

const PS_INSERT_WITHDRAWAL = "INSERT INTO " + internal_db.WITHDRAWAL_TABLE_NAME + " (" +
	internal_db.WITHDRAWAL_FIELD_NAME_WOPID + ", " + internal_db.WITHDRAWAL_FIELD_NAME_RUID + ", " +
	internal_db.WITHDRAWAL_FIELD_NAME_SUGGESTED_AMOUNT + ", " + internal_db.WITHDRAWAL_FIELD_NAME_AMOUNT + ", " +
	internal_db.WITHDRAWAL_FIELD_NAME_TRANSACTION_ID + ", " + internal_db.WITHDRAWAL_FIELD_NAME_FEES + ", " +
	internal_db.WITHDRAWAL_FIELD_NAME_TS + ", " + internal_db.WITHDRAWAL_FIELD_NAME_TERMINAL_ID +
	") VALUES ($1,$2,($3,$4,$5),($6,$7,$8),$9,($10,$11,$12),$13,$14)"

const PS_REGISTER_WITHDRAWAL_PARAMS = "UPDATE " + internal_db.WITHDRAWAL_TABLE_NAME + " SET (" +
	internal_db.WITHDRAWAL_FIELD_NAME_RESPUBKEY + "," +
	internal_db.WITHDRAWAL_FIELD_NAME_STATUS + "," +
	internal_db.WITHDRAWAL_FIELD_NAME_TS + ")" +
	" = ($1,$2,$3)" +
	" WHERE " + internal_db.WITHDRAWAL_FIELD_NAME_WOPID + "=$4"

const PS_GET_UNCONFIRMED_WITHDRAWALS = "SELECT * FROM " + internal_db.WITHDRAWAL_TABLE_NAME +
	" WHERE " + internal_db.WITHDRAWAL_FIELD_NAME_STATUS + " = '" + string(internal_utils.SELECTED) + "'"

const PS_PAYMENT_NOTIFICATION = "UPDATE " + internal_db.WITHDRAWAL_TABLE_NAME + " SET (" +
	internal_db.WITHDRAWAL_FIELD_NAME_FEES + "," + internal_db.WITHDRAWAL_FIELD_NAME_TRANSACTION_ID + "," +
	internal_db.WITHDRAWAL_FIELD_NAME_TERMINAL_ID + ")" +
	" = (($1,$2,$3),$4,$5)" +
	" WHERE " + internal_db.WITHDRAWAL_FIELD_NAME_WOPID + "=$6"

const PS_FINALISE_PAYMENT = "UPDATE " + internal_db.WITHDRAWAL_TABLE_NAME + " SET (" +
	internal_db.WITHDRAWAL_FIELD_NAME_STATUS + "," +
	internal_db.WITHDRAWAL_FIELD_NAME_COMPLETION_PROOF + "," +
	internal_db.WITHDRAWAL_FIELD_NAME_CONFIRMED_ROW_ID + ")" +
	" = ($1, $2, (SELECT ((SELECT MAX(" + internal_db.WITHDRAWAL_FIELD_NAME_CONFIRMED_ROW_ID + ") FROM " + internal_db.WITHDRAWAL_TABLE_NAME + " WHERE " + internal_db.WITHDRAWAL_FIELD_NAME_STATUS + "='" + string(internal_utils.CONFIRMED) + "')+1)))" +
	" WHERE " + internal_db.WITHDRAWAL_FIELD_NAME_ID + "=$3"

const PS_SET_LAST_RETRY = "UPDATE " + internal_db.WITHDRAWAL_TABLE_NAME +
	" SET " + internal_db.WITHDRAWAL_FIELD_NAME_LAST_RETRY + "=$1" +
	" WHERE " + internal_db.WITHDRAWAL_FIELD_NAME_ID + "=$2"

const PS_SET_RETRY_COUNTER = "UPDATE " + internal_db.WITHDRAWAL_TABLE_NAME +
	" SET " + internal_db.WITHDRAWAL_FIELD_NAME_RETRY_COUNTER + "=$1" +
	" WHERE " + internal_db.WITHDRAWAL_FIELD_NAME_ID + "=$2"

const PS_GET_WITHDRAWAL_BY_RUID = "SELECT * FROM " + internal_db.WITHDRAWAL_TABLE_NAME +
	" WHERE " + internal_db.WITHDRAWAL_FIELD_NAME_RUID + "=$1"

const PS_GET_WITHDRAWAL_BY_ID = "SELECT * FROM " + internal_db.WITHDRAWAL_TABLE_NAME +
	" WHERE " + internal_db.WITHDRAWAL_FIELD_NAME_ID + "=$1"

const PS_GET_WITHDRAWAL_BY_WOPID = "SELECT * FROM " + internal_db.WITHDRAWAL_TABLE_NAME +
	" WHERE " + internal_db.WITHDRAWAL_FIELD_NAME_WOPID + "=$1"

const PS_GET_WITHDRAWAL_BY_PTID = "SELECT * FROM " + internal_db.WITHDRAWAL_TABLE_NAME +
	" WHERE " + internal_db.WITHDRAWAL_FIELD_NAME_TRANSACTION_ID + "=$1"

const PS_GET_PROVIDER_BY_TERMINAL = "SELECT * FROM " + internal_db.PROVIDER_TABLE_NAME +
	" WHERE " + internal_db.PROVIDER_FIELD_NAME_ID +
	" = (SELECT " + internal_db.TERMINAL_FIELD_NAME_PROVIDER_ID + " FROM " + internal_db.TERMINAL_TABLE_NAME +
	" WHERE " + internal_db.TERMINAL_FIELD_NAME_ID + "=$1)"

const PS_GET_PROVIDER_BY_NAME = "SELECT * FROM " + internal_db.PROVIDER_TABLE_NAME +
	" WHERE " + internal_db.PROVIDER_FIELD_NAME_NAME + "=$1"

const PS_GET_PROVIDER_BY_PAYTO_TARGET_TYPE = "SELECT * FROM " + internal_db.PROVIDER_TABLE_NAME +
	" WHERE " + internal_db.PROVIDER_FIELD_NAME_PAYTO_TARGET_TYPE + "=$1"

const PS_GET_TERMINAL_BY_ID = "SELECT * FROM " + internal_db.TERMINAL_TABLE_NAME +
	" WHERE " + internal_db.TERMINAL_FIELD_NAME_ID + "=$1"

const PS_GET_TRANSFER_BY_ID = "SELECT * FROM " + internal_db.TRANSFER_TABLE_NAME +
	" WHERE " + internal_db.TRANSFER_FIELD_NAME_ID + "=$1"

const PS_GET_TRANSFER_BY_CREDIT_ACCOUNT = "SELECT * FROM " + internal_db.TRANSFER_TABLE_NAME +
	" WHERE " + internal_db.TRANSFER_FIELD_NAME_CREDIT_ACCOUNT + "=$1"

const PS_ADD_TRANSFER = "INSERT INTO " + internal_db.TRANSFER_TABLE_NAME +
	" (" + internal_db.TRANSFER_FIELD_NAME_ID + ", " + internal_db.TRANSFER_FIELD_NAME_AMOUNT + ", " +
	internal_db.TRANSFER_FIELD_NAME_EXCHANGE_BASE_URL + ", " + internal_db.TRANSFER_FIELD_NAME_WTID + ", " +
	internal_db.TRANSFER_FIELD_NAME_CREDIT_ACCOUNT + ", " + internal_db.TRANSFER_FIELD_NAME_TS +
	") VALUES ($1,$2,$3,$4,$5,$6)"

const PS_UPDATE_TRANSFER = "UPDATE " + internal_db.TRANSFER_TABLE_NAME + " SET (" +
	internal_db.TRANSFER_FIELD_NAME_TS + ", " + internal_db.TRANSFER_FIELD_NAME_STATUS + ", " +
	internal_db.TRANSFER_FIELD_NAME_RETRIES + ", " + internal_db.TRANSFER_FIELD_NAME_TRANSFERRED_ROW_ID + ") = ($1,$2,$3," +
	"(SELECT ((SELECT MAX(" + internal_db.TRANSFER_FIELD_NAME_TRANSFERRED_ROW_ID + ") FROM " + internal_db.TRANSFER_TABLE_NAME + " WHERE " + internal_db.TRANSFER_FIELD_NAME_STATUS + "=0)+1))" +
	") WHERE " + internal_db.TRANSFER_FIELD_NAME_ID + "=$4"

const PS_CONFIRMED_TRANSACTIONS_ASC = "SELECT * FROM c2ec.withdrawal WHERE confirmed_row_id > $1 ORDER BY confirmed_row_id ASC LIMIT $2"

const PS_CONFIRMED_TRANSACTIONS_DESC = "SELECT * FROM c2ec.withdrawal WHERE confirmed_row_id < $1 ORDER BY confirmed_row_id DESC LIMIT $2"

const PS_CONFIRMED_TRANSACTIONS_ASC_MAX = "SELECT * FROM c2ec.withdrawal WHERE confirmed_row_id > $1 ORDER BY confirmed_row_id ASC LIMIT $2"

const PS_CONFIRMED_TRANSACTIONS_DESC_MAX = "SELECT * FROM c2ec.withdrawal WHERE confirmed_row_id < (SELECT MAX(confirmed_row_id) FROM c2ec.withdrawal) ORDER BY confirmed_row_id DESC LIMIT $1"

const PS_GET_TRANSFERS_ASC = "SELECT * FROM c2ec.transfer WHERE transferred_row_id > $1 ORDER BY transferred_row_id ASC LIMIT $2"

const PS_GET_TRANSFERS_DESC = "SELECT * FROM c2ec.transfer WHERE transferred_row_id < $1 ORDER BY transferred_row_id DESC LIMIT $2"

const PS_GET_TRANSFERS_ASC_MAX = "SELECT * FROM c2ec.transfer WHERE transferred_row_id > $1 ORDER BY transferred_row_id ASC LIMIT $2"

const PS_GET_TRANSFERS_DESC_MAX = "SELECT * FROM c2ec.transfer WHERE transferred_row_id < (SELECT MAX(transferred_row_id) FROM c2ec.transfer) ORDER BY transferred_row_id DESC LIMIT $1"

const PS_GET_TRANSFERS_BY_STATUS = "SELECT * FROM " + internal_db.TRANSFER_TABLE_NAME +
	" WHERE " + internal_db.TRANSFER_FIELD_NAME_STATUS + "=$1"

// Postgres implementation of the C2ECDatabase
type C2ECPostgres struct {
	public_db.C2ECDatabase

	ctx  context.Context
	pool *pgxpool.Pool
}

func PostgresConnectionString(cfg *config.C2ECDatabseConfig) string {

	if cfg.ConnectionString != "" {
		return cfg.ConnectionString
	}

	pgHost := os.Getenv("PGHOST")
	if pgHost != "" {
		internal_utils.LogInfo("postgres", "pghost was set")
	} else {
		pgHost = cfg.Host
	}

	pgPort := os.Getenv("PGPORT")
	if pgPort != "" {
		internal_utils.LogInfo("postgres", "pgport was set")
	} else {
		pgPort = strconv.Itoa(cfg.Port)
	}

	pgUsername := os.Getenv("PGUSER")
	if pgUsername != "" {
		internal_utils.LogInfo("postgres", "pghost was set")
	} else {
		pgUsername = cfg.Username
	}

	pgPassword := os.Getenv("PGPASSWORD")
	if pgPassword != "" {
		internal_utils.LogInfo("postgres", "pghost was set")
	} else {
		pgPassword = cfg.Password
	}

	pgDb := os.Getenv("PGDATABASE")
	if pgDb != "" {
		internal_utils.LogInfo("postgres", "pghost was set")
	} else {
		pgDb = cfg.Database
	}

	return fmt.Sprintf(
		"postgres://%s:%s@%s:%s/%s",
		pgUsername,
		pgPassword,
		pgHost,
		pgPort,
		pgDb,
	)
}

func NewC2ECPostgres(cfg *config.C2ECDatabseConfig) (*C2ECPostgres, error) {

	ctx := context.Background()
	db := new(C2ECPostgres)

	connectionString := PostgresConnectionString(cfg)

	dbConnCfg, err := pgxpool.ParseConfig(connectionString)
	if err != nil {
		panic(err.Error())
	}
	dbConnCfg.AfterConnect = db.registerCustomTypesHook
	db.pool, err = pgxpool.NewWithConfig(context.Background(), dbConnCfg)
	if err != nil {
		panic(err.Error())
	}

	db.ctx = ctx

	return db, nil
}

func (db *C2ECPostgres) registerCustomTypesHook(ctx context.Context, conn *pgx.Conn) error {

	t, err := conn.LoadType(ctx, "c2ec.taler_amount_currency")
	if err != nil {
		return err
	}

	conn.TypeMap().RegisterType(t)
	return nil
}

func (db *C2ECPostgres) SetupWithdrawal(
	wopid []byte,
	suggestedAmount internal_utils.Amount,
	amount internal_utils.Amount,
	terminalId int,
	providerTransactionId string,
	terminalFees internal_utils.Amount,
	requestUid string,
) error {

	ts := time.Now()
	res, err := db.pool.Exec(
		db.ctx,
		PS_INSERT_WITHDRAWAL,
		wopid,
		requestUid,
		suggestedAmount.Value,
		suggestedAmount.Fraction,
		suggestedAmount.Currency,
		amount.Value,
		amount.Fraction,
		amount.Currency,
		providerTransactionId,
		terminalFees.Value,
		terminalFees.Fraction,
		terminalFees.Currency,
		ts.Unix(),
		terminalId,
	)
	if err != nil {
		internal_utils.LogError("postgres", err)
		return err
	}
	internal_utils.LogInfo("postgres", "query="+PS_INSERT_WITHDRAWAL)
	internal_utils.LogInfo("postgres", "setup withdrawal successfully. affected rows="+strconv.Itoa(int(res.RowsAffected())))
	return nil
}

func (db *C2ECPostgres) RegisterWithdrawalParameters(
	wopid []byte,
	resPubKey internal_utils.EddsaPublicKey,
) error {

	resPubKeyBytes, err := internal_utils.ParseEddsaPubKey(resPubKey)
	if err != nil {
		return err
	}

	ts := time.Now()
	res, err := db.pool.Exec(
		db.ctx,
		PS_REGISTER_WITHDRAWAL_PARAMS,
		resPubKeyBytes,
		internal_utils.SELECTED,
		ts.Unix(),
		wopid,
	)
	if err != nil {
		internal_utils.LogError("postgres", err)
		return err
	}
	internal_utils.LogInfo("postgres", "query="+PS_REGISTER_WITHDRAWAL_PARAMS)
	internal_utils.LogInfo("postgres", "registered withdrawal successfully. affected rows="+strconv.Itoa(int(res.RowsAffected())))
	return nil
}

func (db *C2ECPostgres) GetWithdrawalByRequestUid(requestUid string) (*db.Withdrawal, error) {

	if row, err := db.pool.Query(
		db.ctx,
		PS_GET_WITHDRAWAL_BY_RUID,
		requestUid,
	); err != nil {
		internal_utils.LogError("postgres", err)
		if row != nil {
			row.Close()
		}
		return nil, err
	} else {
		defer row.Close()
		internal_utils.LogInfo("postgres", "query="+PS_GET_WITHDRAWAL_BY_RUID)
		collected, err := pgx.CollectOneRow(row, pgx.RowToAddrOfStructByName[public_db.Withdrawal])
		if err != nil {
			if errors.Is(err, pgx.ErrNoRows) {
				return nil, nil
			}
			return nil, err
		}
		return collected, nil
	}
}

func (db *C2ECPostgres) GetWithdrawalById(withdrawalId int) (*db.Withdrawal, error) {

	if row, err := db.pool.Query(
		db.ctx,
		PS_GET_WITHDRAWAL_BY_ID,
		withdrawalId,
	); err != nil {
		internal_utils.LogError("postgres", err)
		if row != nil {
			row.Close()
		}
		return nil, err
	} else {

		defer row.Close()
		internal_utils.LogInfo("postgres", "query="+PS_GET_WITHDRAWAL_BY_ID)
		return pgx.CollectExactlyOneRow(row, pgx.RowToAddrOfStructByName[public_db.Withdrawal])
	}
}

func (db *C2ECPostgres) GetWithdrawalByWopid(wopid []byte) (*public_db.Withdrawal, error) {

	if row, err := db.pool.Query(
		db.ctx,
		PS_GET_WITHDRAWAL_BY_WOPID,
		wopid,
	); err != nil {
		internal_utils.LogError("postgres", err)
		if row != nil {
			row.Close()
		}
		return nil, err
	} else {

		defer row.Close()
		internal_utils.LogInfo("postgres", "query="+PS_GET_WITHDRAWAL_BY_WOPID)
		return pgx.CollectExactlyOneRow(row, pgx.RowToAddrOfStructByName[public_db.Withdrawal])
	}
}

func (db *C2ECPostgres) GetWithdrawalByProviderTransactionId(tid string) (*public_db.Withdrawal, error) {
	if row, err := db.pool.Query(
		db.ctx,
		PS_GET_WITHDRAWAL_BY_PTID,
		tid,
	); err != nil {
		internal_utils.LogInfo("postgres", "failed query="+PS_GET_WITHDRAWAL_BY_PTID)
		internal_utils.LogError("postgres", err)
		if row != nil {
			row.Close()
		}
		return nil, err
	} else {

		defer row.Close()
		internal_utils.LogInfo("postgres", "query="+PS_GET_WITHDRAWAL_BY_PTID)
		return pgx.CollectExactlyOneRow(row, pgx.RowToAddrOfStructByName[public_db.Withdrawal])
	}
}

func (db *C2ECPostgres) NotifyPayment(
	wopid []byte,
	providerTransactionId string,
	terminalId int,
	fees internal_utils.Amount,
) error {

	res, err := db.pool.Exec(
		db.ctx,
		PS_PAYMENT_NOTIFICATION,
		fees.Value,
		fees.Fraction,
		fees.Currency,
		providerTransactionId,
		terminalId,
		wopid,
	)
	if err != nil {
		internal_utils.LogError("postgres", err)
		return err
	}
	internal_utils.LogInfo("postgres", "query="+PS_PAYMENT_NOTIFICATION+", affected rows="+strconv.Itoa(int(res.RowsAffected())))
	return nil
}

func (db *C2ECPostgres) GetWithdrawalsForConfirmation() ([]*public_db.Withdrawal, error) {

	if row, err := db.pool.Query(
		db.ctx,
		PS_GET_UNCONFIRMED_WITHDRAWALS,
	); err != nil {
		internal_utils.LogError("postgres", err)
		if row != nil {
			row.Close()
		}
		return nil, err
	} else {

		defer row.Close()

		withdrawals, err := pgx.CollectRows(row, pgx.RowToAddrOfStructByName[public_db.Withdrawal])
		if err != nil {
			internal_utils.LogError("postgres", err)
			return nil, err
		}

		// potentially fills the logs
		// internal_utils.LogInfo("postgres", "query="+PS_GET_UNCONFIRMED_WITHDRAWALS)
		return internal_utils.RemoveNulls(withdrawals), nil
	}
}

func (db *C2ECPostgres) FinaliseWithdrawal(
	withdrawalId int,
	confirmOrAbort internal_utils.WithdrawalOperationStatus,
	completionProof []byte,
) error {

	if confirmOrAbort != internal_utils.CONFIRMED && confirmOrAbort != internal_utils.ABORTED {
		return errors.New("can only finalise payment when new status is either confirmed or aborted")
	}

	query := PS_FINALISE_PAYMENT
	if withdrawalId <= 1 {
		// tweak to intially set confirmed_row_id. Can be removed once confirmed_row_id field is obsolete
		query = "UPDATE c2ec.withdrawal SET (withdrawal_status,completion_proof,confirmed_row_id) = ($1,$2,1) WHERE withdrawal_row_id=$3"
	}

	_, err := db.pool.Exec(
		db.ctx,
		query,
		confirmOrAbort,
		completionProof,
		withdrawalId,
	)
	if err != nil {
		internal_utils.LogError("postgres", err)
		return err
	}
	internal_utils.LogInfo("postgres", "query="+query)
	return nil
}

func (db *C2ECPostgres) SetLastRetry(withdrawalId int, lastRetryTsUnix int64) error {

	_, err := db.pool.Exec(
		db.ctx,
		PS_SET_LAST_RETRY,
		lastRetryTsUnix,
		withdrawalId,
	)
	if err != nil {
		internal_utils.LogError("postgres", err)
		return err
	}
	internal_utils.LogInfo("postgres", "query="+PS_SET_LAST_RETRY)
	return nil
}

func (db *C2ECPostgres) SetRetryCounter(withdrawalId int, retryCounter int) error {

	_, err := db.pool.Exec(
		db.ctx,
		PS_SET_RETRY_COUNTER,
		retryCounter,
		withdrawalId,
	)
	if err != nil {
		internal_utils.LogError("postgres", err)
		return err
	}
	internal_utils.LogInfo("postgres", "query="+PS_SET_RETRY_COUNTER)
	return nil
}

// The query at the postgres database works as specified by the
// wire gateway api.
func (db *C2ECPostgres) GetConfirmedWithdrawals(start int, delta int, since time.Time) ([]*public_db.Withdrawal, error) {

	// +d / +s
	query := PS_CONFIRMED_TRANSACTIONS_ASC
	if delta < 0 {
		// d negatives indicates DESC ordering and backwards reading
		// -d / +s
		query = PS_CONFIRMED_TRANSACTIONS_DESC
		if start < 0 {
			// start negative indicates not explicitly given
			// since -d is the case here we try reading the latest entries
			// -d / -s
			query = PS_CONFIRMED_TRANSACTIONS_DESC_MAX
		}
	} else {
		if start < 0 {
			// +d / -s
			query = PS_CONFIRMED_TRANSACTIONS_ASC_MAX
		}
	}

	limit := int(math.Abs(float64(delta)))
	offset := start
	if offset < 0 {
		offset = 0
	}

	if start < 0 {
		start = 0
	}

	internal_utils.LogInfo("postgres", fmt.Sprintf("selected query=%s (\nparameters:\n delta=%d,\n start=%d, limit=%d,\n offset=%d,\n since=%d\n)", query, delta, start, limit, offset, since.Unix()))

	var row pgx.Rows
	var err error

	if strings.Count(query, "$") == 1 {
		row, err = db.pool.Query(
			db.ctx,
			query,
			limit,
		)
	} else {
		row, err = db.pool.Query(
			db.ctx,
			query,
			offset,
			limit,
		)
	}

	internal_utils.LogInfo("postgres", "query="+query)
	if err != nil {
		internal_utils.LogError("postgres", err)
		if row != nil {
			row.Close()
		}
		return nil, err
	} else {

		defer row.Close()

		withdrawals, err := pgx.CollectRows(row, pgx.RowToAddrOfStructByName[public_db.Withdrawal])
		if err != nil {
			internal_utils.LogError("postgres", err)
			return nil, err
		}

		return internal_utils.RemoveNulls(withdrawals), nil
	}
}

func (db *C2ECPostgres) GetProviderByTerminal(terminalId int) (*public_db.Provider, error) {

	if row, err := db.pool.Query(
		db.ctx,
		PS_GET_PROVIDER_BY_TERMINAL,
		terminalId,
	); err != nil {
		internal_utils.LogWarn("postgres", "failed query="+PS_GET_PROVIDER_BY_TERMINAL)
		internal_utils.LogError("postgres", err)
		if row != nil {
			row.Close()
		}
		return nil, err
	} else {

		defer row.Close()

		provider, err := pgx.CollectExactlyOneRow(row, pgx.RowToAddrOfStructByName[public_db.Provider])
		if err != nil {
			internal_utils.LogError("postgres", err)
			return nil, err
		}

		internal_utils.LogInfo("postgres", "query="+PS_GET_PROVIDER_BY_TERMINAL)
		return provider, nil
	}
}

func (db *C2ECPostgres) GetTerminalProviderByName(name string) (*public_db.Provider, error) {

	if row, err := db.pool.Query(
		db.ctx,
		PS_GET_PROVIDER_BY_NAME,
		name,
	); err != nil {
		internal_utils.LogWarn("postgres", "failed query="+PS_GET_PROVIDER_BY_NAME)
		internal_utils.LogError("postgres", err)
		if row != nil {
			row.Close()
		}
		return nil, err
	} else {

		defer row.Close()

		provider, err := pgx.CollectExactlyOneRow(row, pgx.RowToAddrOfStructByName[public_db.Provider])
		if err != nil {
			internal_utils.LogWarn("postgres", "failed query="+PS_GET_PROVIDER_BY_NAME)
			internal_utils.LogError("postgres", err)
			return nil, err
		}

		internal_utils.LogInfo("postgres", "query="+PS_GET_PROVIDER_BY_NAME)
		return provider, nil
	}
}

func (db *C2ECPostgres) GetTerminalProviderByPaytoTargetType(paytoTargetType string) (*public_db.Provider, error) {

	internal_utils.LogInfo("postgres", "loading provider for payto-target-type="+paytoTargetType)
	if row, err := db.pool.Query(
		db.ctx,
		PS_GET_PROVIDER_BY_PAYTO_TARGET_TYPE,
		paytoTargetType,
	); err != nil {
		internal_utils.LogWarn("postgres", "failed query="+PS_GET_PROVIDER_BY_PAYTO_TARGET_TYPE)
		internal_utils.LogError("postgres", err)
		if row != nil {
			row.Close()
		}
		return nil, err
	} else {

		defer row.Close()

		provider, err := pgx.CollectExactlyOneRow(row, pgx.RowToAddrOfStructByName[public_db.Provider])
		if err != nil {
			internal_utils.LogWarn("postgres", "failed query="+PS_GET_PROVIDER_BY_PAYTO_TARGET_TYPE)
			internal_utils.LogError("postgres", err)
			return nil, err
		}

		internal_utils.LogInfo("postgres", "query="+PS_GET_PROVIDER_BY_PAYTO_TARGET_TYPE)
		return provider, nil
	}
}

func (db *C2ECPostgres) GetTerminalById(id int) (*public_db.Terminal, error) {

	if row, err := db.pool.Query(
		db.ctx,
		PS_GET_TERMINAL_BY_ID,
		id,
	); err != nil {
		internal_utils.LogWarn("postgres", "failed query="+PS_GET_TERMINAL_BY_ID)
		internal_utils.LogError("postgres", err)
		if row != nil {
			row.Close()
		}
		return nil, err
	} else {

		defer row.Close()

		terminal, err := pgx.CollectExactlyOneRow(row, pgx.RowToAddrOfStructByName[public_db.Terminal])
		if err != nil {
			internal_utils.LogWarn("postgres", "failed query="+PS_GET_TERMINAL_BY_ID)
			internal_utils.LogError("postgres", err)
			return nil, err
		}

		internal_utils.LogInfo("postgres", "query="+PS_GET_TERMINAL_BY_ID)
		return terminal, nil
	}
}

func (db *C2ECPostgres) GetTransferById(requestUid []byte) (*public_db.Transfer, error) {

	if rows, err := db.pool.Query(
		db.ctx,
		PS_GET_TRANSFER_BY_ID,
		requestUid,
	); err != nil {
		internal_utils.LogWarn("postgres", "failed query="+PS_GET_TRANSFER_BY_ID)
		internal_utils.LogError("postgres", err)
		if rows != nil {
			rows.Close()
		}
		return nil, err
	} else {

		defer rows.Close()

		transfer, err := pgx.CollectOneRow(rows, pgx.RowToAddrOfStructByName[public_db.Transfer])
		if err != nil {
			if errors.Is(err, pgx.ErrNoRows) {
				return nil, nil
			}
			internal_utils.LogError("postgres", err)
			return nil, err
		}

		internal_utils.LogInfo("postgres", "query="+PS_GET_TRANSFER_BY_ID)
		return transfer, nil
	}
}

func (db *C2ECPostgres) GetTransfersByCreditAccount(creditAccount string) ([]*public_db.Transfer, error) {

	if rows, err := db.pool.Query(
		db.ctx,
		PS_GET_TRANSFER_BY_CREDIT_ACCOUNT,
		creditAccount,
	); err != nil {
		internal_utils.LogWarn("postgres", "failed query="+PS_GET_TRANSFER_BY_CREDIT_ACCOUNT)
		internal_utils.LogError("postgres", err)
		if rows != nil {
			rows.Close()
		}
		return nil, err
	} else {

		defer rows.Close()

		transfers, err := pgx.CollectRows(rows, pgx.RowToAddrOfStructByName[public_db.Transfer])
		if err != nil {
			if errors.Is(err, pgx.ErrNoRows) {
				return make([]*public_db.Transfer, 0), nil
			}
			internal_utils.LogError("postgres", err)
			return nil, err
		}

		internal_utils.LogInfo("postgres", "query="+PS_GET_TRANSFER_BY_CREDIT_ACCOUNT)
		return internal_utils.RemoveNulls(transfers), nil
	}
}

func (db *C2ECPostgres) AddTransfer(
	requestUid []byte,
	amount *internal_utils.Amount,
	exchangeBaseUrl string,
	wtid string,
	credit_account string,
	ts time.Time,
) error {

	dbAmount := internal_utils.TalerAmountCurrency{
		Val:  int64(amount.Value),
		Frac: int32(amount.Fraction),
		Curr: amount.Currency,
	}

	_, err := db.pool.Exec(
		db.ctx,
		PS_ADD_TRANSFER,
		requestUid,
		dbAmount,
		exchangeBaseUrl,
		wtid,
		credit_account,
		ts.Unix(),
	)
	if err != nil {
		internal_utils.LogInfo("postgres", "failed query="+PS_ADD_TRANSFER)
		internal_utils.LogError("postgres", err)
		return err
	}
	internal_utils.LogInfo("postgres", "query="+PS_ADD_TRANSFER)
	return nil
}

func (db *C2ECPostgres) UpdateTransfer(
	rowId int,
	requestUid []byte,
	timestamp int64,
	status int16,
	retries int16,
) error {

	query := PS_UPDATE_TRANSFER
	if rowId <= 1 {
		// tweak to intially set transferred_row_id. Can be removed once transferred_row_id field is obsolete
		query = "UPDATE c2ec.transfer SET (transfer_ts, transfer_status, retries, transferred_row_id) = ($1,$2,$3,1) WHERE request_uid=$4"
	}

	_, err := db.pool.Exec(
		db.ctx,
		query,
		timestamp,
		status,
		retries,
		requestUid,
	)
	if err != nil {
		internal_utils.LogInfo("postgres", "failed query="+query)
		internal_utils.LogError("postgres", err)
		return err
	}
	internal_utils.LogInfo("postgres", "query="+query)
	return nil
}

func (db *C2ECPostgres) GetTransfers(start int, delta int, since time.Time) ([]*public_db.Transfer, error) {

	// +d / +s
	query := PS_GET_TRANSFERS_ASC
	if delta < 0 {
		// d negatives indicates DESC ordering and backwards reading
		// -d / +s
		query = PS_GET_TRANSFERS_DESC
		if start < 0 {
			// start negative indicates not explicitly given
			// since -d is the case here we try reading the latest entries
			// -d / -s
			query = PS_GET_TRANSFERS_DESC_MAX
		}
	} else {
		if start < 0 {
			// +d / -s
			query = PS_GET_TRANSFERS_ASC_MAX
		}
	}

	limit := int(math.Abs(float64(delta)))
	offset := start
	if offset < 0 {
		offset = 0
	}

	if start < 0 {
		start = 0
	}

	internal_utils.LogInfo("postgres", fmt.Sprintf("selected query=%s (\nparameters:\n delta=%d,\n start=%d, limit=%d,\n offset=%d,\n since=%d\n)", query, delta, start, limit, offset, since.Unix()))

	var row pgx.Rows
	var err error

	if strings.Count(query, "$") == 1 {
		row, err = db.pool.Query(
			db.ctx,
			query,
			limit,
		)
	} else {
		row, err = db.pool.Query(
			db.ctx,
			query,
			offset,
			limit,
		)
	}

	if err != nil {
		internal_utils.LogWarn("postgres", "failed query="+query)
		internal_utils.LogError("postgres", err)
		if row != nil {
			row.Close()
		}
		return nil, err
	} else {

		defer row.Close()

		transfers, err := pgx.CollectRows(row, pgx.RowToAddrOfStructByName[public_db.Transfer])
		if err != nil {
			internal_utils.LogWarn("postgres", "failed query="+query)
			internal_utils.LogError("postgres", err)
			return nil, err
		}

		return internal_utils.RemoveNulls(transfers), nil
	}
}

func (db *C2ECPostgres) GetTransfersByState(status int) ([]*public_db.Transfer, error) {

	if rows, err := db.pool.Query(
		db.ctx,
		PS_GET_TRANSFERS_BY_STATUS,
		status,
	); err != nil {
		internal_utils.LogError("postgres", err)
		if rows != nil {
			rows.Close()
		}
		return nil, err
	} else {

		defer rows.Close()

		transfers, err := pgx.CollectRows(rows, pgx.RowToAddrOfStructByName[public_db.Transfer])
		if err != nil {
			internal_utils.LogWarn("postgres", "failed query="+PS_GET_TRANSFERS_BY_STATUS)
			internal_utils.LogError("postgres", err)
			return nil, err
		}

		// this will fill up the logs...
		// internal_utils.LogInfo("postgres", "query="+PS_GET_TRANSFERS_BY_STATUS)
		// internal_utils.LogInfo("postgres", "size of transfer list="+strconv.Itoa(len(transfers)))
		return internal_utils.RemoveNulls(transfers), nil
	}
}

// Sets up a a listener for the given channel.
// Notifications will be sent through the out channel.
func (db *C2ECPostgres) NewListener(
	cn string,
	out chan *public_db.Notification,
) (func(context.Context) error, error) {

	connectionString := PostgresConnectionString(&config.CONFIG.Database)
	cfg, err := pgx.ParseConfig(connectionString)
	if err != nil {
		return nil, err
	}

	listener := &pgxlisten.Listener{
		Connect: func(ctx context.Context) (*pgx.Conn, error) {
			internal_utils.LogInfo("postgres", "listener connecting to the database")
			return pgx.ConnectConfig(ctx, cfg)
		},
	}

	internal_utils.LogInfo("postgres", "handling notifications on channel="+cn)
	listener.Handle(cn, pgxlisten.HandlerFunc(func(ctx context.Context, notification *pgconn.Notification, conn *pgx.Conn) error {
		internal_utils.LogInfo("postgres", fmt.Sprintf("handling postgres notification. channel=%s", notification.Channel))
		out <- &public_db.Notification{
			Channel: notification.Channel,
			Payload: notification.Payload,
		}
		return nil
	}))

	return listener.Listen, nil
}
