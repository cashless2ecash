package internal_db

import "c2ec/pkg/db"

const PROVIDER_TABLE_NAME = "c2ec.provider"
const PROVIDER_FIELD_NAME_ID = "provider_id"
const PROVIDER_FIELD_NAME_NAME = "name"
const PROVIDER_FIELD_NAME_PAYTO_TARGET_TYPE = "payto_target_type"
const PROVIDER_FIELD_NAME_BACKEND_URL = "backend_base_url"
const PROVIDER_FIELD_NAME_BACKEND_CREDENTIALS = "backend_credentials"

const TERMINAL_TABLE_NAME = "c2ec.terminal"
const TERMINAL_FIELD_NAME_ID = "terminal_id"
const TERMINAL_FIELD_NAME_ACCESS_TOKEN = "access_token"
const TERMINAL_FIELD_NAME_ACTIVE = "active"
const TERMINAL_FIELD_NAME_DESCRIPTION = "description"
const TERMINAL_FIELD_NAME_PROVIDER_ID = "provider_id"

const WITHDRAWAL_TABLE_NAME = "c2ec.withdrawal"
const WITHDRAWAL_FIELD_NAME_ID = "withdrawal_row_id"
const WITHDRAWAL_FIELD_NAME_CONFIRMED_ROW_ID = "confirmed_row_id"
const WITHDRAWAL_FIELD_NAME_RUID = "request_uid"
const WITHDRAWAL_FIELD_NAME_WOPID = "wopid"
const WITHDRAWAL_FIELD_NAME_RESPUBKEY = "reserve_pub_key"
const WITHDRAWAL_FIELD_NAME_TS = "registration_ts"
const WITHDRAWAL_FIELD_NAME_AMOUNT = "amount"
const WITHDRAWAL_FIELD_NAME_SUGGESTED_AMOUNT = "suggested_amount"
const WITHDRAWAL_FIELD_NAME_FEES = "terminal_fees"
const WITHDRAWAL_FIELD_NAME_STATUS = "withdrawal_status"
const WITHDRAWAL_FIELD_NAME_TERMINAL_ID = "terminal_id"
const WITHDRAWAL_FIELD_NAME_TRANSACTION_ID = "provider_transaction_id"
const WITHDRAWAL_FIELD_NAME_LAST_RETRY = "last_retry_ts"
const WITHDRAWAL_FIELD_NAME_RETRY_COUNTER = "retry_counter"
const WITHDRAWAL_FIELD_NAME_COMPLETION_PROOF = "completion_proof"

const TRANSFER_TABLE_NAME = "c2ec.transfer"
const TRANSFER_FIELD_NAME_ID = "request_uid"
const TRANSFER_FIELD_NAME_ROW_ID = "row_id"
const TRANSFER_FIELD_NAME_TRANSFERRED_ROW_ID = "transferred_row_id"
const TRANSFER_FIELD_NAME_AMOUNT = "amount"
const TRANSFER_FIELD_NAME_EXCHANGE_BASE_URL = "exchange_base_url"
const TRANSFER_FIELD_NAME_WTID = "wtid"
const TRANSFER_FIELD_NAME_CREDIT_ACCOUNT = "credit_account"
const TRANSFER_FIELD_NAME_TS = "transfer_ts"
const TRANSFER_FIELD_NAME_STATUS = "transfer_status"
const TRANSFER_FIELD_NAME_RETRIES = "retries"

// holds the instance to the database layer at runtime
// initialized by startup
var DB db.C2ECDatabase
