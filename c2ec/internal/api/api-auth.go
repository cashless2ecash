// This file is part of taler-cashless2ecash.
// Copyright (C) 2024 Joel Häberli
//
// taler-cashless2ecash is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as published
// by the Free Software Foundation, either version 3 of the License,
// or (at your option) any later version.
//
// taler-cashless2ecash is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// SPDX-License-Identifier: AGPL3.0-or-later

package internal_api

import (
	internal_utils "c2ec/internal/utils"
	"c2ec/pkg/config"
	"c2ec/pkg/db"
	"encoding/base64"
	"errors"
	"fmt"
	"net/http"
	"strconv"
	"strings"
)

const AUTHORIZATION_HEADER = "Authorization"
const BASIC_AUTH_PREFIX = "Basic "

// Authenticates the Exchange against C2EC
// returns true if authentication was successful, otherwise false
// when not successful, the api shall return immediately
// The exchange is specified to use basic auth
func AuthenticateExchange(req *http.Request) bool {

	auth := req.Header.Get(AUTHORIZATION_HEADER)
	if basicAuth, found := strings.CutPrefix(auth, BASIC_AUTH_PREFIX); found {

		ba := fmt.Sprintf("%s:%s", config.CONFIG.Server.WireGateway.Username, config.CONFIG.Server.WireGateway.Password)
		encoded := base64.StdEncoding.EncodeToString([]byte(ba))
		return encoded == basicAuth
	}
	return false
}

// Authenticates a terminal against C2EC
// returns true if authentication was successful, otherwise false
// when not successful, the api shall return immediately
//
// Terminals are authenticated using basic auth.
// The basic authorization header MUST be base64 encoded.
// The username part is the name of the provider (case sensitive) a '-' sign, followed
// by the id of the terminal, which is a number.
func AuthenticateTerminal(req *http.Request) bool {

	auth := req.Header.Get(AUTHORIZATION_HEADER)
	if basicAuth, found := strings.CutPrefix(auth, BASIC_AUTH_PREFIX); found {

		decoded, err := base64.StdEncoding.DecodeString(basicAuth)
		if err != nil {
			internal_utils.LogWarn("auth", "failed decoding basic auth header from base64")
			return false
		}

		username, password, err := parseBasicAuth(string(decoded))
		if err != nil {
			internal_utils.LogWarn("auth", "failed parsing username password from basic auth")
			return false
		}

		provider, terminalId, err := parseTerminalUser(username)
		if err != nil {
			internal_utils.LogWarn("auth", "failed parsing terminal from username in basic auth")
			return false
		}
		internal_utils.LogInfo("auth", fmt.Sprintf("req=%s by terminal with id=%d, provider=%s", req.RequestURI, terminalId, provider))

		terminal, err := db.DB.GetTerminalById(terminalId)
		if err != nil {
			return false
		}

		if !terminal.Active {
			internal_utils.LogWarn("auth", fmt.Sprintf("request from inactive terminal. id=%d", terminalId))
			return false
		}

		prvdr, err := db.DB.GetTerminalProviderByName(provider)
		if err != nil {
			internal_utils.LogWarn("auth", fmt.Sprintf("failed requesting provider by name %s", err.Error()))
			return false
		}

		if terminal.ProviderId != prvdr.ProviderId {
			internal_utils.LogWarn("auth", "terminal's provider id did not match provider id of supplied provider")
			return false
		}

		return internal_utils.ValidPassword(password, terminal.AccessToken)
	}
	internal_utils.LogWarn("auth", "basic auth prefix did not match")
	return false
}

func AuthenticateWirewatcher(req *http.Request) bool {

	auth := req.Header.Get(AUTHORIZATION_HEADER)
	if basicAuth, found := strings.CutPrefix(auth, BASIC_AUTH_PREFIX); found {

		decoded, err := base64.StdEncoding.DecodeString(basicAuth)
		if err != nil {
			internal_utils.LogWarn("auth", "failed decoding basic auth header from base64")
			return false
		}

		username, password, err := parseBasicAuth(string(decoded))
		if err != nil {
			internal_utils.LogWarn("auth", "failed parsing username password from basic auth")
			return false
		}

		if strings.EqualFold(username, config.CONFIG.Server.WireGateway.Username) &&
			strings.EqualFold(password, config.CONFIG.Server.WireGateway.Password) {

			return true
		}
	} else {
		internal_utils.LogWarn("auth", "expecting exact 'Basic' prefix!")
	}
	internal_utils.LogWarn("auth", "basic auth prefix did not match")
	return false
}

func parseBasicAuth(basicAuth string) (string, string, error) {

	parts := strings.Split(basicAuth, ":")
	if len(parts) != 2 {
		return "", "", errors.New("malformed basic auth")
	}
	return parts[0], parts[1], nil
}

// parses the username of the basic auth param of the terminal.
// the username has following format:
//
//	[PROVIDER_NAME]-[TERMINAL_ID]
func parseTerminalUser(username string) (string, int, error) {

	parts := strings.Split(username, "-")
	if len(parts) != 2 {
		return "", -1, errors.New("malformed basic auth username")
	}

	providerName := parts[0]
	terminalId, err := strconv.Atoi(parts[1])
	if err != nil {
		return "", -1, errors.New("malformed basic auth username")
	}

	return providerName, terminalId, nil
}

// Parses the terminal id from the token.
// This function is used to determine the terminal
// which orchestrates the withdrawal.
func parseTerminalId(req *http.Request) int {
	auth := req.Header.Get(AUTHORIZATION_HEADER)
	if basicAuth, found := strings.CutPrefix(auth, BASIC_AUTH_PREFIX); found {

		decoded, err := base64.StdEncoding.DecodeString(basicAuth)
		if err != nil {
			return -1
		}

		username, _, err := parseBasicAuth(string(decoded))
		if err != nil {
			return -1
		}

		_, terminalId, err := parseTerminalUser(username)
		if err != nil {
			return -1
		}

		return terminalId
	}

	return -1
}

func parseProvider(req *http.Request) (*db.Provider, error) {

	auth := req.Header.Get(AUTHORIZATION_HEADER)
	if basicAuth, found := strings.CutPrefix(auth, BASIC_AUTH_PREFIX); found {

		decoded, err := base64.StdEncoding.DecodeString(basicAuth)
		if err != nil {
			return nil, err
		}

		username, _, err := parseBasicAuth(string(decoded))
		if err != nil {
			return nil, err
		}

		providerName, _, err := parseTerminalUser(username)
		if err != nil {
			return nil, err
		}

		p, err := db.DB.GetTerminalProviderByName(providerName)
		if err != nil {
			return nil, err
		}

		return p, nil
	}

	return nil, errors.New("authorization header did not match expectations")
}
