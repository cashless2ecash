// This file is part of taler-cashless2ecash.
// Copyright (C) 2024 Joel Häberli
//
// taler-cashless2ecash is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as published
// by the Free Software Foundation, either version 3 of the License,
// or (at your option) any later version.
//
// taler-cashless2ecash is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// SPDX-License-Identifier: AGPL3.0-or-later

package internal_api

import (
	internal_utils "c2ec/internal/utils"
	"c2ec/pkg/config"
	"c2ec/pkg/db"
	"crypto/rand"
	"errors"
	"fmt"
	"net/http"
)

type TerminalConfig struct {
	Name           string `json:"name"`
	Version        string `json:"version"`
	ProviderName   string `json:"provider_name"`
	Currency       string `json:"currency"`
	WithdrawalFees string `json:"withdrawal_fees"`
	WireType       string `json:"wire_type"`
}

type TerminalWithdrawalSetup struct {
	Amount                string `json:"amount"`
	SuggestedAmount       string `json:"suggested_amount"`
	ProviderTransactionId string `json:"provider_transaction_id"`
	TerminalFees          string `json:"terminal_fees"`
	RequestUid            string `json:"request_uid"`
	UserUuid              string `json:"user_uuid"`
	Lock                  string `json:"lock"`
}

type TerminalWithdrawalSetupResponse struct {
	Wopid string `json:"withdrawal_id"`
}

type TerminalWithdrawalConfirmationRequest struct {
	ProviderTransactionId string `json:"provider_transaction_id"`
	TerminalFees          string `json:"terminal_fees"`
	UserUuid              string `json:"user_uuid"`
	Lock                  string `json:"lock"`
}

func HandleTerminalConfig(res http.ResponseWriter, req *http.Request) {

	p, auth, err := authAndParseProvider(req)
	if !auth {
		internal_utils.SetLastResponseCodeForLogger(internal_utils.HTTP_UNAUTHORIZED)
		res.WriteHeader(internal_utils.HTTP_UNAUTHORIZED)
		return
	}

	if err != nil || p == nil {
		internal_utils.LogError("terminals-api", err)
		internal_utils.SetLastResponseCodeForLogger(internal_utils.HTTP_BAD_REQUEST)
		res.WriteHeader(internal_utils.HTTP_BAD_REQUEST)
		return
	}

	encoder := internal_utils.NewJsonCodec[TerminalConfig]()
	cfg, err := encoder.EncodeToBytes(&TerminalConfig{
		Name:           "taler-terminal",
		Version:        "0:0:0",
		ProviderName:   p.Name,
		Currency:       config.CONFIG.Server.Currency,
		WithdrawalFees: config.CONFIG.Server.WithdrawalFees,
		WireType:       p.PaytoTargetType,
	})
	if err != nil {
		internal_utils.LogError("terminals-api", err)
		internal_utils.SetLastResponseCodeForLogger(internal_utils.HTTP_INTERNAL_SERVER_ERROR)
		res.WriteHeader(internal_utils.HTTP_INTERNAL_SERVER_ERROR)
		return
	}

	res.Header().Add(internal_utils.CONTENT_TYPE_HEADER, encoder.HttpApplicationContentHeader())
	internal_utils.SetLastResponseCodeForLogger(internal_utils.HTTP_OK)
	res.WriteHeader(internal_utils.HTTP_OK)
	res.Write(cfg)
}

func HandleWithdrawalSetup(res http.ResponseWriter, req *http.Request) {

	p, auth, err := authAndParseProvider(req)
	if !auth {
		internal_utils.SetLastResponseCodeForLogger(internal_utils.HTTP_UNAUTHORIZED)
		res.WriteHeader(internal_utils.HTTP_UNAUTHORIZED)
		return
	}
	if err != nil || p == nil {
		internal_utils.LogError("terminals-api", err)
		internal_utils.SetLastResponseCodeForLogger(internal_utils.HTTP_BAD_REQUEST)
		res.WriteHeader(internal_utils.HTTP_BAD_REQUEST)
		return
	}

	jsonCodec := internal_utils.NewJsonCodec[TerminalWithdrawalSetup]()
	setup, err := internal_utils.ReadStructFromBody[TerminalWithdrawalSetup](req, jsonCodec)
	if err != nil {
		internal_utils.LogWarn("terminals-api", fmt.Sprintf("invalid body for withdrawal registration error=%s", err.Error()))
		internal_utils.SetLastResponseCodeForLogger(internal_utils.HTTP_BAD_REQUEST)
		res.WriteHeader(internal_utils.HTTP_BAD_REQUEST)
		return
	}

	if hasConflict(setup) {
		internal_utils.SetLastResponseCodeForLogger(internal_utils.HTTP_CONFLICT)
		res.WriteHeader(internal_utils.HTTP_CONFLICT)
		return
	}

	terminalId := parseTerminalId(req)
	if terminalId == -1 {
		internal_utils.LogWarn("terminals-api", "terminal id could not be read from authorization header")
		internal_utils.SetLastResponseCodeForLogger(internal_utils.HTTP_BAD_REQUEST)
		res.WriteHeader(internal_utils.HTTP_BAD_REQUEST)
		return
	}

	// generate wopid
	generatedWopid := make([]byte, 32)
	_, err = rand.Read(generatedWopid)
	if err != nil {
		internal_utils.LogWarn("terminals-api", "unable to generate correct wopid")
		internal_utils.LogError("terminals-api", err)
		internal_utils.SetLastResponseCodeForLogger(internal_utils.HTTP_INTERNAL_SERVER_ERROR)
		res.WriteHeader(internal_utils.HTTP_INTERNAL_SERVER_ERROR)
	}

	suggstdAmnt, err := parseAmount(setup.SuggestedAmount)
	if err != nil {
		internal_utils.SetLastResponseCodeForLogger(internal_utils.HTTP_BAD_REQUEST)
		res.WriteHeader(internal_utils.HTTP_BAD_REQUEST)
		return
	}
	amnt, err := parseAmount(setup.Amount)
	if err != nil {
		internal_utils.SetLastResponseCodeForLogger(internal_utils.HTTP_BAD_REQUEST)
		res.WriteHeader(internal_utils.HTTP_BAD_REQUEST)
		return
	}
	fees, err := parseAmount(setup.TerminalFees)
	if err != nil {
		internal_utils.SetLastResponseCodeForLogger(internal_utils.HTTP_BAD_REQUEST)
		res.WriteHeader(internal_utils.HTTP_BAD_REQUEST)
		return
	}

	err = db.DB.SetupWithdrawal(
		generatedWopid,
		suggstdAmnt,
		amnt,
		terminalId,
		setup.ProviderTransactionId,
		fees,
		setup.RequestUid,
	)

	if err != nil {
		internal_utils.LogError("terminals-api", err)
		internal_utils.SetLastResponseCodeForLogger(internal_utils.HTTP_INTERNAL_SERVER_ERROR)
		res.WriteHeader(internal_utils.HTTP_INTERNAL_SERVER_ERROR)
		return
	}

	encoder := internal_utils.NewJsonCodec[TerminalWithdrawalSetupResponse]()
	encodedBody, err := encoder.EncodeToBytes(
		&TerminalWithdrawalSetupResponse{
			Wopid: internal_utils.TalerBinaryEncode(generatedWopid),
		},
	)
	if err != nil {
		internal_utils.LogError("terminal-api", err)
		internal_utils.SetLastResponseCodeForLogger(internal_utils.HTTP_INTERNAL_SERVER_ERROR)
		res.WriteHeader(internal_utils.HTTP_INTERNAL_SERVER_ERROR)
		return
	}

	res.Header().Add(internal_utils.CONTENT_TYPE_HEADER, encoder.HttpApplicationContentHeader())
	res.Write(encodedBody)
}

func HandleWithdrawalCheck(res http.ResponseWriter, req *http.Request) {

	p, auth, err := authAndParseProvider(req)
	if !auth {
		internal_utils.SetLastResponseCodeForLogger(internal_utils.HTTP_UNAUTHORIZED)
		res.WriteHeader(internal_utils.HTTP_UNAUTHORIZED)
		return
	}

	if err != nil || p == nil {
		internal_utils.LogError("terminals-api", err)
		internal_utils.SetLastResponseCodeForLogger(internal_utils.HTTP_BAD_REQUEST)
		res.WriteHeader(internal_utils.HTTP_BAD_REQUEST)
		return
	}

	wopid := req.PathValue(WOPID_PARAMETER)
	wpd, err := internal_utils.ParseWopid(wopid)
	if err != nil {
		internal_utils.LogWarn("terminals-api", "wopid "+wopid+" not valid")
		if wopid == "" {
			internal_utils.SetLastResponseCodeForLogger(internal_utils.HTTP_BAD_REQUEST)
			res.WriteHeader(internal_utils.HTTP_BAD_REQUEST)
			return
		}
	}

	jsonCodec := internal_utils.NewJsonCodec[TerminalWithdrawalConfirmationRequest]()
	paymentNotification, err := internal_utils.ReadStructFromBody[TerminalWithdrawalConfirmationRequest](req, jsonCodec)
	if err != nil {
		internal_utils.LogError("terminals-api", err)
		internal_utils.SetLastResponseCodeForLogger(internal_utils.HTTP_BAD_REQUEST)
		res.WriteHeader(internal_utils.HTTP_BAD_REQUEST)
		return
	}

	internal_utils.LogInfo("terminals-api", "received payment notification")

	terminalId := parseTerminalId(req)
	if terminalId == -1 {
		internal_utils.LogWarn("terminals-api", "terminal id could not be read from authorization header")
		internal_utils.SetLastResponseCodeForLogger(internal_utils.HTTP_BAD_REQUEST)
		res.WriteHeader(internal_utils.HTTP_BAD_REQUEST)
		return
	}

	trmlFees, err := internal_utils.ParseAmount(paymentNotification.TerminalFees, config.CONFIG.Server.CurrencyFractionDigits)
	if err != nil {
		internal_utils.LogError("terminals-api", err)
		internal_utils.SetLastResponseCodeForLogger(internal_utils.HTTP_BAD_REQUEST)
		res.WriteHeader(internal_utils.HTTP_BAD_REQUEST)
		return
	}

	exchangeFees, err := parseAmount(config.CONFIG.Server.WithdrawalFees)
	if err != nil {
		internal_utils.LogError("terminals-api", errors.New("unable to parse withdrawal fees - FATAL SHOULD NEVER HAPPEN"))
		internal_utils.LogError("terminals-api", err)
		internal_utils.SetLastResponseCodeForLogger(internal_utils.HTTP_INTERNAL_SERVER_ERROR)
		res.WriteHeader(internal_utils.HTTP_INTERNAL_SERVER_ERROR)
		return
	}

	// Fees are optional here and since the Exchange can specify
	// zero fees, the value can be zero as well. The case that the
	// the terminal sends no fees and the exchange does not charge
	// fees needs to be covered as compliant request, currently done
	// by the trmlFees < exchangeFees check.
	// Check that fees are at least as high as the configured withdrawal fees.
	// a higher value would indicate that the payment service provider does
	// also charge fees.
	// incoming fees >= specified fees
	if smaller, err := trmlFees.IsSmallerThan(exchangeFees); smaller || err != nil {
		if err != nil {
			internal_utils.LogError("terminals-api", err)
			internal_utils.SetLastResponseCodeForLogger(internal_utils.HTTP_BAD_REQUEST)
			res.WriteHeader(internal_utils.HTTP_BAD_REQUEST)
			return
		}
		if smaller {
			internal_utils.LogError("terminals-api", errors.New("terminal did specify uncorrect fees"))
			internal_utils.SetLastResponseCodeForLogger(internal_utils.HTTP_BAD_REQUEST)
			res.WriteHeader(internal_utils.HTTP_BAD_REQUEST)
			return
		}
	}

	internal_utils.LogInfo("terminals-api", "received valid check request for provider_transaction_id="+paymentNotification.ProviderTransactionId)
	err = db.DB.NotifyPayment(
		wpd,
		paymentNotification.ProviderTransactionId,
		terminalId,
		preventNilAmount(trmlFees),
	)
	if err != nil {
		internal_utils.LogError("terminals-api", err)
		internal_utils.SetLastResponseCodeForLogger(internal_utils.HTTP_BAD_REQUEST)
		res.WriteHeader(internal_utils.HTTP_BAD_REQUEST)
		return
	}

	internal_utils.SetLastResponseCodeForLogger(internal_utils.HTTP_NO_CONTENT)
	res.WriteHeader(internal_utils.HTTP_NO_CONTENT)
}

func HandleWithdrawalStatusTerminal(res http.ResponseWriter, req *http.Request) {

	_, auth, err := authAndParseProvider(req)
	if err != nil || !auth {
		internal_utils.SetLastResponseCodeForLogger(internal_utils.HTTP_UNAUTHORIZED)
		res.WriteHeader(internal_utils.HTTP_UNAUTHORIZED)
		return
	}

	HandleWithdrawalStatus(res, req)
}

func HandleWithdrawalAbortTerminal(res http.ResponseWriter, req *http.Request) {

	_, auth, err := authAndParseProvider(req)
	if err != nil || !auth {
		internal_utils.SetLastResponseCodeForLogger(internal_utils.HTTP_UNAUTHORIZED)
		res.WriteHeader(internal_utils.HTTP_UNAUTHORIZED)
		return
	}

	HandleWithdrawalAbort(res, req)
}

func parseAmount(amountStr string) (internal_utils.Amount, error) {

	a, err := internal_utils.ParseAmount(amountStr, config.CONFIG.Server.CurrencyFractionDigits)
	if err != nil {
		return internal_utils.Amount{Currency: "", Value: 0, Fraction: 0}, err
	}
	return preventNilAmount(a), nil
}

func preventNilAmount(exchangeFees *internal_utils.Amount) internal_utils.Amount {

	if exchangeFees == nil {
		return internal_utils.Amount{Currency: "", Value: 0, Fraction: 0}
	}

	return *exchangeFees
}

func hasConflict(t *TerminalWithdrawalSetup) bool {

	w, err := db.DB.GetWithdrawalByRequestUid(t.RequestUid)
	if err != nil {
		internal_utils.LogError("terminals-api", err)
		return true
	}

	if w == nil {
		return false // no request with this uid
	}

	suggstdAmnt, err := parseAmount(t.SuggestedAmount)
	if err != nil {
		internal_utils.LogError("terminals-api", err)
		return true
	}
	amnt, err := parseAmount(t.Amount)
	if err != nil {
		internal_utils.LogError("terminals-api", err)
		return true
	}
	fees, err := parseAmount(t.TerminalFees)
	if err != nil {
		internal_utils.LogError("terminals-api", err)
		return true
	}

	isEqual := w.Amount.Curr == amnt.Currency &&
		w.Amount.Val == int64(amnt.Value) &&
		w.Amount.Frac == int32(amnt.Fraction) &&
		w.TerminalFees.Curr == fees.Currency &&
		uint64(w.TerminalFees.Val) == fees.Value &&
		uint64(w.TerminalFees.Frac) == fees.Fraction &&
		w.SuggestedAmount.Curr == suggstdAmnt.Currency &&
		uint64(w.SuggestedAmount.Val) == suggstdAmnt.Value &&
		uint64(w.SuggestedAmount.Frac) == suggstdAmnt.Fraction &&
		w.ProviderTransactionId == &t.ProviderTransactionId &&
		w.RequestUid == t.RequestUid

	return !isEqual
}

func authAndParseProvider(req *http.Request) (*db.Provider, bool, error) {

	if authenticated := AuthenticateTerminal(req); !authenticated {
		return nil, false, nil
	}

	p, err := parseProvider(req)
	if err != nil {
		return nil, true, err
	}

	return p, true, nil
}
