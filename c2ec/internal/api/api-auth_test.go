// This file is part of taler-cashless2ecash.
// Copyright (C) 2024 Joel Häberli
//
// taler-cashless2ecash is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as published
// by the Free Software Foundation, either version 3 of the License,
// or (at your option) any later version.
//
// taler-cashless2ecash is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// SPDX-License-Identifier: AGPL3.0-or-later

package internal_api

import (
	internal_utils "c2ec/internal/utils"
	"crypto/rand"
	"encoding/base64"
	"errors"
	"fmt"
	"testing"

	"golang.org/x/crypto/argon2"
)

func TestValidPassword(t *testing.T) {

	pw := "verygoodpassword"
	hashedEncodedPw, err := pbkdf(pw)
	if err != nil {
		fmt.Println("pbkdf failed")
		t.FailNow()
	}

	if !internal_utils.ValidPassword(pw, hashedEncodedPw) {
		fmt.Println("password check failed")
		t.FailNow()
	}
}

// copied from the cli tool. this function is used to obtain a base64 encoded password hash.
func pbkdf(pw string) (string, error) {

	rfcTime := 3
	rfcMemory := 32 * 1024
	salt := make([]byte, 16)
	_, err := rand.Read(salt)
	if err != nil {
		return "", err
	}
	key := argon2.Key([]byte(pw), salt, uint32(rfcTime), uint32(rfcMemory), 4, 32)

	keyAndSalt := make([]byte, 0, 48)
	keyAndSalt = append(keyAndSalt, key...)
	keyAndSalt = append(keyAndSalt, salt...)
	if len(keyAndSalt) != 48 {
		return "", errors.New("invalid password hash and salt")
	}
	return base64.StdEncoding.EncodeToString(keyAndSalt), nil
}
