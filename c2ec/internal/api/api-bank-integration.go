// This file is part of taler-cashless2ecash.
// Copyright (C) 2024 Joel Häberli
//
// taler-cashless2ecash is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as published
// by the Free Software Foundation, either version 3 of the License,
// or (at your option) any later version.
//
// taler-cashless2ecash is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// SPDX-License-Identifier: AGPL3.0-or-later

package internal_api

import (
	internal_utils "c2ec/internal/utils"
	"c2ec/pkg/config"
	"c2ec/pkg/db"
	"c2ec/pkg/provider"
	"context"
	"encoding/base64"
	"errors"
	"fmt"
	http "net/http"
	"strconv"
	"time"
)

const DEFAULT_LONG_POLL_MS = 1000
const DEFAULT_OLD_STATE = internal_utils.PENDING

// https://docs.taler.net/core/api-exchange.html#tsref-type-CurrencySpecification
type CurrencySpecification struct {
	Name                            string            `json:"name"`
	Currency                        string            `json:"currency"`
	NumFractionalInputDigits        int               `json:"num_fractional_input_digits"`
	NumFractionalNormalDigits       int               `json:"num_fractional_normal_digits"`
	NumFractionalTrailingZeroDigits int               `json:"num_fractional_trailing_zero_digits"`
	AltUnitNames                    map[string]string `json:"alt_unit_names"`
}

// https://docs.taler.net/core/api-bank-integration.html#tsref-type-BankIntegrationConfig
type BankIntegrationConfig struct {
	Name                  string                `json:"name"`
	Version               string                `json:"version"`
	Implementation        string                `json:"implementation"`
	Currency              string                `json:"currency"`
	CurrencySpecification CurrencySpecification `json:"currency_specification"`
}

type BankWithdrawalOperationPostRequest struct {
	ReservePubKey    internal_utils.EddsaPublicKey `json:"reserve_pub"`
	SelectedExchange string                        `json:"selected_exchange"`
	Amount           *internal_utils.Amount        `json:"amount"`
}

type BankWithdrawalOperationPostResponse struct {
	Status             internal_utils.WithdrawalOperationStatus `json:"status"`
	ConfirmTransferUrl string                                   `json:"confirm_transfer_url"`
	TransferDone       bool                                     `json:"transfer_done"`
}

type BankWithdrawalOperationStatus struct {
	Status            internal_utils.WithdrawalOperationStatus `json:"status"`
	Amount            string                                   `json:"amount"`
	CardFees          string                                   `json:"card_fees"`
	SenderWire        string                                   `json:"sender_wire"`
	WireTypes         []string                                 `json:"wire_types"`
	ReservePubKey     internal_utils.EddsaPublicKey            `json:"selected_reserve_pub"`
	SuggestedExchange string                                   `json:"suggested_exchange"`
	RequiredExchange  string                                   `json:"required_exchange"`
	Aborted           bool                                     `json:"aborted"`
	SelectionDone     bool                                     `json:"selection_done"`
	TransferDone      bool                                     `json:"transfer_done"`
}

func BankIntegrationConfigApi(res http.ResponseWriter, req *http.Request) {

	internal_utils.LogInfo("bank-integration-api", "reading config")
	cfg := BankIntegrationConfig{
		Name:     "taler-bank-integration",
		Version:  "4:8:2",
		Currency: config.CONFIG.Server.Currency,
		CurrencySpecification: CurrencySpecification{
			Name:                            config.CONFIG.Server.Currency,
			Currency:                        config.CONFIG.Server.Currency,
			NumFractionalInputDigits:        config.CONFIG.Server.CurrencyFractionDigits,
			NumFractionalNormalDigits:       config.CONFIG.Server.CurrencyFractionDigits,
			NumFractionalTrailingZeroDigits: 0,
			AltUnitNames: map[string]string{
				"0": config.CONFIG.Server.Currency,
			},
		},
	}

	encoder := internal_utils.NewJsonCodec[BankIntegrationConfig]()
	serializedCfg, err := encoder.EncodeToBytes(&cfg)
	if err != nil {
		internal_utils.LogInfo("bank-integration-api", fmt.Sprintf("failed serializing config: %s", err.Error()))
		internal_utils.SetLastResponseCodeForLogger(internal_utils.HTTP_INTERNAL_SERVER_ERROR)
		res.WriteHeader(internal_utils.HTTP_INTERNAL_SERVER_ERROR)
		return
	}

	res.Header().Add(internal_utils.CONTENT_TYPE_HEADER, encoder.HttpApplicationContentHeader())
	internal_utils.SetLastResponseCodeForLogger(internal_utils.HTTP_OK)
	res.WriteHeader(internal_utils.HTTP_OK)
	res.Write(serializedCfg)
}

func HandleParameterRegistration(res http.ResponseWriter, req *http.Request) {

	jsonCodec := internal_utils.NewJsonCodec[BankWithdrawalOperationPostRequest]()
	registration, err := internal_utils.ReadStructFromBody[BankWithdrawalOperationPostRequest](req, jsonCodec)
	if err != nil {
		internal_utils.LogWarn("bank-integration-api", fmt.Sprintf("invalid body for withdrawal registration error=%s", err.Error()))
		internal_utils.SetLastResponseCodeForLogger(internal_utils.HTTP_BAD_REQUEST)
		res.WriteHeader(internal_utils.HTTP_BAD_REQUEST)
		return
	}

	// read and validate the wopid path parameter
	wopid := req.PathValue(WOPID_PARAMETER)
	wpd, err := internal_utils.ParseWopid(wopid)
	if err != nil {
		internal_utils.LogWarn("bank-integration-api", "wopid "+wopid+" not valid")
		internal_utils.SetLastResponseCodeForLogger(internal_utils.HTTP_BAD_REQUEST)
		res.WriteHeader(internal_utils.HTTP_BAD_REQUEST)
		return
	}

	if w, err := db.DB.GetWithdrawalByWopid(wpd); err != nil {
		internal_utils.LogError("bank-integration-api", err)
		internal_utils.SetLastResponseCodeForLogger(internal_utils.HTTP_NOT_FOUND)
		res.WriteHeader(internal_utils.HTTP_NOT_FOUND)
		return
	} else {
		if w.ReservePubKey != nil || len(w.ReservePubKey) > 0 {
			internal_utils.LogWarn("bank-integration-api", "tried registering a withdrawal-operation with already existing wopid")
			internal_utils.SetLastResponseCodeForLogger(internal_utils.HTTP_CONFLICT)
			res.WriteHeader(internal_utils.HTTP_CONFLICT)
			return
		}
	}

	if err = db.DB.RegisterWithdrawalParameters(
		wpd,
		registration.ReservePubKey,
	); err != nil {
		internal_utils.LogError("bank-integration-api", err)
		internal_utils.SetLastResponseCodeForLogger(internal_utils.HTTP_INTERNAL_SERVER_ERROR)
		res.WriteHeader(internal_utils.HTTP_INTERNAL_SERVER_ERROR)
		return
	}

	withdrawal, err := db.DB.GetWithdrawalByWopid(wpd)
	if err != nil {
		internal_utils.LogError("bank-integration-api", err)
		internal_utils.SetLastResponseCodeForLogger(internal_utils.HTTP_INTERNAL_SERVER_ERROR)
		res.WriteHeader(internal_utils.HTTP_INTERNAL_SERVER_ERROR)
	}

	resbody := &BankWithdrawalOperationPostResponse{
		Status:             withdrawal.WithdrawalStatus,
		ConfirmTransferUrl: "", // not used in our case
		TransferDone:       withdrawal.WithdrawalStatus == internal_utils.CONFIRMED,
	}

	encoder := internal_utils.NewJsonCodec[BankWithdrawalOperationPostResponse]()
	resbyts, err := encoder.EncodeToBytes(resbody)
	if err != nil {
		internal_utils.LogError("bank-integration-api", err)
		internal_utils.SetLastResponseCodeForLogger(internal_utils.HTTP_INTERNAL_SERVER_ERROR)
		res.WriteHeader(internal_utils.HTTP_INTERNAL_SERVER_ERROR)
	}

	res.Header().Add(internal_utils.CONTENT_TYPE_HEADER, encoder.HttpApplicationContentHeader())
	res.Write(resbyts)
}

// Get status of withdrawal associated with the given WOPID
//
// Parameters:
//   - long_poll_ms (optional):
//     milliseconds to wait for state to change
//     given old_state until responding
//   - old_state (optional):
//     Default is 'pending'
func HandleWithdrawalStatus(res http.ResponseWriter, req *http.Request) {

	// read and validate request query parameters
	shouldStartLongPoll := true
	longPollMilli := DEFAULT_LONG_POLL_MS
	oldState := DEFAULT_OLD_STATE
	if longPollMilliPtr, accepted := internal_utils.AcceptOptionalParamOrWriteResponse(
		"long_poll_ms", strconv.Atoi, req, res,
	); accepted {
		if longPollMilliPtr != nil {
			longPollMilli = *longPollMilliPtr
			if oldStatePtr, accepted := internal_utils.AcceptOptionalParamOrWriteResponse(
				"old_state", internal_utils.ToWithdrawalOperationStatus, req, res,
			); accepted {
				if oldStatePtr != nil {
					oldState = *oldStatePtr
				}
			}
		} else {
			// this means parameter was not given.
			// no long polling (simple get)
			internal_utils.LogInfo("bank-integration-api", "will not start long-polling")
			shouldStartLongPoll = false
		}
	} else {
		internal_utils.LogInfo("bank-integration-api", "will not start long-polling")
		shouldStartLongPoll = false
	}

	// read and validate the wopid path parameter
	wopid := req.PathValue(WOPID_PARAMETER)
	wpd, err := internal_utils.ParseWopid(wopid)
	if err != nil {
		internal_utils.LogWarn("bank-integration-api", "wopid "+wopid+" not valid")
		internal_utils.SetLastResponseCodeForLogger(internal_utils.HTTP_BAD_REQUEST)
		res.WriteHeader(internal_utils.HTTP_BAD_REQUEST)
		return
	}

	var timeoutCtx context.Context
	notifications := make(chan *db.Notification)
	w := make(chan []byte)
	errStat := make(chan int)
	if shouldStartLongPoll {

		go func() {
			// when the current state differs from the old_state
			// of the request, return immediately. This goroutine
			// does this check and sends the withdrawal to through
			// the specified channel, if the withdrawal was already
			// changed.
			withdrawal, err := db.DB.GetWithdrawalByWopid(wpd)
			if err != nil {
				internal_utils.LogError("bank-integration-api", err)
			}
			if withdrawal == nil {
				// do nothing because other goroutine might deliver result
				return
			}
			if withdrawal.WithdrawalStatus != oldState {
				byts, status := formatWithdrawalOrErrorStatus(withdrawal)
				if status != internal_utils.HTTP_OK {
					errStat <- status
				} else {
					w <- byts
				}
			}
		}()

		var cancelFunc context.CancelFunc
		timeoutCtx, cancelFunc = context.WithTimeout(
			req.Context(),
			time.Duration(longPollMilli)*time.Millisecond,
		)
		defer cancelFunc()

		channel := "w_" + base64.StdEncoding.EncodeToString(wpd)

		listenFunc, err := db.DB.NewListener(
			channel,
			notifications,
		)

		if err != nil {
			internal_utils.LogError("bank-integration-api", err)
			errStat <- internal_utils.HTTP_INTERNAL_SERVER_ERROR
		} else {
			go listenFunc(timeoutCtx)
		}
	} else {
		wthdrl, stat := getWithdrawalOrError(wpd)
		internal_utils.LogInfo("bank-integration-api", "loaded withdrawal")
		if stat != internal_utils.HTTP_OK {
			internal_utils.LogWarn("bank-integration-api", "tried loading withdrawal but got error")
			//errStat <- stat
			internal_utils.SetLastResponseCodeForLogger(stat)
			res.WriteHeader(stat)
			return
		} else {
			//w <- wthdrl
			res.Header().Add(internal_utils.CONTENT_TYPE_HEADER, "application/json")
			res.Write(wthdrl)
			return
		}
	}

	for wait := true; wait; {
		select {
		case <-timeoutCtx.Done():
			internal_utils.LogInfo("bank-integration-api", "long poll time exceeded")
			internal_utils.SetLastResponseCodeForLogger(internal_utils.HTTP_NO_CONTENT)
			res.WriteHeader(internal_utils.HTTP_NO_CONTENT)
			wait = false
		case <-notifications:
			wthdrl, stat := getWithdrawalOrError(wpd)
			if stat != 200 {
				internal_utils.SetLastResponseCodeForLogger(stat)
				res.WriteHeader(stat)
			} else {
				res.Header().Add(internal_utils.CONTENT_TYPE_HEADER, "application/json")
				res.Write(wthdrl)
			}
			wait = false
		case wthdrl := <-w:
			res.Header().Add(internal_utils.CONTENT_TYPE_HEADER, "application/json")
			res.Write(wthdrl)
			wait = false
		case status := <-errStat:
			internal_utils.LogInfo("bank-integration-api", "got unsucessful state for withdrawal operation request")
			internal_utils.SetLastResponseCodeForLogger(status)
			res.WriteHeader(status)
			wait = false
		}
	}
	internal_utils.LogInfo("bank-integration-api", "withdrawal operation status request finished")
}

func HandleWithdrawalAbort(res http.ResponseWriter, req *http.Request) {

	// read and validate the wopid path parameter
	wopid := req.PathValue(WOPID_PARAMETER)
	wpd, err := internal_utils.ParseWopid(wopid)
	if err != nil {
		internal_utils.LogWarn("bank-integration-api", "wopid "+wopid+" not valid")
		internal_utils.SetLastResponseCodeForLogger(internal_utils.HTTP_BAD_REQUEST)
		res.WriteHeader(internal_utils.HTTP_BAD_REQUEST)
		return
	}

	withdrawal, err := db.DB.GetWithdrawalByWopid(wpd)
	if err != nil {
		internal_utils.LogError("bank-integration-api", err)
		internal_utils.SetLastResponseCodeForLogger(internal_utils.HTTP_NOT_FOUND)
		res.WriteHeader(internal_utils.HTTP_NOT_FOUND)
		return
	}

	if withdrawal.WithdrawalStatus == internal_utils.CONFIRMED {
		internal_utils.SetLastResponseCodeForLogger(internal_utils.HTTP_CONFLICT)
		res.WriteHeader(internal_utils.HTTP_CONFLICT)
		return
	}

	err = db.DB.FinaliseWithdrawal(int(withdrawal.WithdrawalRowId), internal_utils.ABORTED, make([]byte, 0))
	if err != nil {
		internal_utils.LogError("bank-integration-api", err)
		internal_utils.SetLastResponseCodeForLogger(internal_utils.HTTP_INTERNAL_SERVER_ERROR)
		res.WriteHeader(internal_utils.HTTP_INTERNAL_SERVER_ERROR)
		return
	}

	internal_utils.SetLastResponseCodeForLogger(internal_utils.HTTP_NO_CONTENT)
	res.WriteHeader(internal_utils.HTTP_NO_CONTENT)
}

// Tries to load a WithdrawalOperationStatus from the database. If no
// entry could been found, it will write the correct error to the response.
func getWithdrawalOrError(wopid []byte) ([]byte, int) {
	// read the withdrawal from the database
	withdrawal, err := db.DB.GetWithdrawalByWopid(wopid)
	if err != nil {
		internal_utils.LogError("bank-integration-api", err)
		return nil, internal_utils.HTTP_NOT_FOUND
	}

	if withdrawal == nil {
		// not found -> 404
		return nil, internal_utils.HTTP_NOT_FOUND
	}

	// return the C2ECWithdrawalStatus
	return formatWithdrawalOrErrorStatus(withdrawal)
}

func formatWithdrawalOrErrorStatus(w *db.Withdrawal) ([]byte, int) {

	if w == nil {
		return nil, internal_utils.HTTP_INTERNAL_SERVER_ERROR
	}

	operator, err := db.DB.GetProviderByTerminal(w.TerminalId)
	if err != nil {
		internal_utils.LogError("bank-integration-api", err)
		return nil, internal_utils.HTTP_INTERNAL_SERVER_ERROR
	}

	client := provider.PROVIDER_CLIENTS[operator.Name]
	if client == nil {
		internal_utils.LogError("bank-integration-api", errors.New("no provider client registered for provider "+operator.Name))
		return nil, internal_utils.HTTP_INTERNAL_SERVER_ERROR
	}

	if amount, err := internal_utils.ToAmount(w.Amount); err != nil {
		internal_utils.LogError("bank-integration-api", err)
		return nil, internal_utils.HTTP_INTERNAL_SERVER_ERROR
	} else {
		if fees, err := internal_utils.ToAmount(w.TerminalFees); err != nil {
			internal_utils.LogError("bank-integration-api", err)
			return nil, internal_utils.HTTP_INTERNAL_SERVER_ERROR
		} else {
			withdrawalStatusBytes, err := internal_utils.NewJsonCodec[BankWithdrawalOperationStatus]().EncodeToBytes(&BankWithdrawalOperationStatus{
				Status:            w.WithdrawalStatus,
				Amount:            internal_utils.FormatAmount(amount, config.CONFIG.Server.CurrencyFractionDigits),
				CardFees:          internal_utils.FormatAmount(fees, config.CONFIG.Server.CurrencyFractionDigits),
				SenderWire:        client.FormatPayto(w),
				WireTypes:         []string{operator.PaytoTargetType, "iban"},
				ReservePubKey:     internal_utils.EddsaPublicKey((internal_utils.TalerBinaryEncode(w.ReservePubKey))),
				SuggestedExchange: config.CONFIG.Server.ExchangeBaseUrl,
				RequiredExchange:  config.CONFIG.Server.ExchangeBaseUrl,
				Aborted:           w.WithdrawalStatus == internal_utils.ABORTED,
				SelectionDone:     w.WithdrawalStatus == internal_utils.SELECTED,
				TransferDone:      w.WithdrawalStatus == internal_utils.CONFIRMED,
			})
			if err != nil {
				internal_utils.LogError("bank-integration-api", err)
				return nil, internal_utils.HTTP_INTERNAL_SERVER_ERROR
			}
			return withdrawalStatusBytes, internal_utils.HTTP_OK
		}
	}
}
