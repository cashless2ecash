// This file is part of taler-cashless2ecash.
// Copyright (C) 2024 Joel Häberli
//
// taler-cashless2ecash is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as published
// by the Free Software Foundation, either version 3 of the License,
// or (at your option) any later version.
//
// taler-cashless2ecash is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// SPDX-License-Identifier: AGPL3.0-or-later

package internal_api

import (
	"bytes"
	internal_utils "c2ec/internal/utils"
	"c2ec/pkg/config"
	"c2ec/pkg/db"
	"c2ec/pkg/provider"
	"errors"
	"fmt"
	"log"
	"net/http"
	"strconv"
	"time"
)

const INCOMING_RESERVE_TRANSACTION_TYPE = "RESERVE"

// https://docs.taler.net/core/api-bank-wire.html#tsref-type-WireConfig
type WireConfig struct {
	Name           string `json:"name"`
	Version        string `json:"version"`
	Currency       string `json:"currency"`
	Implementation string `json:"implementation"`
}

// https://docs.taler.net/core/api-bank-wire.html#tsref-type-TransferRequest
type TransferRequest struct {
	RequestUid      string `json:"request_uid"`
	Amount          string `json:"amount"`
	ExchangeBaseUrl string `json:"exchange_base_url"`
	Wtid            string `json:"wtid"`
	CreditAccount   string `json:"credit_account"`
}

// https://docs.taler.net/core/api-bank-wire.html#tsref-type-TransferResponse
type TransferResponse struct {
	Timestamp internal_utils.Timestamp `json:"timestamp"`
	RowId     int                      `json:"row_id"`
}

// https://docs.taler.net/core/api-bank-wire.html#tsref-type-IncomingHistory
type IncomingHistory struct {
	IncomingTransactions []IncomingReserveTransaction `json:"incoming_transactions"`
	CreditAccount        string                       `json:"credit_account"`
}

// type RESERVE | https://docs.taler.net/core/api-bank-wire.html#tsref-type-IncomingReserveTransaction
type IncomingReserveTransaction struct {
	Type         string                   `json:"type"`
	RowId        int                      `json:"row_id"`
	Date         internal_utils.Timestamp `json:"date"`
	Amount       string                   `json:"amount"`
	DebitAccount string                   `json:"debit_account"`
	ReservePub   string                   `json:"reserve_pub"`
}

type OutgoingHistory struct {
	OutgoingTransactions []*OutgoingBankTransaction `json:"outgoing_transactions"`
	DebitAccount         string                     `json:"debit_account"`
}

type OutgoingBankTransaction struct {
	RowId           uint64                       `json:"row_id"`
	Date            internal_utils.Timestamp     `json:"date"`
	Amount          string                       `json:"amount"`
	CreditAccount   string                       `json:"credit_account"`
	Wtid            internal_utils.ShortHashCode `json:"wtid"`
	ExchangeBaseUrl string                       `json:"exchange_base_url"`
}

func NewIncomingReserveTransaction(w *db.Withdrawal) *IncomingReserveTransaction {

	if w == nil {
		internal_utils.LogWarn("wire-gateway", "the withdrawal was nil")
		return nil
	}

	prvdr, err := db.DB.GetProviderByTerminal(w.TerminalId)
	if err != nil {
		internal_utils.LogError("wire-gateway", err)
		return nil
	}

	client := provider.PROVIDER_CLIENTS[prvdr.Name]
	if client == nil {
		internal_utils.LogError("wire-gateway", errors.New("no provider client with name="+prvdr.Name))
		return nil
	}

	t := new(IncomingReserveTransaction)
	a, err := internal_utils.ToAmount(w.Amount)
	if err != nil {
		internal_utils.LogError("wire-gateway", err)
		return nil
	}
	t.Amount = internal_utils.FormatAmount(a, config.CONFIG.Server.CurrencyFractionDigits)
	t.Date = internal_utils.Timestamp{
		Ts: int(w.RegistrationTs),
	}
	t.DebitAccount = client.FormatPayto(w)
	t.ReservePub = internal_utils.FormatEddsaPubKey(w.ReservePubKey)
	if w.ConfirmedRowId == nil {
		internal_utils.LogError("wire-gateway", fmt.Errorf("expected non-nil confirmed_row_id for withdrawal_row_id=%d", w.WithdrawalRowId))
		return nil
	}
	t.RowId = int(*w.ConfirmedRowId)
	t.Type = INCOMING_RESERVE_TRANSACTION_TYPE
	return t
}

func NewOutgoingBankTransaction(tr *db.Transfer) *OutgoingBankTransaction {
	t := new(OutgoingBankTransaction)
	a, err := internal_utils.ToAmount(tr.Amount)
	if err != nil {
		internal_utils.LogError("wire-gateway", err)
		return nil
	}
	t.Amount = internal_utils.FormatAmount(a, config.CONFIG.Server.CurrencyFractionDigits)
	t.Date = internal_utils.Timestamp{
		Ts: int(tr.TransferTs),
	}
	t.CreditAccount = tr.CreditAccount
	t.ExchangeBaseUrl = tr.ExchangeBaseUrl
	if tr.TransferredRowId == nil {
		internal_utils.LogError("wire-gateway", fmt.Errorf("expected non-nil transferred_row_id for row_id=%d", tr.RowId))
		return nil
	}
	t.RowId = uint64(*tr.TransferredRowId)
	t.Wtid = internal_utils.ShortHashCode(tr.Wtid)
	return t
}

func WireGatewayConfig(res http.ResponseWriter, req *http.Request) {

	cfg := WireConfig{
		Name:           "taler-wire-gateway",
		Currency:       config.CONFIG.Server.Currency,
		Version:        "0:0:1",
		Implementation: "",
	}

	serializedCfg, err := internal_utils.NewJsonCodec[WireConfig]().EncodeToBytes(&cfg)
	if err != nil {
		log.Default().Printf("failed serializing config: %s", err.Error())
		internal_utils.SetLastResponseCodeForLogger(internal_utils.HTTP_INTERNAL_SERVER_ERROR)
		res.WriteHeader(internal_utils.HTTP_INTERNAL_SERVER_ERROR)
		return
	}

	internal_utils.SetLastResponseCodeForLogger(internal_utils.HTTP_OK)
	res.WriteHeader(internal_utils.HTTP_OK)
	res.Write(serializedCfg)
}

func Transfer(res http.ResponseWriter, req *http.Request) {

	auth := AuthenticateWirewatcher(req)
	if !auth {
		internal_utils.SetLastResponseCodeForLogger(internal_utils.HTTP_UNAUTHORIZED)
		res.WriteHeader(internal_utils.HTTP_UNAUTHORIZED)
		return
	}

	jsonCodec := internal_utils.NewJsonCodec[TransferRequest]()
	transfer, err := internal_utils.ReadStructFromBody[TransferRequest](req, jsonCodec)
	if err != nil {
		internal_utils.LogError("wire-gateway-api", err)
		internal_utils.SetLastResponseCodeForLogger(internal_utils.HTTP_BAD_REQUEST)
		res.WriteHeader(internal_utils.HTTP_BAD_REQUEST)
		return
	}

	if transfer.Amount == "" || transfer.CreditAccount == "" || transfer.RequestUid == "" {
		internal_utils.LogError("wire-gateway-api", errors.New("invalid request"))
		internal_utils.SetLastResponseCodeForLogger(internal_utils.HTTP_BAD_REQUEST)
		res.WriteHeader(internal_utils.HTTP_BAD_REQUEST)
		return
	}

	paytoTargetType, tid, err := internal_utils.ParsePaytoUri(transfer.CreditAccount)
	internal_utils.LogInfo("wire-gateway-api", fmt.Sprintf("parsed payto-target-type='%s'", paytoTargetType))
	if err != nil {
		internal_utils.LogError("wire-gateway-api", err)
		internal_utils.SetLastResponseCodeForLogger(internal_utils.HTTP_BAD_REQUEST)
		res.WriteHeader(internal_utils.HTTP_BAD_REQUEST)
		return
	}

	p, err := db.DB.GetTerminalProviderByPaytoTargetType(paytoTargetType)
	if err != nil {
		internal_utils.LogWarn("wire-gateway-api", "unable to find provider for provider-target-type="+paytoTargetType)
		internal_utils.LogError("wire-gateway-api", err)
		internal_utils.SetLastResponseCodeForLogger(internal_utils.HTTP_INTERNAL_SERVER_ERROR)
		res.WriteHeader(internal_utils.HTTP_INTERNAL_SERVER_ERROR)
		return
	}

	decodedRequestUid := bytes.NewBufferString(transfer.RequestUid).Bytes()
	t, err := db.DB.GetTransferById(decodedRequestUid)
	if err != nil {
		internal_utils.LogWarn("wire-gateway-api", "failed retrieving transfer for requestUid="+transfer.RequestUid)
		internal_utils.LogError("wire-gateway-api", err)
		internal_utils.SetLastResponseCodeForLogger(internal_utils.HTTP_INTERNAL_SERVER_ERROR)
		res.WriteHeader(internal_utils.HTTP_INTERNAL_SERVER_ERROR)
		return
	}

	if t == nil {

		// limitation: currently only full refunds are implemented.
		//   this means that we also check that no other transaction
		//   to the same recipient with this credit_account is present.
		transfers, err := db.DB.GetTransfersByCreditAccount(transfer.CreditAccount)
		if err != nil {
			internal_utils.LogWarn("wire-gateway-api", "looking for transfers with the credit account failed")
			internal_utils.LogError("wire-gateway-api", err)
			internal_utils.SetLastResponseCodeForLogger(internal_utils.HTTP_INTERNAL_SERVER_ERROR)
			res.WriteHeader(internal_utils.HTTP_INTERNAL_SERVER_ERROR)
			return
		}

		if len(transfers) > 0 {
			//   when the withdrawal was already refunded we act like everything is
			//   ok, because the transfer was registered earlier and the customer
			//   will get their money back (or already have). The Exchange will
			//	 not loose money on the other hand because the refund is done twice.
			internal_utils.LogWarn("wire-gateway-api", "full refunds only limitation")
			internal_utils.LogError("wire-gateway-api", fmt.Errorf("currently only full refunds are supported. Withdrawal %s already refunded", transfer.CreditAccount))
			internal_utils.SetLastResponseCodeForLogger(internal_utils.HTTP_OK)
			res.WriteHeader(internal_utils.HTTP_OK)
			return
		}

		// no transfer for this request_id -> generate new
		amount, err := internal_utils.ParseAmount(transfer.Amount, config.CONFIG.Server.CurrencyFractionDigits)
		if err != nil {
			internal_utils.LogWarn("wire-gateway-api", "failed parsing amount")
			internal_utils.LogError("wire-gateway-api", err)
			internal_utils.SetLastResponseCodeForLogger(internal_utils.HTTP_INTERNAL_SERVER_ERROR)
			res.WriteHeader(internal_utils.HTTP_INTERNAL_SERVER_ERROR)
			return
		}
		err = db.DB.AddTransfer(
			decodedRequestUid,
			amount,
			transfer.ExchangeBaseUrl,
			string(transfer.Wtid),
			transfer.CreditAccount,
			time.Now(),
		)
		if err != nil {
			internal_utils.LogWarn("wire-gateway-api", "failed adding new transfer entry to database")
			internal_utils.LogError("wire-gateway-api", err)
			internal_utils.SetLastResponseCodeForLogger(internal_utils.HTTP_INTERNAL_SERVER_ERROR)
			res.WriteHeader(internal_utils.HTTP_INTERNAL_SERVER_ERROR)
			return
		}
	} else {

		// check that the wanted provider is configured.
		refundClient := provider.PROVIDER_CLIENTS[p.Name]
		if refundClient == nil {
			internal_utils.LogError("wire-gateway-api", errors.New("client for provider "+p.Name+" not initialized"))
			internal_utils.SetLastResponseCodeForLogger(internal_utils.HTTP_INTERNAL_SERVER_ERROR)
			res.WriteHeader(internal_utils.HTTP_INTERNAL_SERVER_ERROR)
			return
		}

		// the transfer is only processed if the body matches.
		ta, err := internal_utils.ToAmount(t.Amount)
		if err != nil {
			internal_utils.LogError("wire-gateway-api", err)
			internal_utils.SetLastResponseCodeForLogger(internal_utils.HTTP_INTERNAL_SERVER_ERROR)
			res.WriteHeader(internal_utils.HTTP_INTERNAL_SERVER_ERROR)
			return
		}
		if transfer.Amount != internal_utils.FormatAmount(ta, config.CONFIG.Server.CurrencyFractionDigits) ||
			transfer.ExchangeBaseUrl != t.ExchangeBaseUrl ||
			transfer.Wtid != t.Wtid ||
			transfer.CreditAccount != t.CreditAccount {

			internal_utils.LogWarn("wire-gateway-api", "idempotency violation")
			internal_utils.SetLastResponseCodeForLogger(internal_utils.HTTP_CONFLICT)
			res.WriteHeader(internal_utils.HTTP_CONFLICT)
			return
		}

		w, err := db.DB.GetWithdrawalByProviderTransactionId(tid)
		if err != nil || w == nil {
			internal_utils.LogWarn("wire-gateway-api", "unable to find withdrawal with given provider transaction id")
			internal_utils.LogError("wire-gateway-api", err)
			internal_utils.SetLastResponseCodeForLogger(internal_utils.HTTP_INTERNAL_SERVER_ERROR)
			res.WriteHeader(internal_utils.HTTP_INTERNAL_SERVER_ERROR)
			return
		}
	}
	internal_utils.SetLastResponseCodeForLogger(internal_utils.HTTP_OK)
}

// :query start: *Optional.*
//
//	Row identifier to explicitly set the *starting point* of the query.
//
// :query delta:
//
//	The *delta* value that determines the range of the query.
//
// :query long_poll_ms: *Optional.*
//
//	If this parameter is specified and the result of the query would be empty,
//	the bank will wait up to ``long_poll_ms`` milliseconds for new transactions
//	that match the query to arrive and only then send the HTTP response.
//	A client must never rely on this behavior, as the bank may return a response
//	immediately or after waiting only a fraction of ``long_poll_ms``.
func HistoryIncoming(res http.ResponseWriter, req *http.Request) {

	auth := AuthenticateWirewatcher(req)
	if !auth {
		internal_utils.SetLastResponseCodeForLogger(internal_utils.HTTP_UNAUTHORIZED)
		res.WriteHeader(internal_utils.HTTP_UNAUTHORIZED)
		return
	}

	// read and validate request query parameters
	timeOfReq := time.Now()
	shouldStartLongPoll := true
	var longPollMilli int
	if longPollMilliPtr, accepted := internal_utils.AcceptOptionalParamOrWriteResponse(
		"long_poll_ms", strconv.Atoi, req, res,
	); accepted {
		if longPollMilliPtr != nil {
			longPollMilli = *longPollMilliPtr
		} else {
			// this means parameter was not given.
			// no long polling (simple get)
			shouldStartLongPoll = false
		}
	}

	var start = 0 // read most recent entries by default
	if startPtr, accepted := internal_utils.AcceptOptionalParamOrWriteResponse(
		"start", strconv.Atoi, req, res,
	); accepted {
		if startPtr != nil {
			start = *startPtr
		}
	} else {
		res.Header().Add(internal_utils.CONTENT_TYPE_HEADER, "application/json")
		internal_utils.LogWarn("wire-gateway-api", "invalid parameter")
		return
	}

	var delta = 0
	if deltaPtr, accepted := internal_utils.AcceptOptionalParamOrWriteResponse(
		"delta", strconv.Atoi, req, res,
	); accepted {
		if deltaPtr != nil {
			delta = *deltaPtr
		}
	} else {
		res.Header().Add(internal_utils.CONTENT_TYPE_HEADER, "application/json")
		internal_utils.LogWarn("wire-gateway-api", "invalid parameter")
		return
	}

	if delta == 0 {
		delta = 10
	}

	if shouldStartLongPoll {

		// this will just wait / block until the milliseconds are exceeded.
		time.Sleep(time.Duration(longPollMilli) * time.Millisecond)
	}

	withdrawals, err := db.DB.GetConfirmedWithdrawals(start, delta, timeOfReq)

	if err != nil {
		internal_utils.LogError("wire-gateway-api", err)
		internal_utils.SetLastResponseCodeForLogger(internal_utils.HTTP_INTERNAL_SERVER_ERROR)
		res.WriteHeader(internal_utils.HTTP_INTERNAL_SERVER_ERROR)
		return
	}

	if len(withdrawals) < 1 {
		internal_utils.SetLastResponseCodeForLogger(internal_utils.HTTP_NO_CONTENT)
		res.WriteHeader(internal_utils.HTTP_NO_CONTENT)
		return
	}

	transactions := make([]IncomingReserveTransaction, 0)
	for _, w := range withdrawals {
		if w.Amount.Val == 0 && w.Amount.Frac == 0 {
			internal_utils.LogInfo("wire-gateway-api", "ignoring zero amount withdrawal")
			continue
		}
		if w.ReservePubKey == nil || len(w.ReservePubKey) == 0 {
			internal_utils.LogWarn("wire-gateway-api", "ignoring confirmed withdrawal with no reserve public key (probably a test transaction)")
			continue
		}
		transaction := NewIncomingReserveTransaction(w)
		if transaction != nil {
			transactions = append(transactions, *transaction)
		}
	}

	hist := IncomingHistory{
		IncomingTransactions: transactions,
		CreditAccount:        config.CONFIG.Server.CreditAccount,
	}

	encoder := internal_utils.NewJsonCodec[IncomingHistory]()
	enc, err := encoder.EncodeToBytes(&hist)
	if err != nil {
		internal_utils.LogError("wire-gateway-api", err)
		internal_utils.SetLastResponseCodeForLogger(internal_utils.HTTP_INTERNAL_SERVER_ERROR)
		res.WriteHeader(internal_utils.HTTP_INTERNAL_SERVER_ERROR)
		return
	}

	res.Header().Add(internal_utils.CONTENT_TYPE_HEADER, encoder.HttpApplicationContentHeader())
	internal_utils.SetLastResponseCodeForLogger(internal_utils.HTTP_OK)
	res.WriteHeader(internal_utils.HTTP_OK)
	res.Write(enc)
}

func HistoryOutgoing(res http.ResponseWriter, req *http.Request) {

	auth := AuthenticateWirewatcher(req)
	if !auth {
		internal_utils.SetLastResponseCodeForLogger(internal_utils.HTTP_UNAUTHORIZED)
		res.WriteHeader(internal_utils.HTTP_UNAUTHORIZED)
		return
	}

	// read and validate request query parameters
	timeOfReq := time.Now()
	shouldStartLongPoll := true
	var longPollMilli int
	if longPollMilliPtr, accepted := internal_utils.AcceptOptionalParamOrWriteResponse(
		"long_poll_ms", strconv.Atoi, req, res,
	); accepted {
	} else {
		if longPollMilliPtr != nil {
			longPollMilli = *longPollMilliPtr
		} else {
			// this means parameter was not given.
			// no long polling (simple get)
			shouldStartLongPoll = false
		}
	}

	var start int
	if startPtr, accepted := internal_utils.AcceptOptionalParamOrWriteResponse(
		"start", strconv.Atoi, req, res,
	); accepted {
	} else {
		if startPtr != nil {
			start = *startPtr
		}
	}

	var delta int
	if deltaPtr, accepted := internal_utils.AcceptOptionalParamOrWriteResponse(
		"delta", strconv.Atoi, req, res,
	); accepted {
	} else {
		if deltaPtr != nil {
			delta = *deltaPtr
		}
	}

	if delta == 0 {
		delta = 10
	}

	if shouldStartLongPoll {

		// this will just wait / block until the milliseconds are exceeded.
		time.Sleep(time.Duration(longPollMilli) * time.Millisecond)
	}

	transfers, err := db.DB.GetTransfers(start, delta, timeOfReq)

	if err != nil {
		internal_utils.LogError("wire-gateway-api", err)
		internal_utils.SetLastResponseCodeForLogger(internal_utils.HTTP_INTERNAL_SERVER_ERROR)
		res.WriteHeader(internal_utils.HTTP_INTERNAL_SERVER_ERROR)
		return
	}

	filtered := make([]*db.Transfer, 0)
	for _, t := range transfers {
		if t.Status == 0 {
			// only consider transfer which were successful
			filtered = append(filtered, t)
		}
	}

	if len(filtered) < 1 {
		internal_utils.SetLastResponseCodeForLogger(internal_utils.HTTP_NO_CONTENT)
		res.WriteHeader(internal_utils.HTTP_NO_CONTENT)
		return
	}

	transactions := make([]*OutgoingBankTransaction, len(filtered))
	for _, t := range filtered {
		transactions = append(transactions, NewOutgoingBankTransaction(t))
	}
	transactions = internal_utils.RemoveNulls(transactions)

	outgoingHistory := OutgoingHistory{
		OutgoingTransactions: transactions,
		DebitAccount:         config.CONFIG.Server.CreditAccount,
	}
	encoder := internal_utils.NewJsonCodec[OutgoingHistory]()
	enc, err := encoder.EncodeToBytes(&outgoingHistory)
	if err != nil {
		internal_utils.LogError("wire-gateway-api", err)
		internal_utils.SetLastResponseCodeForLogger(internal_utils.HTTP_INTERNAL_SERVER_ERROR)
		res.WriteHeader(internal_utils.HTTP_INTERNAL_SERVER_ERROR)
		return
	}

	res.Header().Add(internal_utils.CONTENT_TYPE_HEADER, encoder.HttpApplicationContentHeader())
	internal_utils.SetLastResponseCodeForLogger(internal_utils.HTTP_OK)
	res.WriteHeader(internal_utils.HTTP_OK)
	res.Write(enc)
}

// This method is currently dead and implemented for API conformance
func AdminAddIncoming(res http.ResponseWriter, req *http.Request) {

	// not implemented, because not used
	internal_utils.SetLastResponseCodeForLogger(internal_utils.HTTP_NOT_IMPLEMENTED)
	res.WriteHeader(internal_utils.HTTP_NOT_IMPLEMENTED)
}
