package internal_api

import (
	"c2ec/pkg/config"
	"net/http"
)

func Agpl(res http.ResponseWriter, req *http.Request) {

	res.Header().Set("Location", config.CONFIG.Server.Source)
	res.WriteHeader(301)
}
