// This file is part of taler-cashless2ecash.
// Copyright (C) 2024 Joel Häberli
//
// taler-cashless2ecash is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as published
// by the Free Software Foundation, either version 3 of the License,
// or (at your option) any later version.
//
// taler-cashless2ecash is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// SPDX-License-Identifier: AGPL3.0-or-later

package config

import (
	"errors"
	"fmt"
	"math"
	"os"
	"strconv"
	"strings"

	"gopkg.in/ini.v1"
	"gopkg.in/yaml.v3"
)

var CONFIG C2ECConfig

type C2ECConfig struct {
	Server    C2ECServerConfig     `yaml:"c2ec"`
	Database  C2ECDatabseConfig    `yaml:"db"`
	Providers []C2ECProviderConfig `yaml:"providers"`
}

type C2ECServerConfig struct {
	Source                 string                `yaml:"source"`
	IsProd                 bool                  `yaml:"prod"`
	Host                   string                `yaml:"host"`
	Port                   int                   `yaml:"port"`
	UseUnixDomainSocket    bool                  `yaml:"unix-domain-socket"`
	UnixSocketPath         string                `yaml:"unix-socket-path"`
	UnixPathMode           int                   `yaml:"unix-path-mode"`
	StrictAttestors        bool                  `yaml:"fail-on-missing-attestors"`
	ExchangeBaseUrl        string                `yaml:"exchange-base-url"`
	CreditAccount          string                `yaml:"credit-account"`
	Currency               string                `yaml:"currency"`
	CurrencyFractionDigits int                   `yaml:"currency-fraction-digits"`
	WithdrawalFees         string                `yaml:"withdrawal-fees"`
	MaxRetries             int32                 `yaml:"max-retries"`
	RetryDelayMs           int                   `yaml:"retry-delay-ms"`
	WireGateway            C2ECWireGatewayConfig `yaml:"wire-gateway"`
}

type C2ECWireGatewayConfig struct {
	Username string `yaml:"username"`
	Password string `yaml:"password"`
}

type C2ECDatabseConfig struct {
	ConnectionString string `yaml:"connstr"`
	Host             string `yaml:"host"`
	Port             int    `yaml:"port"`
	Username         string `yaml:"username"`
	Password         string `yaml:"password"`
	Database         string `yaml:"database"`
}

type C2ECProviderConfig struct {
	Name string `yaml:"name"`
	Key  string `yaml:"key"`
}

func Parse(path string) (*C2ECConfig, error) {

	f, err := os.Open(path)
	if err != nil {
		return nil, err
	}
	defer f.Close()

	stat, err := f.Stat()
	if err != nil {
		return nil, err
	}

	content := make([]byte, stat.Size())
	_, err = f.Read(content)
	if err != nil {
		return nil, err
	}

	if strings.HasSuffix(path, ".yml") || strings.HasSuffix(path, ".yaml") {
		cfg := new(C2ECConfig)
		err = yaml.Unmarshal(content, cfg)
		if err != nil {
			return nil, err
		}
		return cfg, nil
	}

	cfg, err := ParseIni(content)
	if err != nil {
		return nil, err
	}

	a, err := parseAmount(cfg.Server.WithdrawalFees, cfg.Server.CurrencyFractionDigits)
	if err != nil {
		panic("invalid withdrawal fees amount")
	}
	if !strings.EqualFold(a.Currency, cfg.Server.Currency) {
		panic("withdrawal fees currency must be same as the specified currency")
	}

	return cfg, nil
}

func ConfigForProvider(name string) (*C2ECProviderConfig, error) {

	for _, provider := range CONFIG.Providers {

		if provider.Name == name {
			return &provider, nil
		}
	}
	return nil, errors.New("no such provider")
}

func ParseIni(content []byte) (*C2ECConfig, error) {

	ini, err := ini.Load(content)
	if err != nil {
		return nil, err
	}

	cfg := new(C2ECConfig)
	for _, s := range ini.Sections() {

		if s.Name() == "c2ec" {

			value, err := s.GetKey("SOURCE")
			if err != nil {
				return nil, err
			}
			cfg.Server.Source = value.String()

			value, err = s.GetKey("PROD")
			if err != nil {
				return nil, err
			}

			cfg.Server.IsProd, err = value.Bool()
			if err != nil {
				return nil, err
			}

			value, err = s.GetKey("SERVE")
			if err != nil {
				return nil, err
			}

			str := value.String()
			cfg.Server.UseUnixDomainSocket = str == "unix"

			value, err = s.GetKey("HOST")
			if err != nil {
				return nil, err
			}

			cfg.Server.Host = value.String()

			value, err = s.GetKey("PORT")
			if err != nil {
				return nil, err
			}

			cfg.Server.Port, err = value.Int()
			if err != nil {
				return nil, err
			}

			value, err = s.GetKey("UNIXPATH")
			if err != nil {
				return nil, err
			}

			cfg.Server.UnixSocketPath = value.String()

			value, err = s.GetKey("UNIXPATH_MODE")
			if err != nil {
				return nil, err
			}

			cfg.Server.UnixSocketPath = value.String()

			value, err = s.GetKey("FAIL_ON_MISSING_ATTESTORS")
			if err != nil {
				return nil, err
			}
			cfg.Server.StrictAttestors, err = value.Bool()
			if err != nil {
				return nil, err
			}

			value, err = s.GetKey("EXCHANGE_BASE_URL")
			if err != nil {
				return nil, err
			}
			cfg.Server.ExchangeBaseUrl = value.String()

			value, err = s.GetKey("EXCHANGE_ACCOUNT")
			if err != nil {
				return nil, err
			}
			cfg.Server.CreditAccount = value.String()

			value, err = s.GetKey("CURRENCY")
			if err != nil {
				return nil, err
			}
			cfg.Server.Currency = value.String()

			value, err = s.GetKey("CURRENCY_FRACTION_DIGITS")
			if err != nil {
				return nil, err
			}
			num, err := value.Int()
			if err != nil {
				return nil, err
			}
			cfg.Server.CurrencyFractionDigits = num

			value, err = s.GetKey("WITHDRAWAL_FEES")
			if err != nil {
				return nil, err
			}
			cfg.Server.WithdrawalFees = value.String()

			value, err = s.GetKey("MAX_RETRIES")
			if err != nil {
				return nil, err
			}

			num, err = value.Int()
			if err != nil {
				return nil, err
			}
			cfg.Server.MaxRetries = int32(num)

			value, err = s.GetKey("RETRY_DELAY_MS")
			if err != nil {
				return nil, err
			}

			cfg.Server.RetryDelayMs, err = value.Int()
			if err != nil {
				return nil, err
			}

		}

		if s.Name() == "wire-gateway" {

			value, err := s.GetKey("USERNAME")
			if err != nil {
				return nil, err
			}
			cfg.Server.WireGateway.Username = value.String()

			value, err = s.GetKey("PASSWORD")
			if err != nil {
				return nil, err
			}
			cfg.Server.WireGateway.Password = value.String()
		}

		if s.Name() == "database" {

			value, err := s.GetKey("CONFIG")
			if err != nil {
				return nil, err
			}

			connstr := value.String()

			cfg.Database.ConnectionString = connstr
		}

		if strings.HasPrefix(s.Name(), "provider-") {

			provider := C2ECProviderConfig{}

			value, err := s.GetKey("NAME")
			if err != nil {
				return nil, err
			}
			provider.Name = value.String()

			value, err = s.GetKey("KEY")
			if err != nil {
				return nil, err
			}
			provider.Key = value.String()

			cfg.Providers = append(cfg.Providers, provider)
		}
	}
	return cfg, nil
}

/*
	START

	COPIED FROM internal/utils/amount.go
	due to structuring issues. Must be resolved later.
*/

// The GNU Taler amount object
type amount struct {

	// The type of currency, e.g. EUR
	Currency string `json:"currency"`

	// The value (before the ".")
	Value uint64 `json:"value"`

	// The fraction (after the ".", optional)
	Fraction uint64 `json:"fraction"`
}

// Create a new amount from value and fraction in a currency
func newAmount(currency string, value uint64, fraction uint64) amount {
	return amount{
		Currency: currency,
		Value:    value,
		Fraction: fraction,
	}
}

// Parses an amount string in the format <currency>:<value>[.<fraction>]
func parseAmount(s string, fractionDigits int) (*amount, error) {

	if s == "" {
		return &amount{CONFIG.Server.Currency, 0, 0}, nil
	}

	if !strings.Contains(s, ":") {
		return nil, fmt.Errorf("invalid amount: %s", s)
	}

	currencyAndAmount := strings.Split(s, ":")
	if len(currencyAndAmount) != 2 {
		return nil, fmt.Errorf("invalid amount: %s", s)
	}

	currency := currencyAndAmount[0]
	valueAndFraction := strings.Split(currencyAndAmount[1], ".")
	if len(valueAndFraction) < 1 && len(valueAndFraction) > 2 {
		return nil, fmt.Errorf("invalid value and fraction part in amount %s", s)
	}
	value, err := strconv.Atoi(valueAndFraction[0])
	if err != nil {
		return nil, fmt.Errorf("invalid value in amount %s", s)
	}

	fraction := 0
	if len(valueAndFraction) == 2 {
		if len(valueAndFraction[1]) > fractionDigits {
			return nil, fmt.Errorf("invalid amount: %s expected at max %d fractional digits", s, fractionDigits)
		}
		k := 0
		if len(valueAndFraction[1]) < fractionDigits {
			k = fractionDigits - len(valueAndFraction[1])
		}
		fractionInt, err := strconv.Atoi(valueAndFraction[1])
		if err != nil {
			return nil, fmt.Errorf("invalid fraction in amount %s", s)
		}
		fraction = fractionInt * int(math.Pow10(k))
	}

	a := newAmount(currency, uint64(value), uint64(fraction))
	return &a, nil
}

/*
	END

	COPIED FROM internal/utils/amount.go
	due to structuring issues. Must be resolved later.
*/
