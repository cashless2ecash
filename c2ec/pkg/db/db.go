// This file is part of taler-cashless2ecash.
// Copyright (C) 2024 Joel Häberli
//
// taler-cashless2ecash is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as published
// by the Free Software Foundation, either version 3 of the License,
// or (at your option) any later version.
//
// taler-cashless2ecash is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// SPDX-License-Identifier: AGPL3.0-or-later

package db

import (
	internal_utils "c2ec/internal/utils"
	"context"
	"time"
)

var DB C2ECDatabase

type Provider struct {
	ProviderId         int64  `db:"provider_id"`
	Name               string `db:"name"`
	PaytoTargetType    string `db:"payto_target_type"`
	BackendBaseURL     string `db:"backend_base_url"`
	BackendCredentials string `db:"backend_credentials"`
}

type Terminal struct {
	TerminalId  int64  `db:"terminal_id"`
	AccessToken string `db:"access_token"`
	Active      bool   `db:"active"`
	Description string `db:"description"`
	ProviderId  int64  `db:"provider_id"`
}

type Withdrawal struct {
	WithdrawalRowId       uint64                                   `db:"withdrawal_row_id"`
	ConfirmedRowId        *uint64                                  `db:"confirmed_row_id"`
	RequestUid            string                                   `db:"request_uid"`
	Wopid                 []byte                                   `db:"wopid"`
	ReservePubKey         []byte                                   `db:"reserve_pub_key"`
	RegistrationTs        int64                                    `db:"registration_ts"`
	Amount                *internal_utils.TalerAmountCurrency      `db:"amount" scan:"follow"`
	SuggestedAmount       *internal_utils.TalerAmountCurrency      `db:"suggested_amount" scan:"follow"`
	TerminalFees          *internal_utils.TalerAmountCurrency      `db:"terminal_fees" scan:"follow"`
	WithdrawalStatus      internal_utils.WithdrawalOperationStatus `db:"withdrawal_status"`
	TerminalId            int                                      `db:"terminal_id"`
	ProviderTransactionId *string                                  `db:"provider_transaction_id"`
	LastRetryTs           *int64                                   `db:"last_retry_ts"`
	RetryCounter          int32                                    `db:"retry_counter"`
	CompletionProof       []byte                                   `db:"completion_proof"`
}

type Transfer struct {
	RowId            int                                 `db:"row_id"`
	TransferredRowId *int                                `db:"transferred_row_id"`
	RequestUid       []byte                              `db:"request_uid"`
	Amount           *internal_utils.TalerAmountCurrency `db:"amount"`
	ExchangeBaseUrl  string                              `db:"exchange_base_url"`
	Wtid             string                              `db:"wtid"`
	CreditAccount    string                              `db:"credit_account"`
	TransferTs       int64                               `db:"transfer_ts"`
	Status           int16                               `db:"transfer_status"`
	Retries          int16                               `db:"retries"`
}

type Notification struct {
	Channel string
	Payload string
}

// C2ECDatabase defines the operations which a
// C2EC compliant database interface must implement
// in order to be bound to the c2ec API.
type C2ECDatabase interface {
	// A terminal sets up a withdrawal
	// with this query.
	// This initiates the withdrawal.
	SetupWithdrawal(
		wopid []byte,
		suggestedAmount internal_utils.Amount,
		amount internal_utils.Amount,
		terminalId int,
		providerTransactionId string,
		terminalFees internal_utils.Amount,
		requestUid string,
	) error

	// Registers a reserve public key
	// belonging to the respective wopid.
	RegisterWithdrawalParameters(
		wopid []byte,
		resPubKey internal_utils.EddsaPublicKey,
	) error

	// Get the withdrawal associated with the given request uid.
	GetWithdrawalByRequestUid(requestUid string) (*Withdrawal, error)

	// Get the withdrawal associated with the given withdrawal identifier.
	GetWithdrawalById(withdrawalId int) (*Withdrawal, error)

	// Get the withdrawal associated with the given wopid.
	GetWithdrawalByWopid(wopid []byte) (*Withdrawal, error)

	// Get the withdrawal associated with the provider specific transaction id.
	GetWithdrawalByProviderTransactionId(tid string) (*Withdrawal, error)

	// When the terminal receives the notification of the
	// Provider, that the payment went through, this will
	// save the provider specific transaction id in the database
	NotifyPayment(
		wopid []byte,
		providerTransactionId string,
		terminalId int,
		fees internal_utils.Amount,
	) error

	// Returns all withdrawals which can be attested by
	// a provider backend. This means that the provider
	// specific transaction id was set and the status is
	// 'selected'. The attestor can then attest and finalise
	// the payments.
	GetWithdrawalsForConfirmation() ([]*Withdrawal, error)

	// When an confirmation (or fail message) could be
	// retrieved by the provider, the withdrawal can
	// be finalised storing the correct final state
	// and the proof of completion of the provider.
	FinaliseWithdrawal(
		withdrawalId int,
		confirmOrAbort internal_utils.WithdrawalOperationStatus,
		completionProof []byte,
	) error

	// Set retry will set the last_retry_ts field
	// on the database. A trigger will then start
	// the retry process. The timestamp must be a
	// unix timestamp
	SetLastRetry(withdrawalId int, lastRetryTsUnix int64) error

	// Sets the retry counter for the given withdrawal.
	SetRetryCounter(withdrawalId int, retryCounter int) error

	// The wire gateway allows the exchange to retrieve transactions
	// starting at a certain starting point up until a certain delta
	// if the delta is negative, previous transactions relative to the
	// starting point are considered. When start is negative, the latest
	// id shall be used as starting point.
	GetConfirmedWithdrawals(start int, delta int, since time.Time) ([]*Withdrawal, error)

	// Get the provider of a terminal by the terminals id
	GetProviderByTerminal(terminalId int) (*Provider, error)

	// Get a provider entry by its name
	GetTerminalProviderByName(name string) (*Provider, error)

	// Get a provider entry by its name
	GetTerminalProviderByPaytoTargetType(paytoTargetType string) (*Provider, error)

	// Get a terminal entry by its identifier
	GetTerminalById(id int) (*Terminal, error)

	// Returns the transfer for the given hashcode.
	GetTransferById(requestUid []byte) (*Transfer, error)

	// Inserts a new transfer into the database.
	AddTransfer(
		requestUid []byte,
		amount *internal_utils.Amount,
		exchangeBaseUrl string,
		wtid string,
		credit_account string,
		ts time.Time,
	) error

	// Updates the transfer, if retries is changed, the transfer will be
	// triggered again.
	UpdateTransfer(
		rowId int,
		requestUid []byte,
		timestamp int64,
		status int16,
		retries int16,
	) error

	// The wire gateway allows the exchange to retrieve transactions
	// starting at a certain starting point up until a certain delta
	// if the delta is negative, previous transactions relative to the
	// starting point are considered. When start is negative, the latest
	// id shall be used as starting point.
	GetTransfers(start int, delta int, since time.Time) ([]*Transfer, error)

	// Load all transfers asscociated with the same credit_account.
	// The query is used to control that the current limitation of
	// only allowing full refunds (partial refunds are currently not supported)
	// is not harmed. It is assumed that the credit_account is unique, which currently
	// is the case, because it depends on the WOPID of the respective
	// withdrawal. This query is part of the limitation to only allow
	// full refunds and not partial refunds. It might be possible to
	// remove this API when partial refunds are implemented.
	GetTransfersByCreditAccount(creditAccount string) ([]*Transfer, error)

	// Returns the transfer entries in the given state.
	// This can be used for retry operations.
	GetTransfersByState(status int) ([]*Transfer, error)

	// A listener can listen for notifications ont the specified
	// channel. Returns a listen function, which must be called
	// by the caller to start listening on the channel. The returned
	// listen function will return an error if it fails, and takes
	// a context as argument which allows the underneath implementation
	// to control the execution context of the listener.
	NewListener(
		channel string,
		out chan *Notification,
	) (func(context.Context) error, error)
}
