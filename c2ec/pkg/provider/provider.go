// This file is part of taler-cashless2ecash.
// Copyright (C) 2024 Joel Häberli
//
// taler-cashless2ecash is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as published
// by the Free Software Foundation, either version 3 of the License,
// or (at your option) any later version.
//
// taler-cashless2ecash is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// SPDX-License-Identifier: AGPL3.0-or-later

package provider

import "c2ec/pkg/db"

// This map contains all clients initialized during the
// startup of the application. The clients SHALL register
// themselfs during the setup!!
var PROVIDER_CLIENTS = map[string]ProviderClient{}

type ProviderTransaction interface {
	AllowWithdrawal() bool
	AbortWithdrawal() bool
	Confirm(w *db.Withdrawal) error
	Bytes() []byte
}

type ProviderClient interface {
	SetupClient(provider *db.Provider) error
	GetTransaction(transactionId string) (ProviderTransaction, error)
	Refund(transactionId string) error
	FormatPayto(w *db.Withdrawal) string
}
