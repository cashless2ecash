module c2ec

go 1.22.0

require (
	internal/api v1.0.0
	internal/postgres v1.0.0
	internal/proc v1.0.0
	internal/provider v1.0.0
	internal/utils v1.0.0

	pkg/config v1.0.0
	pkg/db v1.0.0
	pkg/encoding v1.0.0
	pkg/provider v1.0.0

	github.com/jackc/pgx/v5 v5.5.5
	github.com/jackc/pgxlisten v0.0.0-20230728233309-2632bad3185a
	golang.org/x/crypto v0.22.0
	gopkg.in/ini.v1 v1.67.0
	gopkg.in/yaml.v3 v3.0.1
	gotest.tools/v3 v3.5.1
)

replace (
	internal/api => ./internal/api
	internal/postgres => ./internal/db/postgres
	internal/proc => ./internal/proc
	internal/provider => ./internal/provider
	internal/utils => ./internal/utils

	pkg/config => ./pkg/config
	pkg/db => ./pkg/db
	pkg/encoding => ./pkg/encoding
	pkg/provider => ./pkg/provider
)

require (
	github.com/google/go-cmp v0.5.9 // indirect
	github.com/jackc/pgpassfile v1.0.0 // indirect
	github.com/jackc/pgservicefile v0.0.0-20221227161230-091c0ba34f0a // indirect
	github.com/jackc/puddle/v2 v2.2.1 // indirect
	github.com/rogpeppe/go-internal v1.6.1 // indirect
	golang.org/x/sync v0.1.0 // indirect
	golang.org/x/sys v0.19.0 // indirect
	golang.org/x/text v0.14.0 // indirect
)
