// This file is part of taler-cashless2ecash.
// Copyright (C) 2024 Joel Häberli
//
// taler-cashless2ecash is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as published
// by the Free Software Foundation, either version 3 of the License,
// or (at your option) any later version.
//
// taler-cashless2ecash is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// SPDX-License-Identifier: AGPL3.0-or-later

package main

import (
	"c2ec/internal"
	utils "c2ec/internal/utils"
	"c2ec/pkg/config"
	"fmt"
	"os"
	"time"
)

const DEFAULT_C2EC_CONFIG_PATH = "./configs/c2ec-config.yaml" // "c2ec-config.conf"

// Starts the c2ec process.
// The program takes following arguments (ordered):
//  1. path to configuration file (.yaml | .ini style format) (optional)
//
// The startup follows these steps:
//  1. load configuration or panic
//  2. setup database or panic
//  3. setup provider clients
//  4. setup retrier
//  5. setup attestor
//  6. setup routes for the bank-integration-api
//  7. setup routes for the wire-gateway-api
//  8. listen for incoming requests (as specified in config)
func main() {

	cfgPath := DEFAULT_C2EC_CONFIG_PATH
	if len(os.Args) > 1 {

		nextIsConf := false
		for i, arg := range os.Args {
			if i == 0 {
				continue
			} else if nextIsConf {
				cfgPath = arg
				nextIsConf = false
			} else if arg == "-h" {
				helpAndExit("")
			} else if arg == "-c" {
				nextIsConf = true
			} else {
				helpAndExit(arg)
			}
		}
	}

	utils.LogInfo("main", fmt.Sprintf("starting c2ec at %s", time.Now().Format(time.UnixDate)))
	cfg, err := config.Parse(cfgPath)
	if err != nil {
		panic("unable to load config: " + err.Error())
	}
	if cfg == nil {
		panic("config is nil")
	}
	config.CONFIG = *cfg

	internal.C2EC()
}

func helpAndExit(unknownOptionOrEmpty string) {
	if unknownOptionOrEmpty != "" {
		fmt.Println("unkown option provided:", unknownOptionOrEmpty)
	}
	fmt.Println("usage: -h (help) | -c PATH (config file)")
	os.Exit(0)
}
