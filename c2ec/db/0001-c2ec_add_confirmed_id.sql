BEGIN;

SELECT _v.register_patch('0001-c2ec-add-confirmed-id', ARRAY['0001-c2ec-schema'], NULL);

SET search_path TO c2ec;

ALTER TABLE withdrawal
    ADD confirmed_row_id INT8 DEFAULT 0;
COMMENT ON COLUMN withdrawal.confirmed_row_id
  IS 'Used by the wire-gateway API do not mess up confirmed and unconfirmed transactions row ids.';

ALTER TABLE transfer
    ADD transferred_row_id INT8 DEFAULT 0;
COMMENT ON COLUMN transfer.transferred_row_id
  IS 'Used by the wire-gateway API do not mess up pending and succeeded transfers.';

UPDATE withdrawal SET confirmed_row_id=withdrawal_row_id WHERE withdrawal_status='confirmed';
UPDATE transfer SET transferred_row_id=row_id WHERE transfer_status=0;

COMMIT;