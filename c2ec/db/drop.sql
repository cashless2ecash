-- counter part to initializing / schema
BEGIN;

DROP SCHEMA IF EXISTS c2ec CASCADE;

DELETE FROM _v.patches WHERE patch_name IN (
    '0001-c2ec-schema',
    'proc-c2ec-payment-notification-listener',
    'proc-c2ec-retry-listener',
    'proc-c2ec-status-listener',
    'proc-c2ec-transfer-listener',
    '0001-c2ec-add-confirmed-id',
    'access'
);

DROP OWNED BY c2ec_operator;
DROP OWNED BY c2ec_api;
DROP OWNED BY c2ec_admin;

DROP ROLE IF EXISTS c2ec_operator;
DROP ROLE IF EXISTS c2ec_api;
DROP ROLE IF EXISTS c2ec_admin;

COMMIT;
