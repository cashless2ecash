BEGIN;

SELECT _v.register_patch('proc-c2ec-retry-listener', ARRAY['0001-c2ec-schema'], NULL);

SET search_path TO c2ec;

-- to create a function, the user needs USAGE privilege on arguments and return types
CREATE OR REPLACE FUNCTION emit_retry_notification() 
RETURNS TRIGGER AS $$
BEGIN
    PERFORM pg_notify('retry', '' || NEW.withdrawal_row_id);
    RETURN NULL;
END;
$$ LANGUAGE plpgsql;
COMMENT ON FUNCTION emit_retry_notification 
	IS 'The function emits the id of the withdrawal for which the last 
    retry timestamp was updated. This shall trigger a retry operation.
    How many retries are attempted is specified and handled by the application';

-- for creating a trigger the user must have TRIGGER pivilege on the table.
-- to execute the trigger, the user needs EXECUTE privilege on the trigger function.
CREATE OR REPLACE TRIGGER c2ec_retry_notify 
    AFTER UPDATE OF last_retry_ts
    ON withdrawal
    FOR EACH ROW
    EXECUTE FUNCTION emit_retry_notification();
COMMENT ON TRIGGER c2ec_retry_notify ON withdrawal
    IS 'After setting the last retry timestamp on the withdrawal,
    trigger the retry mechanism through the respective mechanism.';

COMMIT;
