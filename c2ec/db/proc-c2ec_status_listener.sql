BEGIN;

SELECT _v.register_patch('proc-c2ec-status-listener', ARRAY['0001-c2ec-schema'], NULL);

SET search_path TO c2ec;

-- to create a function, the user needs USAGE privilege on arguments and return types
CREATE OR REPLACE FUNCTION emit_withdrawal_status() 
RETURNS TRIGGER AS $$
BEGIN
    PERFORM pg_notify('w_' || encode(NEW.wopid::BYTEA, 'base64'), NEW.withdrawal_status::TEXT);
    RETURN NULL;
END;
$$ LANGUAGE plpgsql;
COMMENT ON FUNCTION emit_withdrawal_status 
	IS 'The function encodes the wopid in base64 and 
    sends a notification on the channel "w_{wopid}" 
    with the status in the payload.';

-- for creating a trigger the user must have TRIGGER pivilege on the table.
-- to execute the trigger, the user needs EXECUTE privilege on the trigger function.
CREATE OR REPLACE TRIGGER c2ec_withdrawal_created 
    AFTER INSERT
    ON withdrawal
    FOR EACH ROW
    EXECUTE FUNCTION emit_withdrawal_status();
COMMENT ON TRIGGER c2ec_withdrawal_created ON withdrawal
    IS 'After creation of the withdrawal entry a notification shall 
    be triggered using this trigger.';

CREATE OR REPLACE TRIGGER c2ec_withdrawal_status_changed 
    AFTER UPDATE OF withdrawal_status
    ON withdrawal
    FOR EACH ROW
    WHEN (OLD.withdrawal_status IS DISTINCT FROM NEW.withdrawal_status)
    EXECUTE FUNCTION emit_withdrawal_status();
COMMENT ON TRIGGER c2ec_withdrawal_status_changed ON withdrawal
    IS 'After the update of the status (only the status is of interest) 
    a notification shall be triggered using this trigger.';

COMMIT;
