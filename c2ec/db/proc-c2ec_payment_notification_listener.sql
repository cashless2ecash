BEGIN;

SELECT _v.register_patch('proc-c2ec-payment-notification-listener', ARRAY['0001-c2ec-schema'], NULL);

SET search_path TO c2ec;

-- to create a function, the user needs USAGE privilege on arguments and return types
CREATE OR REPLACE FUNCTION emit_payment_notification() 
RETURNS TRIGGER AS $$
DECLARE
    provider_name TEXT;
BEGIN
    SELECT p.name INTO provider_name FROM c2ec.provider AS p 
        LEFT JOIN c2ec.terminal AS t 
        ON t.provider_id = p.provider_id
        LEFT JOIN c2ec.withdrawal AS w
        ON t.terminal_id = NEW.terminal_id
        WHERE w.withdrawal_row_id = NEW.withdrawal_row_id;
    PERFORM pg_notify('payment_notification',
        provider_name || '|' ||
        NEW.withdrawal_row_id || '|' || 
        NEW.provider_transaction_id
    );
    RETURN NULL;
END;
$$ LANGUAGE plpgsql;
COMMENT ON FUNCTION emit_payment_notification 
	IS 'The function emits the name of the provider, row id of the withdrawal
    and the provider_transaction_id, on the channel "payment_notification".
    The format of the payload is as follows: 
    "{PROVIDER_NAME}|{WITHDRAWAL_ID}|{PROVIDER_TRANSACTION_ID}". The subscriber
    shall decide which confirmation process to use, based on the name of 
    the provider.';

-- for creating a trigger the user must have TRIGGER pivilege on the table.
-- to execute the trigger, the user needs EXECUTE privilege on the trigger function.
CREATE OR REPLACE TRIGGER c2ec_on_payment_notify 
    AFTER UPDATE OF provider_transaction_id
    ON withdrawal
    FOR EACH ROW    
    WHEN (NEW.provider_transaction_id IS NOT NULL)
    EXECUTE FUNCTION emit_payment_notification();
COMMENT ON TRIGGER c2ec_on_payment_notify ON withdrawal
    IS 'After setting the provider transaction id following a payment notification,
    trigger the emit to the respective channel.';

COMMIT;
