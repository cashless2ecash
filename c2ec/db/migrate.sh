#!/bin/bash

if [[ ($# -ne 3) && ($# -ne 6) ]]; then
    echo "Usage: $0 <db-username> <db-password> <db-name> [<new-db-admin-pw> <new-db-operator-pw> <new-db-api-pw>]"
    exit 1
fi

DB_USERNAME=$1
DB_PASSWORD=$2
DB_NAME=$3

ACCESS_WITH_PASSWORDS="./access-with-passwords.sql"
INITIAL_SETUP=0
if [ "$#" -eq 6 ]; then
    if [[ ($4 = $5) || ($5 = $6) || ($4 = $6) ]]; then
        echo "PROBLEM: passwords for db admin, operator and api user must be different..."
        echo "Usage: $0 <db-username> <db-password> <db-name> <source-root> <new-db-admin-pw> <new-db-admin-pw> <new-db-admin-pw>"
        exit 1
    fi

    ADMIN_PASSWORD=$4
    OPERATOR_PASSWORD=$5
    API_PASSWORD=$6

    cp "./access.sql" $ACCESS_WITH_PASSWORDS
    sed -i "s/ADMIN_PASSWORD/'$ADMIN_PASSWORD'/g" $ACCESS_WITH_PASSWORDS
    sed -i "s/OPERATOR_PASSWORD/'$OPERATOR_PASSWORD'/g" $ACCESS_WITH_PASSWORDS
    sed -i "s/API_PASSWORD/'$API_PASSWORD'/g" $ACCESS_WITH_PASSWORDS
    sed -i "s/DB_NAME/$DB_NAME/g" $ACCESS_WITH_PASSWORDS

    INITIAL_SETUP=1
fi

SQL_SCRIPTS=(
    "./versioning.sql"
    "./0001-c2ec_schema.sql"
    "./proc-c2ec_status_listener.sql"
    "./proc-c2ec_payment_notification_listener.sql"
    "./proc-c2ec_retry_listener.sql"
    "./proc-c2ec_transfer_listener.sql"
    "./0001-c2ec_add_confirmed_id.sql"
)

execute_sql_scripts() {
    for script in "${SQL_SCRIPTS[@]}"; do
        echo "Executing SQL script: $script"
        PGPASSWORD=$DB_PASSWORD psql -U $DB_USERNAME -d $DB_NAME -f $script
        if [ $? -ne 0 ]; then
            echo "Failed to execute SQL script: $script"
            exit 1
        fi
    done

    if [ $INITIAL_SETUP -eq 1 ]; then
        PGPASSWORD=$DB_PASSWORD psql -U $DB_USERNAME -d $DB_NAME -f $ACCESS_WITH_PASSWORDS
        rm $ACCESS_WITH_PASSWORDS
    fi

    PGPASSWORD=""
}

execute_sql_scripts
if [ $? -ne 0 ]; then
    exit 1
fi
