-- DO NOT EXECUTE THIS BY HAND (use migrate.sh instead)
BEGIN;

SELECT _v.register_patch('access', ARRAY['0001-c2ec-schema', 'proc-c2ec-payment-notification-listener', 'proc-c2ec-retry-listener', 'proc-c2ec-status-listener', 'proc-c2ec-transfer-listener'], NULL);

SET search_path TO c2ec;

-- For migration and other db management tasks
CREATE USER c2ec_admin WITH ENCRYPTED PASSWORD ADMIN_PASSWORD;
--For CLI (managing terminals and providers):
CREATE USER c2ec_operator WITH ENCRYPTED PASSWORD OPERATOR_PASSWORD;
--For the API (handling withdrawals):
CREATE USER c2ec_api WITH ENCRYPTED PASSWORD API_PASSWORD;

GRANT USAGE ON SCHEMA _v TO c2ec_admin;
GRANT ALL PRIVILEGES ON _v.patches TO c2ec_admin;
GRANT EXECUTE ON ALL FUNCTIONS IN SCHEMA _v TO c2ec_admin;

GRANT ALL PRIVILEGES ON DATABASE DB_NAME TO c2ec_admin;

GRANT USAGE ON SCHEMA c2ec TO c2ec_operator;
GRANT ALL PRIVILEGES ON c2ec.terminal TO c2ec_operator;
GRANT ALL PRIVILEGES ON c2ec.provider TO c2ec_operator; 

GRANT USAGE ON SCHEMA c2ec TO c2ec_api;
GRANT ALL PRIVILEGES ON c2ec.withdrawal TO c2ec_api;
GRANT ALL PRIVILEGES ON c2ec.transfer TO c2ec_api;
GRANT SELECT ON c2ec.terminal TO c2ec_api;
GRANT SELECT ON c2ec.provider TO c2ec_api;
GRANT EXECUTE ON FUNCTION c2ec.emit_withdrawal_status TO c2ec_api;
GRANT EXECUTE ON FUNCTION c2ec.emit_payment_notification TO c2ec_api;
GRANT EXECUTE ON FUNCTION c2ec.emit_retry_notification TO c2ec_api;
GRANT EXECUTE ON FUNCTION c2ec.emit_transfer_notification TO c2ec_api;

COMMIT;