# Payto Payment Target Type for Wallee Transactions

## Target Type "wallee-transaction"

Name: wallee-transaction

Description:  Wallee Transaction

   A Wallee transaction is targeted by this payto target-type.
   It aims to allow refunding a transaction which was initiated
   by Wallee.    

   For refunding a transaction using Wallee, this payto specification
   shall be used. To trigger a refund operation in Wallee, the identifier
   of the transaction must be given as path parameter. The identifier is
   a 64-bit integer.

Syntax:

   payto-wallee-transaction-URI = "payto://wallee-transaction/" [ id ]

   'id' is a Long as defined in 
   [Wallee Type System](https://app-wallee.com/en-us/doc/api/web-service#_type_system)


Examples:

   payto://wallee-transaction/18446744073709551616
   payto://wallee-transaction/13434452243525552616


Contact:  <mailto:habej2@bfh.ch>


Normative References

* [RFC 8905] The 'payto' URI Scheme for Payments
  <https://tools.ietf.org/html/rfc8905>

Informative References

* [Wallee Transaction Model] The Transaction model of Wallee
  <https://app-wallee.com/en-us/doc/api/model/transaction>

* [Wallee Transaction API] The Transaction api of Wallee
  <https://app-wallee.com/doc/api/web-service#_transaction>

* [Wallee Type System] The type system used by Wallee
  <https://app-wallee.com/en-us/doc/api/web-service#_type_system>
