package main

import (
	"bytes"
	"encoding/json"
	"io"
)

type Codec[T any] interface {
	HttpApplicationContentHeader() string
	Encode(*T) (io.Reader, error)
	EncodeToBytes(body *T) ([]byte, error)
	Decode(io.Reader) (*T, error)
}

type JsonCodec[T any] struct {
	Codec[T]
}

func NewJsonCodec[T any]() Codec[T] {

	return new(JsonCodec[T])
}

func (*JsonCodec[T]) Encode(body *T) (io.Reader, error) {

	encodedBytes, err := json.Marshal(body)
	if err != nil {
		return nil, err
	}

	return bytes.NewReader(encodedBytes), err
}

func (c *JsonCodec[T]) EncodeToBytes(body *T) ([]byte, error) {

	reader, err := c.Encode(body)
	if err != nil {
		return make([]byte, 0), err
	}
	buf, err := io.ReadAll(reader)
	if err != nil {
		return make([]byte, 0), err
	}
	return buf, nil
}

func (*JsonCodec[T]) Decode(reader io.Reader) (*T, error) {

	body := new(T)
	err := json.NewDecoder(reader).Decode(body)
	return body, err
}
