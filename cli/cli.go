package main

import (
	"bufio"
	"bytes"
	"context"
	"crypto/rand"
	"encoding/base64"
	"errors"
	"fmt"
	"os"
	"strconv"
	"strings"
	"time"

	"github.com/jackc/pgx/v5"
	"github.com/jackc/pgx/v5/pgxpool"
	"golang.org/x/crypto/argon2"
	"gopkg.in/ini.v1"
)

const ACTION_HELP = "h"
const ACTION_SETUP_SIMULATION = "sim"
const ACTION_REGISTER_PROVIDER = "rp"
const ACTION_REGISTER_TERMINAL = "rt"
const ACTION_DEACTIVATE_TERMINAL = "dt"
const ACTION_ACTIVATE_TERMINAL = "at"
const ACTION_WITHDRAWAL_INFOMRATION = "w"
const ACTION_WITHDRAWAL_INFOMRATION_BY_PTID = "wp"
const ACTION_PROVIDER_CREDENTIALS = "wc"
const ACTION_CONNECT_DB = "db"
const ACTION_QUIT = "q"

// format of wallee backend credentials.
type WalleeCredentials struct {
	SpaceId            int    `json:"spaceId"`
	UserId             int    `json:"userId"`
	ApplicationUserKey string `json:"application-user-key"`
}

var DB *pgxpool.Pool

// enter database credentials (host, port, database, username, password)
// register terminal -> read password, hash, save to database
// register provider -> read wallee, read space(id), read userid, read password, hash what needs to be hashed, save to database

func main() {

	iniFilePath := ""
	connstr := ""
	if len(os.Args) > 1 {

		nextIsConf := false
		for i, arg := range os.Args {
			if i == 0 {
				continue
			} else if nextIsConf {
				iniFilePath = arg
				nextIsConf = false
			} else if arg == "-c" {
				nextIsConf = true
			} else if arg == "-h" {
				showHelp()
				os.Exit(0)
			}
		}
	}

	if iniFilePath != "" {
		optionalPgConnStr, err := parseDbConnstrFromIni(iniFilePath)
		if err != nil {
			fmt.Println("failed parsing config:", err.Error())
		}
		fmt.Println("read connection string from ini:", optionalPgConnStr)
		connstr = optionalPgConnStr
	}

	if connstr != "" {
		err := connectDbUsingString(connstr)
		if err != nil {
			fmt.Println("error while connecting to database, using connection string from config. error:", err.Error())
		}
	}
	fmt.Println("What do you want to do?")
	showHelp()
	for {
		err := dispatchCommand(read("Type command (term in brackets): "))
		if err != nil {
			fmt.Println("Error occured:", err.Error())
		}
	}
}

func parseDbConnstrFromIni(path string) (string, error) {

	f, err := os.Open(path)
	if err != nil {
		return "", err
	}
	defer f.Close()

	stat, err := f.Stat()
	if err != nil {
		return "", err
	}

	content := make([]byte, stat.Size())
	_, err = f.Read(content)
	if err != nil {
		return "", err
	}

	ini, err := ini.Load(content)
	if err != nil {
		return "", err
	}

	section := ini.Section("database")
	if section == nil {
		fmt.Println("database section not configured in ini file")
		os.Exit(0)
	}

	value, err := section.GetKey("CONFIG")
	if err != nil {
		return "", err
	}

	return value.String(), nil
}

func walleePrepareCredentials() (string, string, string, string, error) {

	name := "Wallee"
	paytotargettype := "wallee-transaction"
	backendUrl := read("Wallee backend base url: ")
	spaceIdStr := read("Wallee Space Id: ")
	spaceId, err := strconv.Atoi(spaceIdStr)
	if err != nil {
		return "", "", "", "", err
	}
	userIdStr := read("Wallee User Id: ")
	userId, err := strconv.Atoi(userIdStr)
	if err != nil {
		return "", "", "", "", err
	}
	key := read("Wallee Application User Key: ")
	hashedKey, err := pbkdf(key)
	if err != nil {
		return "", "", "", "", err
	}

	creds, err := NewJsonCodec[WalleeCredentials]().EncodeToBytes(&WalleeCredentials{
		SpaceId:            spaceId,
		UserId:             userId,
		ApplicationUserKey: hashedKey,
	})
	if err != nil {
		return "", "", "", "", err
	}
	credsEncoded := base64.StdEncoding.EncodeToString(creds)

	return name, paytotargettype, backendUrl, credsEncoded, nil
}

func generateProviderCredentials() error {

	name, paytottargettype, backendUrl, credsEncoded, err := walleePrepareCredentials()
	if err != nil {
		return err
	}

	fmt.Println("provider-name:", name)
	fmt.Println("provider-payto-target-type:", paytottargettype)
	fmt.Println("provider-backend-url:", backendUrl)
	fmt.Println("base64 encoded credentials (can be added to the database like this):", credsEncoded)

	return nil
}

func registerWalleeProvider() error {

	if DB == nil {
		return errors.New("connect to the database first (cmd: db)")
	}

	name, paytotargettype, backendUrl, credsEncoded, err := walleePrepareCredentials()
	if err != nil {
		return err
	}

	_, err = DB.Exec(
		context.Background(),
		INSERT_PROVIDER,
		name,
		paytotargettype,
		backendUrl,
		credsEncoded,
	)
	if err != nil {
		return err
	}

	return nil
}

func registerWalleeTerminal() error {

	if DB == nil {
		return errors.New("connect to the database first (cmd: db)")
	}

	description := read("Description (location, inventory identifier, etc.): ")
	providerName := read("Provider Name: ")

	rows, err := DB.Query(
		context.Background(),
		GET_PROVIDER_BY_NAME,
		providerName,
	)
	if err != nil {
		return err
	}

	p, err := pgx.CollectOneRow(rows, pgx.RowToAddrOfStructByName[Provider])
	if err != nil {
		return err
	}
	fmt.Println("Collected provider by name")
	rows.Close() // release rows / connection

	accessToken := make([]byte, 32)
	_, err = rand.Read(accessToken)
	if err != nil {
		return err
	}

	accessTokenBase64 := base64.StdEncoding.EncodeToString(accessToken)

	hashedAccessToken, err := pbkdf(accessTokenBase64)
	if err != nil {
		return err
	}

	fmt.Println("adding terminal")
	_, err = DB.Exec(
		context.Background(),
		INSERT_TERMINAL,
		hashedAccessToken,
		description,
		p.ProviderId,
	)
	if err != nil {
		return err
	}

	fmt.Println("looking up last inserted terminal")
	rows, err = DB.Query(
		context.Background(),
		GET_LAST_INSERTED_TERMINAL,
	)
	if err != nil {
		return err
	}
	t, err := pgx.CollectOneRow(rows, pgx.RowToAddrOfStructByName[Terminal])
	if err != nil {
		return err
	}
	rows.Close()

	fmt.Println("Terminal-User-Id (used to identify terminal at the api. You want to note this):", "Wallee-"+strconv.Itoa(int(t.TerminalID)))
	fmt.Println("GENERATED ACCESS-TOKEN (save it in your password manager. Can't be recovered!!):")
	fmt.Println(accessTokenBase64)

	return nil
}

func deactivateTerminal() error {

	if DB == nil {
		return errors.New("connect to the database first (cmd: db)")
	}

	fmt.Println("You are about to deactivate terminal which allows withdrawals. This will make the terminal unusable.")
	tuid := read("Terminal-User-Id: ")
	parts := strings.Split(tuid, "-")
	if len(parts) != 2 {
		return errors.New("invalid terminal-user-id (format=[PROVIDER_NAME]-[NUMBER])")
	}
	tid, err := strconv.Atoi(parts[1])
	if err != nil {
		return err
	}

	_, err = DB.Exec(
		context.Background(),
		DEACTIVATE_TERMINAL,
		tid,
	)
	if err != nil {
		return err
	}

	return nil
}

func activateTerminal() error {

	if DB == nil {
		return errors.New("connect to the database first (cmd: db)")
	}

	fmt.Println("You are about to activate a terminal which allows withdrawals. This will make the terminal operational.")
	tuid := read("Terminal-User-Id: ")
	parts := strings.Split(tuid, "-")
	if len(parts) != 2 {
		return errors.New("invalid terminal-user-id (format=[PROVIDER_NAME]-[NUMBER])")
	}
	tid, err := strconv.Atoi(parts[1])
	if err != nil {
		return err
	}

	_, err = DB.Exec(
		context.Background(),
		ACTIVATE_TERMINAL,
		tid,
	)
	if err != nil {
		return err
	}

	return nil
}

func withdrawalInformationByWopid() error {

	if DB == nil {
		return errors.New("connect to the database first (cmd: db)")
	}

	wopid := read("WOPID (encoded): ")
	wopidDecoded, err := decodeCrock(wopid)
	if err != nil {
		return err
	}
	rows, err := DB.Query(
		context.Background(),
		GET_WITHDRAWAL_BY_WOPID,
		wopidDecoded,
	)
	if err != nil {
		return err
	}

	return readPrintWithdrawal(rows)
}

func withdrawalInformationByProviderTransactionId() error {

	if DB == nil {
		return errors.New("connect to the database first (cmd: db)")
	}

	ptid := read("Provider Transaction ID: ")
	rows, err := DB.Query(
		context.Background(),
		GET_WITHDRAWAL_BY_PROVIDER_TRANSACTION_ID,
		ptid,
	)
	if err != nil {
		return err
	}

	return readPrintWithdrawal(rows)
}

func readPrintWithdrawal(rows pgx.Rows) error {

	type TalerAmountCurrency struct {
		Val  int64  `db:"val"`
		Frac int32  `db:"frac"`
		Curr string `db:"curr"`
	}
	type Withdrawal struct {
		WithdrawalRowId       uint64               `db:"withdrawal_row_id"`
		ConfirmedRowId        uint64               `db:"confirmed_row_id"`
		RequestUid            string               `db:"request_uid"`
		Wopid                 []byte               `db:"wopid"`
		ReservePubKey         []byte               `db:"reserve_pub_key"`
		RegistrationTs        int64                `db:"registration_ts"`
		Amount                *TalerAmountCurrency `db:"amount" scan:"follow"`
		SuggestedAmount       *TalerAmountCurrency `db:"suggested_amount" scan:"follow"`
		TerminalFees          *TalerAmountCurrency `db:"terminal_fees" scan:"follow"`
		WithdrawalStatus      string               `db:"withdrawal_status"`
		TerminalId            int                  `db:"terminal_id"`
		ProviderTransactionId *string              `db:"provider_transaction_id"`
		LastRetryTs           *int64               `db:"last_retry_ts"`
		RetryCounter          int32                `db:"retry_counter"`
		CompletionProof       []byte               `db:"completion_proof"`
	}

	w, err := pgx.CollectOneRow(rows, pgx.RowToAddrOfStructByName[Withdrawal])
	if err != nil {
		return err
	}
	rows.Close()

	indent := " -"
	fmt.Println("Withdrawal:")
	fmt.Println(indent, "wopid             :", encodeCrock(w.Wopid))
	fmt.Println(indent, "status            :", w.WithdrawalStatus)
	fmt.Println(indent, "reserve public key:", encodeCrock(w.ReservePubKey))
	fmt.Println(indent, "provider tid      :", *w.ProviderTransactionId)
	fmt.Println(indent, "amount            :", w.Amount)
	fmt.Println(indent, "terminal          :", w.TerminalId)
	fmt.Println(indent, "attest retries    :", w.RetryCounter)
	if w.LastRetryTs != nil {
		fmt.Println(indent, "last retry        :", time.Unix(*w.LastRetryTs, 0).Format("yyyy-MM-dd hh:mm:ss"))
	}

	return nil
}

func setupSimulation() error {

	if DB == nil {
		return errors.New("connect to the database first (cmd: db)")
	}

	// SETTING UP PROVIDER
	fmt.Println("Setting up simulation provider and terminal.")
	name := "Simulation"
	paytotargettype := "void"
	backendUrl := "simulation provider will not contact any backend."
	credsEncoded := base64.StdEncoding.EncodeToString(bytes.NewBufferString("simulation provider will not contact any backend.").Bytes())

	_, err := DB.Exec(
		context.Background(),
		INSERT_PROVIDER,
		name,
		paytotargettype,
		backendUrl,
		credsEncoded,
	)
	if err != nil {
		return err
	}

	// SETTING UP TERMINAL
	description := "simulation terminal"

	rows, err := DB.Query(
		context.Background(),
		GET_PROVIDER_BY_NAME,
		name,
	)
	if err != nil {
		return err
	}

	p, err := pgx.CollectOneRow(rows, pgx.RowToAddrOfStructByName[Provider])
	if err != nil {
		return err
	}
	rows.Close() // release rows / connection

	accessToken := make([]byte, 32)
	_, err = rand.Read(accessToken)
	if err != nil {
		return err
	}

	accessTokenBase64 := base64.StdEncoding.EncodeToString(accessToken)

	hashedAccessToken, err := pbkdf(accessTokenBase64)
	if err != nil {
		return err
	}

	_, err = DB.Exec(
		context.Background(),
		INSERT_TERMINAL,
		hashedAccessToken,
		description,
		p.ProviderId,
	)
	if err != nil {
		return err
	}

	fmt.Println("looking up last inserted terminal")
	rows, err = DB.Query(
		context.Background(),
		GET_LAST_INSERTED_TERMINAL,
	)
	if err != nil {
		return err
	}
	t, err := pgx.CollectOneRow(rows, pgx.RowToAddrOfStructByName[Terminal])
	if err != nil {
		return err
	}
	rows.Close()

	fmt.Println("Terminal-User-Id (used to identify terminal at the api. You want to note this):", name+"-"+strconv.Itoa(int(t.TerminalID)))
	fmt.Println("GENERATED ACCESS-TOKEN (save it in your password manager. Can't be recovered!!):")
	fmt.Println(accessTokenBase64)

	return nil
}

func connectDatabase() error {

	u := read("Username: ")
	pw := read("Password: ")
	h := read("Host: ")
	ps := read("Port: ")
	p, err := strconv.Atoi(ps)
	if err != nil {
		return err
	}
	d := read("Database: ")

	connstring := PostgresConnectionString(u, pw, h, p, d)
	return connectDbUsingString(connstring)
}

func connectDbUsingString(connString string) error {
	dbCfg, err := pgxpool.ParseConfig(connString)
	if err != nil {
		return err
	}

	dbCfg.AfterConnect = registerCustomTypesHook
	DB, err = pgxpool.NewWithConfig(context.Background(), dbCfg)
	if err != nil {
		return err
	}
	fmt.Println("connected to database")
	return nil
}

func showHelp() error {

	fmt.Println("register wallee provider (", ACTION_REGISTER_PROVIDER, ")")
	fmt.Println("register wallee terminal (", ACTION_REGISTER_TERMINAL, ")")
	fmt.Println("deactivate wallee terminal (", ACTION_DEACTIVATE_TERMINAL, ")")
	fmt.Println("activate wallee terminal (", ACTION_ACTIVATE_TERMINAL, ")")
	fmt.Println("setup simulation (", ACTION_SETUP_SIMULATION, ")")
	fmt.Println("withdrawal information by wopid (", ACTION_WITHDRAWAL_INFOMRATION, ")")
	fmt.Println("witdhrawal information by provider transaction id (", ACTION_WITHDRAWAL_INFOMRATION_BY_PTID, ")")
	fmt.Println("create wallee provider credentials string (", ACTION_PROVIDER_CREDENTIALS, ")")
	if DB == nil {
		fmt.Println("connect database (", ACTION_CONNECT_DB, ")")
	}
	fmt.Println("show help (", ACTION_HELP, ")")
	fmt.Println("quit (", ACTION_QUIT, ")")
	return nil
}

func quit() error {
	fmt.Println("bye...")
	os.Exit(0)
	return nil
}

func pbkdf(pw string) (string, error) {

	rfcTime := 3
	rfcMemory := 32 * 1024
	salt := make([]byte, 16)
	_, err := rand.Read(salt)
	if err != nil {
		return "", err
	}
	key := argon2.Key([]byte(pw), salt, uint32(rfcTime), uint32(rfcMemory), 4, 32)

	keyAndSalt := make([]byte, 0, 48)
	keyAndSalt = append(keyAndSalt, key...)
	keyAndSalt = append(keyAndSalt, salt...)
	if len(keyAndSalt) != 48 {
		return "", errors.New("invalid password hash and salt")
	}
	return base64.StdEncoding.EncodeToString(keyAndSalt), nil
}

func PostgresConnectionString(
	username string,
	password string,
	host string,
	port int,
	database string,
) string {
	return fmt.Sprintf(
		"postgres://%s:%s@%s:%d/%s",
		username,
		password,
		host,
		port,
		database,
	)
}

func dispatchCommand(cmd string) error {

	var err error
	switch cmd {
	case ACTION_HELP:
		err = showHelp()
	case ACTION_QUIT:
		err = quit()
	case ACTION_CONNECT_DB:
		err = connectDatabase()
	case ACTION_REGISTER_PROVIDER:
		err = registerWalleeProvider()
	case ACTION_REGISTER_TERMINAL:
		err = registerWalleeTerminal()
	case ACTION_DEACTIVATE_TERMINAL:
		err = deactivateTerminal()
	case ACTION_ACTIVATE_TERMINAL:
		err = activateTerminal()
	case ACTION_WITHDRAWAL_INFOMRATION:
		err = withdrawalInformationByWopid()
	case ACTION_WITHDRAWAL_INFOMRATION_BY_PTID:
		err = withdrawalInformationByProviderTransactionId()
	case ACTION_PROVIDER_CREDENTIALS:
		err = generateProviderCredentials()
	case ACTION_SETUP_SIMULATION:
		err = setupSimulation()
	default:
		fmt.Println("unknown action")
	}
	return err
}

func read(prefix string) string {
	reader := bufio.NewReader(os.Stdin)
	fmt.Print(prefix)
	inp, err := reader.ReadString('\n')
	if err != nil {
		fmt.Println(err.Error())
		return ""
	}
	return strings.Trim(inp, "\n")
}

func registerCustomTypesHook(ctx context.Context, conn *pgx.Conn) error {

	t, err := conn.LoadType(ctx, "c2ec.taler_amount_currency")
	if err != nil {
		return err
	}

	conn.TypeMap().RegisterType(t)
	return nil
}
