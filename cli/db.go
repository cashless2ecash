package main

const INSERT_PROVIDER = "INSERT INTO c2ec.provider (name, payto_target_type, backend_base_url, backend_credentials) VALUES ($1,$2,$3,$4)"
const INSERT_TERMINAL = "INSERT INTO c2ec.terminal (access_token, description, provider_id) VALUES ($1,$2,$3)"
const DEACTIVATE_TERMINAL = "UPDATE c2ec.terminal SET active = false WHERE terminal_id=$1"
const ACTIVATE_TERMINAL = "UPDATE c2ec.terminal SET active = true WHERE terminal_id=$1"
const GET_PROVIDER_BY_NAME = "SELECT * FROM c2ec.provider WHERE name=$1"
const GET_LAST_INSERTED_TERMINAL = "SELECT * FROM c2ec.terminal WHERE terminal_id = (SELECT MAX(terminal_id) FROM c2ec.terminal)"
const GET_WITHDRAWAL_BY_WOPID = "SELECT * FROM c2ec.withdrawal WHERE wopid=$1"
const GET_WITHDRAWAL_BY_PROVIDER_TRANSACTION_ID = "SELECT * FROM c2ec.withdrawal WHERE provider_transaction_id=$1"

type Provider struct {
	ProviderId         int64  `db:"provider_id"`
	Name               string `db:"name"`
	PaytoTargetType    string `db:"payto_target_type"`
	BackendBaseURL     string `db:"backend_base_url"`
	BackendCredentials string `db:"backend_credentials"`
}

type Terminal struct {
	TerminalID  int64  `db:"terminal_id"`
	AccessToken []byte `db:"access_token"`
	Active      bool   `db:"active"`
	Description string `db:"description"`
	ProviderID  int64  `db:"provider_id"`
}
