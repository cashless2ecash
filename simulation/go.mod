module c2ec-simulation

go 1.22.1

require github.com/gofrs/uuid v4.4.0+incompatible

require gopkg.in/yaml.v3 v3.0.1
