// This file is part of taler-go, the Taler Go implementation.
// Copyright (C) 2022 Martin Schanzenbach
// Copyright (C) 2024 Joel Häberli
//
// Taler Go is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as published
// by the Free Software Foundation, either version 3 of the License,
// or (at your option) any later version.
//
// Taler Go is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// SPDX-License-Identifier: AGPL3.0-or-later

package main

import (
	"errors"
	"fmt"
	"math"
	"strconv"
	"strings"
)

// The GNU Taler Amount object
type Amount struct {

	// The type of currency, e.g. EUR
	Currency string `json:"currency"`

	// The value (before the ".")
	Value uint64 `json:"value"`

	// The fraction (after the ".", optional)
	Fraction uint64 `json:"fraction"`
}

func FormatAmount(amount *Amount, fractionalDigits int) string {

	if amount == nil {
		return ""
	}

	if amount.Currency == "" && amount.Value == 0 && amount.Fraction == 0 {
		return ""
	}

	if amount.Fraction <= 0 {
		return fmt.Sprintf("%s:%d", amount.Currency, amount.Value)
	}

	fractionStr := toFractionStr(int(amount.Fraction), fractionalDigits)
	return fmt.Sprintf("%s:%d.%s", amount.Currency, amount.Value, fractionStr)
}

// The maximim length of a fraction (in digits)
const FractionalLength = 8

// The base of the fraction.
const FractionalBase = 1e8

// The maximum value
var MaxAmountValue = uint64(math.Pow(2, 52))

// Create a new amount from value and fraction in a currency
func NewAmount(currency string, value uint64, fraction uint64) Amount {
	return Amount{
		Currency: currency,
		Value:    value,
		Fraction: fraction,
	}
}

func toFractionStr(frac int, fractionalDigits int) string {

	if fractionalDigits > 8 {
		return ""
	}

	leadingZerosStr := ""
	strLengthTens := int(math.Pow10(fractionalDigits - 1))
	strLength := int(math.Log10(float64(strLengthTens)))
	leadingZeros := 0
	if strLengthTens > frac {
		for i := 0; i < strLength; i++ {
			if strLengthTens > frac {
				leadingZeros++
				strLengthTens = strLengthTens / 10
			}
		}
		for i := 0; i < leadingZeros; i++ {
			leadingZerosStr += "0"
		}
	}

	return leadingZerosStr + strconv.Itoa(frac)
}

// Subtract the amount b from a and return the result.
// a and b must be of the same currency and a >= b
func (a *Amount) Sub(b Amount) (*Amount, error) {
	if a.Currency != b.Currency {
		return nil, errors.New("currency mismatch")
	}
	v := a.Value
	f := a.Fraction
	if a.Fraction < b.Fraction {
		v -= 1
		f += FractionalBase
	}
	f -= b.Fraction
	if v < b.Value {
		return nil, errors.New("amount overflow")
	}
	v -= b.Value
	r := Amount{
		Currency: a.Currency,
		Value:    v,
		Fraction: f,
	}
	return &r, nil
}

// Add b to a and return the result.
// Returns an error if the currencies do not match or the addition would
// cause an overflow of the value
func (a *Amount) Add(b Amount) (*Amount, error) {
	if a.Currency != b.Currency {
		return nil, errors.New("currency mismatch")
	}
	v := a.Value +
		b.Value +
		uint64(math.Floor((float64(a.Fraction)+float64(b.Fraction))/FractionalBase))

	if v >= MaxAmountValue {
		return nil, fmt.Errorf("amount overflow (%d > %d)", v, MaxAmountValue)
	}
	f := uint64((a.Fraction + b.Fraction) % FractionalBase)
	r := Amount{
		Currency: a.Currency,
		Value:    v,
		Fraction: f,
	}
	return &r, nil
}

// Parses an amount string in the format <currency>:<value>[.<fraction>]
func ParseAmount(s string, fractionDigits int) (*Amount, error) {

	if s == "" {
		return &Amount{"", 0, 0}, nil
	}

	if !strings.Contains(s, ":") {
		return nil, fmt.Errorf("invalid amount: %s", s)
	}

	currencyAndAmount := strings.Split(s, ":")
	if len(currencyAndAmount) != 2 {
		return nil, fmt.Errorf("invalid amount: %s", s)
	}

	currency := currencyAndAmount[0]
	valueAndFraction := strings.Split(currencyAndAmount[1], ".")
	if len(valueAndFraction) < 1 && len(valueAndFraction) > 2 {
		return nil, fmt.Errorf("invalid value and fraction part in amount %s", s)
	}
	value, err := strconv.Atoi(valueAndFraction[0])
	if err != nil {
		return nil, fmt.Errorf("invalid value in amount %s", s)
	}

	fraction := 0
	if len(valueAndFraction) == 2 {
		if len(valueAndFraction[1]) > fractionDigits {
			return nil, fmt.Errorf("invalid amount: %s expected at max %d fractional digits", s, fractionDigits)
		}
		fractionInt, err := strconv.Atoi(valueAndFraction[1])
		if err != nil {
			return nil, fmt.Errorf("invalid fraction in amount %s", s)
		}
		fraction = fractionInt
	}

	a := NewAmount(currency, uint64(value), uint64(fraction))
	return &a, nil
}

// Check if this amount is zero
func (a *Amount) IsZero() bool {
	return (a.Value == 0) && (a.Fraction == 0)
}

// Returns the string representation of the amount: <currency>:<value>[.<fraction>]
// Omits trailing zeroes.
func (a *Amount) String() string {
	v := strconv.FormatUint(a.Value, 10)
	if a.Fraction != 0 {
		f := strconv.FormatUint(a.Fraction, 10)
		f = strings.TrimRight(f, "0")
		v = fmt.Sprintf("%s.%s", v, f)
	}
	return fmt.Sprintf("%s:%s", a.Currency, v)
}
