package main

import (
	"encoding/base64"
	"errors"
	"fmt"
	"strconv"
	"time"

	"github.com/gofrs/uuid"
)

// retrieved from the cli tool when added the terminal
var TERMINAL_USER_ID string

var EXCHANGE_FEES string

func Terminal(in chan *SimulatedPhysicalInteraction, out chan *SimulatedPhysicalInteraction, kill chan error) {

	var C2EC_TERMINAL_CONFIG_API = CONFIG.C2ecBaseUrl + "/config"
	var C2EC_TERMINAL_SETUP_WITHDRAWAL_API = CONFIG.C2ecBaseUrl + "/withdrawals"
	var C2EC_TERMINAL_STATUS_WITHDRAWAL_API = CONFIG.C2ecBaseUrl + "/withdrawals/:wopid"
	var C2EC_TERMINAL_CHECK_WITHDRAWAL_API = CONFIG.C2ecBaseUrl + "/withdrawals/:wopid/check"
	TERMINAL_USER_ID = "Simulation-" + CONFIG.TerminalId

	fmt.Println("TERMINAL: Terminal idle... awaiting readiness message of sim-wallet")
	<-in

	fmt.Println("TERMINAL: basic auth header:", TerminalAuth())
	fmt.Println("TERMINAL: loading terminal api config")
	terminalApiCfg, status, err := HttpGet(
		C2EC_TERMINAL_CONFIG_API,
		map[string]string{"Authorization": TerminalAuth()},
		NewJsonCodec[TerminalConfig](),
	)
	if err != nil {
		kill <- err
		return
	}
	if status != 200 {
		kill <- errors.New("terminal api configuration failed with status " + strconv.Itoa(status))
		return
	}

	EXCHANGE_FEES = terminalApiCfg.WithdrawalFees

	fmt.Println("TERMINAL: API config loaded.", terminalApiCfg.Name, terminalApiCfg.Version, terminalApiCfg.ProviderName, terminalApiCfg.WireType, EXCHANGE_FEES)

	fmt.Println("TERMINAL: Sim-Wallet ready, intiating withdrawal...")

	uuid, err := uuid.NewGen().NewV7()
	if err != nil {
		kill <- err
		return
	}

	setupReq := &TerminalWithdrawalSetup{
		Amount:                CONFIG.Amount,
		SuggestedAmount:       "",
		ProviderTransactionId: "",
		TerminalFees:          EXCHANGE_FEES,
		RequestUid:            uuid.String(),
		UserUuid:              "",
		Lock:                  "",
	}

	url := FormatUrl(
		C2EC_TERMINAL_SETUP_WITHDRAWAL_API,
		map[string]string{},
		map[string]string{},
	)
	fmt.Println("TERMINAL: requesting url:", url)
	response, status, err := HttpPost(
		url,
		map[string]string{"Authorization": TerminalAuth()},
		setupReq,
		NewJsonCodec[TerminalWithdrawalSetup](),
		NewJsonCodec[TerminalWithdrawalSetupResponse](),
	)
	if err != nil {
		kill <- err
		return
	}
	if status != 200 {
		kill <- errors.New("status of withdrawal setup response was " + strconv.Itoa(status))
		return
	}

	wopidEncoded := response.Wopid
	fmt.Println("TERMINAL: received wopid:", wopidEncoded)

	// this decoding encoding cycle is useless but tests
	// decoding and encoding of the wopid. That's why it is
	// done here.
	wopidDecoded, err := ParseWopid(wopidEncoded)
	if err != nil {
		kill <- err
		return
	}
	wopidEncoded = FormatWopid(wopidDecoded)

	uri := CONFIG.TerminalQrCodeBase + wopidEncoded
	fmt.Println("TERMINAL: Taler Withdrawal URI:", uri)

	// note for realworld implementation
	// -> start long polling always before showing the QR code
	awaitSelection := make(chan *BankWithdrawalOperationStatus)
	longPollFailed := make(chan error)

	fmt.Println("TERMINAL: now sending long poll request to c2ec from terminal and await parameter selection")
	go func() {

		url := FormatUrl(
			C2EC_TERMINAL_STATUS_WITHDRAWAL_API,
			map[string]string{"wopid": wopidEncoded},
			map[string]string{"long_poll_ms": CONFIG.TerminalLongPollMs},
		)
		fmt.Println("TERMINAL: requesting status update for withdrawal", url)
		response, status, err := HttpGet(
			url,
			map[string]string{"Authorization": TerminalAuth()},
			NewJsonCodec[BankWithdrawalOperationStatus](),
		)
		if err != nil {
			kill <- err
			return
		}
		if status != 200 {
			longPollFailed <- errors.New("status of withdrawal status response was " + strconv.Itoa(status))
			return
		}

		fmt.Printf("TERMINAL: wallet should use exchange=%s\n", response.RequiredExchange)
		awaitSelection <- response
	}()

	fmt.Println("need to sleep a bit that long polling request is guaranteed to be executed before the POST of the registration. This won't be a problem in real world appliance.")
	time.Sleep(time.Duration(10) * time.Millisecond)

	if !CONFIG.DisableDelays {
		fmt.Println("TERMINAL: simulating QR Code scan. delay:", CONFIG.WalletScanQrDelay)
		time.Sleep(time.Duration(CONFIG.WalletScanQrDelay) * time.Millisecond)
	} else {
		fmt.Println("TERMINAL: simulating QR Code scan.")
	}
	out <- &SimulatedPhysicalInteraction{Msg: uri}
	for {
		select {
		case w := <-awaitSelection:
			fmt.Println("TERMINAL: parameters selected:", w.ReservePubKey)
			if !CONFIG.DisableDelays {
				fmt.Println("TERMINAL: simulating user interaction. customer presents card. delay:", CONFIG.TerminalAcceptCardDelay)
				time.Sleep(time.Duration(CONFIG.TerminalAcceptCardDelay) * time.Millisecond)
			} else {
				fmt.Println("TERMINAL: simulating user interaction. customer presents card.")
			}
			if !CONFIG.DisableDelays {
				fmt.Println("TERMINAL: card accepted. terminal waits for response of provider backend. delay:", CONFIG.ProviderBackendPaymentDelay)
				time.Sleep(time.Duration(CONFIG.ProviderBackendPaymentDelay) * time.Millisecond)
			} else {
				fmt.Println("TERMINAL: card accepted. terminal waits for response of provider backend.")
			}

			fmt.Println("TERMINAL: payment was processed at the provider backend. sending check notification.")
			checkNotification := &TerminalWithdrawalConfirmationRequest{
				ProviderTransactionId: uuid.String(),
				TerminalFees:          EXCHANGE_FEES,
			}
			checkurl := FormatUrl(
				C2EC_TERMINAL_CHECK_WITHDRAWAL_API,
				map[string]string{"wopid": wopidEncoded},
				map[string]string{},
			)
			fmt.Println("TERMINAL: check url", checkurl)
			_, status, err = HttpPost[TerminalWithdrawalConfirmationRequest, any](
				checkurl,
				map[string]string{"Authorization": TerminalAuth()},
				checkNotification,
				NewJsonCodec[TerminalWithdrawalConfirmationRequest](),
				nil,
			)
			if err != nil {
				fmt.Println("TERMINAL: error on POST request:", err.Error())
				kill <- err
			}
			if status != 204 {
				fmt.Println("TERMINAL: error while check payment POST: " + strconv.Itoa(status))
				kill <- errors.New("payment check request by terminal failed")
			}
			fmt.Println("TERMINAL: Terminal flow ended succesful")
			return
		case f := <-longPollFailed:
			fmt.Println("TERMINAL: long-polling for selection failed... error:", err)
			kill <- f
		}
	}
}

func TerminalAuth() string {

	userAndPw := fmt.Sprintf("%s:%s", TERMINAL_USER_ID, CONFIG.TerminalAccessToken)
	return "Basic " + base64.StdEncoding.EncodeToString([]byte(userAndPw))
}

// Structs copied from c2ec
type TerminalConfig struct {
	Name           string `json:"name"`
	Version        string `json:"version"`
	ProviderName   string `json:"provider_name"`
	Currency       string `json:"currency"`
	WithdrawalFees string `json:"withdrawal_fees"`
	WireType       string `json:"wire_type"`
}

type TerminalWithdrawalSetup struct {
	Amount                string `json:"amount"`
	SuggestedAmount       string `json:"suggested_amount"`
	ProviderTransactionId string `json:"provider_transaction_id"`
	TerminalFees          string `json:"terminal_fees"`
	RequestUid            string `json:"request_uid"`
	UserUuid              string `json:"user_uuid"`
	Lock                  string `json:"lock"`
}

type TerminalWithdrawalSetupResponse struct {
	Wopid string `json:"withdrawal_id"`
}

type TerminalWithdrawalConfirmationRequest struct {
	ProviderTransactionId string `json:"provider_transaction_id"`
	TerminalFees          string `json:"terminal_fees"`
	UserUuid              string `json:"user_uuid"`
	Lock                  string `json:"lock"`
}
