package main

import (
	"fmt"
)

// https://docs.taler.net/core/api-common.html#hash-codes
type WithdrawalIdentifier string

// https://docs.taler.net/core/api-common.html#cryptographic-primitives
type EddsaPublicKey string

// https://docs.taler.net/core/api-common.html#hash-codes
type HashCode string

// https://docs.taler.net/core/api-common.html#hash-codes
type ShortHashCode string

// https://docs.taler.net/core/api-common.html#timestamps
type Timestamp struct {
	Ts int `json:"t_s"`
}

// https://docs.taler.net/core/api-common.html#wadid
type WadId [6]uint32

// according to https://docs.taler.net/core/api-bank-integration.html#tsref-type-BankWithdrawalOperationStatus
type WithdrawalOperationStatus string

const (
	PENDING   WithdrawalOperationStatus = "pending"
	SELECTED  WithdrawalOperationStatus = "selected"
	ABORTED   WithdrawalOperationStatus = "aborted"
	CONFIRMED WithdrawalOperationStatus = "confirmed"
)

func ToWithdrawalOpStatus(s string) (WithdrawalOperationStatus, error) {
	switch s {
	case string(PENDING):
		return PENDING, nil
	case string(SELECTED):
		return SELECTED, nil
	case string(ABORTED):
		return ABORTED, nil
	case string(CONFIRMED):
		return CONFIRMED, nil
	default:
		return "", fmt.Errorf("unknown withdrawal operation status '%s'", s)
	}
}

type C2ECWithdrawRegistration struct {
	ReservePubKey EddsaPublicKey `json:"reserve_pub_key"`
}

type C2ECWithdrawalStatus struct {
	Status        WithdrawalOperationStatus `json:"status"`
	Amount        string                    `json:"amount"`
	CardFees      string                    `json:"card_fees"`
	SenderWire    string                    `json:"sender_wire"`
	WireTypes     []string                  `json:"wire_types"`
	ReservePubKey EddsaPublicKey            `json:"selected_reserve_pub"`
	Aborted       bool                      `json:"aborted"`
	SelectionDone bool                      `json:"selection_done"`
	TransferDone  bool                      `json:"transfer_done"`
}
