package main

import (
	"fmt"
	"os"

	"gopkg.in/yaml.v3"
)

var CONFIG SimulationConfig

type SimulatedPhysicalInteraction struct {
	Msg string
}

func main() {

	p := "./config.yaml"
	if len(os.Args) > 1 && os.Args[1] != "" {
		p = os.Args[1]
	}

	cfg, err := parseSimulationConfig(p)
	if err != nil {
		fmt.Println(err.Error())
		return
	}
	CONFIG = *cfg
	TERMINAL_USER_ID = "Simulation-" + CONFIG.TerminalId

	if !c2ecAlive() {
		fmt.Println("start c2ec first.")
		return
	}

	kill := make(chan error)
	finish := make(chan interface{})
	// start simulated wire watch
	go WireWatch(finish, kill)

	go func() {
		for i := 0; i < CONFIG.ParallelWithdrawals; i++ {

			fmt.Println("Starting withdrawal", i)

			toTerminal := make(chan *SimulatedPhysicalInteraction, 10)
			toWallet := make(chan *SimulatedPhysicalInteraction, 10)

			// start simulated terminal
			go Terminal(toTerminal, toWallet, kill)

			// start simulated wallet
			go Wallet(toWallet, toTerminal, kill)

		}
	}()

	for err := range kill {
		if err == nil {
			fmt.Print("simulation successful.")
			os.Exit(0)
		}
		fmt.Println("simulation error: ", err.Error())
		os.Exit(1)
	}

	<-finish
	fmt.Println("simulation ended")
}

func c2ecAlive() bool {

	cfg, status, err := HttpGet(
		CONFIG.C2ecBaseUrl+"/config",
		map[string]string{
			"Authorization": TerminalAuth(),
		},
		NewJsonCodec[TerminalConfig]())
	if err != nil || status != 200 {
		fmt.Println("Error from c2ec:", err)
		fmt.Println("Status from c2ec:", status)
		return false
	}

	fmt.Println("ALIVE   :", cfg.Name, cfg.Version, cfg.ProviderName, cfg.WireType)
	return true
}

type SimulationConfig struct {
	DisableDelays       bool   `yaml:"disable-delays"`
	ParallelWithdrawals int    `yaml:"parallel-withdrawals"`
	C2ecBaseUrl         string `yaml:"c2ec-base-url"`
	// simulates the terminal talking to its backend system and executing the payment.
	ProviderBackendPaymentDelay int `yaml:"provider-backend-payment-delay"`
	// simulates the user presenting his card to the terminal
	Amount                  string `yaml:"amount"`
	Fees                    string `yaml:"fees"`
	TerminalAcceptCardDelay int    `yaml:"terminal-accept-card-delay"`
	TerminalProvider        string `yaml:"terminal-provider"`
	TerminalId              string `yaml:"terminal-id"`
	TerminalUserId          string `yaml:"terminal-user-id"`
	TerminalAccessToken     string `yaml:"terminal-access-token"`
	TerminalLongPollMs      string `yaml:"terminal-long-poll-ms"`
	TerminalQrCodeBase      string `yaml:"terminal-qr-base"`
	// simulates the user scanning the QR code presented at the terminal
	WalletScanQrDelay   int    `yaml:"wallet-scan-qr-delay"`
	WalletLongPollMs    string `yaml:"wallet-long-poll-ms"`
	WireGatewayUsername string `yaml:"wire-gateway-user"`
	WireGatewayPassword string `yaml:"wire-gateway-password"`
}

func parseSimulationConfig(path string) (*SimulationConfig, error) {

	f, err := os.Open(path)
	if err != nil {
		return nil, err
	}
	defer f.Close()

	stat, err := f.Stat()
	if err != nil {
		return nil, err
	}

	content := make([]byte, stat.Size())
	_, err = f.Read(content)
	if err != nil {
		return nil, err
	}

	cfg := new(SimulationConfig)
	err = yaml.Unmarshal(content, cfg)
	if err != nil {
		return nil, err
	}
	return cfg, nil
}
