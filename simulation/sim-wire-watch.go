package main

import (
	"encoding/base64"
	"errors"
	"fmt"
	"strconv"
)

type IncomingHistory struct {
	IncomingTransactions []IncomingReserveTransaction `json:"incoming_transactions"`
	CreditAccount        string                       `json:"credit_account"`
}

// type RESERVE | https://docs.taler.net/core/api-bank-wire.html#tsref-type-IncomingReserveTransaction
type IncomingReserveTransaction struct {
	Type         string         `json:"type"`
	RowId        int            `json:"row_id"`
	Date         Timestamp      `json:"date"`
	Amount       string         `json:"amount"`
	DebitAccount string         `json:"debit_account"`
	ReservePub   EddsaPublicKey `json:"reserve_pub"`
}

func WireWatch(finish chan interface{}, kill chan error) {

	var wirewatchLongPoll int
	if CONFIG.DisableDelays {
		wirewatchLongPoll = 3000
	} else {
		wirewatchLongPoll = CONFIG.ProviderBackendPaymentDelay +
			CONFIG.TerminalAcceptCardDelay +
			CONFIG.WalletScanQrDelay +
			5000 // add some delay for operations
	}

	fmt.Println("WIRE-WATCH: long poll to c2ec for ", wirewatchLongPoll, "milliseconds")

	var HISTORY_ENDPOINT = CONFIG.C2ecBaseUrl + "/taler-wire-gateway/history/incoming"

	url := FormatUrl(
		HISTORY_ENDPOINT,
		map[string]string{},
		map[string]string{
			"delta":        "1024", // this value is used by the wirewatch
			"long_poll_ms": strconv.Itoa(wirewatchLongPoll),
		},
	)
	fmt.Println("WIRE-WATCH: requesting status update for withdrawal", url)
	response, status, err := HttpGet(
		url,
		map[string]string{"Authorization": WireGatewayAuth()},
		NewJsonCodec[IncomingHistory](),
	)
	if err != nil {
		kill <- err
		return
	}
	if status != 200 {
		kill <- errors.New("wire-watch could not retrieve transaction: " + strconv.Itoa(status))
		return
	}

	res := *response
	for _, r := range res.IncomingTransactions {
		fmt.Println("WIRE-WATCH: Incoming Reserve Transaction(", r.RowId, r.DebitAccount, r.Amount, r.ReservePub, ")")
	}

	close(kill)
	finish <- nil
}

func WireGatewayAuth() string {

	userAndPw := fmt.Sprintf("%s:%s", CONFIG.WireGatewayUsername, CONFIG.WireGatewayPassword)
	return "Basic " + base64.StdEncoding.EncodeToString([]byte(userAndPw))

}
