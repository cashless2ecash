package main

import (
	"encoding/base64"
	"fmt"
	"testing"
	"time"
)

func Test(t *testing.T) {

	base64encoded := "li0v/6Si7cqmH3z3L62IYCqCxvQlw2Xijw469zzqdZQ="
	bytes, _ := base64.StdEncoding.DecodeString(base64encoded)
	crockencoded := encodeCrock(bytes)
	decoded, _ := decodeCrock(crockencoded)

	fmt.Println("HEX:", fmt.Sprintf("%x", bytes))
	fmt.Println("BASE64:", base64encoded)
	fmt.Println("CROCK:", crockencoded)
	fmt.Println("HEX:", fmt.Sprintf("%x", decoded))
}

func Test2(t *testing.T) {

	wopid := "JRPJZZX4MBPWN9GZFKVJZBC8C0N85HQM4Q1PBRMF1RXFEF7AEPA0"

	awaitSelection := make(chan *BankWithdrawalOperationStatus)

	fmt.Println("TERMINAL: now sending long poll request to c2ec from terminal and await parameter selection")
	go func() {

		url := FormatUrl(
			"http://taler-c2ec.ti.bfh.ch/withdrawals/:wopid",
			map[string]string{"wopid": wopid},
			map[string]string{"long_poll_ms": "10000"},
		)
		fmt.Println("TERMINAL: requesting status update for withdrawal", url)
		response, status, err := HttpGet(
			url,
			map[string]string{"Authorization": "Basic " + base64.StdEncoding.EncodeToString([]byte(fmt.Sprintf("%s:%s", "Simulation-1", "")))},
			NewJsonCodec[BankWithdrawalOperationStatus](),
		)
		if err != nil {
			fmt.Println(err.Error())
			return
		}
		if status != 200 {
			fmt.Println("long poll failed")
			return
		}

		awaitSelection <- response
	}()

	var C2EC_BANK_WITHDRAWAL_REGISTRATION_URL = "http://taler-c2ec.ti.bfh.ch/taler-integration/withdrawal-operation/:wopid"

	registrationUrl := FormatUrl(
		C2EC_BANK_WITHDRAWAL_REGISTRATION_URL,
		map[string]string{"wopid": wopid},
		map[string]string{},
	)

	reg := new(BankWithdrawalOperationPostRequest)
	reg.ReservePubKey = EddsaPublicKey(simulateReservePublicKey())
	reg.Amount = nil
	reg.SelectedExchange = C2EC_BANK_BASE_URL

	time.Sleep(time.Duration(5000) * time.Millisecond)

	res, status, err := HttpPost(
		registrationUrl,
		map[string]string{
			"Authorization": "application/json",
		},
		reg,
		NewJsonCodec[BankWithdrawalOperationPostRequest](),
		NewJsonCodec[BankWithdrawalOperationPostResponse](),
		//cdc.HttpApplicationContentHeader(),
		//bytes.NewReader(regByte.Bytes()),
	)

	if err != nil {
		fmt.Println("WALLET  : error on POST request:", err.Error())
	}

	if status != 200 {
		fmt.Println("WALLET  : response status from registration:", status)
	}

	fmt.Println(res.ConfirmTransferUrl, res.Status, res.TransferDone)

	for r := range awaitSelection {
		fmt.Println("selected parameters:", r.ReservePubKey, r.Status)
	}
}
