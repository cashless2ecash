package main

import (
	"crypto/rand"
	"errors"
	"fmt"
	"strconv"
	"strings"
	"time"
)

var C2EC_BANK_BASE_URL string

func Wallet(in chan *SimulatedPhysicalInteraction, out chan *SimulatedPhysicalInteraction, kill chan error) {

	C2EC_BANK_BASE_URL = CONFIG.C2ecBaseUrl + "/taler-integration"
	var C2EC_BANK_WITHDRAWAL_STATUS_URL = C2EC_BANK_BASE_URL + "/withdrawal-operation/:wopid"
	var C2EC_BANK_WITHDRAWAL_REGISTRATION_URL = C2EC_BANK_BASE_URL + "/withdrawal-operation/:wopid"
	var SIM_WALLET_LONG_POLL_MS_STR = CONFIG.WalletLongPollMs

	fmt.Println("WALLET  : Wallet started. Signaling terminal readiness (this is simulation specific)")
	out <- &SimulatedPhysicalInteraction{Msg: "wallet ready"}

	uriFromQrCode := <-in
	if !CONFIG.DisableDelays {
		time.Sleep(time.Duration(CONFIG.WalletScanQrDelay) * time.Millisecond)
	}
	fmt.Println("WALLET  : simulated QR code scanning... scanned", uriFromQrCode)
	wopid, err := parseTalerWithdrawUri(uriFromQrCode.Msg)
	if err != nil {
		fmt.Println("WALLET  : failed parsing taler withdraw uri. error:", err.Error())
	}
	fmt.Println("WALLET  : Wallet parsed wopid:", wopid)

	// Register Withdrawal
	registrationUrl := FormatUrl(
		C2EC_BANK_WITHDRAWAL_REGISTRATION_URL,
		map[string]string{"wopid": wopid},
		map[string]string{},
	)

	reg := BankWithdrawalOperationPostRequest{
		ReservePubKey:    EddsaPublicKey(simulateReservePublicKey()),
		Amount:           nil,
		SelectedExchange: C2EC_BANK_BASE_URL,
	}

	fmt.Println("WALLET  : wallet sends withdrawal registration request with freshly generated public key.")
	fmt.Printf("HTTP    : requesting POST %s\n", registrationUrl)
	_, status, err := HttpPost[BankWithdrawalOperationPostRequest, any](
		registrationUrl,
		map[string]string{
			"Authorization": "application/json",
		},
		&reg,
		NewJsonCodec[BankWithdrawalOperationPostRequest](),
		nil,
	)

	if err != nil {
		fmt.Println("WALLET  : error on POST request:", err.Error())
		kill <- err
	}

	if status != 200 {
		fmt.Println("WALLET  : response status from registration:", status)
		kill <- errors.New("failed registering the withdrawal parameters")
	}

	// Start long poll for confirmed or abort
	awaitConfirmationOrAbortion := make(chan *C2ECWithdrawalStatus)
	longPollFailed := make(chan error)

	// long poll for confirmation or abortion by c2ec (whihc proves that it worked)
	go func() {
		url := FormatUrl(
			C2EC_BANK_WITHDRAWAL_STATUS_URL,
			map[string]string{"wopid": wopid},
			map[string]string{
				"long_poll_ms": SIM_WALLET_LONG_POLL_MS_STR,
				"old_state":    string(SELECTED),
			},
		)
		println("WALLET  : asking for confirmation or abortion of the withdrawal.")
		response, status, err := HttpGet(
			url,
			map[string]string{"Authorization": TerminalAuth()},
			NewJsonCodec[C2ECWithdrawalStatus](),
		)
		if err != nil {
			kill <- err
			return
		}
		if status != 200 {
			longPollFailed <- errors.New("status of withdrawal status response was " + strconv.Itoa(status))
			return
		}
		if response.CardFees != EXCHANGE_FEES {
			fmt.Printf("WALLET  : ATTENTION -> fees do not match expected=%s, got=%s (can be ok but can also indicate a problem, especially when the fees are lower than the expected, it indicates problems.)\n", EXCHANGE_FEES, response.CardFees)
		} else {
			fmt.Printf("WALLET  : Fees expected=%s, got=%s\n", EXCHANGE_FEES, response.CardFees)
		}
		awaitConfirmationOrAbortion <- response
	}()

	for {
		select {
		case w := <-awaitConfirmationOrAbortion:
			fmt.Println("WALLET  : payment processed:", w.Status)
			if w.Status == CONFIRMED {
				fmt.Println("WALLET  : the exchange would now create the reserve and the wallet can withdraw the reserve")
			}
			if w.Status == ABORTED {
				fmt.Println("WALLET  : the withdrawal was aborted. c2ec cleans up withdrawal")
			}
		case f := <-longPollFailed:
			fmt.Println("WALLET  : long-polling for selection failed... error:", f.Error())
			kill <- f
		}
	}
}

// returns wopid.
func parseTalerWithdrawUri(s string) (string, error) {

	wopid, found := strings.CutPrefix(s, CONFIG.TerminalQrCodeBase)
	if !found {
		return "", errors.New("invalid uri " + s)
	}
	return wopid, nil
}

// creates format compliant reserve public key
func simulateReservePublicKey() string {

	mockedPubKey := make([]byte, 32)
	_, err := rand.Read(mockedPubKey)
	if err != nil {
		return ""
	}
	return talerBinaryEncode(mockedPubKey)
}

type CurrencySpecification struct {
	Name                            string `json:"name"`
	Currency                        string `json:"currency"`
	NumFractionalInputDigits        int    `json:"num_fractional_input_digits"`
	NumFractionalNormalDigits       int    `json:"num_fractional_normal_digits"`
	NumFractionalTrailingZeroDigits int    `json:"num_fractional_trailing_zero_digits"`
	AltUnitNames                    string `json:"alt_unit_names"`
}

// https://docs.taler.net/core/api-bank-integration.html#tsref-type-BankIntegrationConfig
type BankIntegrationConfig struct {
	Name                  string                `json:"name"`
	Version               string                `json:"version"`
	Implementation        string                `json:"implementation"`
	Currency              string                `json:"currency"`
	CurrencySpecification CurrencySpecification `json:"currency_specification"`
	// TODO: maybe add exchanges payto uri for transfers etc.?
}

type BankWithdrawalOperationPostRequest struct {
	ReservePubKey    EddsaPublicKey `json:"reserve_pub"`
	SelectedExchange string         `json:"selected_exchange"`
	Amount           *Amount        `json:"amount"`
}

type BankWithdrawalOperationPostResponse struct {
	Status             WithdrawalOperationStatus `json:"status"`
	ConfirmTransferUrl string                    `json:"confirm_transfer_url"`
	TransferDone       bool                      `json:"transfer_done"`
}

type BankWithdrawalOperationStatus struct {
	Status            WithdrawalOperationStatus `json:"status"`
	Amount            string                    `json:"amount"`
	CardFees          string                    `json:"card_fees"`
	SenderWire        string                    `json:"sender_wire"`
	WireTypes         []string                  `json:"wire_types"`
	ReservePubKey     EddsaPublicKey            `json:"selected_reserve_pub"`
	SuggestedExchange string                    `json:"suggested_exchange"`
	RequiredExchange  string                    `json:"required_exchange"`
	Aborted           bool                      `json:"aborted"`
	SelectionDone     bool                      `json:"selection_done"`
	TransferDone      bool                      `json:"transfer_done"`
}
