// COPIED FROM C2EC
package main

import (
	"bytes"
	"fmt"
	"net/http"
	"strings"
)

const HTTP_GET = "GET"
const HTTP_POST = "POST"

const HTTP_OK = 200
const HTTP_NO_CONTENT = 204
const HTTP_BAD_REQUEST = 400
const HTTP_UNAUTHORIZED = 401
const HTTP_NOT_FOUND = 404
const HTTP_METHOD_NOT_ALLOWED = 405
const HTTP_CONFLICT = 409
const HTTP_INTERNAL_SERVER_ERROR = 500

const TALER_URI_PROBLEM_PREFIX = "taler://problem"

type RFC9457Problem struct {
	TypeUri  string `json:"type"`
	Title    string `json:"title"`
	Detail   string `json:"detail"`
	Instance string `json:"instance"`
}

// Writes a problem as specified by RFC 9457 to
// the response. The problem is always serialized
// as JSON.
func WriteProblem(res http.ResponseWriter, status int, problem *RFC9457Problem) error {

	c := NewJsonCodec[RFC9457Problem]()
	problm, err := c.EncodeToBytes(problem)
	if err != nil {
		return err
	}

	res.WriteHeader(status)
	res.Write(problm)
	return nil
}

// Function reads and validates a param of a request in the
// correct format according to the transform function supplied.
// When the transform fails, it returns false as second return
// value. This indicates the caller, that the request shall not
// be further processed and the handle must be returned by the
// caller. Since the parameter is optional, it can be null, even
// if the boolean return value is set to true.
func AcceptOptionalParamOrWriteResponse[T any](
	name string,
	transform func(s string) (T, error),
	req *http.Request,
	res http.ResponseWriter,
) (*T, bool) {

	ptr, err := OptionalQueryParamOrError(name, transform, req)
	if err != nil {
		err := WriteProblem(res, HTTP_BAD_REQUEST, &RFC9457Problem{
			TypeUri:  TALER_URI_PROBLEM_PREFIX + "/C2EC_INVALID_REQUEST_QUERY_PARAMETER",
			Title:    "invalid request query parameter",
			Detail:   "the withdrawal status request parameter '" + name + "' is malformed (error: " + err.Error() + ")",
			Instance: req.RequestURI,
		})
		if err != nil {
			res.WriteHeader(HTTP_INTERNAL_SERVER_ERROR)
		}
		return nil, false
	}

	if ptr == nil {
		err := WriteProblem(res, HTTP_INTERNAL_SERVER_ERROR, &RFC9457Problem{
			TypeUri:  TALER_URI_PROBLEM_PREFIX + "/C2EC_INVALID_REQUEST_QUERY_PARAMETER",
			Title:    "invalid request query parameter",
			Detail:   "the withdrawal status request parameter '" + name + "' resulted in a nil pointer)",
			Instance: req.RequestURI,
		})
		if err != nil {
			res.WriteHeader(HTTP_INTERNAL_SERVER_ERROR)
		}
		return nil, false
	}

	obj := *ptr
	assertedObj, ok := any(obj).(T)
	if !ok {
		// this should generally not happen (due to the implementation)
		err := WriteProblem(res, HTTP_INTERNAL_SERVER_ERROR, &RFC9457Problem{
			TypeUri:  TALER_URI_PROBLEM_PREFIX + "/C2EC_FATAL_ERROR",
			Title:    "Fatal Error",
			Detail:   "Something strange happened. Probably not your fault.",
			Instance: req.RequestURI,
		})
		if err != nil {
			res.WriteHeader(HTTP_INTERNAL_SERVER_ERROR)
		}
		return nil, false
	}
	return &assertedObj, true
}

// The function parses a parameter of the query
// of the request. If the parameter is not present
// (empty string) it will not create an error and
// just return nil.
func OptionalQueryParamOrError[T any](
	name string,
	transform func(s string) (T, error),
	req *http.Request,
) (*T, error) {

	paramStr := req.URL.Query().Get(name)
	if paramStr != "" {

		if t, err := transform(paramStr); err != nil {
			return nil, err
		} else {
			return &t, nil
		}
	}
	return nil, nil
}

// Reads a generic argument struct from the requests
// body. It takes the codec as argument which is used to
// decode the struct from the request. If an error occurs
// nil and the error are returned.
func ReadStructFromBody[T any](req *http.Request, codec Codec[T]) (*T, error) {

	bodyBytes, err := ReadBody(req)
	if err != nil {
		return nil, err
	}

	return codec.Decode(bytes.NewReader(bodyBytes))
}

// Reads the body of a request into a byte array.
// If the body is empty, an empty array is returned.
// If an error occurs while reading the body, nil and
// the respective error is returned.
func ReadBody(req *http.Request) ([]byte, error) {

	if req.ContentLength < 0 {
		return make([]byte, 0), nil
	}

	body := make([]byte, req.ContentLength)
	_, err := req.Body.Read(body)
	if err != nil {
		return nil, err
	}
	return body, nil
}

// Executes a GET request at the given url.
// Use FormatUrl for to build the url.
// Headers can be defined using the headers map.
func HttpGet[T any](
	url string,
	headers map[string]string,
	codec Codec[T],
) (*T, int, error) {

	req, err := http.NewRequest(HTTP_GET, url, bytes.NewBufferString(""))
	if err != nil {
		return nil, -1, err
	}

	for k, v := range headers {
		req.Header.Add(k, v)
	}
	req.Header.Add("Accept", codec.HttpApplicationContentHeader())

	fmt.Printf("HTTP    : requesting GET %s\n", url)
	res, err := http.DefaultClient.Do(req)
	if err != nil {
		return nil, -1, err
	}

	if res.StatusCode > 299 {
		return nil, res.StatusCode, nil
	}

	if codec == nil {
		return nil, res.StatusCode, err
	} else {
		resBody, err := codec.Decode(res.Body)
		return resBody, res.StatusCode, err
	}
}

func HttpPost[T any, R any](
	url string,
	headers map[string]string,
	body *T,
	reqCodec Codec[T],
	resCodec Codec[R],
) (*R, int, error) {

	bodyEncoded, err := reqCodec.EncodeToBytes(body)
	if err != nil {
		return nil, -1, err
	}

	req, err := http.NewRequest(HTTP_POST, url, bytes.NewBuffer(bodyEncoded))
	if err != nil {
		return nil, -1, err
	}

	for k, v := range headers {
		req.Header.Add(k, v)
	}
	req.Header.Add("Accept", reqCodec.HttpApplicationContentHeader())

	res, err := http.DefaultClient.Do(req)
	if err != nil {
		return nil, -1, err
	}

	if resCodec == nil {
		return nil, res.StatusCode, err
	} else {
		resBody, err := resCodec.Decode(res.Body)
		return resBody, res.StatusCode, err
	}
}

// builds request URL containing the path and query
// parameters of the respective parameter map.
func FormatUrl(
	req string,
	pathParams map[string]string,
	queryParams map[string]string,
) string {

	return setUrlQuery(setUrlPath(req, pathParams), queryParams)
}

// Sets the parameters which are part of the url.
// The function expects each parameter in the path to be prefixed
// using a ':'. The function handles url as follows:
//
//	/some/:param/tobereplaced -> ':param' will be replaced with value.
//
// For replacements, the pathParams map must be supplied. The map contains
// the name of the parameter with the value mapped to it.
// The names MUST not contain the prefix ':'!
func setUrlPath(
	req string,
	pathParams map[string]string,
) string {

	if pathParams == nil || len(pathParams) < 1 {
		return req
	}

	var url = req
	for k, v := range pathParams {

		if !strings.HasPrefix(k, "/") {
			// prevent scheme postfix replacements
			url = strings.Replace(url, ":"+k, v, 1)
		}
	}
	return url
}

func setUrlQuery(
	req string,
	queryParams map[string]string,
) string {

	if queryParams == nil || len(queryParams) < 1 {
		return req
	}

	var url = req + "?"
	for k, v := range queryParams {

		url = strings.Join([]string{url, k, "=", v, "&"}, "")
	}

	url, _ = strings.CutSuffix(url, "&")
	return url
}
