// This file is part of taler-cashless2ecash.
// Copyright (C) 2024 Joel Häberli
//
// taler-cashless2ecash is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as published
// by the Free Software Foundation, either version 3 of the License,
// or (at your option) any later version.
//
// taler-cashless2ecash is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// SPDX-License-Identifier: AGPL3.0-or-later

package ch.bfh.habej2.wallee_c2ec.withdrawal

import android.content.Intent
import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.width
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.CheckCircle
import androidx.compose.material.icons.filled.Warning
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalConfiguration
import androidx.compose.ui.unit.dp
import ch.bfh.habej2.wallee_c2ec.MainActivity
import ch.bfh.habej2.wallee_c2ec.client.taler.model.Amount
import com.squareup.moshi.Json

data class Summary(
    @Json(name = "amount") val amount: Amount,
    @Json(name = "fees") val fees: Amount,
    @Json(name = "currency") val currency: String,
    @Json(name = "encodedWopid") val encodedWopid: String,
    var success: Boolean = true
)

class SummaryActivity : ComponentActivity() {

    companion object {
        var summary: Summary = reset()

        private fun reset(): Summary = Summary(
            Amount("", 0, 0),
            Amount("", 0, 0),
            "",
            ""
        )
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContent {
            val configuration = LocalConfiguration.current
            if (summary.success) {
                Column(
                    Modifier.width(configuration.screenWidthDp.dp),
                    verticalArrangement = Arrangement.Center,
                    horizontalAlignment = Alignment.CenterHorizontally
                ) {
                    Image(imageVector = Icons.Filled.CheckCircle, contentDescription = "Amount authorized successful")

                    Text(text = "Summary")
                    Text(text = "Amount: ${summary.amount.toString(true)}")
                    Text(text = "Fees: ${summary.fees.toString(true)}")

                    FinishButton()
                }
            } else {
                Column(
                    Modifier.width(configuration.screenWidthDp.dp),
                    verticalArrangement = Arrangement.Center,
                    horizontalAlignment = Alignment.CenterHorizontally
                ) {
                    Image(imageVector = Icons.Filled.Warning, contentDescription = "Failed authorizing payment")

                    Text(text = "Failed authorizing payment.")
                    Text(text = "Withdrawal aborted.")

                    FinishButton()
                }
            }
        }
    }

    @Composable
    private fun FinishButton() {
        TalerButton(
            text = "finish",
            onClick = {
                this@SummaryActivity.startActivity(
                    Intent(
                        baseContext,
                        MainActivity::class.java
                    )
                )
                reset()
                finish()
            }
        )
    }
}