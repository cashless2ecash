// This file is part of taler-cashless2ecash.
// Copyright (C) 2024 Joel Häberli
//
// taler-cashless2ecash is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as published
// by the Free Software Foundation, either version 3 of the License,
// or (at your option) any later version.
//
// taler-cashless2ecash is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// SPDX-License-Identifier: AGPL3.0-or-later

package ch.bfh.habej2.wallee_c2ec.client.taler

import ch.bfh.habej2.wallee_c2ec.client.taler.encoding.CyptoUtils.encodeCrock
import ch.bfh.habej2.wallee_c2ec.client.taler.model.BankWithdrawalOperationStatus
import ch.bfh.habej2.wallee_c2ec.client.taler.model.TerminalApiConfig
import ch.bfh.habej2.wallee_c2ec.client.taler.model.TerminalWithdrawalConfirmationRequest
import ch.bfh.habej2.wallee_c2ec.client.taler.model.TerminalWithdrawalSetup
import ch.bfh.habej2.wallee_c2ec.client.taler.model.TerminalWithdrawalSetupResponse
import ch.bfh.habej2.wallee_c2ec.client.taler.model.WithdrawalOperationStatus
import java.security.SecureRandom
import java.util.Optional

class TerminalClientMock: TerminalClient {

    override fun terminalsConfig(callback: (Optional<TerminalApiConfig>) -> Unit) = callback.invoke(
        Optional.of(
            TerminalApiConfig(
                "taler-terminal",
                "0:0:0",
                "Wallee",
                "CHF",
                "CHF:0",
                "wallee-transaction"
            )
        )
    )

    override fun setupWithdrawal(
        setupReq: TerminalWithdrawalSetup,
        callback: (Optional<TerminalWithdrawalSetupResponse>) -> Unit
    ) = callback.invoke(
        Optional.of(
            TerminalWithdrawalSetupResponse(
                mockWopidOrReservePubKey()
            )
        )
    )

    override fun retrieveWithdrawalStatus(
        wopid: String,
        longPollMs: Int,
        oldState: WithdrawalOperationStatus,
        callback: (Optional<BankWithdrawalOperationStatus>) -> Unit
    ) {

        Thread.sleep(longPollMs.toLong())
        callback.invoke(
            Optional.of(
                BankWithdrawalOperationStatus(
                    WithdrawalOperationStatus.selected,
                    "CHF:10",
//                    Amount(10,0),
//                    Amount(0,0),
//                    Amount(0,0),
                    "wallee-transaction",
//                    "http://mock.com/api",
//                    "http://mock.com/api",
//                    "",
                    "payto://IBAN/CH1111111111111",
                    mockWopidOrReservePubKey(),
                    "http://test.com",
                    Array(0) {""},
                    mockWopidOrReservePubKey(),
                    false,
                    false,
                    false
                )
            )
        )
    }

    override fun sendConfirmationRequest(
        encodedWopid: String,
        confirmationRequest: TerminalWithdrawalConfirmationRequest,
        callback: (Boolean) -> Unit
    ) {
        println("sending confirmation request")
    }

    override fun abortWithdrawal(
        encodedWopid: String,
        callback: (Boolean) -> Unit
    ) {
        println("aborting withdrawal")
    }

    private fun mockWopidOrReservePubKey(): String {

        val wopid = ByteArray(32)
        SecureRandom().nextBytes(wopid)
        return encodeCrock(wopid)
    }
}