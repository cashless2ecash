// This file is part of taler-cashless2ecash.
// Copyright (C) 2024 Joel Häberli
//
// taler-cashless2ecash is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as published
// by the Free Software Foundation, either version 3 of the License,
// or (at your option) any later version.
//
// taler-cashless2ecash is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// SPDX-License-Identifier: AGPL3.0-or-later

package ch.bfh.habej2.wallee_c2ec.withdrawal

import android.app.Activity
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.width
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalConfiguration
import androidx.compose.ui.unit.dp
import ch.bfh.habej2.wallee_c2ec.client.taler.config.TalerTerminalConfig

var exchanges = listOf(
    TalerTerminalConfig(
        "BFH Taler (CHF)",
        "https://terminals.chf.taler.net",
        "Wallee-2",
        "YTLYIPPOTNqen//Rl7uc58FUTb8K3Kk0Zp/OSlC0Ufk="
    )
)

@Composable
fun ExchangeSelectionScreen(
    model: WithdrawalViewModel,
    activity: Activity,
    onNavigateToWithdrawal: () -> Unit
) {

    val configuration = LocalConfiguration.current

    Column(
        Modifier.width(configuration.screenWidthDp.dp),
        verticalArrangement = Arrangement.Center,
        horizontalAlignment = Alignment.CenterHorizontally
    ) {

        TalerLogo()

        Text(text = "Choose the exchange to withdraw from")

        exchanges.forEach {
            Row {
                Column(
                ) {
                    TalerButton(text = it.displayName, onClick = {
                        model.chooseExchange(it, activity)
                        onNavigateToWithdrawal()
                    })
                }
            }
        }

        TalerButton(text = "abort", onClick = { activity.finish() })
    }
}
