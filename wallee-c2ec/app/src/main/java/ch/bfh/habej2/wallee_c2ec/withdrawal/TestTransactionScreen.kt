// This file is part of taler-cashless2ecash.
// Copyright (C) 2024 Joel Häberli
//
// taler-cashless2ecash is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as published
// by the Free Software Foundation, either version 3 of the License,
// or (at your option) any later version.
//
// taler-cashless2ecash is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// SPDX-License-Identifier: AGPL3.0-or-later

package ch.bfh.habej2.wallee_c2ec.withdrawal

import androidx.compose.foundation.layout.Column
import androidx.compose.material3.Button
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import com.wallee.android.till.sdk.ApiClient
import com.wallee.android.till.sdk.data.LineItem
import com.wallee.android.till.sdk.data.Transaction
import com.wallee.android.till.sdk.data.TransactionProcessingBehavior
import java.math.BigDecimal
import java.util.Currency
import java.util.UUID

const val TEST_TRANSACTION_PREFIX = "TEST_TRANSACTION"

@Composable
fun TestTransactionScreen(walleeClient: ApiClient) {

    val lineitemuuid = UUID.randomUUID().toString()
    val merchRef = UUID.randomUUID().toString()
    val invRef = UUID.randomUUID().toString()

    val withdrawalAmount = LineItem
        .ListBuilder(TEST_TRANSACTION_PREFIX+lineitemuuid, BigDecimal(9.0))
        .build()

    val transaction = Transaction.Builder(withdrawalAmount)
        .setCurrency(Currency.getInstance("CHF"))
        .setInvoiceReference(invRef)
        .setMerchantReference(merchRef)
        .setTransactionProcessingBehavior(TransactionProcessingBehavior.COMPLETE_IMMEDIATELY)
        .build()

    Column {
        Button(onClick = {
            try {
                walleeClient.authorizeTransaction(transaction)
                //walleeClient.unbind(activity)
            } catch (e: Exception) {
                println("FAILED authorizing transaction ${e.message}")
                e.printStackTrace()
            }
        }) {
            Text(text = "Authorize test transaction")
        }
    }
}