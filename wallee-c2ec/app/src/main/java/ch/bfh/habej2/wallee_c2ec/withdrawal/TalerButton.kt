package ch.bfh.habej2.wallee_c2ec.withdrawal

import androidx.compose.foundation.layout.width
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Button
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalConfiguration
import androidx.compose.ui.unit.dp

@Composable
fun TalerButton(text: String, onClick: () -> Unit, enableExpr: () -> Boolean = { true }) {

    val configuration = LocalConfiguration.current

    Button(
        onClick = onClick,
        enabled = enableExpr(),
        modifier = Modifier
            .width((configuration.screenWidthDp * 0.8).dp),
        shape = RoundedCornerShape(0.dp)
    ) {
        Text(text = text)
    }
}