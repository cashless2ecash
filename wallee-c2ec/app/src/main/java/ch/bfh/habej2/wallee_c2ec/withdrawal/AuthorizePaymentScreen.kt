// This file is part of taler-cashless2ecash.
// Copyright (C) 2024 Joel Häberli
//
// taler-cashless2ecash is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as published
// by the Free Software Foundation, either version 3 of the License,
// or (at your option) any later version.
//
// taler-cashless2ecash is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// SPDX-License-Identifier: AGPL3.0-or-later

package ch.bfh.habej2.wallee_c2ec.withdrawal

import android.app.Activity
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.width
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalConfiguration
import androidx.compose.ui.unit.dp
import com.wallee.android.till.sdk.ApiClient
import com.wallee.android.till.sdk.data.LineItem
import com.wallee.android.till.sdk.data.Transaction
import com.wallee.android.till.sdk.data.TransactionCompletion
import com.wallee.android.till.sdk.data.TransactionProcessingBehavior
import java.util.Currency

@Composable
fun AuthorizePaymentScreen(model: WithdrawalViewModel, activity: Activity, client: ApiClient) {

    val uiState by model.uiState.collectAsState()
    val configuration = LocalConfiguration.current

    val withdrawalAmount = LineItem
        .ListBuilder(uiState.encodedWopid, uiState.amount.plus(uiState.withdrawalFees).toBigDecimal())
        .build()

    LaunchedEffect(Unit) {

        println("LaunchedEffect: starting authorization")

        if (uiState.transactionState == TransactionState.READY_FOR_AUTHORIZATION) {
            val transaction = Transaction.Builder(withdrawalAmount)
                .setCurrency(Currency.getInstance(uiState.currency))
                .setInvoiceReference(uiState.encodedWopid)
                .setMerchantReference(uiState.encodedWopid)
                .setTransactionProcessingBehavior(TransactionProcessingBehavior.COMPLETE_IMMEDIATELY)
                .build()

            try {
                client.authorizeTransaction(transaction)
                model.setAuthorizing()
            } catch (e: Exception) {
                println("FAILED authorizing transaction ${e.message}")
                model.withdrawalOperationFailed(activity)
                e.printStackTrace()
            }
        }
    }

    Column(
        Modifier.width(configuration.screenWidthDp.dp),
        verticalArrangement = Arrangement.Center,
        horizontalAlignment = Alignment.CenterHorizontally
    ) {

        when (uiState.transactionState) {
            TransactionState.UNREADY_FOR_AUTHORIZATION -> model.withdrawalOperationFailed(activity)
            TransactionState.READY_FOR_AUTHORIZATION -> WalleeProcessingTransaction()
            TransactionState.AUTHORIZATION_TRIGGERED -> WalleeProcessingTransaction()
            TransactionState.AUTHORIZATION_FAILED -> model.withdrawalOperationFailed(activity)
            // TODO : Coordination of Wallee SDK components and this app must be looked at
            TransactionState.AUTHORIZED -> WalleeCompletingPayment(client, model) // TODO: Maybe following is not needed because completion immediately is specified... WalleeCompletingPayment(client, model)
            TransactionState.COMPLETION_TRIGGERED -> WalleeProcessingTransaction()
            TransactionState.COMPLETED -> model.talerCheckWalleeRequest(activity)
            TransactionState.COMPLETION_FAILED -> model.withdrawalOperationFailed(activity)
        }
    }
}

@Composable
private fun WalleeCompletingPayment(
    client: ApiClient,
    model: WithdrawalViewModel
) {

    val uiState by model.uiState.collectAsState()

    Text(text = "completing transaction...")

    LaunchedEffect(key1 = Unit) {

        if (uiState.transactionState == TransactionState.AUTHORIZED) {
            client.completeTransaction(
                TransactionCompletion.Builder(uiState.transaction!!.transaction.lineItems)
                    .setCurrency(uiState.transaction!!.transaction.currency)
                    .setReserveReference(uiState.transaction!!.reserveReference!!)
                    .build()
            )
            model.setCompleting()
        }
    }
}

@Composable
private fun WalleeProcessingTransaction() {
    Text(text = "processing transaction...")
}
