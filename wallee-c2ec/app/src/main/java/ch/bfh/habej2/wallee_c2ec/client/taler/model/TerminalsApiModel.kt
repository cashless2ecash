// This file is part of taler-cashless2ecash.
// Copyright (C) 2024 Joel Häberli
//
// taler-cashless2ecash is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as published
// by the Free Software Foundation, either version 3 of the License,
// or (at your option) any later version.
//
// taler-cashless2ecash is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// SPDX-License-Identifier: AGPL3.0-or-later

package ch.bfh.habej2.wallee_c2ec.client.taler.model

import com.squareup.moshi.Json

data class TerminalApiConfig(
    @Json(name = "name") val name: String,
    @Json(name = "version") val version: String,
    @Json(name = "provider_name") val providerName: String,
    @Json(name = "currency") val currency: String,
    @Json(name = "withdrawal_fees") val exchangeFees: String,
    @Json(name = "wire_type") val wireType: String
)

data class TerminalWithdrawalSetup(
    @Json(name = "request_uid") val requestUid: String,
    @Json(name = "amount") val amount: String,
    @Json(name = "suggested_amount") val suggestedAmount: String = "",
    @Json(name = "provider_transaction_id") val providerTransactionId: String = "",
    @Json(name = "terminal_fees") val terminalFees: String = "",
    @Json(name = "user_uuid") val userUuid: String = "",
    @Json(name = "lock") val lock: String = ""
)

data class TerminalWithdrawalSetupResponse(
    @Json(name = "withdrawal_id") val withdrawalId: String
)

data class TerminalWithdrawalConfirmationRequest(
    @Json(name = "provider_transaction_id") val providerTransactionId: String,
    @Json(name = "terminal_fees") val terminalFees: String
)

enum class WithdrawalOperationStatus(val value: String) {

    pending("pending"),
    selected("selected"),
    confirmed("confirmed"),
    aborted("aborted")
}

data class BankWithdrawalOperationStatus(
    @Json(name = "status") val status: WithdrawalOperationStatus,
    @Json(name = "amount") val amount: String,
//    @Json(name = "suggested_amount") val suggestedAmount: String,
//    @Json(name = "max_amount") val maxAmount: String,
    @Json(name = "card_fees") val cardFees: String,
    @Json(name = "sender_wire") val senderWire: String,
    @Json(name = "suggested_exchange") val suggestedExchange: String,
    @Json(name = "required_exchange") val requiredExchange: String,
//    @Json(name = "confirm_transfer_url") val confirmTransferUrl: String,
    @Json(name = "wire_types") val wireTypes: Array<String>,
    @Json(name = "selected_reserve_pub") val selectedReservePub: String,
//    @Json(name = "selected_exchange_account") val selectedExchangeAccount: String,
    @Json(name = "aborted") val aborted: Boolean,
    @Json(name = "selection_done") val selectionDone: Boolean,
    @Json(name = "transfer_done") val transferDone: Boolean
)
