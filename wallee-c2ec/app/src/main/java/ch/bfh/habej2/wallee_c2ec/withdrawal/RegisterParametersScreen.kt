// This file is part of taler-cashless2ecash.
// Copyright (C) 2024 Joel Häberli
//
// taler-cashless2ecash is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as published
// by the Free Software Foundation, either version 3 of the License,
// or (at your option) any later version.
//
// taler-cashless2ecash is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// SPDX-License-Identifier: AGPL3.0-or-later

package ch.bfh.habej2.wallee_c2ec.withdrawal

import android.app.Activity
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.width
import androidx.compose.material3.Button
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalConfiguration
import androidx.compose.ui.unit.dp

private const val MAX_RETRIES = 6 // one retry runs 10sec (long-poll) -> 6x10sec = 1min

@Composable
fun RegisterParametersScreen(
    model: WithdrawalViewModel,
    activity: Activity,
    navigateToWhenRegistered: () -> Unit
) {

    val uiState by model.uiState.collectAsState()
    val configuration = LocalConfiguration.current

    LaunchedEffect(uiState.setupWithdrawalRetries) {
        // this will run exactly once -> Unit as parameter to LaunchedEffect.
        if (ManageActivity.SIM_WALLET_ENABLED.value) {
            ManageActivity.simulateParameterRegistration(
                uiState.terminalsApiBasePath,
                uiState.encodedWopid
            )
        }
        if (model.uiState.value.setupWithdrawalRetries < 1 &&
            model.uiState.value.transactionState == TransactionState.UNREADY_FOR_AUTHORIZATION) {
            println("starting authorization")
            model.startAuthorizationWhenReadyOrSetRetry()
        }
    }

    Column(
        Modifier.width(configuration.screenWidthDp.dp),
        verticalArrangement = Arrangement.Center,
        horizontalAlignment = Alignment.CenterHorizontally
    ) {

        if (uiState.transactionState == TransactionState.READY_FOR_AUTHORIZATION) {
            navigateToWhenRegistered()
            return
        }

        if (uiState.setupWithdrawalRetries > MAX_RETRIES &&
            uiState.transactionState == TransactionState.UNREADY_FOR_AUTHORIZATION) {
            println("maximal retries exceeded")
            model.withdrawalOperationFailed(activity)
            return
        }

        Text(text = "Scan the QR Code with your Taler Wallet to")
        Text(text = "register the withdrawal parameters")

        QRCode(
            TalerConstants.formatTalerUri(
                uiState.terminalsApiBasePath,
                uiState.encodedWopid
            )
        )

        TalerButton(text = "abort", onClick = { model.withdrawalOperationFailed(activity) })
    }
}