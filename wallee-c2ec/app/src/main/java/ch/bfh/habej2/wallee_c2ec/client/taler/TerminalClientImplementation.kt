// This file is part of taler-cashless2ecash.
// Copyright (C) 2024 Joel Häberli
//
// taler-cashless2ecash is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as published
// by the Free Software Foundation, either version 3 of the License,
// or (at your option) any later version.
//
// taler-cashless2ecash is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// SPDX-License-Identifier: AGPL3.0-or-later

package ch.bfh.habej2.wallee_c2ec.client.taler

import ch.bfh.habej2.wallee_c2ec.client.taler.config.TalerTerminalConfig
import ch.bfh.habej2.wallee_c2ec.client.taler.model.BankWithdrawalOperationStatus
import ch.bfh.habej2.wallee_c2ec.client.taler.model.TerminalApiConfig
import ch.bfh.habej2.wallee_c2ec.client.taler.model.TerminalWithdrawalConfirmationRequest
import ch.bfh.habej2.wallee_c2ec.client.taler.model.TerminalWithdrawalSetup
import ch.bfh.habej2.wallee_c2ec.client.taler.model.TerminalWithdrawalSetupResponse
import ch.bfh.habej2.wallee_c2ec.client.taler.model.WithdrawalOperationStatus
import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import kotlinx.coroutines.asCoroutineDispatcher
import okhttp3.HttpUrl.Companion.toHttpUrl
import okhttp3.Interceptor
import okhttp3.MediaType.Companion.toMediaType
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.RequestBody.Companion.toRequestBody
import okhttp3.Response
import java.util.Optional
import java.util.concurrent.Executors

class TerminalClientImplementation  (
    private val config: TalerTerminalConfig
): TerminalClient {

    private val dispatcher = Executors.newSingleThreadExecutor().asCoroutineDispatcher()

    private val client: OkHttpClient =
        OkHttpClient.Builder()
            .addInterceptor(TerminalsApiBasicAuthInterceptor(config))
            .build()

    companion object {
        fun <T> serializer(clazz: Class<T>) = Moshi.Builder()
            .add(KotlinJsonAdapterFactory())
            .build()
            .adapter(clazz)
    }

    private fun baseUrlBuilder() = config.terminalApiBaseUrl.toHttpUrl()

    private fun terminalsConfigUrl() = baseUrlBuilder().newBuilder()
        .addPathSegment("config")
        .build()

    private fun setupWithdrawalUrl() = baseUrlBuilder().newBuilder()
        .addPathSegment("withdrawals")
        .build()

    private fun withdrawalsByWopid(encodedWopid: String) = baseUrlBuilder().newBuilder()
        .addPathSegment("withdrawals")
        .addPathSegment(encodedWopid)
        .build()

    private fun withdrawalsConfirm(encodedWopid: String) = withdrawalsByWopid(encodedWopid)
        .newBuilder()
        .addPathSegment("check")
        .build()

    private fun withdrawalsAbort(encodedWopid: String) = withdrawalsByWopid(encodedWopid)
        .newBuilder()
        .addPathSegment("abort")
        .build()

    override fun terminalsConfig(
        callback: (Optional<TerminalApiConfig>) -> Unit
    ) = dispatcher.executor.execute {
        val url = terminalsConfigUrl()
        println("requesting $url")
        val req = Request.Builder()
            .get()
            .url(url)
            .build()
        client.newCall(req).execute().use {
            if (it.isSuccessful) {
                callback(parseOrEmpty(it))
            } else {
                callback(Optional.empty())
            }
        }
    }

    override fun setupWithdrawal(
        setupReq: TerminalWithdrawalSetup,
        callback: (Optional<TerminalWithdrawalSetupResponse>) -> Unit
    ) = dispatcher.executor.execute {
        val url = setupWithdrawalUrl()
        println("requesting $url")
        val reqBody = serializer(TerminalWithdrawalSetup::class.java)
            .toJson(setupReq)
            .toRequestBody("application/json".toMediaType())
        val req = Request.Builder()
            .post(reqBody)
            .url(url)
            .build()
        try {
            client.newCall(req).execute().use {
                if (it.isSuccessful) {
                    callback(parseOrEmpty(it))
                } else {
                    callback(Optional.empty())
                }
            }
        } catch (ex: Exception) {
            println("exception occured: $ex")
            callback(Optional.empty())
        }
    }

    override fun retrieveWithdrawalStatus(
        wopid: String,
        longPollMs: Int,
        oldState: WithdrawalOperationStatus,
        callback: (Optional<BankWithdrawalOperationStatus>) -> Unit
    ) = dispatcher.executor.execute  {
        val url = withdrawalsByWopid(wopid)
            .newBuilder()
            .addQueryParameter("long_poll_ms", longPollMs.toString())
            .addQueryParameter("old_state", oldState.value)
            .build()
        println("requesting $url")
        val req = Request.Builder()
            .get()
            .url(url)
            .build()
        try {
            client.newCall(req).execute().use {
                if (it.isSuccessful) {
                    if (it.code == 204) {
                        // in case when old_state == current state (for the terminal this doesn't matter
                        val url2 = withdrawalsByWopid(wopid)
                            .newBuilder()
                            .build()
                        println("requesting $url2")
                        val req2 = Request.Builder()
                            .get()
                            .url(url2)
                            .build()
                        client.newCall(req2).execute().use { res2 ->
                            if (res2.isSuccessful) {
                                val opt = parseOrEmpty<BankWithdrawalOperationStatus>(res2)
                                if (opt.isPresent) {
                                    if (opt.get().status == WithdrawalOperationStatus.pending) {
                                        callback(Optional.empty())
                                    } else {
                                        callback(opt)
                                    }
                                } else {
                                    callback(opt)
                                }
                                callback(parseOrEmpty(res2))
                            } else {
                                callback(Optional.empty())
                            }
                        }
                    } else {
                        callback(parseOrEmpty(it))
                    }
                } else {
                    callback(Optional.empty())
                }
            }
        } catch (ex: Exception) {
            println("withdrawal status request failed: ${ex.message}")
            callback(Optional.empty())
        }
    }

    override fun sendConfirmationRequest(
        encodedWopid: String,
        confirmationRequest: TerminalWithdrawalConfirmationRequest,
        callback: (Boolean) -> Unit
    ) = dispatcher.executor.execute {
        val url = withdrawalsConfirm(encodedWopid)
        println("requesting $url")
        val reqBody = serializer(TerminalWithdrawalConfirmationRequest::class.java)
            .toJson(confirmationRequest)
            .toRequestBody("application/json".toMediaType())
        val req = Request.Builder()
            .post(reqBody)
            .url(url)
            .build()
        client.newCall(req).execute().use {
            if (it.code != 204) {
                callback(false)
            }
            callback(true)
        }
    }

    override fun abortWithdrawal(
        encodedWopid: String,
        callback: (Boolean) -> Unit
    ) = dispatcher.executor.execute {
        val url = withdrawalsAbort(encodedWopid)
        println("requesting $url")
        val req = Request.Builder()
            .delete()
            .url(withdrawalsAbort(encodedWopid))
            .build()
        client.newCall(req).execute().use {
            if (it.isSuccessful) {
                callback(true)
            } else {
                callback(false)
            }
        }
    }

    private inline fun <reified T: Any> parseOrEmpty(response: Response): Optional<T> {

        if (response.isSuccessful) {
            if (response.body != null) {
                val content = serializer(T::class.java).fromJson(response.body!!.source())
                if (content != null) {
                    return Optional.of(content)
                }
                return Optional.empty()
            }
            return Optional.empty()
        }
        return Optional.empty()
    }

    private class TerminalsApiBasicAuthInterceptor(
        private val config: TalerTerminalConfig
    ) : Interceptor {

        override fun intercept(chain: Interceptor.Chain): Response {

            val base64EncodedCredentials = java.util.Base64
                .getEncoder()
                .encodeToString("${config.username}:${config.accessToken}".toByteArray())

            val authorizationHeaderValue = "Basic $base64EncodedCredentials"
            return chain.proceed(
                chain.request().newBuilder()
                    .header("Authorization", authorizationHeaderValue)
                    .build()
            )
        }
    }
}
