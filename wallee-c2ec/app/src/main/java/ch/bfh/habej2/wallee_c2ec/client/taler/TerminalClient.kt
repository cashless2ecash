// This file is part of taler-cashless2ecash.
// Copyright (C) 2024 Joel Häberli
//
// taler-cashless2ecash is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as published
// by the Free Software Foundation, either version 3 of the License,
// or (at your option) any later version.
//
// taler-cashless2ecash is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// SPDX-License-Identifier: AGPL3.0-or-later

package ch.bfh.habej2.wallee_c2ec.client.taler

import ch.bfh.habej2.wallee_c2ec.client.taler.model.Amount
import ch.bfh.habej2.wallee_c2ec.client.taler.model.BankWithdrawalOperationStatus
import ch.bfh.habej2.wallee_c2ec.client.taler.model.TerminalApiConfig
import ch.bfh.habej2.wallee_c2ec.client.taler.model.TerminalWithdrawalConfirmationRequest
import ch.bfh.habej2.wallee_c2ec.client.taler.model.TerminalWithdrawalSetup
import ch.bfh.habej2.wallee_c2ec.client.taler.model.TerminalWithdrawalSetupResponse
import ch.bfh.habej2.wallee_c2ec.client.taler.model.WithdrawalOperationStatus
import java.util.Optional

interface TerminalClient {

    companion object {

        fun FormatAmount(a: Amount) = a.toJSONString()
    }

    fun terminalsConfig(
        callback: (Optional<TerminalApiConfig>) -> Unit
    )

    fun setupWithdrawal(
        setupReq: TerminalWithdrawalSetup,
        callback: (Optional<TerminalWithdrawalSetupResponse>) -> Unit
    )

    fun retrieveWithdrawalStatus(
        wopid: String,
        longPollMs: Int,
        oldState: WithdrawalOperationStatus = WithdrawalOperationStatus.pending,
        callback: (Optional<BankWithdrawalOperationStatus>) -> Unit
    )

    fun sendConfirmationRequest(
        encodedWopid: String,
        confirmationRequest: TerminalWithdrawalConfirmationRequest,
        callback: (Boolean) -> Unit
    )

    fun abortWithdrawal(
        encodedWopid: String,
        callback: (Boolean) -> Unit
    )
}