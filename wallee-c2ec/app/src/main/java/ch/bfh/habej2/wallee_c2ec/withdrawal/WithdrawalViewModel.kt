// This file is part of taler-cashless2ecash.
// Copyright (C) 2024 Joel Häberli
//
// taler-cashless2ecash is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as published
// by the Free Software Foundation, either version 3 of the License,
// or (at your option) any later version.
//
// taler-cashless2ecash is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// SPDX-License-Identifier: AGPL3.0-or-later

package ch.bfh.habej2.wallee_c2ec.withdrawal

import android.app.Activity
import android.content.Intent
import androidx.compose.runtime.Stable
import androidx.compose.runtime.derivedStateOf
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableIntStateOf
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import ch.bfh.habej2.wallee_c2ec.client.taler.TerminalClient
import ch.bfh.habej2.wallee_c2ec.client.taler.TerminalClientImplementation
import ch.bfh.habej2.wallee_c2ec.client.taler.config.TalerTerminalConfig
import ch.bfh.habej2.wallee_c2ec.client.taler.encoding.CyptoUtils
import ch.bfh.habej2.wallee_c2ec.client.taler.model.Amount
import ch.bfh.habej2.wallee_c2ec.client.taler.model.AmountParserException
import ch.bfh.habej2.wallee_c2ec.client.taler.model.TerminalWithdrawalConfirmationRequest
import ch.bfh.habej2.wallee_c2ec.client.taler.model.TerminalWithdrawalSetup
import ch.bfh.habej2.wallee_c2ec.withdrawal.WithdrawalViewModel.Companion.generateTransactionIdentifier
import com.wallee.android.till.sdk.ApiClient
import com.wallee.android.till.sdk.data.State
import com.wallee.android.till.sdk.data.TransactionCompletionResponse
import com.wallee.android.till.sdk.data.TransactionResponse
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch
import java.io.Closeable
import java.security.SecureRandom
import java.util.Optional
import java.util.UUID

object TalerConstants {
    const val TALER_INTEGRATION = "/taler-integration"

    fun formatTalerUri(terminalsApiBasePath: String, encodedWopid: String) =
        "taler://withdraw/${stripLeadingProtocolIfPresent(terminalsApiBasePath)}/$encodedWopid"

    private fun stripLeadingProtocolIfPresent(terminalsApiBasePath: String) = terminalsApiBasePath
            .removePrefix("https://")
            .removePrefix("http://")
}

@Stable
interface WithdrawalOperationState {
    val requestUid: String
    val setupWithdrawalRetries: Int
    val terminalsApiBasePath: String
    val encodedWopid: String
    val amount: Amount
    val amountStr: String
    val amountError: String
    val currency: String
    val withdrawalFees: Amount
    val transactionState: TransactionState
    val transaction: TransactionResponse?
    val transactionCompletion: TransactionCompletionResponse?
    val transactionId: String
}

enum class TransactionState {
    UNREADY_FOR_AUTHORIZATION,
    READY_FOR_AUTHORIZATION,
    AUTHORIZATION_TRIGGERED,
    AUTHORIZED,
    AUTHORIZATION_FAILED,
    COMPLETION_TRIGGERED,
    COMPLETION_FAILED,
    COMPLETED,
}

private class MutableWithdrawalOperationState : WithdrawalOperationState {
    override val requestUid: String by derivedStateOf { UUID.randomUUID().toString() }
    override var setupWithdrawalRetries: Int by mutableIntStateOf(0)
    override var terminalsApiBasePath: String by mutableStateOf("")
    override var encodedWopid: String by mutableStateOf("")
    override var amount: Amount by mutableStateOf(Amount("",0, 0))
    override var amountStr: String by mutableStateOf("")
    override var amountError: String by mutableStateOf("")
    override var currency: String by mutableStateOf("")
    override var withdrawalFees: Amount by mutableStateOf(Amount("", 0,0))
    override var transactionState: TransactionState by mutableStateOf(TransactionState.UNREADY_FOR_AUTHORIZATION)
    override var transaction: TransactionResponse? by mutableStateOf(null)
    override var transactionCompletion: TransactionCompletionResponse? by mutableStateOf(null)
    override val transactionId: String by derivedStateOf { generateTransactionIdentifier() }
}

class WithdrawalViewModel(
    private val walleeClient: ApiClient,
    vararg closeables: Closeable
) : ViewModel(*closeables) {

    private var exchangeSelected = false
    private var terminalClient: TerminalClient? = null
    private val _uiState = MutableStateFlow(MutableWithdrawalOperationState())
    val uiState: StateFlow<WithdrawalOperationState> = _uiState

    companion object {
        fun generateTransactionIdentifier(): String {
            val wopid = ByteArray(32)
            SecureRandom().nextBytes(wopid)
            return CyptoUtils.encodeCrock(wopid)
        }
    }

    fun chooseExchange(cfg: TalerTerminalConfig, activity: Activity) {

        if (exchangeSelected) {
            println("exchange cannot be changed after selection.")
            return
        }

        terminalClient = TerminalClientImplementation(cfg)
        //terminalClient = TerminalClientMock()
        _uiState.value = MutableWithdrawalOperationState() // reset withdrawal operation

        terminalClient!!.terminalsConfig {
            println("terminal config request result present: ${it.isPresent}")
            if (!it.isPresent) {
                activity.finish()
            } else {
                _uiState.value.terminalsApiBasePath =
                    "${cfg.terminalApiBaseUrl}${TalerConstants.TALER_INTEGRATION}"
                this@WithdrawalViewModel.updateCurrency(it.get().currency)
                if (!this@WithdrawalViewModel.updateWithdrawalFees(it.get().exchangeFees)) {
                    activity.finish()
                }
            }
        }
        exchangeSelected = true
    }

    fun setupWithdrawal(
        activity: Activity,
        navigateToWhenAmountEntered: () -> Unit
    ) {

        val setupReq = TerminalWithdrawalSetup(
            _uiState.value.requestUid,
            TerminalClient.FormatAmount(_uiState.value.amount)
        )

        viewModelScope.launch {
            terminalClient!!.setupWithdrawal(setupReq) {
                if (!it.isPresent) {
                    withdrawalOperationFailed(activity)
                }
                val wopid = it.get().withdrawalId
                println("retrieved WOPID from c2ec: $wopid")
                _uiState.value.encodedWopid = wopid
            }
        }
    }

    fun validateInput(amount: String): Boolean {

        println("validating amount input: $amount")
        val validAmount = parseAmount(amount)
        if (validAmount.isPresent) {
            _uiState.value.amountError = ""
            _uiState.value.amountStr = amount
            _uiState.value.amount = validAmount.get()
            return true
        } else {
            _uiState.value.amountError = "invalid amount (format: X[.X])"
            return false
        }
    }

    private fun updateCurrency(currency: String) {
        _uiState.value.currency = currency
        _uiState.value.amount = Amount(currency, _uiState.value.amount.value, _uiState.value.amount.fraction)
    }

    private fun updateWithdrawalFees(exchangeFees: String): Boolean {

        val optRes = this.parseAmount(exchangeFees)
        if (!optRes.isPresent) {
            return false
        }

        _uiState.value.withdrawalFees = optRes.get()

        return true
    }

    fun setAuthorizing() {
        _uiState.value.transactionState = TransactionState.AUTHORIZATION_TRIGGERED
    }

    fun setCompleting() {
        _uiState.value.transactionState = TransactionState.COMPLETION_TRIGGERED
    }

    fun updateWalleeTransactionReply(response: TransactionResponse) {

        println("updating wallee transaction: $response")
        _uiState.value.transaction = response
        _uiState.value.transactionState =
            if (response.state.name == "SUCCESSFUL")
                TransactionState.AUTHORIZED
            else
                TransactionState.AUTHORIZATION_FAILED
    }

    fun updateWalleeTransactionCompletion(
        completion: TransactionCompletionResponse
    ) {

        if (completion.state == State.FAILED) {
            println("completion of the transaction failed... aborting")
            _uiState.value.transactionState = TransactionState.COMPLETION_FAILED
            return
        }

        if (ManageActivity.INSTANT_SETTLEMENT_ENABLED.value) {
            walleeClient.executeFinalBalance()
        }

        _uiState.value.transactionState = TransactionState.COMPLETED
        _uiState.value.transactionCompletion = completion
    }

    fun startAuthorizationWhenReadyOrSetRetry() = viewModelScope.launch {
        terminalClient!!.retrieveWithdrawalStatus(uiState.value.encodedWopid, 10000) {
            if (!it.isPresent) {
                println("setting retry because no status was present")
                _uiState.value.setupWithdrawalRetries += 1
            } else {
                println("withdrawal status: ${it.get().status }")
                _uiState.value.transactionState = TransactionState.READY_FOR_AUTHORIZATION
            }
        }
    }

    fun talerCheckWalleeRequest(activity: Activity) {
        viewModelScope.launch {
            terminalClient!!.sendConfirmationRequest(
                _uiState.value.encodedWopid,
                TerminalWithdrawalConfirmationRequest(
                    _uiState.value.encodedWopid,
                    _uiState.value.withdrawalFees.toJSONString()
                )
            ) {
                if (!it) {
                    withdrawalOperationFailed(activity)
                    return@sendConfirmationRequest
                }
                SummaryActivity.summary =
                    Summary(
                        _uiState.value.amount,
                        _uiState.value.withdrawalFees,
                        _uiState.value.currency,
                        _uiState.value.encodedWopid
                    )
                val intent = Intent(activity, SummaryActivity::class.java)
                activity.startActivity(intent)
            }
        }
    }

    fun withdrawalOperationFailed(activity: Activity) {
        viewModelScope.launch {
            if (_uiState.value.transactionState == TransactionState.UNREADY_FOR_AUTHORIZATION ||
                _uiState.value.transactionState == TransactionState.AUTHORIZATION_FAILED ||
                _uiState.value.transactionState == TransactionState.COMPLETION_FAILED) {
                terminalClient!!.abortWithdrawal(uiState.value.encodedWopid) {
                    SummaryActivity.summary =
                        Summary(
                            _uiState.value.amount,
                            _uiState.value.withdrawalFees,
                            _uiState.value.currency,
                            _uiState.value.encodedWopid,
                            success = false
                        )
                    val intent = Intent(activity, SummaryActivity::class.java)
                    activity.startActivity(intent)
                }
            }
        }
    }

    fun validAmount(inp: String) = Regex("\\d+(\\.\\d+)?").matches(inp)

    /**
     * Format expected [CURRENCY:]X[.X], X an integer
     */
    private fun parseAmount(inp: String): Optional<Amount> {

        return if (inp.contains(":")) {
            try {
                Optional.of(Amount.fromJSONString(inp))
            } catch (ex: AmountParserException) {
                println("failed parsing amount with currency prefix: $ex")
                Optional.empty()
            }
        } else {
            try {
                Optional.of(Amount.fromString(uiState.value.currency, inp))
            } catch (ex: AmountParserException) {
                println("failed parsing plain amount: $ex")
                Optional.empty()
            }
        }
    }

    fun resetAmountStr() {
        println("resetting amountStr. was ${_uiState.value.amountStr}")
        _uiState.value.amountStr = ""
    }
}