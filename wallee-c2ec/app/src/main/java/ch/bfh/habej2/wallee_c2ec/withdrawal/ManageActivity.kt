// This file is part of taler-cashless2ecash.
// Copyright (C) 2024 Joel Häberli
//
// taler-cashless2ecash is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as published
// by the Free Software Foundation, either version 3 of the License,
// or (at your option) any later version.
//
// taler-cashless2ecash is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// SPDX-License-Identifier: AGPL3.0-or-later

package ch.bfh.habej2.wallee_c2ec.withdrawal

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material3.Button
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.mutableStateOf
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import ch.bfh.habej2.wallee_c2ec.client.wallee.WalleeResponseHandler
import ch.bfh.habej2.wallee_c2ec.ui.theme.Walleec2ecTheme
import com.squareup.moshi.Json
import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import com.wallee.android.till.sdk.ApiClient
import kotlinx.coroutines.asCoroutineDispatcher
import okhttp3.MediaType.Companion.toMediaType
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.RequestBody.Companion.toRequestBody
import java.util.concurrent.Executors

class ManageActivity : ComponentActivity()  {

    private lateinit var walleeClient: ApiClient

    private var unbound = false
    
    companion object {

        var SIM_WALLET_ENABLED = mutableStateOf(false)

        var INSTANT_SETTLEMENT_ENABLED = mutableStateOf(false)

        private data class Reg(
            @Json(name = "reserve_pub") val reservePubKey: String,
            @Json(name = "selected_exchange") val selectedExchange: String
        )

        // this is only for testing and simulates a wallet registering parameters
        fun simulateParameterRegistration(
            exchangeBankIntegrationApiUrl: String,
            wopid: String,

        ) {
            println("simulating wallet parameter selection")
            // we will just use the wopid as reserve pub key...
            val reg = Moshi.Builder()
                .add(KotlinJsonAdapterFactory())
                .build()
                .adapter(Reg::class.java)
                .toJson(Reg(wopid, exchangeBankIntegrationApiUrl))
                .toRequestBody("application/json".toMediaType())
            Executors.newSingleThreadExecutor().asCoroutineDispatcher().executor.execute {
                val url = "$exchangeBankIntegrationApiUrl/withdrawal-operation/$wopid"
                println("requesting $url")
                val req = Request.Builder()
                    .post(reg)
                    .url(url)
                    .build()
                OkHttpClient.Builder().build().newCall(req).execute().use {
                    println("registered parameters: HTTP ${it.code}")
                }
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            Walleec2ecTheme {
                // A surface container using the 'background' color from the theme
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    Column(
                        horizontalAlignment = Alignment.CenterHorizontally
                    ) {
                        TalerButton(
                            text = "Execute Final Balance",
                            onClick = { walleeClient.executeFinalBalance() }
                        )

                        TalerButton(
                            text = "${if (INSTANT_SETTLEMENT_ENABLED.value) "disable" else "enable"} instant settlement",
                            onClick = { INSTANT_SETTLEMENT_ENABLED.value = !INSTANT_SETTLEMENT_ENABLED.value }
                        )

                        TalerButton(
                            text = "back",
                            onClick = {
                                onDestroy()
                                this@ManageActivity.finish()
                            }
                        )

//                      The following few lines are meant for local testing, debugging etc.
//                        TestTransactionScreen(walleeClient)
//                        TalerButton(
//                            text = "${if (SIM_WALLET_ENABLED.value) "disable" else "enable"} wallet simulation",
//                            onClick = { SIM_WALLET_ENABLED.value = !SIM_WALLET_ENABLED.value }
//                        )
                    }
                }
            }
        }

        val responseHandler = WalleeResponseHandler(this)
        walleeClient = ApiClient(responseHandler)
        walleeClient.bind(this)
        val model = WithdrawalViewModel(walleeClient)
        responseHandler.model = model
        walleeClient.checkApiServiceCompatibility()
    }

    override fun onDestroy() {
        super.onDestroy()
        if (!unbound) {
            walleeClient.unbind(this)
            unbound = true
        }
    }
}