package ch.bfh.habej2.wallee_c2ec.withdrawal

import androidx.compose.foundation.Image
import androidx.compose.runtime.Composable
import androidx.compose.ui.res.painterResource
import ch.bfh.habej2.wallee_c2ec.R

@Composable
fun TalerLogo() = Image(
        painter = painterResource(id = R.drawable.taler_logo),
        contentDescription = "Taler logo"
)
