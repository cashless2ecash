// This file is part of taler-cashless2ecash.
// Copyright (C) 2024 Joel Häberli
//
// taler-cashless2ecash is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as published
// by the Free Software Foundation, either version 3 of the License,
// or (at your option) any later version.
//
// taler-cashless2ecash is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// SPDX-License-Identifier: AGPL3.0-or-later

package ch.bfh.habej2.wallee_c2ec

import android.content.Intent
import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import ch.bfh.habej2.wallee_c2ec.ui.theme.Walleec2ecTheme
import ch.bfh.habej2.wallee_c2ec.withdrawal.ManageActivity
import ch.bfh.habej2.wallee_c2ec.withdrawal.TalerButton
import ch.bfh.habej2.wallee_c2ec.withdrawal.TalerLogo
import ch.bfh.habej2.wallee_c2ec.withdrawal.WithdrawalActivity

class MainActivity : ComponentActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            Walleec2ecTheme {
                // A surface container using the 'background' color from the theme
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    val ctx = LocalContext.current
                    Column(
                        horizontalAlignment = Alignment.CenterHorizontally
                    ) {

                        TalerLogo()

                        TalerButton(
                            text = "Withdrawal",
                            onClick = { ctx.startActivity(Intent(this@MainActivity, WithdrawalActivity::class.java)) }
                        )

                        TalerButton(
                            text = "Settings",
                            onClick = { ctx.startActivity(Intent(this@MainActivity, ManageActivity::class.java)) }
                        )

                    }
                }
            }
        }
    }
}
