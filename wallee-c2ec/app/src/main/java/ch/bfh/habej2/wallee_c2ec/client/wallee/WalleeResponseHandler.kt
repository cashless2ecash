// This file is part of taler-cashless2ecash.
// Copyright (C) 2024 Joel Häberli
//
// taler-cashless2ecash is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as published
// by the Free Software Foundation, either version 3 of the License,
// or (at your option) any later version.
//
// taler-cashless2ecash is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// SPDX-License-Identifier: AGPL3.0-or-later

package ch.bfh.habej2.wallee_c2ec.client.wallee

import android.app.Activity
import ch.bfh.habej2.wallee_c2ec.withdrawal.WithdrawalViewModel
import com.wallee.android.till.sdk.ResponseHandler
import com.wallee.android.till.sdk.data.FinalBalanceResult
import com.wallee.android.till.sdk.data.TransactionCompletionResponse
import com.wallee.android.till.sdk.data.TransactionResponse

class WalleeResponseHandler(
    private val activity: Activity,
) : ResponseHandler() {

    private val enableDebugLogs = true

    lateinit var model: WithdrawalViewModel

    override fun authorizeTransactionReply(response: TransactionResponse?) {

        if (response == null) {
            model.withdrawalOperationFailed(activity)
            activity.finish()
            return
        }

        if (enableDebugLogs)
            this.printTransactionResponse(response)

        model.updateWalleeTransactionReply(response)
    }

    override fun completeTransactionReply(response: TransactionCompletionResponse?) {

        if (response == null) {
            model.withdrawalOperationFailed(activity)
            activity.finish()
            return
        }

        if (enableDebugLogs)
            this.printCompletionResponse(response)

        model.updateWalleeTransactionCompletion(response)
    }

    override fun executeFinalBalanceReply(result: FinalBalanceResult?) {

        if (result == null) {
            println("executed final balance but got no result")
        } else {
            println("executed final balance: ${result.resultCode.code} ${result.resultCode.description}")
        }
    }

    override fun checkApiServiceCompatibilityReply(
        isCompatible: Boolean?,
        apiServiceVersion: String?
    ) {

        println("terminal app sdk compatible: $isCompatible")
        if (isCompatible == null || !isCompatible) {
            // just dont start withdrawals when api is not compatible
            println("not starting application because sdk version not compatible")
            activity.finish()
        }
    }

    private fun printTransactionResponse(response: TransactionResponse) {

        println("C2EC-TRANSACTION-RESPONSE: $response")

        if (response.transaction.metaData.isNotEmpty()) {
            response.transaction.metaData.entries.forEach {
                println("METADATA: ${it.key}=${it.value}")
            }
        } else {
            println("METADATA EMPTY")
        }

        println("TRANSACTION: ${response.transaction.totalAmountIncludingTax} ${response.transaction.currency}")
        response.transaction.lineItems.forEach {
            println("TRANSACTION: LINE-ITEM id=${it.id}, name=${it.name}, amount-including-taxes=${it.totalAmountIncludingTax}")
        }
        println("TRANSACTION: ${response.state}")
        println("TRANSACTION: ${response.authorizationCode}")
        println("TRANSACTION: ${response.acquirerId}")
        println("TRANSACTION: ${response.reserveReference}")
    }

    private fun printCompletionResponse(response: TransactionCompletionResponse) {

        println("C2EC-COMPLETION-RESPONSE: ${response.transactionCompletion}")
    }
}