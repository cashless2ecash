// This file is part of taler-cashless2ecash.
// Copyright (C) 2024 Joel Häberli
//
// taler-cashless2ecash is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as published
// by the Free Software Foundation, either version 3 of the License,
// or (at your option) any later version.
//
// taler-cashless2ecash is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// SPDX-License-Identifier: AGPL3.0-or-later

package ch.bfh.habej2.wallee_c2ec.withdrawal

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import ch.bfh.habej2.wallee_c2ec.client.wallee.WalleeResponseHandler
import com.wallee.android.till.sdk.ApiClient


class WithdrawalActivity : ComponentActivity() {

    private lateinit var walleeClient: ApiClient

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val walleeResponseHandler = WalleeResponseHandler(this)
        walleeClient = ApiClient(walleeResponseHandler)
        walleeClient.bind(this)
        walleeClient.checkApiServiceCompatibility()

        val model = WithdrawalViewModel(walleeClient)
        walleeResponseHandler.model = model

        setContent {

            val navController = rememberNavController()

            NavHost(navController = navController, startDestination = "chooseExchangeScreen") {
                composable("chooseExchangeScreen") {
                    ExchangeSelectionScreen(model, this@WithdrawalActivity) {
                        navController.navigate("amountScreen")
                    }
                }
                composable("amountScreen") {
                    AmountScreen(model, this@WithdrawalActivity) {
                        navController.navigate("registerParametersScreen")
                    }
                }
                composable("registerParametersScreen") {
                    RegisterParametersScreen(model, this@WithdrawalActivity) {
                        navController.navigate("authorizePaymentScreen")
                    }
                }
                composable("authorizePaymentScreen") {
                    AuthorizePaymentScreen(model, this@WithdrawalActivity, walleeClient)
                }
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()

        try {
            walleeClient.unbind(this)
        } catch (ex: Exception) {
            println("unbinding from wallee service throw error: ${ex.message}")
        }
    }
}
