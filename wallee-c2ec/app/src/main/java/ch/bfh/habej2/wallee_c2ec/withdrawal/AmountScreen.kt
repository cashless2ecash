// This file is part of taler-cashless2ecash.
// Copyright (C) 2024 Joel Häberli
//
// taler-cashless2ecash is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as published
// by the Free Software Foundation, either version 3 of the License,
// or (at your option) any later version.
//
// taler-cashless2ecash is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// SPDX-License-Identifier: AGPL3.0-or-later

package ch.bfh.habej2.wallee_c2ec.withdrawal

import android.app.Activity
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material3.Button
import androidx.compose.material3.Text
import androidx.compose.material3.TextField
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalConfiguration
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.unit.dp

@Composable
fun AmountScreen(
    model: WithdrawalViewModel,
    activity: Activity,
    navigateToWhenAmountEntered: () -> Unit
) {

    val uiState by model.uiState.collectAsState()
    val configuration = LocalConfiguration.current

    Column(
        Modifier.width(configuration.screenWidthDp.dp),
        verticalArrangement = Arrangement.Center,
        horizontalAlignment = Alignment.CenterHorizontally
    ) {

        if (uiState.encodedWopid.isNotBlank()) {
            navigateToWhenAmountEntered()
        }

        Text(uiState.amountError, color = Color.Red)

        TextField(
            uiState.amountStr,
            onValueChange = {
                println(it)
                if (!model.validateInput(it)) {
                    model.resetAmountStr()
                }
            },
            label = { Text(text = "Withdrawal amount") },
            keyboardOptions = KeyboardOptions(
                autoCorrect = false,
                keyboardType = KeyboardType.Number
            )
        )

        Text(text = "fees: ${if (uiState.withdrawalFees.currency == "") "" else uiState.withdrawalFees}")

        Text(text = "total: ${if (uiState.withdrawalFees.currency == "") "" else uiState.amount.plus(uiState.withdrawalFees)}")

        TalerButton(
            text = "withdraw",
            onClick = {
                println("clicked 'pay'")
                model.setupWithdrawal(activity, navigateToWhenAmountEntered)
            },
            enableExpr = {model.validAmount(uiState.amountStr) && uiState.withdrawalFees.currency != ""}
        )

        TalerButton(text = "abort", onClick =  { model.withdrawalOperationFailed(activity) })
    }
}
