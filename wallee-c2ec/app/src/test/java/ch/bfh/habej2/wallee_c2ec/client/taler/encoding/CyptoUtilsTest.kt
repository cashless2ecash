// This file is part of taler-cashless2ecash.
// Copyright (C) 2024 Joel Häberli
//
// taler-cashless2ecash is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as published
// by the Free Software Foundation, either version 3 of the License,
// or (at your option) any later version.
//
// taler-cashless2ecash is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// SPDX-License-Identifier: AGPL3.0-or-later

package ch.bfh.habej2.wallee_c2ec.client.taler.encoding

import ch.bfh.habej2.wallee_c2ec.client.taler.encoding.CyptoUtils.decodeCrock
import ch.bfh.habej2.wallee_c2ec.client.taler.encoding.CyptoUtils.encodeCrock
import org.junit.Test
import java.security.SecureRandom

class CyptoUtilsTest {

    @Test
    fun crockford() {

        val origin = rand32Bytes()
        println("origin: $origin")
        val encoded = encodeCrock(origin)
        println("encoded: $encoded")
        val decoded = decodeCrock(encoded)
        println("decoded: $decoded")

        assert(origin.contentEquals(decoded))
    }

    @Test
    fun crockford_taler_special() {
        // see https://docs.taler.net/core/api-common.html#binary-data

        val origin = "BNR1DRKYZ676T5KMHJMNPV68V32W95S9Q35P081TJ5ZZTJ8M5WJG"
        println("origin: $origin")
        val decodedNormal = decodeCrock(origin)
        val originWithReplacedOcrProblems1 = origin
            .replace('1', 'L')
            .replace('V', 'U')
            .replace('0', 'O')
        val originWithReplacedOcrProblems2 = origin
            .replace('1', 'I')
            .replace('V', 'U')
            .replace('0', 'O')
        println("encoded 1: $originWithReplacedOcrProblems1")
        println("encoded 2: $originWithReplacedOcrProblems2")
        val decoded1 = decodeCrock(originWithReplacedOcrProblems1)
        val decoded2 = decodeCrock(originWithReplacedOcrProblems2)

        assert(decodedNormal.contentEquals(decoded1))
        assert(decodedNormal.contentEquals(decoded2))
        assert(decoded1.contentEquals(decoded2))
    }

    fun rand32Bytes(): ByteArray {
        val wopid = ByteArray(32)
        val rand = SecureRandom()
        rand.nextBytes(wopid) // will seed automatically
        return wopid
    }
}