// This file is part of taler-cashless2ecash.
// Copyright (C) 2024 Joel Häberli
//
// taler-cashless2ecash is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as published
// by the Free Software Foundation, either version 3 of the License,
// or (at your option) any later version.
//
// taler-cashless2ecash is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// SPDX-License-Identifier: AGPL3.0-or-later

package ch.bfh.habej2.wallee_c2ec

import org.junit.Test

import org.junit.Assert.*

class AmountPatternTest {

    private val regex = Regex( "\\d+(\\.\\d+)?")

    @Test
    fun valid_amount_pattern_test() {

        val validPatterns = listOf(
            "0.5",
            "0.05",
            "1.34533434",
            "45243.3254245235235335",
            "1000",
            "9383"
        )

        validPatterns.map {
            assertTrue(regex.matches(it))
        }
    }

    @Test
    fun invalid_amount_pattern_test() {

        val invalidPattern = listOf(
            "100.",
            "12344.234523.34",
            ".12344.234523",
            ".",
            ".932487",
            "8989.2223..",
            "2323..342"
        )

        invalidPattern.map {
            assertFalse(regex.matches(it))
        }
    }

    @Test
    fun how_does_split_work() {

        val str = "1."
        val first = str.split(".")[0]
        val second = str.split(".")[1]

        assertEquals("1", first)
        assertEquals("", second)
    }
}