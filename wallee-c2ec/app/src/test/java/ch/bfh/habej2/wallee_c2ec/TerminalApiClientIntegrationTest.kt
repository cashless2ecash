// This file is part of taler-cashless2ecash.
// Copyright (C) 2024 Joel Häberli
//
// taler-cashless2ecash is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as published
// by the Free Software Foundation, either version 3 of the License,
// or (at your option) any later version.
//
// taler-cashless2ecash is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// SPDX-License-Identifier: AGPL3.0-or-later

package ch.bfh.habej2.wallee_c2ec

import ch.bfh.habej2.wallee_c2ec.client.taler.TerminalClientImplementation
import ch.bfh.habej2.wallee_c2ec.client.taler.config.TalerTerminalConfig
import ch.bfh.habej2.wallee_c2ec.client.taler.encoding.CyptoUtils.encodeCrock
import ch.bfh.habej2.wallee_c2ec.client.taler.encoding.CyptoUtilsTest
import ch.bfh.habej2.wallee_c2ec.client.taler.model.TerminalWithdrawalConfirmationRequest
import ch.bfh.habej2.wallee_c2ec.client.taler.model.TerminalWithdrawalSetup
import ch.bfh.habej2.wallee_c2ec.withdrawal.ManageActivity
import org.junit.Assert.assertTrue
import org.junit.Test

class TerminalApiClientIntegrationTest {

    private val config = TalerTerminalConfig(
        "TEST Exchange",
        "http://taler-c2ec.ti.bfh.ch",
        "Simulation-1",
        "Gofobz8XYEX0H3rts4+w3K7bU68wSP2N5Y36FBe4/f8="
    )

    private val config2 = TalerTerminalConfig(
        "CHF Exchange",
        "https://terminals.chf.taler.net",
        "Simulation-1",
        "Gofobz8XYEX0H3rts4+w3K7bU68wSP2N5Y36FBe4/f8="
    )

    private val client = TerminalClientImplementation(config2)

    private var currency = ""
    private var wopid = ""

    private val sleepMs = 1000L
    private val longPollMs = 3000L

    @Test
    fun flow() {
        // load config
        client.terminalsConfig {
            assertTrue(it.isPresent)
            currency = it.get().currency
            println("loaded config. currency=$currency")
        }

        Thread.sleep(sleepMs)

        // setup withdrawal
        client.setupWithdrawal(TerminalWithdrawalSetup(
            encodeCrock(CyptoUtilsTest().rand32Bytes()),
            "CHF:21.30"
        )) {
            assertTrue(it.isPresent)
            wopid = it.get().withdrawalId
            println("setup withdrawal. wopid=$wopid")
        }

        Thread.sleep(sleepMs)

        // send check request
        client.sendConfirmationRequest(wopid, TerminalWithdrawalConfirmationRequest("TEST-$wopid", "CHF:21.30")) {
            assertTrue(it)
            println("payment is attested. result=$it")
        }

        // load status
        client.retrieveWithdrawalStatus(wopid, longPollMs.toInt()) {
            assertTrue(it.isPresent)
            println("status retrieved. status=${it.get().status}")
        }

        Thread.sleep(longPollMs+1000)
    }

    @Test
    fun simulateRegistration() {

        // ATD2GW6ZNJ5FQ8B6R890C4G13RN3RZTQ1P6VT2ZT5V04AP7DG9K0
        wopid = "";
        ManageActivity.simulateParameterRegistration(
            "https://taler-c2ec.ti.bfh.ch/taler-integration",
            wopid
        )
    }
}