\section{C2EC}

This section treats the implementation of the C2EC component. C2EC is the core of the withdrawal using a third party. Besides different API for different client types such as the terminal, wallet or the Exchange, it must also deal with background tasks as described in \autoref{sec-implementation-processes}. The component also implements a framework to extend the application to accept withdrawals through other providers than Wallee. In \autoref{sec-implementation-providers} the requirements for the integration of other providers is explained and shown at the example of Wallee.

\subsection{Endpoints}
\label{sec-implementation-endpoints}

The diagram in \autoref{fig-diagram-c2ec-apis-sequence} shows the perspective of the C2EC component in the withdrawal flow. The numbers in brackets represent the numbers of the diagram in \autoref{fig-diagram-all-sequence} depicting the process in the architecture chapter at \autoref{sec-architecture-process}. The requests represented in \autoref{fig-diagram-c2ec-apis-sequence} only show the requests of the succesful path. In case of an error in the process, abort endpoints are implemented as described per client type.

The implementation of the terminals API can be found in \autoref{sec-implementation-terminal-api}, the bank integration API is documented in \autoref{sec-implementation-bank-integration-api} and the wire gateway API implementation is documented in \autoref{sec-implementation-wire-gateway-api}.

\newpage
\KOMAoptions{paper=landscape,pagesize}
\recalctypearea
\thispagestyle{empty}
\newgeometry{left=4cm, right=4cm, top=3cm, bottom=0cm}

\begin{figure}[H]
    \centering
    \includegraphics[width=1.7\textwidth]{pictures/diagrams/c2ec_apis.png}
    \caption{C2EC and its interactions with various components}
    \label{fig-diagram-c2ec-apis-sequence}
\end{figure}
  
\restoregeometry
\newpage
\KOMAoptions{paper=portrait,pagesize}
\recalctypearea

\subsubsection{Decoupling Withdrawal Steps}

The concept of publishers and subscribers is used in the implementation. It allows decoupling different steps of the process and allows different steps to be handled and executed in their own processes. Subscriptions are implemented using long-polling and listeners for Postgres notifications. Long-polling imitates subscribers by rescheduling a request after some time and keeping the connection open until the specified timespan exceeds and the requested component answers the request.

The communication of publishers and subscribers happens through channels or long-polling. A publisher will publish to a certain channel when a defined state is reached. The subscriber who listens to this channel will capture the message sent through the channel by the publisher and start processing it.

Every action leading to a state transition of the withdrawal triggers an event. The trigger can be a real event trigger like a database trigger or a retry mechanism which periodically checks for state updates. The applications processes are listening to those events. Consuming clients such as the wallet or the terminal can wait to be notified by the API. The notification is achieved by registering the respective events via a long polling request. The long-polling request will then wait until the backend is ready to send the response. If the long-poll time exceeds and the result the consumer is looking for is not available, it must reschedule a long-poll request.

Following a short list of state transitions and from whom they are triggered and who awaits them:

\begin{itemize}
    \item From pending to selected
    \begin{itemize}
        \item Description: Registration of the withdrawal operation parameters.
        \item Triggered by: Wallet
        \item Awaited by: Terminal
    \end{itemize}
    \item From selected to confirming
    \begin{itemize}
        \item Description: Payment confirmation request sent to the Terminals API of C2EC.
        \item Triggered by: Terminal
        \item Awaited by: Confirmation process
    \end{itemize}
    \item From selected to confirmed
    \begin{itemize}
        \item Description: Payment confirmation success will send a withdrawal operation status update event.
        \item Triggered by: Confirmation process
        \item Awaited by: Consumer wallets (via Bank-Integration-API)
    \end{itemize}
    \item From selected to aborted
    \begin{itemize}
        \item Description: Payment confirmation failure will trigger a retry event.
        \item Triggered by: Confirmation process
        \item Awaited by: Retry process
    \end{itemize}
    \item Refunds as transfer requests
    \begin{itemize}
        \item Description: Transfers which represent refunds in C2EC.
        \item Triggered by: Exchange (using the Wire Gateway API of C2EC)
        \item Awaited by: Transfer process
    \end{itemize}
\end{itemize}

\subsection{Abort Handling}

A withdrawal might be aborted through the terminal or the wallet. These cases are implemented through the respective \textit{abort} endpoint in the bank-integration API \autoref{sec-implementation-bank-integration-api-abort} and terminals API \autoref{sec-implementation-terminal-api-abort}. If in doubt whether to abort the withdrawal or not, it should be aborted. In case of abortion and failure cases, the security of the money is weighted higher than the user-experience. If the user must restart the withdrawal in case of a failure in the process, it is less severe than opening possible security holes by somehow processing the withdrawal anyway. On the other hand the system must be as stable as possible to make these error cases very rare. If they occur too often, the customer might not use the technology and therefore would make it worthless.

The withdrawal can only be aborted, when it is not yet confirmed by the confirmation process (described in \autoref{sec-implementation-processes-confirmation}). When the customer wants his money back they can wait for the reserve to be closed by the Exchange or get in touch with the operator who might trigger a manual refund. The manual refund will revert the payment and give the money back to the customer.

\newpage
\include{content/implementation/a-terminal-api}

\newpage
\include{content/implementation/a-bank-integration-api}

\newpage
\include{content/implementation/a-wire-gateway-api}

\newpage
\include{content/implementation/a-processes}

\newpage
\include{content/implementation/a-providers}

\newpage
\include{content/implementation/a-fees}
