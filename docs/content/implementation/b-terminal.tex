\section{Wallee Payment Terminal}

\subsection{Withdrawal Flow}
\label{sec-wallee-withdrawal-flow}

The process (\autoref{fig-diagram-terminal-flow}) starts by first selecting the Exchange and loading the configuration from the Terminals-API. When this is successful the terminal app will navigate to the amount screen. Otherwise the withdrawal is terminated. 

On the amount screen the terminal operator enters the amount to withdraw and clicks on the "withdraw" button. If the operator clicks on the abort button, the withdrawal is terminated. When the user clicks the "withdraw" button, the terminal sets up the withdrawal at the exchanges terminals api and retrieves the wopid. When this step is unsuccessful, the withdrawal operation is aborted and terminated. Otherwise the terminal navigates to the register parameters screen.

In the register parameters screen, a QR code is displayed, which must be scanned by the withdrawer using their wallet app. When the user scanned the QR Code and the terminal gets the withdrawal operation in state 'selected' from C2EC, the wallet has successfully registered its withdrawal parameters. In this case the terminal application changes to the authorization screen in which the withdrawing person must authorize the transaction using their credit card (or another supported payment mean). In any other case, the withdrawal operation is aborted and terminated. When the terminals backend sends the response of the authorization to the terminal, it sends the Terminals API of C2EC the confirmation request. This will start the confirmation process of the withdrawal immediately. If the confirmation request is successful, the terminal shows a summary of the transaction. Otherwise it shows that the transaction was not succesful and the withdrawal is aborted.

\newpage
\KOMAoptions{paper=landscape,pagesize}
\recalctypearea
\thispagestyle{empty}
\newgeometry{left=4cm, right=4cm, top=3cm, bottom=0cm}

\begin{figure}[H]
    \centering
    \includegraphics[width=1.7\textwidth]{pictures/diagrams/terminal_flow.png}
    \caption{The flow of the terminal app}
    \label{fig-diagram-terminal-flow}
\end{figure}

\restoregeometry
\newpage
\KOMAoptions{paper=portrait,pagesize}
\recalctypearea

\subsection{Screens}

The application is implemented using Jetpack Compose \cite{app-jetpack-compose} and each of the screens described in \autoref{sec-wallee-withdrawal-flow} is implemented as composable screen. This allows to handle the entire withdrawal flow in one single activity and therefore makes state handling easier. For the summary a standalone activity is used. The state is bound to the activity and compose will make sure to rebuild the UI if values change. It also prevents illegal states and that different withdrawals interfere each other. The state is maintained in a view model as described by Android's documentation \cite{app-viewmodel}. The withdrawal activity handles the lifecycle of the view model instance and initializes the routing of the screens using Android's navigation controller as documented \cite{app-navigation}. The navigation integration of Android allows the declarative definition of the in-app routing and is defined at the creation of the withdrawal activity.

\subsubsection{Start Screen}

When the app is started there will be two options for the user. The first option is to start a withdrawal and the second option is to go to the manage screen.

\begin{figure}[H]
    \centering
    \includegraphics[width=0.7\textwidth]{pictures/wallee/0_start_screen.jpg}
    \caption{Terminal: Start withdrawal or go to manage section}
    \label{fig-terminal-screen-start}
\end{figure}

\subsubsection{Manage Screen}
\label{sec-implementation-terminal-manage-screen}

There are options and special functionalities which can be configured by the terminal operator. These operations are implemented in the manage screen of the app. The app allows to execute the final balance actions which will settle all authorized transactions of this specific terminal. The test transaction was used during the development to learn how the transaction is triggered. In the newest release it won't be part of the manage screen. The operator might want to test the implementation without having a wallet at hand. This can be done by enabling the wallet simulation. It will lead to the creation of a mocked reserve public key which will not be withdrawable by a wallet. Triggering a payment using the wallet simulation will lead to temporary loss of money (it will eventually be bounced and refunded through the Exchange when the reserve is closed). However, the fees charged will be gone. The wallet simulation should therefore only be used in a test environment. The last option allows the activation of instant settlement. This means that the final balance action will be triggered after each payment authorization.

\begin{figure}[H]
    \centering
    \includegraphics[width=0.7\textwidth]{pictures/wallee/1_manage_screen.jpg}
    \caption{Terminal: Manage activities}
    \label{fig-terminal-screen-manage}
\end{figure}

\subsubsection{Choose Exchange Screen}

When a new withdrawal is started, the user chooses the exchange to withdraw from (\autoref{fig-terminal-screen-choose-exchange}). This allows the terminal to support withdrawals from various exchanges and therefore enhances the flexibility. When the user selected the exchange, the configuration of the exchange is loaded. This will define the currency of the withdrawal and tell the terminal where to reach the Terminals API of the C2EC server.

\begin{figure}[H]
    \centering
    \includegraphics[width=0.7\textwidth]{pictures/wallee/2_choose_exchange.jpg}
    \caption{Terminal: Select the exchange to withdraw from}
    \label{fig-terminal-screen-choose-exchange}
\end{figure}

\subsubsection{Amount Screen}

The amount screen in \autoref{fig-terminal-screen-amount} is used to ask the user what amount they would like to withdraw. When the amount was entered and the \textit{withdraw}-button was clicked, the terminal sets up the withdrawal using the Terminal API. The Terminals API will send the \textit{WOPID} to the terminal, which allows the terminal to generate the taler withdraw URI according to \cite{taler-uri-scheme-rfc}. Also the fees are shown to the customer on this screen and made transparent in advance.

\begin{figure}[H]
    \centering
    \includegraphics[width=0.7\textwidth]{pictures/wallee/3_amount_valid.jpg}
    \caption{Terminal: Enter the desired amount to withdraw}
    \label{fig-terminal-screen-amount}
\end{figure}

In case the withdrawal amount is invalid, the withdrawal is not possible and an error shown to the customer as depicted in \autoref{fig-terminal-screen-amount-invalid}.

\begin{figure}[H]
    \centering
    \includegraphics[width=0.7\textwidth]{pictures/wallee/4_amount_invalid.jpg}
    \caption{Terminal: Fix the amount}
    \label{fig-terminal-screen-amount-invalid}
\end{figure}

\subsubsection{Parameter Registration Screen}

After entering the amount, a QR code containing the taler withdraw URI is displayed (\autoref{fig-terminal-screen-register-parameters}). The customers scan it using their Taler wallet app and register the parameters for the withdrawal (namely the reserve public key). The withdrawal can be aborted on the screen. This step is important to make sure, that the customer has a working Taler wallet installed and allows them to accept the terms of service for the respective exchange if they did not yet registered the exchange on their wallet. Once this is done the authorization can be started by clicking on the \textit{authorize} button. When clicking on the \textit{abort} button the withdrawal is aborted.

\begin{figure}[H]
    \centering
    \includegraphics[width=0.7\textwidth]{pictures/wallee/5_register_params.jpg}
    \caption{Terminal: Register withdrawal parameters}
    \label{fig-terminal-screen-register-parameters}
\end{figure}

\subsubsection{Authorization Screen}

The authorization uses the \textit{Android Till SDK} \cite{wallee-till-sdk} to authorize the amount at the Wallee backend. It covers reading and verifiying the payment mean of the customer. The response handler of the SDK will delegate the response to the implementation of the terminal, which allows triggering the confirmation request using the Terminals API of C2EC. When the authorization process is not started and the transaction therefore is created at the backend system of Wallee, the screen \autoref{fig-terminal-screen-authorizing} will be displayed. This signals the user, that the payment authorization must still be done and is about to be started. The user can abort the transaction at this point.

\begin{figure}[H]
    \centering
    \includegraphics[width=0.7\textwidth]{pictures/wallee/6_authorize_screen.jpg}
    \caption{Terminal: Authorization using the Android Till SDK}
    \label{fig-terminal-screen-authorizing}
\end{figure}

\subsubsection{Summary Screen}

When the transaction was processed a summary of the transaction is displayed to the customer (\autoref{fig-terminal-screen-authorized}).

\begin{figure}[H]
    \centering
    \includegraphics[width=0.7\textwidth]{pictures/wallee/8_authorized.jpg}
    \caption{Terminal: Payment authorized}
    \label{fig-terminal-screen-authorized}
\end{figure}

\subsubsection{Failure Screen}

To give the customer a feedback when anything goes wrong or when the withdrawal is aborted, the terminal will always show the rudimentary abort screen (\autoref{fig-terminal-screen-abort}). This gives the users feedback what happened. What can go wrong and how the terminal acts upon these failures is described in \autoref{sec-implementation-abort-handling}.

\begin{figure}[H]
    \centering
    \includegraphics[width=0.7\textwidth]{pictures/wallee/7_abort.jpg}
    \caption{Terminal: Payment authorized}
    \label{fig-terminal-screen-abort}
\end{figure}

\subsection{Abort Handling}
\label{sec-implementation-abort-handling}

During the flow various steps can fail or lead to the abort of the withdrawal. These edge cases must be considered and handled the right way. Generally we can split the abort handling on the terminal side into two different phases. The implementation of the Wallee POS Terminal follows a strict \textit{abort on failure} strategy. This means that if anything goes wrong the withdrawal is aborted and a new withdrawal might be started. Generally the abort handling strategy is to abort the withdrawal when in doubt and values security (of the money) over the user-experience. The idea behind this strategy is that even when a nice user-experience is implemented, nobody will operate or use a withdrawal process if security of the money cannot be guaranteed.

\subsubsection{Abort before authorization}

The first phase are abortions \textit{before} the payment is authorized. In this case the withdrawal operation can be aborted using the \textit{abort} operation described in \autoref{sec-implementation-terminal-api}. Every problem which cannot be recovered or not further processed must lead to the abort of the withdrawal. 

\subsubsection{Abort after authorization}

When the transaction was authorized, the process is a little bit more complex. The customer has two possibilities. The first one is automatically covered with the given implementation, while the second is not guaranteed and needs manual interaction of the customer with the Taler Exchange operator.

\textbf{Wait for automatic refund due to closing of the reserve}

The Taler Exchange configures a duration for which a reserve is kept open (and can be withdrawn). When the configured duration exceeds the reserve is closed autmatically and the money transferred back to the customer. In the case of Wallee payments, this is realized through a refund request at the provider backend upon receiving a transfer request at the wire-gateway API \autoref{sec-implementation-wire-gateway-api} of the C2EC component.

\textbf{Manual request to refund money}

Depending on the operator of the Taler Exchange it might be possible to somehow manually trigger a refund and get back the money spent for the withdrawal. 

\subsection{Fulfilling Transactions}

To achieve finality and real-time behaviour of the withdrawal flow, the transaction must be forcefully transitioned to the \textit{fulfill} state in the Wallee backend. For this reason, the payments are not only authorized but also automatically completed in the background. After the successful completion, the payment can be directly settled and with this guarantee the finality property. If the completion fails, the withdrawal will be aborted. Wallee was asked if the instant settlement using the final balance call has cost effects for the merchant. It appears that this is not the case. But Wallee wrote it is not the idea to trigger the final balance every minute. Because of this, the instant settlement is deactivated by default and the terminal operator might decide on their own if they want to activate it or not. Instant settlement can be activated in the manage screen as described in \autoref{sec-implementation-terminal-manage-screen}.

\newpage