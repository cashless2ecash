\subsection{Providers}
\label{sec-implementation-providers}

This section treats the integration of providers into the system by explaining the generic structures and showing how they were implemented for Wallee. It is also explained, what must be done in order to integrate other third parties into the system therefore showing the extensibility of the system.

\subsubsection{Provider Client}
\label{sec-provider-client-interface}

The provider client interface is called by the confirmation process depending on the notification received by the database upon receiving a payment notification of the provider's terminal. The specific provider clients are registered at the startup of the component and the confirmation process will delegate the information gathering to the specific client, based on the notification received by the database.

The provider client interface defines three functions:

\begin{enumerate}
    \item SetupClient: The setup function is called by the startup of the application and used to initialize the client. Here it makes sense to check that everything needed for the specific client is in place and that properties like access credentials are available.
    \item FormatPayto: This function is responsible to create the correct payto URI given a withdrawal.
    \item GetTransaction: This function is used by the confirmation process to retrieve the transaction of the provider system. It takes the transaction identifier supplied with the withdrawal confirmation request and loads the information about the transaction. Based on this information the decision to confirm or abort the transaction is done.
    \item Refund: Since the transaction of the money itself is done by the provider, also refunds will be unwind by the provider. This functions mean is to trigger this refund transaction at the provider. Before triggering the transaction, the refunded amount should be checked. The amount must not be bigger than the withdrawen amount. It can be smaller though, if the Exchange makes a partial refund.
\end{enumerate}

\subsubsection{Provider Transaction}
\label{sec-provider-transaction-interface}

Since the confirmation process is implemented to support any provider, also the transaction received by the provider clients \textit{GetTransaction} function is abstracted using an interface. This interface must be implemented by any provider transaction which belongs to a specific provider client.

The provider client interface defines following functions:

\begin{enumerate}
    \item AllowWithdrawal: This function shall return true, when the transaction received by the provider enters a positive final state. This means that the provider accepted the transaction and could process it. This means that the Exchange can create the reserve and allow the customer the withdrawal of the digital cash. This function is responsible to guarantee the \textbf{finality} (\autoref{sec-goals-properties}).
    \item AbortWithdrawal: It doesn't mean that if a transaction does not allow to do the withdrawal, that the transaction shall be cancelled immediately. It could also be that the transaction was not yet processed by the provider. In this case a handle to check if the provider transaction is in an abort state. An abort state is a final state which will not change anymore. This indicates C2EC to stop retrying and abort the withdrawal.
    \item Confirm: This function is called during the confirmation and contains business specific checks wether to confirm the payment or not. It must be separately implemented, because the transaction format varies between different payment system providers.
    \item Bytes: This function shall return a byte level representation of the transaction which will be used as proof of the transaction and stored in the exchanges database.
\end{enumerate}

\subsubsection{Wallee Client}

The Wallee client is the first implementation of the provider client interface and allows the confirmation of transactions using the Wallee backend system. The backend of Wallee provides a REST-API to their customers, which allows them to request information about payments, refunds and so on. To access the API, the consumer must authenticate themself to Wallee by using their own authentication token as explained in \autoref{sec-security-auth-wallee}.

As indicated by the provider client interface, two services of the Wallee backend are leveraged:

\begin{itemize}
    \item Transaction service: The transaction service aims to provide information about a transaction registered using a Wallee terminal.
    \item Refund service: The refund service allows to trigger a refund for a given transaction using the transaction identifier. The refund will then be executed by the Wallee backend, back to the Customer.
\end{itemize}

To integrate Wallee as provider into C2EC, the provider client interface as described in \autoref{sec-provider-client-interface} was implemented. The transaction received by Wallee's transaction service implement the provider transaction interface as described in \autoref{sec-provider-transaction-interface}.

\subsubsection{Simulation Client}

Additionally to the Wallee Client a Simulation Client was implemented which can be used for testing. It allows end-to-end tests of the C2EC component by stubbing the actions performed against a provider and returning accurate results. 

\subsubsection{Adding Providers}

Adding a new provider requires the implementation of the client- and transaction-interfaces as described in \autoref{sec-provider-client-interface} and \autoref{sec-provider-transaction-interface}. The \texttt{SetupClient} function of the client interface must make sure to register itself to the global map of registered providers (accessible through \texttt{PROVIDER\_CLIENTS}). Additionally, to the newly added provider implementation, the provider must also be registered in the database (\autoref{sec-implementation-cli} describes how this could work). When the client adds itself to the registered providers clients, the application will load the provider client at startup of C2EC. If C2EC fails to find the specified provider in the database, it won't start. This behaviour makes sure, that only needed providers are running and that if a new provider was added, it is effectively registered and configured correctly (the setup function of the provider interface is responsible to check the provider specific configuration and do readiness or liveness checks if needed). If the new added provider requires a new payto target type, adding a new entry to the GANA in order to prevent conflicts in the future might be a good idea.
