\section{Security}

\subsection{General Security Considerations}

To review and validate the security of the design two cases were reviewed. The first mirrors the easiest attack (EAV eavesdropping and trying to abuse \textit{WOPID}). The second case reviews where the most harm can possibly be done to the system. 

\subsubsection{EAV Abusing WOPID}

The \textit{WOPID} is used to link a reserve public key to a withdrawal operation. Since the registration is done through an API, an attacker could try to be first and register its own reserve public key before the customer. When the \textit{WOPID} is somehow precomputable, an attacker could steal the money by registering their own reserve public key before the customer. This threat is mitigated by the request of the wallet resulting in a conflict response code when trying to add a reserve public key to an already registered withdrawal operation. The customer will see this error and not authorize the transaction and instead abort the withdrawal.

Further a \textit{WOPID} can be abused triggering a confirmation or an abort request at the Terminals API or an abort request at the Bank-Integration API. The confirmation or abort from the side of the terminal are mitigated through the authentication of the terminals. When the eavesdropping adversary (EAV) \cite{katz2020introduction} can somehow access the communication between a terminal and C2EC, the \textit{WOPID} cannot be abused without also breaking the terminals credentials. What if the attacker decides to use the unauthenticated Bank-Integration API the wallet would normally use? The specification does not require some proof that the requester is the wallet owning the private key of the reserve. This could lead to tampering of the withdrawals in the time window of the confirmation of the payment. The problem could be mitigated by sending a signed token in the request (the request already is a POST request). The wallet could use its reserve private key to sign the token. The Bank-Integration API could then verify the token using the reserve public key assigned to the withdrawal operation. It is understandable that the risk is accepted, since a potential adversary would need to be sophisticated (needs to redirect requests of the wallet and read \textit{WOPID} from the request). What about wallets run by people in countries which are politically not as stable as Switzerland and censorship is a problem? Maybe it's a good idea to add some mean of authentication to at least the abort endpoint of the Bank-Integration API. On the other hand the attacker needs access to the victims phone anyway and could possibly also use the keys.

\subsubsection{Trying To Withdraw Money Without Paying}

This case is possible, when an attacker can trick the C2EC to have confirmed withdrawals in its withdrawal table, without having a real confirmation of the payment service provider. This means the attacker can steal money from the exchange. For this an attacker would need to have the possibility to somehow trick the confirmation process of C2EC to issue confirmation requestes against a backend controlled by the attacker. This backend would then confirm the withdrawal. This will lead to the creation of the reserve on the side of the Exchange.

\subsubsection{Implementation Issues}

Another problem could be developers introducing confirmation bugs. The confirmation process of a transaction must be considered as the holy grail from the perspective of the developers. If they do not take biggest care implementing the confirmation process, this could lead to loss of money on the side of the Exchange operator. The program should strictly disallow withdrawals, if the transaction is not guaranteed to be final by the payment system provider. Otherwise the property of the guarantees concerning the finality is harmed and the system no longer secure (in terms of money). When adding new integrations, this section of the code needs great care and review before going to production.

\subsection{Withdrawal Operation Identifier (WOPID)}
\label{sec-security-wopid}

The \textit{WOPID} needs great care when generated. When the \textit{WOPID} becomes somehow foreseeable, it opens the door for attackers allowing them to hijack the withdrawal from a remote location or bully the operators by simply aborting any withdrawal. Therefore the \textit{WOPID} needs to leverage high entropy sources to be generated. This is achieved by using the crypto random library of Go. The library is part of the standard library and gains entropy through the entropy sources of the device running the application (in case of linux it is \textit{getrandom(2)} which takes its entropy from \textit{/dev/urandom}, according to the documentation \cite{golang-crypto-rand}).

\subsection{Database Security}

The database is very important as it decides wether to allow a withdrawal or not and it manages terminals and providers which hold sensitive credentials. Therefore two important aspects need to be considered.

\subsubsection{Storing Credentials}

Even if a database leak occurs, it shall be very hard for the attacker to access the API using the credentials stored in the database. This is why credentials are stored using the PBKDF \textit{argon2} \cite{password-competition-argon2}. \textit{Argon2} is the winner of the password hashing competition initiated by the cryptographer Jean-Philippe Aumasson \cite{password-competition-argon2}. It is a widely adopted best practice approach for hashing passwords. Storing the hash of the credentials makes abusing stolen credentials very hard and therefore prevents the abuse of credentials gathered through a database leak. The CLI described in \autoref{sec-implementation-cli} implements operations which will register providers and terminals also hashing the credentials using \textit{argon2}.

\subsubsection{Access Data Through Correct User}
\label{sec-security-db-users}

The database user executing a database query must have enough rights to execute its duties but not more. Therefore different database users are created for different tasks within the database. The described setup and installation process in \autoref{sec-deployment} will automatically generate the users and grant them the correct rights, when the respective variables are specified.

\begin{table}[H]
    \centering
    \caption{Database users}
    \label{sec-implementation-security-database-users}
    \begin{tabularx}{\textwidth}{p{0.2\textwidth}|p{0.2\textwidth}|p{0.5\textwidth}}
        \hline
        \textbf{Username} & \textbf{Component} & \textbf{Description} \\
        \hline
        c2ec\_admin & None & This user is possibly never to be used but during maintenance of the database itself (adding database users\, doing backups\, adding and granting users or others) \\
        \hline
        c2ec\_api & C2EC & This user has all rights it needs to manage a withdrawal \\
        \hline
        c2ec\_operator & CLI & This user shall be used by an operator of the C2EC component to add providers and terminals. It has no access to withdrawals \\
        \hline
    \end{tabularx}
\end{table}

\subsection{Authenticating At The Wallee REST API}
\label{sec-security-auth-wallee}

The Wallee API specifies four Wallee specific headers which are used to authenticate against the API. It defines its own authentication standard and flow. The flow builds on a message authentication code (MAC) which is built on a version, user identifier, and a timestamp. For the creation of the MAC the hash based message authentication code (HMAC) SHA-512 is leveraged which takes \textit{application-user-key} (which is just an access-token the user receives when creating a new API user in the management backend of Wallee) as key and the above mentioned properties plus information about the requested HTTP method and the exactly requested path (including request parameters) as message \cite{wallee-api-authentication}. The format of the message is specified like: 

\begin{center}
    \texttt{Version|User-Id|Unix-Timestamp|HTTP-Method|Path}
\end{center}

\begin{itemize}
    \item Version: The version of the algorithm
    \item User-Id: The user-id of the requesting user
    \item Unix-Timestamp: A unix timestamp (seconds since 01.01.1970)
    \item HTTP-Method: one of \texttt{HEAD}, \texttt{GET}, \texttt{POST}, \texttt{PUT}, \texttt{DELETE}, \texttt{TRACE}, \texttt{CONNECT}
    \item Path: The path of the requested URL including the query string (if any)
\end{itemize}

The resulting string must then be UTF-8 encoded according to RFC-3629 \cite{rfc3629}. There are implementations of the mechanism in Java and other languages available. For Go it was implemented during the thesis.

\subsubsection{Wallee User Access rights}

In order for Wallee to successfully authorize the user's requests, the API user must have the corret access rights. The C2EC Wallee API user must be able to access the transaction service for reading transactions and the refund service to write create refunds at the Wallee backend. Therefore following rigths must be assigned to the API user:

\begin{enumerate}
    \item Refund-service
    \item Transaction-Service
\end{enumerate}

These rights can be assigned on Wallee's management interface by creating a role and assigning the rights to it. The role must then be added to the API user. The assignment of the roles must be done for the space context (Three different contexts are available. The relevant context is the space context, since requests are scoped to a space).

\subsection{API Access}

\textbf{Terminals API}
\label{sec-terminal-api-auth}

The terminal API is accessed by terminals and the authentication mechanism is based on a basic auth scheme as specified by RFC-7617 \cite{rfc7617} an specified in the terminals API specification \cite{taler-terminal-api}. Therefore a generated access-token used as password and a username which is generated registering the terminal using the cli explained in \autoref{sec-security-registering-providers} are leveraged. Currently the terminal id and the provider name of the requesting terminal is included in the username part of the basic auth scheme.

\textbf{Bank-Integration API}

The Bank-Integration API is accessed by Wallets and specified to be unauthenticated.

\textbf{Wire-Gateway API}

The wire gateway specifies a basic authentication scheme \cite{taler-wire-gateway-api-authentication} as described in RFC-7617 \cite{rfc7617}. Therefore the C2EC component allows the configuration of a username and password for the exchange. During the request of the exchange at the wire gateway API, the credentials are checked.

\subsection{Registering Providers And Terminals}
\label{sec-security-registering-providers}

A provider may want to register a new Terminal or maybe even a new provider shall be registered for the exchange. To make this step easier for the exchange operators, a simple cli program (command line interface) was implemented (\autoref{sec-implementation-cli}). The cli will either ask for a password or generate an access token in case of the terminal registration. The credentials are stored has hashes using a PBKDF (password based key derivation function) so that even if the database leaks, the credentials cannot be easily read by an attacker.

\subsection{Hijacking And Stealing Terminals}

A Terminal can be stolen, hijacked or hacked by malicious actors. Therefore it must be possible to disable a terminal immediately and no longer allow withdrawals using this terminal. Therefore the \textit{active} flag can be set to \textit{false} for a registered terminal. The Terminals-API which processes withdrawals and authenticates terminals, checks that the requesting terminal is active and is allowed to initiate withdrawals. Since the check for the \textit{active} flag must be done for each request of a terminal, the check can be centralized and is implemented as part of the authentication flow. A Wallee terminal can be deactivated using the cli described in \autoref{sec-implementation-cli}.
