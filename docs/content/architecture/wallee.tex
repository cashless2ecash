\section{Wallee}
Wallee offers level 1 PCI-DSS \cite{pci-dss} compliant payment processes to its customers \cite{wallee-pcidss-compliance} and allows a simple integration of its process into various kinds of merchant systems (e.g. websites, terminals, etc).

\subsection{Wallee Perspective}
From the perspective of Wallee, the system looks as follows:

\begin{itemize}
    \item The Wallee terminal uses the new Terminals API of C2EC to get notified about parameter selection and inform C2EC about the payment.
    \item The Wallee terminal needs the credit card (or other supported payment means) of the customer to authorize the payment.
    \item The Wallee terminal uses the Wallee Backend to authorize the payment using the supplied Android Till SDK \autoref{ref-wallee-till-api}
\end{itemize}

\subsection{Wallee Terminal}
Wallee Terminals are based on Android and run a modified, certified android version as operating system. Thus they can be used for payments and establish strong authentication in a trusted way.

\subsubsection{Withdrawal Operation Identifier}
The \textbf{W}ithdrawal-\textbf{OP}eration-\textbf{ID}entifier (\textit{WOPID}) is leveraged by all components to establish the connection to an entry in the withdrawal table (\autoref{fig-erd-withdrawal}) of C2EC. The \textit{WOPID} is therefore crucial and every participant of the withdrawal must eventually gain knowledge about the value of the \textit{WOPID} to process the withdrawal. The \textit{WOPID} is created by the Terminal and advertised to the Exchange by requesting notification, when the reserve public key belonging to the \textit{WOPID} was received and the mapping could be created.
The Wallet gains the \textit{WOPID} value when scanning the QR code at the Terminal and then sends the \textit{WOPID} (and the other parameters) to the Exchange.

\textbf{Creation of the WOPID}

The creation of the \textit{WOPID} is a crucial step in the process. The \textit{WOPID} must be cryptographically sound. Therefore a cryptographically secure PRNG must be leveraged. Otherwise a \textit{WOPID} might be guessed by an attacker. This would open the door for attacks as described in \autoref{sec-security-wopid}.

\subsubsection{Wallee Till API}
\label{ref-wallee-till-api}
Wallee supplies the Wallee Android Till SDK \cite{wallee-till-sdk} which allows the implementation of custom application for their android based terminals. The API facilitates the integration with the Wallee backend and uses it to authorize payments.

\subsection{Wallee Backend and API}
Terminals of Wallee are used to communicate with the customer at the shop of the merchant. The payment and processing of the transaction is run on the \textit{Wallee Backend}. The \textit{Wallee Backend} is used by C2EC to attest a payment, when a \texttt{C2ECPaymentNotification} message reaches C2EC. The \textit{Wallee Backend} is also used to do refunds, in case something goes wrong during the payment. Therefore the API of \textit{Wallee Backend} is used to collect this information or process a refund. Wallee structures its API using \textit{Services}. For C2EC this means that the \textit{Transaction Service} \cite{wallee-transaction-service-docs} and \textit{Refund Service} \cite{wallee-refund-service-docs} must be implemented.

\subsubsection{Transaction Service}
The \textit{Transaction Service} is used by C2EC to attest a transaction was successfully processed and the reserve can be created by the \textit{Exchange}. Therefore the \texttt{GET /api/transaction/read} API of the \textit{Transaction Service} is used. If the returned transaction is in state \textit{fulfill}, the transaction can be stored as \textit{completion\_proof} for the withdrawal as specified in the withdrawal table \autoref{fig-erd-withdrawal} and the withdrawal status can be transitioned to \textit{confirmed}. This will tell the \textit{Exchange} to create the reserve which can eventually be withdrawn by the wallet. 

\subsubsection{Refund Service}
The \textit{Refund Service} is used by C2EC in case of a refund. Therefore the C2EC gets notified by the \textit{Exchange} that the transaction shall be refunded. To trigger the refund process at the Wallee backend, the \texttt{POST /api/refund/refund} is used.

\subsubsection{Wallee Transaction State}
To decide if a transaction was successful, the states of a transaction within Wallee must be mapped to the world of Taler. This means that a reserve shall only be created, if the transaction is in a state which allows Taler not having any liabilities regarding the transaction and that Wallee could process the payment successfully. The documentation states that \textit{only} in the transaction state \textit{fulfill}, the delivery of the goods (in case of withdrawal this means, that the reserve can be created) shall be started \cite{wallee-transaction-process}. For the withdrawal this means, that the only interesting state for fulfillment is the \textit{fulfill} state. Every other state means, that the transaction was not successful and the reserve shall not be created.

\section{Payto Wallee-Transaction Extension}
RFC 8905 \cite{rfc8905} specifies a URI scheme (complying with RFC 3986 \cite{rfc3986}), which allows to address a creditor with theoretically any protocol that can be used to pay someone (such as IBAN, BIC etc.) in a standardized way. It introduces a registry which holds the specific official values of the standard. The registry is supervised by the GANA (GNUnet Assigned Numbers Authority) \cite{gnunet-gana}.

In case a refund becomes necessary, which might occur if a transaction does not succeed or a reserve is not withdrawn within the specified time, a new \textit{target type} called \textit{wallee-transaction} is registered. It takes a transaction identifier as \textit{target identifier} which identifies the transaction for which a refund process shall be triggered. The idea is that the handler of the payto URI is able to deduct the transaction from the payto-uri and trigger the refund process.

\subsection{Payto refund using Wallee}
Wallee allows to trigger refunds using the Refund Service of the Wallee backend. The service allows to trigger a refund given a transaction identifier. Therefore the C2EC component can trigger the refund using the refund service if needed. The payto-uri retrieved as debit account by the wire gateway API, is leveraged to delegate the refund process to the Wallee Backend using the Refund Service and parsing the transaction identifier of the payto-uri.

\subsection{Extensibility}
The flow is extensible and other providers like Wallee might be added. New payment service providers might want to register their own refund payto-uri with the GNU Assigned Numbers Authority (GANA) if needed. This will allow a simple integration of the refund process into the system. C2EC establishes structures which abstracts the general flow and the integration of new provider is simply adding new requests within this structures.
