\section{C2EC}

The C2EC component is the central component in the cashless withdrawal of digital cash using Taler. It coordinates and initializes the parameters and mediates between the different stakeholders of a withdrawal. This finally allows the customer to withdraw digital cash from a reserve owned by the Exchange. To achieve this, C2EC provides an API which can be integrated and used by the terminal, wallet and the Exchange.

The API of the C2EC (cashless2ecash) component handles the flow from the creation of a C2EC mapping to the creation of the reserve. For the integration into the Taler ecosystem, C2EC must implement the Taler Wire-Gateway API \cite{taler-wire-gateway-api} and the Taler Bank Integration API \cite{taler-bank-integration-api}. The new Terminals API \cite{taler-terminal-api} is the interface of the payment service provider terminals to the GNU Taler ecosystem.

\subsection{C2EC Perspective}
From the perspective of C2EC, the system looks as follows:

\begin{itemize}
    \item The C2EC component is requested by the Taler wallet to register a new \textit{WOPID} to reserve public key mapping.
    \item Then, the C2EC component is notified by the terminal (e.g. a Wallee terminal) about a payment.
    \item The C2EC component confirms a payment by requesting the payment confirmation from the payment service provider backend (e.g. Wallee backend)
    \item The C2EC component runs the Taler Wire Gateway API that the respective Taler Exchange can retrieve fresh transactions and create reserves. This reserves are eventually withdrawn by the customer using their Taler wallet.
\end{itemize}

\subsection{Withdrawal-Operation State Transitions}
\label{sec-architecture-state-transitions}

The C2EC component mediates between the stakeholders of a withdrawal in order to maintain the correct state of the withdrawal. It decides when a withdrawal's status can be transitioned. The diagram in \autoref{fig-withdrawal-operation-state-transition-diagram} shows the transitions of states in which a withdrawal operation can be and which events will trigger a transition. The term confirmation in this context means, that the backend of the provider was asked and the transaction was successfully processed (or not). So if a transaction was successfully processed by the provider, the final state is the success case \textit{confirmed}, where the Exchange will create a reserve and allow the withdrawal. If the confirmation fails, indicating the provider could not process the transaction successfully, the failure case is \textit{aborted}. \textit{confirmed} and \textit{aborted} are the final states.

\begin{figure}[H]
  \centering
  \includegraphics[width=0.7\textwidth]{pictures/diagrams/withdrawal-op-state-transition.png}
  \caption{Withdrawal Operation state transition diagram}
  \label{fig-withdrawal-operation-state-transition-diagram}
\end{figure}

\subsection{Authentication}

Terminals and the Exchange wire watch process who authenticate against the C2EC API using Basic-Auth \cite{rfc7617} must provide their respective access token. Fot his they provide a \texttt{Authorization: Basic \$ACCESS\_TOKEN} header, where \texttt{\$ACCESS\_TOKEN} is a basic-auth value configured by the operator of the Exchange consisting of the terminal username and password. The header value must begin with the prefix specified in RFC 7617 \cite{rfc7617}: \textit{Basic}.

\subsection{The C2EC RESTful API}

All components involved in the withdrawal process must interact with the C2EC component. Therefore this section describes the various API implemented in the C2EC component. The description contains a short list of the consumers of the respective API. Consumer in this context does not necessarily mean that data is consumed but rather that the consumer uses the API to either gather data or send requests or data to C2EC.

\subsubsection{Terminals API}
\label{sec-architecture-c2ec-terminals-api}

That terminals can initiate and serve withdrawals in Taler, the Terminals API \cite{taler-terminal-api} is specified and implemented. The Terminals API mirrors all actions of a terminal at the C2EC component. This covers following endpoints:

\begin{enumerate}
  \item Config (/config)
  \item Withdrawal setup (/withdrawals)
  \item Status of the withdrawal (/withdrawals/\$WOPID)
  \item Confirmation Request (/withdrawals/\$WOPID/check)
  \item Terminal side abort (/withdrawals/\$WOPID/abort)
\end{enumerate}

\textbf{Fees}

Fees are an important aspect of the withdrawal flow using established payment service providers. When the withdrawal operation is not supplied by some Exchanges as standard service, the provider possibly wants to charge fees to the customer in order to make a profit and cover its costs. It is likely that these costs are rolled over to the customer in form of fees. This means that a terminal must have the capability to inform the Terminals API about fees. This can be achieved through the confirmation request in the Terminals API. Also the Exchange operator itself wants to charge fees to cover its costs. For example cashback is causing a lot of fees to the merchants supporting it:

\begin{quote}
  \textit{de:}Die Händler zahlen jedoch für den Cashback-Service bereits Gebühren an
  die Banken. Aktuell sind es laut EHI im Schnitt 0,14 Prozent, insgesamt
  waren das 2023 rund 17,2 Millionen Euro. \cite[Crefeld, ZEIT]{zeit-cashback}
\end{quote}

\subsubsection{Taler Bank Integration API}

Withdrawals by the wallet with a C2EC are based on withdrawal operations which register a reserve public key at the C2EC component. The provider must first create a unique identifier for the withdrawal operation (the \texttt{WOPID}) to interact with the withdrawal operation (as described in \autoref{sec-architecture-c2ec-terminals-api}) and eventually withdraw digital cash using the wallet. The withdrawal operation API is an implementation of the \textit{Bank Integration API} \cite{taler-bank-integration-api}.

\subsubsection{Taler Wire-Gateway API}

The Taler Wire-Gateway API must be implemented in order to capture incoming transactions and allow the withdrawal of digital cash. The specification of the Taler Wire-Gateway API can be found in the official Taler documentation \cite{taler-wire-gateway-api}.

The Wire-Gateway API enables the Exchange to communicate with the C2EC component using the API. The Exchange fetchs final withdrawals. The finality is guaranteed by C2EC by confirming the payment at the terminal backend. The Wire-Gateway API is implemented as part of C2EC. When the Exchange's wire watch process loads a confirmed withdrawal, the transaction was successfully processed. The Exchange will create a reserve with the corresponding reserve public key which can then be withdrawn by the wallet with the corresponding reserve private key.

The Wire-Gateway API of C2EC does not implement the testing endpoint \textit{/admin/add-incoming}. The endoint will respond with HTTP status code 501 (not implemented).

\subsection{C2EC Entities}
\label{sec-architecture-entities}

The entities of the C2EC component must track two different aspects. The first is the mapping of a nonce (the \textit{WOPID}) to a reserve public key to enable withdrawals and the second aspect is the authentication and authorization of terminals allowing withdrawals owned by terminal providers like \textit{Wallee}.

A detailed explanation and ERD can be found in \autoref{sec-implementation-database}.

\subsubsection{Terminal Provider}
\autoref{fig-erd-terminal-provider} describes the provider entity of C2EC compliant terminals. The name of the provider is important, because it decides which flow shall be taken in order to attest the payment. For example will the name \textit{Wallee} signal the terminal provider to trigger the confirmation flow of \textit{Wallee} once the payment notification for the withdrawal reaches C2EC.

\subsubsection{Terminal}
Entity displayed in \autoref{fig-erd-terminal} contains information about terminals of providers. This includes the provider they belong to and an access-token, which is generated by the operator of the C2EC component. It allows authenticating the terminal. A terminal belongs to one terminal provider.

\subsubsection{Withdrawal}
The Entity displayed in \autoref{fig-erd-withdrawal} represents the withdrawal processes initiated by terminals. This entity contains information about the withdrawal like the amount, which terminal the withdrawal was initiated from and which reserve public key is used to create a reserve in the Exchange.

\subsubsection{Relationships}
\label{sec-architecture-entities-relationships}
The structure of the three entities form a tree which is rooted at the terminal provider. Each provider can have many terminals and each terminal can have many withdrawals. The reverse does not apply. A withdrawal does always belong to exactly one terminal and a terminal is always linked to exactly one provider. These relations are installed by using foreign keys, which link the sub-entities (terminal and withdrawal) to their corresponding owners (provider and terminal). A provider owns its terminals and a terminal owns its withdrawals.

\begin{figure}[H]
  \centering
  \includegraphics[width=0.7\textwidth]{pictures/database/logical_model_relations.png}
  \caption{Relationships of the entities.}
  \label{fig-logical-model-relations}
\end{figure}
