#!/bin/bash

if [ "$#" -ne 2 ]; then
    echo "Usage: $0 <source-root> <install-dir>"
    exit 1
fi

REPO_ROOT=$1
INSTALL_DIR=$2

build_c2ec() {
    go build -o $INSTALL_DIR $REPO_ROOT
    if [ $? -ne 0 ]; then
        echo "Failed to build C2EC using Go"
        exit 1
    fi
}

build_c2ec
