#!/bin/bash


REPO_ROOT=./../c2ec #/home/joel/studies/thesis/cashless2ecash/c2ec
DB_NAME=c2ec
ADMIN_PASSWORD=secret
OPERATOR_PASSWORD=secret2
API_PASSWORD=secret3

if [[ ($ADMIN_PASSWORD = $OPERATOR_PASSWORD) || ($OPERATOR_PASSWORD = $API_PASSWORD) || ($ADMIN_PASSWORD = $API_PASSWORD) ]]; then
    echo "PROBLEM: passwords for db admin, operator and must be different..."
    echo "Usage: $0 <db-username> <db-password> <db-name> <source-root> <new-db-admin-pw> <new-db-admin-pw> <new-db-admin-pw>"
    exit 1
fi

ACCESS_WITHOUT_PASSWORD="$REPO_ROOT/db/access.sql"
ACCESS_WITH_PASSWORDS="$REPO_ROOT/db/access-with-passwords.sql"

cp $ACCESS_WITHOUT_PASSWORD $ACCESS_WITH_PASSWORDS

sed -i "s/ADMIN_PASSWORD/$ADMIN_PASSWORD/g" $ACCESS_WITH_PASSWORDS
sed -i "s/OPERATOR_PASSWORD/$OPERATOR_PASSWORD/g" $ACCESS_WITH_PASSWORDS
sed -i "s/API_PASSWORD/$API_PASSWORD/g" $ACCESS_WITH_PASSWORDS
sed -i "s/DB_NAME/$DB_NAME/g" $ACCESS_WITH_PASSWORDS

cat $ACCESS_WITH_PASSWORDS

rm $ACCESS_WITH_PASSWORDS