# Installation of C2EC and surrounding components

how to install exchange and C2EC

## Prerequisites

- Debian
- git
- postgres >= 15.6
- go ([installing Go](https://go.dev/doc/install))
- it's a good idea to read [Exchange Operator Manual](https://docs.taler.net/taler-exchange-manual.html) first

## Required Exchange binaries

To allow the withdrawal of Taler, I will need following binaries:

- taler-exchange-httpd
- taler-exchange-secmod-rsa
- taler-exchange-secmod-cs
- taler-exchange-secmod-eddsa
- taler-exchange-closer
- taler-exchange-wirewatch

## Setup Commands

```bash
# Add taler repo and install
sudo echo "deb [signed-by=/etc/apt/keyrings/taler-systems.gpg] https://deb.taler.net/apt/debian bookworm main" > /etc/apt/sources.list.d/taler.list`
sudo wget -O /etc/apt/keyrings/taler-systems.gpg https://taler.net/taler-systems.gpg
sudo apt update
sudo apt install taler-exchange

# install git
sudo apt install git

# download Go and install
export GO_TAR=go1.22.2.linux-amd64.tar.gz
sudo wget -O $GO_TAR https://go.dev/dl/$GO_TAR
sudo rm -rf /usr/local/go && sudo tar -C /usr/local -xzf $GO_TAR
echo 'export PATH=$PATH:/usr/local/go/bin' >> $HOME/.profile
source $HOME/.profile
go version
```

## Configure

### Getting the source

`git clone https://git.taler.net/cashless2ecash.git`

### Preparing the database

```bash
sudo passwd postgres  # change default password of postgres user
su postgres
psql
```

```sql
CREATE DATABASE c2ec;
CREATE USER c2ec_admin WITH ENCRYPTED PASSWORD [..]; -- keepass
GRANT ALL PRIVILEGES ON DATABASE c2ec TO c2ec_admin;
--For CLI (managing terminals and providers):
CREATE USER c2ec_operator WITH ENCRYPTED PASSWORD [..]; -- keepass
--For the API (handling withdrawals):
CREATE USER c2ec_api WITH ENCRYPTED PASSWORD [..]; -- keepass
```

exit psql, change back to normal user (type 2x 'exit')

### Setting up c2ec schmema
find src directory of cashless2ecash, find c2ec/install/setup_db.sh

```bash
export PGHOST=localhost`
export PGPORT=5432
./setup_db.sh c2ec_admin [PASSWORD OBFUSCATED] c2ec ./..
su postgres
psql -d c2ec
```

Grant rights on tables, triggers and functions to the users `c2ec_api` and `c2ec_operator`

```sql
GRANT USAGE ON SCHEMA c2ec TO c2ec_api;
GRANT USAGE ON SCHEMA c2ec TO c2ec_operator;
GRANT ALL PRIVILEGES ON c2ec.withdrawal TO c2ec_api;
GRANT SELECT ON c2ec.terminal TO c2ec_api;
GRANT SELECT ON c2ec.provider TO c2ec_api;
GRANT EXECUTE ON FUNCTION c2ec.emit_withdrawal_status TO c2ec_api;
GRANT EXECUTE ON FUNCTION c2ec.emit_payment_notification TO c2ec_api;
GRANT EXECUTE ON FUNCTION c2ec.emit_retry_notification TO c2ec_api;
GRANT EXECUTE ON FUNCTION c2ec.emit_transfer_notification TO c2ec_api;
GRANT ALL PRIVILEGES ON c2ec.terminal TO c2ec_operator;
GRANT ALL PRIVILEGES ON c2ec.provider TO c2ec_operator;
```

### Building and Running the app

Building the cli (used to manage terminals and providers)

```bash
./build_cli.sh ./../cli $HOME
.$HOME/cli
```

Building c2ec

```bash
./build_app.sh ./../c2ec $HOME
cp $HOME/cashless2ecash/c2ec/c2ec-config.yaml $HOME
# configure correctly
.$HOME/c2ec
```
