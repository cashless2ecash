#!/bin/bash

if [ "$#" -ne 3 ]; then
    echo "Usage: $0 <db-username> <db-password> <db-name>"
    exit 1
fi

DB_USERNAME=$1
DB_PASSWORD=$2
DB_NAME=$3

SQL_SCRIPTS=(
    "./../c2ec/db/drop.sql"
)

execute_sql_scripts() {
    for script in "${SQL_SCRIPTS[@]}"; do
        PGPASSWORD=$DB_PASSWORD psql -U $DB_USERNAME -d $DB_NAME -f "$script"
        if [ $? -ne 0 ]; then
            echo "Failed to execute SQL script: $script"
            exit 1
        fi
    done
    PGPASSWORD=""
}

execute_sql_scripts
