#!/bin/bash

if [ "$#" -ne 7 ]; then
    echo "Usage: $0 <db-username> <db-password> <db-name> <source-root> <new-db-admin-pw> <new-db-operator-pw> <new-db-api-pw>"
    exit 1
fi

if [[ ($5 = $6) || ($6 = $7) || ($5 = $7) ]]; then
    echo "PROBLEM: passwords for db admin, operator and must be different..."
    echo "Usage: $0 <db-username> <db-password> <db-name> <source-root> <new-db-admin-pw> <new-db-admin-pw> <new-db-admin-pw>"
    exit 1
fi

DB_USERNAME=$1
DB_PASSWORD=$2
DB_NAME=$3
REPO_ROOT=$4

ADMIN_PASSWORD=$5
OPERATOR_PASSWORD=$6
API_PASSWORD=$7

ACCESS_WITHOUT_PASSWORD="$REPO_ROOT/db/access.sql"
ACCESS_WITH_PASSWORDS="$REPO_ROOT/db/access-with-passwords.sql"

cp $ACCESS_WITHOUT_PASSWORD $ACCESS_WITH_PASSWORDS

sed -i "s/ADMIN_PASSWORD/$ADMIN_PASSWORD/g" $ACCESS_WITH_PASSWORDS
sed -i "s/OPERATOR_PASSWORD/$OPERATOR_PASSWORD/g" $ACCESS_WITH_PASSWORDS
sed -i "s/API_PASSWORD/$API_PASSWORD/g" $ACCESS_WITH_PASSWORDS
sed -i "s/DB_NAME/$DB_NAME/g" $ACCESS_WITH_PASSWORDS

SQL_SCRIPTS=(
    "$REPO_ROOT/db/versioning.sql"
    "$REPO_ROOT/db/0001-c2ec_schema.sql"    
    "$REPO_ROOT/db/proc-c2ec_status_listener.sql"
    "$REPO_ROOT/db/proc-c2ec_payment_notification_listener.sql"
    "$REPO_ROOT/db/proc-c2ec_retry_listener.sql"
    "$REPO_ROOT/db/proc-c2ec_transfer_listener.sql"
    "$REPO_ROOT/db/access-with-passwords.sql"
)

execute_sql_scripts() {
    for script in "${SQL_SCRIPTS[@]}"; do
        echo "Executing SQL script: $script"
        PGPASSWORD=$DB_PASSWORD psql -U $DB_USERNAME -d $DB_NAME -f $script
        if [ $? -ne 0 ]; then
            echo "Failed to execute SQL script: $script"
            exit 1
        fi
    done
    PGPASSWORD=""
    rm $ACCESS_WITH_PASSWORDS
}

execute_sql_scripts
if [ $? -ne 0 ]; then
    exit 1
fi
