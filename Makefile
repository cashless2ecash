C2EC_HOME ?= ${HOME}
C2EC_USER ?= $(shell whoami)
C2EC_POSTGRES_USER ?= postgres
C2EC_POSTGRES_PASSWORD ?= postgres
C2EC_DB_NAME ?= postgres
C2EC_DB_ADMIN_PW ?= secret
C2EC_DB_OPERATOR_PW ?= secret
C2EC_DB_API_PW ?= secret
C2EC_FILE_PERMISSIONS ?= 660

dependencies-check:
	go version
	psql --version
	ls ${C2EC_HOME}

build: dependencies-check
	go build -C ./c2ec/ -o ${C2EC_HOME}

stop:
	kill $(pgrep c2ec)

start: stop
	(cd ${C2EC_HOME}; ./c2ec &)

migrate: build
	(cd ./c2ec/db; ./migrate.sh c2ec_admin ${C2EC_DB_ADMIN_PW} ${C2EC_DB_NAME})

install: dependencies-check
	cp ./c2ec/c2ec-config.yaml ${C2EC_HOME}/c2ec-config.yaml
	chmod ${C2EC_FILE_PERMISSIONS} ${C2EC_HOME}/c2ec-config.yaml
	chown ${C2EC_USER} ${C2EC_HOME}/c2ec-config.yaml
	cp ./c2ec/c2ec-config.conf ${C2EC_HOME}/c2ec-config.conf
	chmod ${C2EC_FILE_PERMISSIONS} ${C2EC_HOME}/c2ec-config.conf
	chown ${C2EC_USER} ${C2EC_HOME}/c2ec-config.conf
	touch ${C2EC_HOME}/c2ec-log.txt
	chmod ${C2EC_FILE_PERMISSIONS} ${C2EC_HOME}/c2ec-log.txt
	chown ${C2EC_USER} ${C2EC_HOME}/c2ec-log.txt
	cp ./simulation/config.yaml ${C2EC_HOME}/sim-config.yaml
	(cd ./c2ec/db; ./migrate.sh ${C2EC_POSTGRES_USER} ${C2EC_POSTGRES_PASSWORD} ${C2EC_DB_NAME} ${C2EC_DB_ADMIN_PW} ${C2EC_DB_OPERATOR_PW} ${C2EC_DB_API_PW})
	echo "you may want to alter the current configuration of your installation at ${C2EC_HOME}/c2ec-config.conf"

cli: dependencies-check
	go build -C ./cli/ -o ${C2EC_HOME}

simulation: dependencies-check
	go build -C ./simulation/ -o ${C2EC_HOME}

# ONLY DO THIS WHEN YOU NOW WHAT YOUR DOING 
#wipe: 
#	(cd ./install; ./wipe_db.sh ${C2EC_POSTGRES_USER} ${C2EC_POSTGRES_PASSWORD} ${C2EC_DB_NAME})
